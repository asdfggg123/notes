#Trusts

##Introduction

A trust is a legal arrangement whereby a settlor vests a right in a trustee who is then required to use that right for the benefit of the beneficiary.

It requires a certainty of intention, subject matter and object

##Express Trust
 
-	An express trust of land must fulfil the formality requirements in s. 53(1)(b) LPA 1925. 
-	The presence of an express trust is conclusive as to the existence of the trust _Goodman v Gallant [1986]_
-	Where there is a manifest intention to create a trust but the formality 
-	A testamentary trust must fulfil the formality requirements in s. 9 Wills Act 1837

##Resulting Trust

-	Two views
	-	Traditional approach
		-	Trust arises where A pays or contributes to the purchase of a property in B's name (presumed intention resulting trust)
			-	The law presumes that A does not intend a gift
		-	Trust arises where an express trust does not exhaust the beneficial interest (automatic resulting trust)
			-	The beneficial interest reverts back to the settlor
	-	Intention of benefit
		-	Trust arises where B receives property in relation to which he was not intended to benefit
			-	Trust arises from A's lack of intent to benefit B
			-	Trust is to provide restitution for unjust enrichment
	-	HL rejected the second approach in _Westdeutsche v Landesbank Girozentrale v Islington LBC [1996]_
-	For property law, the relevant aspect is the purchase money resulting trust
	-	Presumed absence of an intention to make a gift
	-	Parties' shares are proportional to their contributions towards the purchase
-	Presumption of trust
	-	A trust is presumed where A purchases or contributes to the purchase of land in B's name
	-	A trust is presumed where land is purchased in the joint names of A and B without an express declaration of trust
-	Presumption of advancement
	-	Certain transactions are presumed to be a gift and therefore a resulting trust does not operate
	-	Examples include:
		-	Husband to wife (but not vice versa)
		-	Parent to child
	-	Presumption is now much weaker but not completely gone - Lord Neuberger's dictum in _Stack v Dowden_
	-	Presumption to be abolished when Equality Act 2010 comes into force
-	Presumptions are rebuttable where parties are able to adduce evidence against them
	-	Presumption of trust is rebutted by evidence of any intention inconsistent with the trust - Lord Browne-Wilkinson in _Westdeutsche v Landesbank Girozentrale v Islington LBC [1996]_
	-	Illegal purpose of transfer cannot be invoked to rebut a presumption
		-	Exception applies where illegal purpose had not been carried out _Tribe v Tribe_

##Constructive Trust

-	Trust arises by operation of law to give effect to parties' common intention (common intention constructive trust)
	-	No specific judicial definition
-	Constructive trusts are institutional in that they arise when the conditions required are fulfilled, and not at the discretion of the court

##Rochefoucauld v Boustead [1897]

-	An express trust was created that fails to fulfil the formality requirements
-	Trustee fraudulently claims ownership of the property
-	Oral evidence was admitted as to the existence of the trust
-	Express or constructive?
	-	Court used "express" to describe the trust
	-	EWCA considered it a constructive trust in _Bannister v Bannister [1948]_

#Trusts in Family Homes

##Introduction

-	Where an express trust is present, it is considered conclusive
-	Two situations to consider
	-	Sole legal ownership
		-	Need to establish the presence of a trust
		-	Quantify the shares of the parties
	-	Joint legal ownership
		-	A trust already exists by operation of law
		-	Quantify the shares of the parties

##Sole legal ownership

-	Starting point is that there is a sole beneficial ownership _Stack v Dowden_
-	A resulting trust may be imposed by virtue of any direct financial contribution to the purchase of property
	-	Lord Walker's dictum in _Stack v Dowden_ suggests that a resulting trust should not be used
	-	Two problems with resulting trusts in family homes as pointed out by Lord Neuberger in _Stack v Dowden_:
		-	Overwhelming prevailence of mortgages in the purchase of family homes
			-	Difficulty in evaluating contributions to the mortgage payments vs cash contribution up front
		-	Lack of evidence considering domestic context may make it impossible to reach a clear contributions as to parties' contributions
-	A constructive trust may be imposed by virtue of an express or inferred agreement between the parties
	-	Leading case is _Lloyds Bank plc v Rosset [1991]_
		-	Express agreement that has been detrimentally or significantly relied upon is one way to establish a constructive trust
			-	Express agreement very rare and usually highly artificial - Waite J in _Hammond v Mitchell [1991]_
			-	Nourse LJ on detrimental reliance in _Grant v Edwards [1986]_
			> conduct on which [someone] could not reasonably have been expected to embark unless [he/she] was to have an interest in the house
					-	Agreement may be inferred from contributions by the partner towards the purchase price or mortgage instalments
			-	The law has moved on since then - _Abbott v Abbott [2007] UKPC_
			-	Much broader approach taken in as to what factors may be taken into account when inferring a common intention _Aspden v Elvy [2012] EWHC_
	-	Quantification follows rules in _Stack v Dowden_ as clarified in _Jones v Kernott_

##Joint legal ownership

-	Starting point is that there is joint beneficial ownership _Stack v Dowden_
	-	Burden of proof lies upon the party seeking to establish otherwise
-	Quantification of shares
	-	Quantification follows rules in _Stack v Dowden_ as clarified in _Jones v Kernott_

##Quantification of shares

This is an area of great debate and the two UKHL/UKSC cases of _Stack v Dowden_ and _Jones v Kernott_ are the leading authorities.

###Stack v Dowden

-	A need for consistency between the approaches taken for cases of sole and joint legal ownership, notwithstanding the different starting point
-	Approach taken with respect to family homes should be that of a common intention constructive and not resulting trust
-	Initial respective presumption can be rebutted by proving that parties had a different common intention at the time of purchase, or that they subsequently formed such a common intention (ambulatory)
	-	No room for ambulatory constructive trust in the presence of an express trust
-	Common intention may be "expressed, inferred, or imputed" - Lady Hale
	-	Expressed (rare)
	-	Inferred
		-	Courts may infer parties' common intention giving regard to the parties' whole course of conduct (Long and non-exhaustive list by Lady Hale)
		-	Courts may not abandon the search for a common intention in favour of what the court views is fair upon the situation
-	Lord Neuberger's dissent
	-	Law Commission's consultation paper refers to the impossibility of devising a scheme which can "operate fairly and evenly across the diversity of domestic circumstances which are now to be encountered"
	-	Present (unchanged) law is flexible enough
	-	Many potential reasons for joint legal ownership - does not automatically point towards equal beneficial interest
	-	Legal co-ownership may just reflect common intention for "some beneficial interest" for the party who contributes less financially rather than equal beneficial interest
	-	He considers the list of possible relevant factors given by Lady Hale inappropriately broad
		-	The economic, social and emotional significance of a home purchase makes it easy to suggest parties may have taken a different approach than their other ordinary conduct would suggest
	-	He considers it inappropriate to impute an intention for the purposes of a constructive trust when an imputation would necessarily mean that there was no actual common intention
		-	In the absence of an express or inferred common intention, an imputation would necessarily means an imposition of the courts' view of fairness
		-	The court's view of fairness is not the right criterion for determining the parties' shares
	-	In line with his preferred resulting trust approach, he disagrees with the "ambulatory" nature of constuctive trusts in that ordinary day-to-day actions should not be sufficient to alter the apportionment of beneficial interest. Significant capital expenditure is required.
	-	The overall approach should be legally principled and not be based on the ordinary person's idea of fairness

###Jones v Kernott

- On imputing an intention:
	-	Where it is clear that beneficial interesta re to be shared but it is impossible to divine a common intention, the court may impute an intention to the parties "which they may never have had"
	-	Little difference between inferring and imputing an intention since courts may often infer an intention that a party subjectively did not have 
-	Overview of relevant procedure for joint legal ownership
	1.	Starting point is joint tenancy in law and equity
	1.	Presumption may be displaced by showing:
		-	a different common intention at the time of acquisition
		-	a different common intention formed following acquisition that their respective shares would change
	1.	Common intention is to be deduced objectively from their conduct notwithstanding the possibility that one "did not consciously formulate that intention in his own mind or even acted with some different intention which he did not communicate"
	1.	Where parties did not intend joint tenancy or had changed their original intention but there is insufficient evidence to ascertain their actual intention, courts will apportion the beneficial interest based on what it considers is fair, considering the whole course of dealing (broad approach)
-	Lord Kerr on imputing an intention:
	-	A clear distinction between inferring and imputing an intention is required
	-	Imputing an intention is a facade that the court hides behind when determining fairness
	-	An outright declaration that the courts will seek fairness is preferred
-	Lord Wilson's dissent:
	-	Lady Hale's speech in _Stack v Dowden_ is inherently self contradictory:
		-	The parties' common intention must be given effect to notwithstanding the court's view of fairness
		-	Imputing a common intention can only be done by giving effect to what the court views as fair

##Comments

-	Piska: fairness is clothed in the language of intention without providing explicit guitance for determining the content of either
-	IMHO:
	-	The basis of a common intention constructive trust is the presence of a common intention. Relying on an imputed intention for this purpose is logically fallacious
	-	In the absence of an express or inferred common intention, the default position should fall back to that of a resulting trust
		-	Considering the evolution of trust of land from trust for sale to trust for actual use of land, a beneficial owner with a minority share is nevertheless entitled to occupancy and other use of the property
		-	_Stack v Dowden_ and _Jones v Kernott_ are both cases concerning a long term non-married couple with children that provided the context for the judges' consideration of the issue - Perhaps they take a much more idealistic view of such cohabitation than is justified
	-	It appears that the Hale and Walker in _Jones v Kernott_ dropped the facade of imputation completely in their summary, declaring outright that courts' should seek fairness in the absence of an express or implied common intention
		-	Considering the entire basis of their approach is that of a constructive trust, is this justifiable?
		-	This approach gives wide unfettered discretion to the courts and lacks any principled basis
		-	Remniscent of the approach in the Boustead case, where the courts found the existence of an "express" trust even where formality requirements were not complied with and therefore by definition an express trust does not exist
		-	This is clearly a case of unprincipled judicial activism - contrary to rule of law?
		-	Perhaps this reliance on an imputed intention is due to the lack of any alternative method for courts' to apportion beneficial interests. While the desire to achieve fairnes is commendable, the law operates on the basis of principles, and fairness of the law depends on the development of fair principles that are equally applied. Where the principle is merely to achieve what is fair, this fundamentally undermines the basis of our legal system.

