#Property Notes

##Co-ownership

###Introduction

Co-ownership refers to when two or more people enjoy the rights of ownership of land at the same time. (Focus here is on concurrent co-ownership i.e. simultaneous enjoyment of land by more than one party)

Examples of co-owners:

-	Husband and wife
-	Civil partners
-	Unmarried partners
-	Family members
-	Business partners

Law of co-ownership includes both statutes and common law

-	Law of Property Act (LPA) 1925
-	Trusts of Land and Appointment of Trustees Act 1996 (TOLATA)

###Nature and types of concurrent Co-ownership

####Joint Tenancy

Each co-owner is treated as being entitled to the whole of the land:

-	No distinct shares
-	No single co-owner can claim any greater right over any part of the land than another
-	Land is treated as if it is owned by one person only
-	Only one formal title to it that is jointly owned by all the joint tenants
-	Only one title registered with each co-owner registered as proprietor
-	Only one set of title deeds specifying all the co-owners

__Right of survivorship__

Upon the death of any joint tenant, his interest in the joint tenancy automatically accrues to the remaining joint tenants.

-	No formal conveyance or written document is needed to reflect the new situation
-	Right of survivorship takes precedence over any attempted transfer on death
-	There is nothing left to a beneficiary of the will

__The four unites__

1.	The unity of Possession
	-	Each joint tenant is entitled to physical possession of the whole of the land
	-	No physical division of the land
	-	No restriction on any joint tenant's use of each and every part of the land
	-	Each tenant has the right to participate fully in the fruits of possession (rent/profit)
	-	A joint tenant may be excluded from the land under s. 12, 13 of TOLATA, Part IV of Family Law Act 1996
1.	Unity of interest
	-	Each joint tenant's interest in the property must be of the same extent, nature, duration
1.	Unity of title
	-	Each joint tenant must derive their title from the same conveyancing documents
	-	Joint tenancy may still exist even if different documents were signed by parties See: _Antoniades v Villiers [1990] AC 417_ __UKHL__
		-	Whether joint tenancy exists in this situation is a question of substance, not form
1.	Unity of time
	-	Interest of each joint tenant must arise at the same time

####Tenancy in Common

Tenants in common have "undivided shares in land"

-	A tenant in common can point to a precise share of ownership of the land, even though the land is undivided and treated as a single unit
-	Shares in land are distinct and quantifiable
-	There remains essentially a unity in possession
-	Other unities required for a joint tenancy may be present
-	No right of survivorship

###Effect of LPA 1925 and TOLATA

####Before 1 Jan 1926

Both forms of co-ownership applied to legal and equitable estates in land

####After 1 Jan 1926

LPA 1925 placed restrictions on the manner in which these forms of co-ownership could come into existence (s. 34, 35 LPA 1925, as amended by TOLATA)

-	It is now impossible to create a tenancy in common at law (s. 1(6) LPA 1925)
	-	Only joint tenancies of the legal title are possible
	-	Joint tenancy of a legal title is "unseverable" (s. 36(2) LPA 1925)
-	Joint tenants of the legal title are trustees of the title
	-	Statutorily imposed trust of land (s. 34, 36 LPA 1925)
	-	Trustees have a statutorily imposed duty to hold the land: 
		-	For the persons beneficially interested in the land (equitable owners)
		-	For the purposes for which it was purchased
		-	To which end they are given various powers of management
-	Co-owners could possibly hold land as trustees for themselves
-	Maximum of four legal joint tenants (s. 34(2) LPA 1925)

###The distinction in the equitable interest

-	If the four unities are absent, a joint tenancy in equity cannot exist
-	The original conveyance documents are normally conclusive as to the nature of the co-ownership in equity _Goodman v Gallant [1986] 1 All ER 311_ __EWCA__
	-	Mere "unfairness" is insufficient to alter the nature of the co-ownership
	-	A written declaration is conclusive only for the parties to that declaration
	-	A written declaration is conclusive only if valid under general law. Can be invalidated by:
		-	Fraud
		-	Misrepresentation
		-	Undue influence
		-	Any other vitiating factor
	-	A valid written declaration may be departed from if later conduct of one of the parties amounts to an estoppel _Clarke v Meadus [2010] EWHC 3117_ __EWHC__
		-	Difficult to establish in court
	-	Parties are bound only when declaration refers clearly to the equitable interest
		-	A declaration of the nature of the legal title is insufficient to determine the nature of the equitable interests
		-	General principle does not hold if it can be proven that all parties intended otherwise
-	"Words of severance" will create a tenancy in common e.g.
	-	A description of the shares of each owner
	-	The creation of unequal interests in different co-owners
-	Presence of four unities and absence of an express declaration or severance will create a joint tenancy in equity
	-	Presumption of "equity follows the law"
	-	Presumption can be rebutted:
		-	Right to survivorship is inappropriate:
		-	Common intention to hold other than as joint tenants _Stack v Dowden [2007] UKHL 17_ __UKHL__ _Jones v Kernott [2011] UKSC 53_ __UKSC__
		-	Examples include:
			-	Business arrangements _Malayan Credit Ltd v Jack Chia-MPH [1986] AC 549_ __UKPC__
			-	Co-owned interest is of a mortgage held by co-mortgagees _Re Jackson [1887] 34 Ch D 732_ __EWHC__
			-	Purchasers have provided purchase money in unequal shares, which, in the absence of other evidence, establishes a lack of unity of interest _Lake v Craddock [1732] 24 ER 1011_ __EWHC__

###The trust of land

The trustees hold the land for the persons interested in it, with the powers of an absolute owner.

-	Trustees may delegate any of their functions to a beneficiary of full age (s. 9 TOLATA)
	-	Only trustees may give a valid receipt to a purchaser if the land is sold (s. 9 TOLATA)
-	Trustees are under no duty to sell the land following TOLATA
	-	Under LPA 1925, trustees held the land on a trust for sale, with the power to postpone sale "doctrine of conversion"
	-	Land could be retained if the trustees agreed, but would be sold if they disagreed (duty to sell)
-	An equitable owner is treated as having an interest in the land itself (s. 3 TOLATA) 
	-	Equally true prior to TOLATA _Williams & Glyn's Bank v Boland [1981] AC 487_ __UKHL__
-	Courts may intervene by way of an order under s. 14 TOLATA at the request of a trustee or beneficiary
-	Trustees' powers may be restricted by the instrument creating the trust
	-	Exceptions are public, ecclesiastical or charitable trusts (s.8 TOLATA)
-	Not everything done by a trustee will be a "function relating to" the trust _Brackley v Notting Hill Housing Trust [2001] All ER 164_ __EWCA__
-	If the trustees sell the land, they hold the proceeds of sale on trust for the equitable owners
-	All trustees must formally join in a conveyance if the land is sold
	-	A sale by all of the trustees will overreach the interests of the equitable owners (s. 2(1)(ii), 27 LPA 1925)
	-	If there is only one trustee of the land, the interests of the equitable owners cannot be overreached _Williams & Glyn's Bank v Boland [1981]_
-	The courts will take into account factors in s. 15 TOLATA when deciding whether to order a sale under s. 14 TOLATA


###Severance

Severance is effected by the division of one of the "unities"

Real life examples:

-	Mortgage by one joint tenant without the consent or knowledge of another
-	Written document(notice) under s. 36(2) LPA 1925 sent to every beneficiary
-	Mutual agreement to sever among all parties
	-	Doesn't require formalities _Burgess v Rawnsley [1975] Ch 429_ __EWCA__
	-	Destroys the joint tenancy entirely
	-	All contracts in relation to land must be in writing (s.2 LP(MP)A 1989)
-	Mutual conduct among all parties
	-	Destroys the joint tenancy entirely
-	A unilateral act that effects a division of shares
	-	Only affects the party's share
	-	Re Me 1971
-	Court order (s.14 TOLATA)
-	Murder _Re K [1985] Ch 180_ __EWCA__

By default, the subsequent division of assets when a joint tenancy is severed is equal.
-	A declaration of sufficient formalilty as to the division
