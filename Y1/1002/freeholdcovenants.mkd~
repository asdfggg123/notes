#Freehold Covenants

##Introduction

Freehold covenants are similar in that they derive their existence from the contract for sale of land.

Terminology:

-	Covenantor - person who grants a covenant over his land and whose land is therefore burdened
-	Covenantee - person whose land benefits from the covenant
-	Dominant land - land benefitting from the covenant

##Burden

-	Transmission of the burden is impossible at law _Rhone v Stephens_
-	Transmission of the burden of a negative covenant is possible in equity _Tulk v Moxhay_
	-	Burden must relate to land
	-	Burden must be intended to run with the land
		-	This is presumed under s. 79(1) LPA 1925
		-	Operates to create a rebuttable presumption that covenants relating to land are intended to be made on behalf of the covenantor's successors in title
	-	Dominant land must be capable of benefitting from the covenant
		-	Reasonable proximity
		-	Presumption of benefit
		-	Dominant land must be ascertainable or identifiable in the conveyance or otherwise shown with reasonable clarity _Newton Abbott Co-op Society v WWilliamson & Treadgold Ltd_
	-	Covenant must be negative
		-	Positive covenants will not be enforcible
		-	For negative covenants, Lord Templeman in _Rhone v Stephens_ argues that the successor in title never acquiries the right to do what is restricted under the covenant because the covenantor himself does not possess that right and therefore is incapable of passing it on
		-	Gardner finds this argument unconvincing and suggests that the continued refusal to enforce positive covenants stems from the courts' reluctance towards judicial activism
-	Indirect enforcement of positive covenants
	-	Chain of indemnity covenants
		-	Purely contractual, similar in operation to leasehold covenants prior to legislative intervention
	-	Mutual benefit and burden
		-	Following _Rhone v Stephens_, the operation of mutual benefit and burden is conditional in that burden is enforced only when benefit is enjoyed
		-	Two elements per _Thamesmead Town Ltd v Allotey_
			-	Benefit is conditional on discharging burden
				-	Real relation, some sort of benefit, between benefit and burden is necessary instead. No need for strictly conditional arrangement. _Wilkinson v Kerdene_
			-	Successors in title must have the opportunity to opt out of the arrangement
	-	Estate rentcharges
		-	Previous rentcharges are still enforcable but new rentcharges may generally no longer be created - Rentcharges Act 1977

##Benefit

-	

