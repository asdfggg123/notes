#The Rule of Law

##Introduction

The rule of law is the idea that governance of the country is to be carried out through the operation of law instead of arbitrary discretion. Dicey's classic exposition of the rule requires equal subjection of all classes to the ordinary law of the land administerd by the ordinary law courts, and therefore precludes the exemption of groups from the duties imposed by the law on other citizens.

Lord Bingham divided the rule of law into eight sub rules in his book _The Rule of Law_:

-	The law must be accessible and, so far as possible, intelligible, clear and predictable
-	Questions of legal right and liability should be ordinarily resolved by application of the law and not the exercise of discretion
-	The laws of the land should paply equally to all, save to the extent that objective differences justify differentiation
-	The law must afford adequate protection of fundamental human rights
-	Means must be provided for resolving, without prohibitive cost or inordinate delay, bona fide civil disputes which the parties themselves are unable to resolve
-	Ministers and public officials at all levels must exercise the powers ocnferred on them reasonably, in good faith, for the purpose for which powers were conferred and without exceeding the limits of such powers
-	Adjudicative procedures provided by the state should be fair
-	The existence of the rule of law requires compliance by the state with its obligations in international law

##Government under law

-	The government must be subject to the law and may exercise its power only in accordance with law
-	The exercise of public power must be justified by law and not claims of state necessity (prerogatives and law????) _Entick v Carrington (1765) 19 St Tr 1029_ __Court of Common Pleas__
	-	Public officer must show express legal authority for any interference with the person or property of the citizen
	-	The granting of executive powers with wide discretion has led to the dilution of the rule of law _IRC v Rossminster Ltd [1980] AC 952_ __UKHL__
	-	The negative approach taken in the UK towards individual rights means that few rights are protected this way, especially considering the relatively undefined scope of royal prerogatives
-	_Malone v MPC_ seems to suggest that the government has the same legal rights and privileges as the private citizen; this was countered by Laws J in _R v Somerset County Council, ex p Fewings_ where he suggested that public bodies may only act where they have been accorded express authority
-	The rule of law is undermined if the state exercises its powers in a way as to make it impossible or highly difficult for persons affected to challenge its legality
-	The rule of law is undermined where the state connives at breaches of the law, and takes active steps to prevent the discovery of such acts and judicial review (consider the case of Binyam Mohamed as well as allegations of complicity in torture)
-	Entrapment and interference with legal process are also acts that contravene the rule of law and will not find favour in court _R v Horseferry Road Magistrates' Court, ex p Bennett [1994] 1 AC 42_ __UKHL__
	-	Consider national security concerns, which have been found to be non-justicable
-	Where the government legislates to overturn unfavourable judicial opinion (War Damages Act 1965), it is considered an infringement of the rule of law
-	Consider the case of _M v Home Office_:
	-	An injunction, a remedy traditionally thought to be unavailable against the Crown, was held to be valid where a minister of the Crown acts contrary to statutorily imposed duties
	-	The Crown itself cannot be subject to injunctive relief, although ministers and other representatives of the crown may be
	-	The declaration of contempt in itself was sufficient due satisfaction to right the wrong (Consider ECHR cases where declaration of breach was sometimes held to be sufficient by Strasbourg and no other forms of relief were awarded)

##Discretion

Discretionary powers vested in the executive are a necessary "evil" for the smooth and efficient functioning of the government. The potential for abuses of power is great where wide unfettered discretion is vested in the executive and therefore its exercise should be ideally limited by legislation. Where such controls are absent, the courts have imposed common law principles of fairness and legality as in _R v Secretary of State for the Home Department, ex p Pierson [1998] AC 539_ __UKHL__. The ECHR and HRA provide partial human rights safeguards in this respect.

##Lawmaking

-	The law should be prospective
	-	Presumptive of prospective of legislation in Ennglish law _Lauri v Renad] 1892] 3 Ch 402_
-	The law should be made known to those affected by it _Salih v Secretary of State for the Home Department [2003] EWHC 2273_
	-	Consider that statutory instruments are not required to be published and the Statutory Instruments Act 1946 provides for a qualified defence
-	The law should be clear and accessible _R (Purdy) v DPP [2009] UKHL 45_ __UKHL__
-	There must be certainty in the law _Sunday Times v United Kingdom (1979) 2 EHHR 245_ "A norm cannot be regarded as 'law' unless it is formulated with sufficient precision to enable the citizen to regulate his conduct"

