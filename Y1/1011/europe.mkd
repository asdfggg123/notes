#European Dimensions of the UK constitution

##Human Rights and the European Convention on Human Rights

###Human Rights before the ECHR

-	Prior to the HRA 1998, the UK did not have a set of constitutionally protected rights.
-	The approach taken by the UK towards human rights was a negative instead of positive one _Sovereignty in Comparative Perspective: Constitutionalism in UK and America_ __NYU Law Review__ Lord Irvine
	-	The UK operated on the principle of legality, where citizens are free to do as they wish except where forbidden by law.
	-	The protection of individual rights was effected by ministerial responsiblitiy and Parliamentary scrutiny
		-	Political accountability vs legal accountability
	-	This can be compared with the American system with a set of rights enshrined in their constitution

###Introduction to the ECHR

-	The UK ratified the ECHR in 1951 and it came into force in 1951.
-	The European Court of Human Rights was established in 1959
	-	It consists of 47 judges, one from each member state of the Council of Europe
	-	Its court of highest appeal is the Grand Chamber of the ECtHR, Constisting of 17 Judges
	-	It handed down its first judgement in 1961

###Impact of ECHR before HRA

-	The dualist system in the UK means that convention rights were not enforceable in domestic courts _R v Secretary of State for the Home Department, ex parte Brind [1991] 1 All ER 720_ __UKHL__
-	The ECHR itself gives individuals a right to petition the ECtHR in Strasbourg
-	The Courts would take into account the ECHR when developing previously unclear rules of common law _Derbyshire County Council v Times Newspapers [1992] QB 770_ __EWCA__
-	When interpreting legislation, the courts would operate under the presumption that Parliament intended to legislate in conformity with treaty obligations when resolving ambiguity or uncertainty in statutes _Brind_

###HRA 1998

-	The HRA incorporated the ECHR into domestic legislation
	-	s. 1 sets out the relevant articles
		-	Note that Articles 1 and 13 are not included
			-	Article 1: Obligation to respect the rights
			-	Article 13: Right to an effective remedy
	-	s. 2 gives interpretative guidelines as to the relevant rights. Courts or tribunals must take into account:
		-	Judgements, decisions and declarations of the ECtHR
		-	Opinions of the European Commission on Human Rights
		-	Decisions of the Commission and Committee of Ministers
	-	s. 3 obliges courts to interpret domestic legislation in a way that is compatible with Convention Rights
	-	s. 4 allows the superior courts to issue a declaration of incompability where legislation cannot be read to comply with Convention Rights under s.3
		-	Legislation remains in force following the declaration

####Section 1

The two notable exceptions in s. 1 of the HRA are necessary to allow the operation of the rest of the Act. 

s. 4 and s. 19(1)(b) act to preserve Parliamentary Sovereignty in allowing Parliament to depart from Convention Rights where they so desire. By providing for such departures, they would necessarily contravene Article 13 of the HRA. Therefore, incorporation of Article 13 would make the act self contradictory. Excluding Article 13 would necessarily contravene Article 1 and hence it must also be excluded.

####Section 2

S. 2 sets out interpretative rules for Convention Rights and has been the source of considerable judicial and academic debate.

Notable Cases:

-	_R(Ullah) v Special Adjudicator [2004] UKHL 26_ __UKHL__ established the _Ullah_ principle. In the words of Lord Bingham:
	>	the duty of national courts is to keep pace with the Strasbourg jurisprudence as it evolves over time: no more, but certainly no less
	-	"no less, but certainly no more" _R (Al-Skeini) v Secretary of State for Defence (The Redress Trust intervening) [2008] AC 153_ __UKSC__ Lord Brown
-	_Secretary of State for the Home Department v AF and others [2009] UKHL 28_ __UKHL__ was a case where the Lords unanimously expressed reservations on the outcome but considered themselves bound to follow Strasbourg jurisprudence. It is the source of Lord Rodger's famous dictum:
	>	Argentoratum locutum: iudicium finitum - Strasbourg has spoken, the case is closed.
-	_Ambrose v Harris [2011] UKSC 43_ __UKSC__ held that the _Ullah_ principle was good law and courts should not expand the scope of Convention Rights through judicial interpretation and should follow Strasbourg jurisprudence where available
-	Where the Grand Chamber is not clear and decisive on an issue, national courts may choose to depart from Strasbourg case law and in so doing invite Strasbourg to clarify the law on the matter _R v Horncastle [2009] UKSC 14_ __UKSC__
-	Consider the recent case _R (on the application of Chester) v Secretary of State for Justice [2013] UKSC 63_ __UKSC__
	>	it would have to involve some truly fundamental principle of law or the most egregious oversight or misunderstanding before it could be appropriate for the Supreme Court to refuse to follow Grand Chamber decisions of the ECtHR.

####Section 3

S. 3 requires courts to interpret and apply legislation in a manner that is compatible with Convention rights in so far as it is possible to do so. _Ghaidan v Mendoza [2004] UKHL 30_ is the leading case on this issue:

-	The courts have generally taken a rather bold approach when applying s. 3, going so far as to effectly amend legislation
	-	This power of amendment is not limited to where there is ambiguity in the statutory expression
	-	The courts may depart from the unambiguous meaning the statute would otherwise bear and therefore depart from Parliamentary intention
	-	The courts' interpretative powers under s. 3 nevertheless do not allow them to adopt an approach inconsistent with the fundamental feature of the legislation
-	The courts may not ursup Parliament's role where issues with very wide implications that require legislative deliberation arise (such as _In re S_ and _Bellinger v Bellinger_)

Consider the ideas of "constitution of reason" and "constitution of will"(legal positivism) as conceived by TRS Allan, and the implications of s. 3.

####Section 4

S. 4 allows courts to issue a declaration of incompatibility where they find it impossible to interpret statute under s. 3. S. 4 does not provide the appellant with any redress, but instead applies political pressure on the relevant minister or Parliament to act. The only occasion where Parliament declined to act following a declaration of incompatibility was _Hirst v HM Attorney General_, the prisoner voting case. An appellant who obtains a declaration may continue to appeal to Strasbourg to obtain personal relief.

####Sections 6-8

The traditional approach towards human rights is a vertical one where individuals are protected from interference of by the state and its bodies. However, s. 6(3) requires public bodies, including courts, to act compatibly with convention rights, and the effect is that courts must now give effect to convention rights even when deciding issues of private law.

-	A "public authority" for the purposes of s. 6 is a body that exercises broadly governmental (and therefore public) functions or a hybrid authority some of whose functinos are of a public nature _Aston Cantlow and Wilmcote with Billesley Parochial Church Council v Wallbank [2003] 3 All ER 1213_
-	Determining whether a private entity is providing a functions of a public nature for the purposes of s. 6 is highly difficult and there are no hard and fast rules. _YL v Birmingham City Council and Others [2007] UKHL 27_ sets out the criteria to be used in deciding such issues, and suggests that courts will err in favour of expanding the scope of s.6 when in doubt.

##The European Union and the European Communities Act 1972

The European Union is a regional body founded with the intention of increasing economic cooperation to the point where it would not be in any country's interest to wage war with another. The EU has expanded greatly since its inception, both in membership numbers as well as areas of competency. 

To satisfy the requirements for the UK's accession to the EU, and due to the dualist system here, Parliament enacted the European Communities Act 1972. This Act was of great constitutional significance, both due to the immense impact it has had on society at large as well as its legal effects. 

The sections of interest are:

-	S. 2(1) allows directly applicable EU law to be recognised and enforcable in domestic courts
-	S. 2(2) is a Henry VIII clause that gives ministers the power to amend primary legislation to give effect to EU obligations
-	S. 2(4) gives that any subsequent legislation must have effect subject to the foregoing provisions. This establishes domestic law's subordination to EU law.
-	S. 3(1) gives that the ECJ is the supreme authority on the interpretation and application of EU law
	-	Compare this with s. 2 of the HRA, where domestic courts need only take into account the ECtHR's judgements

As the culmulative effect of these provisions is to import the European legal order into the UK constitution, it is important to first discuss EU's legal order.

###The European Union

####General Principles and Powers

-	Five major principles of EU law are stated in Articles 4, 5 of the TEU
	1. Conferred Powers
		-	The EU only has powers conferred upon it by the Treaties
	1.	There are some essential state functions which the EU must respect
		-	This provision is to prevent the intrusion of the EU into matters that are clearly within the sole competency of member states
	1.	The duty of loyal cooperation
		-	Member states must take appropriate measures to fulfil their obligations 
	1.	Subsidiarity 
		-	Where an area is not within the exclusive competence of the EU, the EU should not act unless the same effect cannot be achieved by Member states or is better achieved at the Union level
	1.	Proportionality
		-	Content and form of Union action shall not exceed what is necessary to achieve the objectives of the Treaties

####EU's Structure

-	The European Council
	-	The European Council has been said to be "the most politically authoritative institution" of the EU (S Bulmer and W Wessels, _The European Council_; it determines the future institutional shape and tasks of the EU and develops broad principles of EU policy.
-	The Council of Ministers
	-	The Council consists of representatives of the governments of the Member States at the ministerial level. 
	-	The Council has the power of final decision on the adoption of legislation, whereby decisions are made either uninamously or by "qualified majority voting.
-	The European Commission
	-	Legislative powers - The Commission may be granted the power to make delegated legislation to give effect to Council decisions
	-	Agenda-setting powers - The Commission initiates policy processes and therefore sets the EU's annual legislative programme
	-	Executive powers - It holds the EU's purse strings and represents the EU in some international bodies such as the WTO
	-	Supervisory powers - It enforces EU law by initiating legal action against member states in the Court of Justice
-	The European Parliament
	-	The Parliament allows for local representation and while it is less powerful than the European Council and the Council of Ministers, it has become increasingly relevant, especially in the areas of human rights and civil liberties
-	The Court of Justice of the European Union
	-	The European Court of Justice (ECJ) is the highest court in the EU
	-	The General Court is the court of first instance
	-	The Civil Service Tribunal

####European Law

The CJEU's core competencies are judicial review, infringement proceedings and preliminary rulings. 

Judicial review may be initiated by a member state or one of the EU institutions or privatep arties with a direct concern in the matter (instead of "direct and individual" pre-Libson). The four grounds of review are:

-	Lack of competence
-	Breach of essential procedural requirement
-	Infringement of the Treaty or any rule of law relating to its application
-	Misuse of powers

Preliminary reference is a procedure by which national courts may request a ruling from the CJEU on the interpretation of EU law when the question arises in domestic proceedings. 
	-	National courts are entitled to refer a question to the CJEU
	-	Courts of last resort are bound to refer such a question
		-	Where the question has previously been resolved or where the national court is convinced the law is clear beyond reasonable doubt, it is not necessary _CILFIT v Ministry of Health_ (the acte clair doctrine)
			-	See _R (Imperial TAbacco Ltd) v SS Health_ for an example of judicial disagreement on whether an issue was acte clair
	-	The preliminary ruling is binding on national courts on the point of EU law that was referred, but all domestic aspects of the case remains within the jurisdiction of the domestic court

#####Principles of European Law

-	Supremacy
	-	Law stemming from the Treaty could not be overridden by domestic legal provisions _Costa v ENEL_
	-	National courts are under a duty to disapply conflicting domestic provisions (even where there is a doctrine of implied repeal) _Simmenthal_
-	Direct effect
	-	Treaties and regulations that are clear, unconditional and negative have direct effect on member states _Van Gend en Loos_
	-	The requirement for negative provision was dropped in _Reyners v Belgium_
	-	The requirement for unconditional and not requiring national implementing legislation was dropped in _Defrenne v Sabena_
	-	This was extended to directives in _Van Duyn v Home OFfice_, a judgement subject to much criticism
		-	This has been subsequently explained with an estoppel-like argument
		-	By virtue of the argument, directives may only be relied upon in proceedings against a public authority of a member state, and not private entities
-	State liability
	-	Member states could, in certain circumstances, be liable in damages to individuals who suffer loss as a result of the member state's failure to properly implement a directive _Francovich and Bonifaci v Italy_. Its conditions are:
		-	The directive must grant the rights to individuals
		-	The content tof those rights must be identifiable from the provisions of the directive
		-	There is a casual link between the state's failure and the subsequent loss and damage of the injured parties
	-	State liability applies where individuals suffer loss and damage as a result of national legislation _Factortame (EU Court)_

####EU and UK Law

EU law has had significant impacts on the UK constitutional order, especially where Parliamentary sovereignty is concerned. 

The recognition of the ECA as a "constitutional" statute and thus immune to implied repeal is a sharp deviation from the orthodox view. In _Thoburn v Sunderland CC [2003] QB 151_, Laws LJ found that statutes that broadly define the relationship between citizen and state such as the ECA were of a constitutional nature. This was a novel development because the orthodox view is that all statutes are equal.

Factortame was another constitutionally significant case. The House of Lords first held that they were unable to grant Factortame an injunction against the crown because that would be contradicting the will of Parliament as well as because they lacked the requisite jurisdiction. Factortame then brought action in the ECJ, where the supremacy of EU law was upheld and the House subsequently granted an injunction in favour of Factortame. This case was highly unusual because it essentially established the supremacy of EU law in its areas of competency over domestic law, directly contradicting the doctrine of Parliamentary sovereignty. Lord Bridge remarked that the UK government has accepted such curtailment of its sovereignty when the UK joined the EU, and the supremacy of EU law was well established in Luxembourg jurisprudence.

#####Revolution or Evolution

The academic opinion on Factortame is divided. Wade views it as a revolution because it was a significant departure from the Diceyan view of absolute Parliamentary sovereignty. Parliament has, by virtue of its membership of the EU, agreed to a significant reduction to their sovereignty and this was recognised and accepted by the courts. Wade appears to view Parliamentary sovereignty as a matter of political fact, and Factortame was a case of the courts giving recognition to a change to this political fact.

Laws LJ argued that it was more of an evolution, and that similar provisions immune to implied repeal already exist (impose taxes, criminal liability or have retroactive effect), and this case was merely an extension. TRS Allan takes the view that sovereignty should be taken as a rule of common law, and Factortame was just a case of the House determining what was required of the existing constitutional order in novel circumstances.

Section 8 of the European Union Act 2011 is interesting in that it reasserts that the status of EU law as essentially a "higher" form of law is purely based upon statutory provision in the ECA. Part 1 of the Act also requires referendum and Parliamentary approval for amendment to the EU treaties as well as certain decisions made under EU treaties. This is interesting in that the provisions, while asserting the need for Parliamentary approval and thus reaffirming Parliamentary sovereignty, also require a referendum, which effectively means a curtailment of Parliamentary sovereignty in the traditional sense. Perhaps there is a systemic acceptance of a reduction of Parliamentary sovereignty within the political world and those not overly hung up with traditional legal doctrines. 
