#Crown and Government

##Crown

###Introduction

'The Crown may denote the Queen in her constitutional role, but more broadly it "personifies the executive government of the country"' _Diplock LJ  in BBC v Johns [1965] Cj 32. 79_

'[T]he Crown [is] the executive government in its various manifestations' _Deutsche Morgan Grenfell v Inland Revenue Commissioners [2006] UKHL 49_

It is clear that the Crown does not refer to the monarch herself but rather the executive government acting on behalf of the Crown.

Therefore it follows that executive acts of government done by ministers and parliamentary secretaries (who exercise Her Majesty's executive powers) are considered as acts of the Crown _M v Home Office [1994] 1 AC 377_ __UKHL__

"The indivisibility of the Crown" - There must be consistency in the actions of the Crown since it acts as one body, and therefore expectations created by one branch of government would bind another _R (Bapio Action Ltd) v Secretary of State for Home Department [2008] UKHL 27_ __UKHL__
	-	Lord Scott dissented, saying that this principle is "an archaic constitutional theory that has become legal fiction"
	-	The Queen's government is a separate legal entity in each Commonwealth territory still owing allegiance to the Crown _R (Quark Fishing Ltd) v Secretary of State for Foreign and Commonwealth Affairs [2005] UKHL 57_ __UKHL__

###Crown Privileges

-	The Crown is not bound by statute unless by express words or necessary implication (the "rule of construction") _Lord Advocate v Dumbarton District Council [1990] 2 AC 580_ __UKHL__
	-	If it was apparent at the time when statute was passed that the beneficent purpose would be frustrated unless the Crown were bound, it may be inferred that the Crown has agreed to be bound.
-	The Crown historically had the right (privilege) to disallow the production of any document in court by invoking the public interest. 
	-	This discretion was made reviewable and no longer absolute under _Conway v Rimmer [1968] AC 910_ __UKHL__
	-	This was reinterpreted as "public interest immunity" and requires courts to balance state interests with due administration of justice _Rogers v Home Secretary [1973] AC 388_ 
	-	Judicial authority to review such claims was reaffirmed, although "very substantial weight" was to be accorded to the Executive's view and "cogent reasons" would be required for a court to differ from their assessment of risk to national security: only if their view was irrational or without a sufficient evidential basis should courts override it _R (Binyam Mohamed) v Secretary of state for Foreign and Commonwealth Affairs [2010] EWCA Civ 65_ __EWCA__
	-	Treasury Solicitor issued a _Guidance on Discharging the Duty of Candour and Disclosure in Judicial Review Proceedings_ that the Government's aim when facing judicial review should not be to win at all costs but instead assist the court to reach the correct result. PII applies only where disclosure of material would cause serious harm or real damage to the public interest.
-	The Crown may not be subject to orders of injunction or specific performance s. 21 Crown Proceedings Act 1947
	-	Courts may issue declarations to that effect, that are "invariably respected" - Lord Bridge in _Factortame Ltd v Secretary of State for Transport [1990] 2 AC 85_ __UKHL__
-	The Crown is generally immune from criminal liability but exceptions can be made by statute
-	The Crown enjoys no immunity from civil liability _Deutsche Morgan Grenfell v Inland Revenue Commissioners [2006] UKHL 49_ __UKHL__
-	The creation of new statutory bodies usually specify whether they are to enjoy the privileges or immunities of the Crown; where absent, it is to be established by considering the functions of the body and the degree of its independence _Tamlin v Hannaford [1950] 1 KB 18_ 

###Royal rights and prerogatives

The Monarch has the right "to be consulted, to encourage, to warn" - Walter Bagehot

####Appoint the PM

The Monarch has the legal right to appoint the Prime Minister as she pleases, although the leader of the majority party in Parliament is invariably appointed. 

Where the incumbent government loses a majority in the lower House, the government resigns and the Monarch calls on the leader of the opposition party to form the new government.

Where no party obtains a majority following an election, the incumbent goverment would remain in power for a "reasonable time" while a parties attempt to form a coalition that controls a majority of the seats, following which the government would resign if they were not a member of that coalition (as in 2010 elections)
	-	Perhaps the lower House can elect a PM notwithstanding the lack of a coalition with a majority
	-	If no coalition can be formed, perhaps a fresh round of elections will be called under s. 2(2) of the Fixed Term Parliaments Act 2011

Golden rule is that the monarch is not to be drawn into controversy or political negotiations. 

####Dismissal of ministers

The Monarch has the prerogative power to dismiss her ministers, although this power is exercised by the Prime minister by virtue of a constitutional convention. 

It has been suggested that the Monarch could independantly exercise this prerogative where the government refuses to resign despite losing a vote of no confidence. This is no longer relevant by virtue of the Fixted Term Parliaments Act.

####Dissolution of Parliament

This power was historically vested in the Monarch, to be exercised at the request of the Prime Minister. This has now been abolished by virtue of s. 3(2) of the Act

####Royal assent to legislation

The Royal assent is required for bills to become legislation, and this assent has not been refused on legislation that has satisfied all procedural requirements since 1707. It is now an established constitutional convention that the Royal assent will not be refused where parliamentary proceudres were properly observed in a bill's passage. 

####Granting of Peerages and Orders of Chivalry

The granting of Peerages is a Royal prerogative that is exercised at the PM's recommendations. 

The granting of most Orders of Chivalry is also at the PM's recommendations, although the Monarch appears to retain almost complete discretion in the granting of the Royal Victorian Order (for personal service to the Royal family) and the Order of the Garter (for those who have held public office, contributed in a special way to national life or served the Sovereign personally) 

##The Central Government

###Introduction

The central government embraces "embrace both collectively and individually all of the ministers of the Crown and parliamentary secretaries [junior ministers] under whose direction the administrative work of government is carried on by the civil servants employed in the various government departments." - Lord Diplock in _Town Investments Ltd. v Dept of the Environment [1978] AC 359_ __UKHL__

The central government consists of:

1.	The PM
1.	Ministers and the Cabinet
1.	Departments
1.	Civil Service
1. Agencies, quangos, etc.
1.	Special Advisors

###The Prime Minister

The office of the PM is a creation of convention, and few powers are expressly vested in the PM, although he may be able to exercise the prerogative powers belonging to the crown that require ministerial advice by convention, as well as those devolved upon his ministers.

###Ministers and the Cabinet

####Ministers

-	Ministers are appointed by Royal prerogative and under convention this prerogative is exercised only with the advice of the PM.
-	Ministers must be members of one of the Houses of Parliament
-	Ministers can generally be divided into senior ministers, junior ministers, Law Officers, and whips
	-	Senior ministers
		-	Secretaries of state
		-	Full or senior ministers
	-	Junior ministers
		-	Ministers of state
	-	Law Officers of the Crown
		-	Attorney General for England and Wales
		-	Solicitor General for England and Wales
		-	Advocate General for Scotland
		-	Attorney General for Northern Ireland
		-	Advocate General for Northern Ireland
		-	The Law Officers are in a rather curious and uncomfortable position where they are by nature of their elected office creatures of politics, but they are expected to provide the government with impartial legal advice. The Attorney General, as the chief government prosecutor, is expected to be independent in carrying out those duties. Considering the _Spycatcher_ case, where the AG said that "nobody can influence [him] and [he] would not accept any attempt to influence [him]" when he is acting in his capacity as AG and prosecutor. However, he subsequently commented that the decision to take proceedings in Australia was a government decision and collective ministerial responsiblity applied. Lord Steyn commented extra-judicially that the AG is a "political figure responsive to public pressure"
	-	Government whips
		-	Commons Chief Whip: Parliamentary Secretary to the Treasury
		-	Commons Deputy Chief Whip: Treasurer of HEr Majesty's Household
-	Ministers are expected to abide by the _Ministerial Code_, a document issued and revised on the authority of the PM; it does not have the force of law

####The Cabinet

-	The Cabinet is the core government body consisting of the PM and senior ministers
-	The PM typically decides on the membership of the Cabinet and the portfolios of the members; this discretion, although not limited by law, is not absolute due to political reality
-	Non-Cabinet ministers may be authorised to attend without being admitted membership
-	The main business of the cabinet, per the _Ministerial Code_, is:
	-	Questions which significantly engage the collective responsibility of the Government because they raise major issues of policy or because they are of critical importance to the public
	-	Questions on which there is an unresolved argument between departments
-	The Cabinet is usually organised into committees tackling specific areas of governance; Membership and portfolio of these committees is decided by the PM

###Balance of Power in the Core Government

Although the system of government in UK is said to be one of "Cabinet government", the concentration of power in the hands of the PM has led to a situation where the PM makes most of the important decisions and the Cabinet merely "rubber stamps" them. The PM's power to appoint and dismiss ministers and set Cabinet agenda, on top of his political influence as head of his party, has led to former ministers advocating a greater degree of democratic control over the PM. 

Margaret Thatcher and Tony Blair are two examples of highly authoritarian PMs in recent times whose personal views and convictions dominated governmental policy. Mrs Thatcher has in particular exercised her powers to appoint and dismiss ministers to significantly strengthen her power base in 1981. However, this concentration of PM power is subject to political limitations and where the PM loses popular support within the electorate and his party, he can be (in effect) forced to resign. Tony Blair's system of governance has also led to the coining of the term "armchair politics", where matters of national importance are decided not in cabinet but rather on a sofa in his study in consultation with an inner circle of ministers and advisors. One exceptional example of this was during the Iraq war where the Defence and Overseas Policy Committee of the Cabinet did not meet and instead an informal "war cabinet" was formed. Other historical examples of decisions made purely under PM discretion (with a select group of ministers) and not full Cabinet discussion include the development of the atomic bomb and the Polaris missile.

###Departments

-	The structure and organisation of government departments is not fixed and the PM exercises devolved prerogative powers in the management of departments
-	Departmental reorganisations do not have requirements of formalities but may be announced to Parliament where significant.
-	Departmental reorganisations frequently occur, especially when the party in power changes. This is a costly process done in the quest for efficiency or in response to change in priorities, although actual benefits are hard to determine

###Civil Service

A civil servant is defined as:
> a servant of the Crown working in a civil capacity who is not: the holder of a political (or judicial) office; the holder of certain other offices in respect of whose tenure of office special provision has been made; a servant of the crown in a personal capacity paid from the Civil List

-	The civil service is traditionally managed by the PM, and this has now been put on a statutory footing under the Constitutional Reform and Governance Act 2010
-	Civil servants serve and advise Ministers, and are expected to do so in an impartial and non-political manner. They typically continue to hold office regardless of the party in power
-	Departments in the civil service are headed by Permanent Secretaries, who report to ministers
-	Their core values are: integrity, honesty, objectivity and impartiality
-	The House of Commons Disqualification Act 1975 places limits on civil servants' rights to participate in political activities depending on their seniority and access to sensivity information

###Agencies

These bodies are typically set up within government departments to address operational issues, while core governmental departments address the policy aspect of things. Many of these agencies are staffed by civil servants.

Non-departmental public bodies (NDPBs) are set up to harness expertise only available outside the civil service or to carry out functions whhich it is thought should be detached from direct ministerial control and civil service organisation.

-	These bodies may exercise advisory or executive duties and are generally politically independent
-	They enjoy substantial autonomy
-	Senior members of these bodies are typically appointed by ministers
	-	Concern that these appointments are overly political in nature has led to the creation of a Commissioner for Public Appointments to review them
-	They are typically not Crown bodies
-	Their staff are usually not civil servants
-	These bodies have faced significant cuts under recent administrations

###Special Advisors

Special advisors are typically appointed to provide advice or expertise different from that of the "Whitehall mandarin". They are generally involved where issues are too politically sensitive to involve permanent civil servants.

They are temporary civil servants and subject to the same rules of conduct, although they are notably allowed to be more political in their recommendations. They are nevertheless not allowed to take part in public political controversy

##Government Powers

###Primary and Secondary Legislation

The government is an important party in the process of forming legislation both primary and secondary. This is discussed in greater detail under the legislature

###Statutorily granted executive powers

-	A number of powers are grated by statute and vested in Ministers or Secretaries of State. 
-	These powers are rarely exercised by the ministers himself and are instead usually devolved to departmental officials
	-	The proper exercise of these powers by officials are considered in law to be exercised by the Minister himself _Carltona Ltd v Commissioner of Works [1943] 2 All ER 560_ __EWCA__
	-	Statute may require that the power is to be executed by the Minister personally

###Prerogative powers

-	Ministers are vested with a number of prerogative powers that are strictly speaking Royal prerogatives but are now exercised under ministerial advice by convention.
-	Where prerogative and statutory powers overlap, the goverment must act under authority of statute _AG v De Keyser's Royal Hotel Ltd [1920] AC 508_ __UKHL__
-	The full range of prerogative powers are not practically identifiable and "it is only by a process of piecemeal decision over a period of  enturies that particular powers are seen to exist or not exist" -Nourse LJ in _R v Secretary of State for the Home Department, ex parte Northumbria Police Authority [1989] QB 26_ __EWCA__

###Prerogative Legislation

The government may make primary legislation under the Royal prerogative by enacting a prerogative Order in Council. The scope of these orders are limited and yield to statute; notably, the order can be used to legislate for a few remaining colonies and create new courts of common law.

###Guidance, Codes of Practice, and other Administrative Rules

Administrative rules are typically made to regulate the exercise of statutory or other discretionary powers by government departments. These rules lack statutory footing but are generally followed within their scope of application.

Guidances and Codes of Practice may apply to public or private bodies and similarly lack statutory basis. They are therefore not legally enforcible, especially on private bodies who have no duty to act in accordance with them. Certain public bodies on the other hand are required by statute to consider relevant guidances and codes in the exercise of their power. These are therefore generally persuasive in nature.

###Voluntary agreement, self-regulation and contracting out

Voluntary agreement is a way for the government to work with private parties to achieve certain objectives that are too politically controvertial or complicated to administer via legislation. 

Self-regulation is also a manner for the government to exert a pursuasive instead of compulsive influence on industry practice and other matters; where self-regulation fails, recourse to legislation is likely. 

The government may also enter into contracts with commercial parties over the provision of goods of services. The Deregulation and Contracting Out Act 1994 allows ministers to transfer public functions to the private sector without specific legislation. These functions are typically routine and contracting out is used to engage skills not available among civil servants or to achieve greater efficiency.

##Accountability

###Political Accountability

####Parliamentary Control

The government is subject to supervision from select committees in Parliament, who perform general oversight roles while retaining the authority to launch more in depth inquiries into the exercise of executive powers

Where statutory instruments are concerned, many Acts that grant the government powers to create secondary legislation have requirements that such instruments must be laid before Parliament prior to their coming into force. The Statutory Instruments Act 1946 also require most statutory instruments to be published per s. 2(1) and subject instruments laid before parliament to control under s. 5. There are generally two kinds of parliamentary control that are exercised over statutory instruments, positive and negative; the specific type that is applicable is determined in the original Act granting such powers.

The House of Lords has set up a Delegated Powers and Regulatory Reform Committee to examine whether bills "inappropriately delegate legislative power" and report to the House before the bill reaches the committee stage. This was in response to concerns over the immense volume of subordinate legislation which by its nature limit the opportunities for participation by politically responsible members of the Commons. An example of where this committee and many others criticized the Government's proposal and the Government subsequently yielded to the pressure was the Policing and Crime Bill 2009.

The Constitutional Reform and Governance Act 2010 has also brought about reform to the Civil Service and the Government's treaty making powers. Part 1 has placed the management of the Civil service on a statutory platform, while Part 2 incorporated the "Ponsonby Rule" into statute. 

Some argue that there is an emerging constitutional convention of seeking Parliamentary approval before commencing major military action. __READ MOAR ABOUT DIS__

The "Ponsonby rule", a convention whereby treaties brought into force by executive order must be laid before Parliament for 21 days and be subject to negative resolution, was developed to allow Parliamentary oversight into the exercise of the ministerial prerogative to enter treaties. It has since been brought onto statutory footing via the Constitutional Reform and Governance Act 2010.

####Constitutional Conventions of Ministerial Responsibility

The general principle is one of collective ministerial responsibiliy:

-	All government ministers are bound by the collective decisions of the Cabinet, except where it is explicitly set aside _Cabinet Manual_
-	Ministers may express their frank opinions in private during Cabinet sessions, but must present a unified front in public
-	The strict convention requires ministers who to resign from office if he wishes to express public dissent from government policies
-	This convention, combined with the intrinsically political nature of office holders, has led to the prevalence of ministerial leaks
-	The number of ministers with a wide range of portfolios means that ministers are rarely involved in the making of decisions they are required to stand behind under this convention
-	The present coalition government necessarily requires the allowance for public disagreement where parties have different views on policy
-	The case of _AG v Jonathan Cape [1976] QB 752_ __EWCA__ judicially recognized the existence of this doctrine, and maintenance of this doctrine is in the public interest and therefore is a valid public interest ground for suppressing the release of the documents. 
	-	The court found no generally applicable principle, instead holding that this was merely one factor to be weighed against other facets of public interest

The case of Jonathan Cape raises the question as to whether the judicial recognition of a convention necessary makes it enforcible in a court of Law. TRS Allan suggests that this recognition establishes the legitimacy of the principle and is necessarily an endorsement of it; enforcibility follows recognition in the legal world. Joseph Jaconelli disagrees, arguing instead that the enforcement of conventions in these cases is due to the ascription of established legally enforcible rights, and the convention does not by itself give rise to a cause of action. This was the approach taken in Jonathan Cape, and is probably the right one to take in general. This is especially when considering that one of the benefits of constitutional conventions is the flexibility it accords to the system. Another possible argument is that many conventions are in direct contradiction to the established law of the land. The convention that the Monarch may not refuse Royal Assent to a bill validly passed contradicts the strict legal stance that the Monarch possesses this right. Furthermore, conventions have been violated on occasions with no subsequent repercussions, and it would be improper for the Courts to enforce such rules.


Within government departments, there is a doctrine of individual ministerial responsibility:

-	The Minister is held responsible for everything done by the department
-	This is the means through which government departments are held accountable to Parliament without which Parliament will be unable to exercise legislative supervision of the executive

Consider the Crichel Down affair, where Sir Thomas Dugdale resigned due to the failings of civil servants in his department. Sir David Maxwell Fye clarified this convention by stating that while ministers are constitutionally accountable to Parliament for the actions of those under him, he is not always obliged to shield the offending civil servant. The minister should shield the civil servant where he acts pursuant to the minister's direct instructions or policies, or where he makes a minor mistake or delay; only where a civil servant acts reprehensibly in a manner which the Minister disapproves and has no prior knowledge of will he be exposed to public criticism. This has subsequently been incorporated into the _Ministerial Code_

This doctrine is now less effective due to the prevalence of executive agencies which while situated within government departments are not directly accountable to ministers and therefore ministers are not held accountable for their failings.

###Legal Accountability

-	Aggrieved persons can apply to the High Court or in some cases the Upper Tribunal for permission to obtain judicial review of a decision. If granted, the court will inspect the legality of the decision taken and may issue, by way of relief, a declaration, quashing order, mandatory order, or prohibiting order.
-	Legal control of statutorily conferred duties and powers is carried out on grounds of procedural impropriety, illegality, and irrationality
-	Ministers may not shelter behind the immunity of the Crown _M v Home Office_
-	One noteworthy case is _Council of Civil Service Unions v Minister for the Civil Service [1985] AC 374_ __UKHL__ where the House held that:
	-	Prerogative powers are reviewable
	-	National security could be a ground for frustrating an established legitimate expectation
	-	National security was a non-justicable issue
	-	__READ THIS CASE LATER__
-	Express and unequivocal terms of the statute must exclude the operation of the prerogative _R v SSHD, Ex p Northumbria Police Authority [1989] QB 26_ __EWCA__
-	Government cannot rely on prerogative powers to introduce a policies that effectively renounce one embodied in an Act of Parliament _R v SSHD, ex p Fire Brigades Union [1995] 2 AC 513_ __UKHL__


