#Defamation

##Elements

###Defamatory Statement

-	The definition of what constitutes a defamatory statement has not been established satisfactorily _Berkoff v Burchill [1996] 4 All ER 1008_ __EWCA__
	-	Neill LJ presents an overview of previously accepted definitions:
		-	"Classic" definition in _Parmiter v. Coupland (1840) 6 M.&W. 105_, subsequently considered to be insufficient in _Tournier v. National Provincial Bank [1924] 1 K.B. 461_ __EWCA__
		>	A publication, without justification or lawful excuse, which is calculated to injure the reputation of another, by exposing him to hatred, contempt, or ridicule.
		-	Expanded to include words which cause a person to be shunned or avoided in _Youssoupoff v. MGM Pictures Ltd (1934) 50 T.L.R. 581_ __EWCA__
		>	not only is the matter defamatory if it brings the plaintiff into hatred, ridicule, or contempt by reason of some moral discredit on [the plaintiff's] part, but also if it tends to make the plaintiff be shunned and avoided and that without any moral discredit on [the plaintiff's] part. It is for that reason that persons who have been alleged to have been insane, or be suffering from certain diseases, and other cases where no direct moral responsibility could be placed upon them, have been held to be entitled to bring an action to protect their reputation and their honour.
		-	Lord Atkin in _Sim v. Stretch [1936] 2 All E.R. 1237_ __UKHL__
		>	I propose in the present case the test: would the words tend to lower the plaintiff in the estimation of right-thinking members of society generally?
		
			-	"Right-thinking members" tend to be good law abiding citizens _Byrne v Dean [1937] 1 KB 818_ __EWCA__
		-	In a professional context, _Drummond-Jackson v. British Medical Association [1970] 1 W.L.R. 688_ __EWCA__ gives
		>	Words may be defamatory of a trader or business man or professional man, though they do not impute any moral fault or defect of personal character. They can be defamatory of him if they impute lack of qualification, knowledge, skill, capacity, judgement or efficiency in the conduct of his trade or business or professional activity.
-	Abusive and offensive words are generally not defamatory
	-	Lord Atkin in _Sim v Stretch_:
	>	exhibitions of bad manners or discourtesy are [not to be] placed on the same level as attacks on character.
-	In a business context, a statement that casts aspersions on the way a business is run will be defamatory if its (likely) effect is to harm the business' credit or custom _South Hetton Coal Company Ltd v North-Easten News Association Ltd [1984] 1 QB 133_ __EWCA__
	-	Casting aspersions on the quality of a business' goods or services is not defamatory
		-	Exception where statement is malicious falsehood
	-	Casting aspersions on the way a business is run can give rise to an action for defamation
-	Interpretation
	-	Innuendo
		-	Legal vs ordinary innuendo per Lord Devlin in _Lewis v Daily Telegraph_
			1.	Plain ordinary meaning of words
			1.	Indirect meanings (ordinary innuendos) inherent in, but beyond, the plain meaning of the words
			1.	Secondary meaning (legal innuendo) supported by extrinsic facts that give rise to a separate action in defamation
		-	If the plain ordinary meaning of defamatory words also give rise to defamatory innuendo, two separate courses of action arise _Lewis v Daily Telegraph [1964] AC 234_ __UKHL__
		-	Where no innuendo is pleaded, the claimant need not define the natural or implied meaning of defamatory words _Lewis v Daily Telegraph_
		>	[Words are to be read] in the sense in which ordinary persons, or in which we ourselves out of court ... would understand them.
			-	Ordinary person are not hungry for scandal
		-	A statement that a party is under investigation implies suspicion and does not impute guilt _Lewis v Daily Telegraph_
		-	The judge has a responsility to exclude meanings that the words are incapable of (especially where pleaded by the claimant) _Lewis v Daily Telegraph_
			-	No longer necessary per s. 11 Defamation Act 2013 that virtually abolishes trial by jury
	-	Whether a publication is defamatory depends on the content of the entire publication, even if elements of it are capable of being defamatory _Charleston v News Group Newspapers Ltd [1995] 2 AC 65_ __UKHL__
	-	Example of where the reasonable person was attributed with knowledge that would induce them to come to a conclusion that has a defamatory effect (legal innuendo) _Cassidy v Daily Mirror Newspapers Ltd [1929] 2 KB 331_ __EWCA__
-	Per s. 1 Defamation Act 2013, there is a "serious harm" requirement

####Libel or Slander

-	General rule given in _Thorley v Lord Kerry (1812) 4 Taunt 355_ __UKHL__
	-	Libel - Defamatory statement made in permanent form
		-	No requirement to prove actual damage
		-	Libellous statement, read aloud, is still libel _Forrester v Tyrrell_
	-	Slander - Defamatory statement made in transient form (usually verbal)
		-	Requires cconsequential loss ("special damage" in judgments)
		-	No need to prove actual damage where the defamatory statement:
			-	Imputes an indictable criminal offence _Gray v Jones [1939] 1 All ER 795_ 
			-	Imputes professional incompetence per s. 2 Defamation Act 1952
			-	s. 14 of Act abolishes two other exceptions concerning infectious diseases and chastity
-	Special rules
	-	Speech may be considered libel:
		-	Public performance of a theatrical act per ss. 4, 7 Theatres Act 1968
		-	Public broadcast programmes per s. 166(1) Broadcasting Act 1990

###Referring to Claimant

-	Companies
	-	Companies may sue since they are legal persons; normal rules of libel and slander apply _Jameel v Wall Street Journal Europe sprl [2007] 1 AC 359_ __UKHL__
	-	"Serious harm" requirement refers to the likely or actual "serious financial loss" that results per s. 1(2) Defamation Act 2013
-	Groups of people
	-	An individual may sue over words that are defamatory of a group if he is individually identifiable _Knupffer v London Express Newspaper Ltd. [1944] AC 116_ __UKHL__
-	Public bodies
	-	Central and local governments do not possess the right to maintain an action in defamation _Derbyshire CC  v Times Newspapers Ltd. [1993] AC 534_ __UKHL__
-	Political parties
	-	A political party does not possess the right to maintain an action in defamation _Goldsmith v Bhoyrul [1998] QB 459_ __EWHC__
-	Strict liability applies whereby the intention of the respondent is of very limited relevance
	-	Whether the defamatory effect of words were foreseeable or intentional is irrelevant _Newstead v London Express Newspapers [1940] 1 KB 377_ __EWCA__
	> What does the tort consist in? It consists in using language which others knowing the circumstances would reasonably think to be defamatory of the person complaining of and injured by it.
	
###Made to Third Party
