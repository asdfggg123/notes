#Nuisance

##Public Nuisance

- Defined in Archbold's Criminal Pleading and Practice, 2010:
> Person guilty of a public nuisance who does an act not warranted by law, or omits to discharge a legal duty, if the effect or the act is to endaneger life, health, property morals, or to obstruct public in exercise or enjoyment of common rights
- Primarily a crime, but may be actionable in tort
- Must affect a consider number of people
	- Affect "a class of Her Majesty's subjects"; necessary size of community determined on a case-by-case basis _AG v PYA Quarries Ltd_
	- Act must affect the community as a whole, rather than merely individuals (common injury by necessary implication) _R v Rimmington & Goldstein_
- Objective test of whether D knew or ought to have known there was a real risk that conduct would lead to nuisance _R v Shorrock_
- Tortious remedies are available where:
	- AG may apply for injunction
	- Individuals may claim only where special damage is established
		- Individual must suffer damage more than the general inconvenience and injury suffered by the public _Tate & Lyle Industries v Greater London Council_
		- Claims are available for: 
			- Property damage
			- Personal injury _(Corby Group Litigation v Corby BC_
			- Economic Loss _Color Quest v Total Downstream_
		- Mere inconvenience is insufficient _Winterbottom v Lord Derby_
- A single activity may give rise to both public and private nuisance
	- Even where part of the nuisance occurred on public property, a claim may still lie in private nuisance if it intereferes with claimant's right to the use and enjoyment of his property _Halsey v Esso Petroleum_
	- Some use of land is necessary to incur liability _Southport Corporation v Esso Petroleum_
		- Not a private nuisance because "it did not involve the use by the defendants of any land"
	- See _Tate & Lyle Industries v GLC_ for example:
		- Ferry terminal was erected by defendant and caused stilting, leading to restriction in access to plaintiff's jetty; plaintiff incurred extra costs to remedy
		- Held: No claim in private nuisance because it did not threaten to cause damage to plaintiff's land or interfere with his acknowledged rights; Claim lay in public nuisance due to interference with public right to navigation and plaintiff suffered special damage (costs of remedy)

##Private Nuisance

###Definition and Doctrine

- Nuisance is primarily a tort against land _Hunter v Canary Wharf Ltd_
	- "Concerned with interference with the enjoyment of land" -Lord Goff
	- One must have a proprietary interest in land to have legal standing to sue
		- Right to exclusive possession -Lords Goff and Hoffman
- Remedies generally serve to protect property rights and compensate for their interference
- Outline of academic opinion:
	- Nolan argues that the less restrictive approach in _Khorasandjian v Bush_ that individuals may sue in the case of property used as a home was vague and could cause incoherence in the law of private nuisance
	- Cane considers that _Hunter_ did not sufficiently take into account important social considerations and it might be desirable to take a more obligation based approach.  
	- O'Sullivan prefers Lord Cooke's point of view that a relaxation of the traditional requirement is desirable on policy grounds. Judicial discretion would be emplyed to resolve uncertainties.
- Under Human Rights Act 1998, individuals' right to private and family life is protected under Art 8 ECHR
	- Action lies under Art. 6 against public authorities, but damages will only be available if it is necessary to afford just satisfaction.
	- Some claimants in _Hunter_ took their case to Strasbourg, where ECtHR held that individuals should have a right to action whether they have a proprietary interest in land or not

###The Action

- The action is generally upon C's right to the use and enjoyment of his land
	- D's interference with this right as permitted in so far as he acts like a reasonable user of his own land _Barr v Biffa Waste Services_
		- It is not a free-standing test, but rather a shorthand for common-law concepts.
	- C's right to use is not "according to the elegant and dainty habits of living but according to plain and sober notions among our people" _Walter v Selfe_
	- Principle of give and take between neighbours in land _Cambridge Water Co Ltd v Eastern Counties Leather Plc_
	- Nuisance does not give rise to a claim for personal injury _Transco plc v Stockport MBC_
		- Except indirectly where personal injury is a manifestation of the loss of amenity.

####Locality

- Locality is generally relevant for the purposes of determining what is reasonable use of land. 
	- Locality factors are generally specific to the particular area: what is a nuisance in one area is not necessarily a nuisance in another _Baxter v Camden London Borough Council (No. 2)_
	- Locality is relevant only for loss of amenity but not physical encroachment of land _St Helen's Smelting Co v Tipping_
- Characteristics of the locality may change over time due to organic development; this will lead to a corresponding developemt in what is "reasonable use" for that land _Coventry v Lawrence_
- Whether planning permissions change a locality has been an uncertain part of the law. This was recently clarified by the UKSC in _Coventry v Lawrence_
	- Prior to _Coventry v Lawrence_:
		- Some high level/strategic planning permissions could change the locality _Gillingham Borough Council v Medway (Chatham) Dock Co Ltd_
		- EWCA in _Coventry v Lawrence_ held that permission to build a race track changed the character of the neighbourhood
		- Regulatory consent by Environment Agency to build waste disposal facility did not change neighbourhood_Barr v Biffa Waste Services_
		- Allowing the intensification of activities did not change the neighbourhood _Wheeler v JJ Saunders_
	- In _Coventry v Lawrence_:
		- Lord Neuberger in the majority held that planning permission, in almost all cases, should not be considered to have changed the neighbourhood
			- Planning permission only changes neighbourhood incidentally when it has been acted upon. It is the actual change that alters the locality, rather than the planning permission itself. 
			- Planning permission may alter the locality only when it pertains specifically for the nuisance alleged
				- E.g. Planning permission specifically permits D to emit a noise at a certain level (with other restrictions)
			- Planning permission documents may be of some assistance to the courts (but less so when people who produced the documents are not available for cross examination)
				- Perhaps useful to establish what is presently acceptable use (observations of the committee?)
				- These documents may point towards, but are not definitive on the reasons behind planning authority's decision
			- There are a number of reasons to justify this position:
				- Granting a planning permission merely removes a legal bar rather than grants legal affirmation to an activity. This is especially clear when considering the Planning Act 2008 and Civil Aviation Act 1982, where there are specific statutory exclusions of claims in nuisance. 
				- Planning authorities have not been expressly granted the right to deprive a property owner's rights through granting planning permission, especially without providing adequate financial compensation. (This perhaps explains his position on the remedies for nuisance.)
					- Consideration that administrative decisions are often hard to challenge.
					- Consider s.1 Land Compensation Act 1973 which provides for compensation for certain types of nuisance caused by public works enjoying statutory immunity 
				- Planning permission may often be granted or denied based upon considerations of public interest, including political and economic issues that while valid, are insufficient in themselves to alter C's common law rights [95]
					- Consider example such as in _Barr v Biffa_, where planning permission is granted by an environmental protection authority
				- Interestingly, Lord Neuberger held that local authorities are entitled to disregard issues of common law rights (at least with regards to nuisance) and assume that the affected parties are able and willing to enforce those rights themselves. This perhaps heavily favours members of the higher social classes, especially considering the high cost of litigation.
		- Lord Carnwath takes the view in _Gillingham_ that planning permission, within reason and by itself, is sufficient to render innocent what would otherwise be acts of nuisance:
			- "exceptional cases" where planning permission is a "considered policy decision" leading to a "fundamental change in pattern of uses"
			- Planning permission may reflect the authority's view on acceptable use that would be helpful as a starting position in litigation
			- Planning officer's reports, insofar as his recommendations are adopted, often provide a "very good indication" of the planning authorities reasons for their decision.
			- However, Lord Carnwath's speech conspicuously avoids the issue of deprivation of rights without compensation. While he acknowledged that courts should be cautious to do so, he appears to take the view that where the planning permission causes a sufficiently fundament change in the pattern of uses, property owners may be deprived of their rights. This is highly contrary to fundamental values of the English legal system.
- Whether D's prior use of the land itself should be a factor when deciding what is reasonable use of land per _Coventry v Lawrence_
	- D may acquire a right to cause nuisance by prescription (enjoying the right for more than 20 years)
		- Prescription is based upon consent and acquiescence (_Sturges v Bridgman_), and therefore what is required is not just that D carried out the activity for 20 years, even if it had the same physical impact, but rather that D was allowed to perpetrate that specific nuisance for 20 years
	- Lord Neuberger's view is that D's prior activity may be taken into account insofar as it did not amount to nuisance in law.
		- He acknowledges the circularity since the issue to be determined by the court is whether an act was nuisance at [71] but considers that it presents no real problems:
			- In many cases, it is fairly clear whether D's activities amount to nuisance once the facts are established, and "precise identification of the locality or its character do not have to be addressed". What feels like a potential area of judicial intuition that contravenes the rule of law actually is actually completely satisfactory since his point is that the activities so clearly fall outside what could be reasonable use of land that the precise identification of reasonable use in that context is not necessary.
			- Where the precise nature of the locality is of importance, he appears to consider that this can be adequately resolved by looking at the accepted interference caused by similar activities in similar neighbourhoods elsewhere in the country at [71]
				- While this appears to satisfy the general principle in nuisance of an objective standard of reasonable use, it mayhap unreasonably assumes a relatively monolithic population when it comes to tolerance of alleged nuisance. Alternatively, it could give rise to the question of what factors should be taken into account when comparing with similar neighbourhoods and lead to an undesirable complication of the law in this area.
			- He considers this a solution that is better than the alternatives. (maybe the least undesirable test?)
		- C may nevertheless require D to take reasonable steps to reduce his interference with C's rights to land.
	- Lord Carnwath's view appears that D's conduct, even if it could amount to nuisance, should be taken into account if it has become a part of the established pattern of uses in the area. However, an intensification of activities leading to an increase in the interference to C's right to land could amount to an action in nuisance for that increase.
		- While intuitively attractive, this presents a clear problem in that it undermines the doctrine of prescription. It allows D to acquire a right against C (likely an easement) despite not satisfying the requirements for prescription through judicial discretion.

####Public Benefit

- Public benefit is generally not a relevant factor in considering what is "reasonable use" of land _Bramwell v Turnley_
	- Individual property rights should not be extinguished for the public benefit without compensation (see _Coventry v Lawrence_)

####Sensitivity of C's Use

- Where C's use of land is abnormally sensitive, D will not be liable for nuisance to the extent that it interferes with C's exceptional use of land. D remains liable for any interference with normal use and enjoyment of land _Robinson v Kilvert_
- An approach based on reasonable foreseeability appears to be taken in _Network Rail v Morris_
	- The EWCA in considering what is reasonable conduct appeared to take the view that the reasonable foreseeability of interference should be a factor. 
	- However, the correct interpretation of this judgement should be that reasonable foreseeability of interference provides an absolute minimum for establishing liability for nuisance. Where D could not reasonably foresee interference with C's rights as a neighbour (with no special knowledge), he would most definitely satisfy the reasonable use test. This case should not be used to suggest a reasonable foreseeability test for nuisance. As long as D's conduct would otherwise be a reasonable use of his land, knowledge of C's exceptional employment of land should not be sufficient to interfere with his rights.

####Malice
- Malice in D's conduct could be a factor in considering what is reasonable use of his land _Christie v Davey_
- Case law is not completely unanimous on this issue, and in _Bradford Corporation v Pickles_, Lord Halsbury LC held that one had the right to perform lawful acts on his premises however malicious, and vice versa.
	- This appears to beg the question since the issue was whether malice affets the lawfulness of conduct.
- The idea of relative and absolute rights was introduced in _Allen v Flood_ and _Hollywood Silver Fox Farm v Emmett_, where it was held that no person had an absolute right to creat noises on his own land. 
	- Winfield & Jolowicz suggests that where absolute rights are concerned, malice had no bearing on the matter. However, in the case of relativie rights to emnate noxious sounds/substances onto another's land, the question of what is reasonable use could depend on the presence or absence of malice.
		- However, IMO it is clear that malice should not be conclusive on this matter, but only one of the many issues to be considered. Where conduct is clearly reasonable, malice should not render it unlawful. Consider the case of an individual practising on a musical instrument: If he limits his sessions to reasonable periods of time and takes reasonable steps to limit the sounds he create, it is irrelevant that he practises every day primarily to annoy his neighbour. 

####Coming to the Nuisance

- The law that coming to the nuisance is generally not a defence in law was first discussed in _Miller v Jackson_ 
- This was conclusively established in _Coventry v Lawrence_ by in the case where "the claimant in nuisance uses her property for essentially the same purpose as that for which it has been used by her predecessors since before the alleged nuisance started"

####When is it Actionable?

- When physical encroachment or damage is suffered by the claimant
	- Nature of locality is irrelevant in the case of actual damage _St Helen's Smelting v Tipping_
	- Damage must be more than de minimis _Mitchell v Darley Main Colliery Co_
- When there is an unlawful interference with the comfort and convenience of C's land (loss of amenity)
	- It is necessary to consider the extent of interference (under the reasonable use principle) _Barr v Biffa_

####Negligence and Fault

- Active nuisance (perpetrated by the defendant) is based completely upon the concept of reasonable use _Cambridge Water_
	- Where the use of land is not reasonable, it is irrelevant whether D took all reasonable care and skill to avoid nuisance
	- At least where damages (for past nuisance) is concerned, D will only be held liable for reasonably forseeable harm.
		- View of Carnwath LJ in _Barr v Biffa_ considered that the question was what objectively a normal person would feel reasonable to have put up with. (mirror of the D's liability?)
		- Alternative view that principles of damages in negligence apply
- Passive nuisance (perpetrated by third-parties or acts of nature)
	- Third-parties
		- D would be liable if he continues or adopts the nuisance_Sedleigh-Denfield v O'Callaghan_
			- Continues: D did or should have known of nuisance but fails to take reasonable steps to bring it to an end
			- Adopts: D makes use of the object or activity which constitutes the nuisance
		- Although the language used is that of "nuisance", it appears that potential hazards or potential sources of nuisance were also included. It is therefore clear that in cases of isolated incidents, a claim in nuisance arises upon the first occasion. _British Celanese Ltd v Hunt_
	- Acts of god/nature
		- _Goldman v Hargrave_ extended _Sedleigh-Denfield_ principles to acts of nature
		- If D knew or should have known about potential/actual hazard, he is under a duty of care to take reasonable steps to minimise/remove the hazard. 
		- Circumstances such as the resources of D are to be taken into account when decided if he had taken reasonable steps to mitigate the hazard _Leakey v National Trust_
		- D is only liable for foreseeable damage, and is not expected to act as if he possessed expert knowledge or expertise _Holbeck Hall Hotel Ltd v Scarborough Borough Council_
	- There is authority at the highest level that standards of liability in private nuisance apply principles of negligence where the nuisance was not actively brought about by D _Cambridge water_ and _Goldman v Hargrave_

###Remedies

- The law used to be that injunctions were to be the default remedy, but the court had powers to award damages under s. 50 Supreme Court Act 1981
	- _Shelfer v City of London Electric Lighting Company_ provided a fourfold test to decide whether damages were to be awarded in lieu of an injunction:
		1. Injury to C's rights was small
		1. Injury was capable of being estimated in money
		1. Injury can be adequately compensated by a small money payment
		1. It would be oppressive against D to grant an injunction
- _Coventry v Lawrence_ changed the law in this area:
	- While the default position remains to be the grant of an injunction, courts were granted more flexibilty to award damages in lieu of it
		- Courts were to have unfettered discretion, and the Shelfer test, while helpful, does not bind the court to act either way.
		- Public interest/benefit is a valid factor in considering whether to award damages
			- Planning permission could be highly pursuasive as to the public interest/benefit of D's conduct
		- Lord Sumption suggested that damages are ordinarily an adequate remedy
- Quantification of damages
	- Damages compensate for harm to land: it does not matter how many people were in actual occupation _Hunter v Canary Wharf_
		- However, it is inevitable that the actual use of land plays an important role in quantifying damages _Dobson v Thames Water Utilities Ltd_
	- Damages were to be quantified not just based on consequential economic loss. _Coventry v Lawrence_
		- It may sometimes also be appropriate to award damages for C's loss of the ability to enforce his rights. 
		- It might be appropriate to consider D's benefit from the conduct, since D's profits would be highly relevant to the sum he would otherwise be willing to pay to secure the right to continue his conduct _Jaggard_

##The Rule in _Rylands v Fletcher_

###Doctrine

- The orthodox view is that this is a special sub-category of private nuisance and not a distinct tort _Cambridge Water_
- Less orthodox view that it goes beyond nuisance to establish strict liability for harmful results of abnormal harm
	- Advanced by certain academic writers and in some US jurisdictions
	- Rejected by Lord Goff in _Cambridge Water_
- Issues arise from strict liability and the more narrow scope of application

###Elements

- Accumulation of dangerous thing 
	- Even people could be included _AG v Corke_
	- No special version applied to spread of fire; thing must be dangerous when brought upon land _Stannard v Gore_
- Amounted to a non-natural use of land
	- Original formulation in _Rylands v Fletcher_ that the dangerous thing must be "non-natural"
	- Dangerous thing has to bring increased danger to others and must not be merely ordinary use or for the benefit of the community _Rickards v Lothian_
		- Original formulation meant that almost all industrial activity provided some kind of general benefit of the community
		- The UKHL in _Cambridge Water_ moved away from the general benefit of the community concern in light of the development of reasonable foreseeability of damage requirement.
		- Lord Goff in _Cambridge Water_: Storage of chemicals on land was classic case of non-natural use
	- Restrictive approach reaffirmed that use must introduce extraordinary risk/use that imposes a high treshold _Transco plc v Stockport Metropolitan Borough Council_
		- Took a stricter approach than _Cambridge Water_
- Escape of dangerous thing
	- Dangerous thing must escape from a place where the defendant had occupation of or control over to a place in which he did not _Read v J. Lyons & Company Ltd_
	- Liability for fire that escapes from D's land was not strict; some element of negligence might be needed due to the presence of a statutory defence _Stannard v Gore_
- Damage caused is reasonably foreseeable _Cambridge Water_
	- Escape need not be reasonably foreseeable

###Defences

- Acts of god _Rickards v Lothian_
	- Damaged caused directly by natural causes without human intervention in circumstances in which no human foresight can provide against and of which human prudence is not bound to recognise the possibility _Tennet v Earl of Glasgow_
- Deliberate act of third party _Rickards v Lothian_
	- A stranger was one over whom the occupier had no control such that it would be wrong to see the conduct of occupiers with D's consent as the cause _Perry v Kendricks Transport Ltd_
	- Act might not amount to a defence if it was reasonably foreseeable that it would happen
		- This is highly similar to actions in negligence

###Miscellaneous

- Possessory or proprietary interest in land is required to have standing to sue _Transco_
	- Beneficary of an easement may also have a right (obiter)
- _Rylands v Fletcher_ liability is for damge to land and therefore private nuisance rules for damages apply _Transco_
