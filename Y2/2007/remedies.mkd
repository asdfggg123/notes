#Remedies

##Compensatory Damages

###Introduction

- Seeks to reflect the value of the recoverable loss that C has suffered _Livingstone v Raywards Coal Co_
- Generally focuses on the consequences of the acts 
- Varuhas points out that damages in tort also reflects the interests of C:
	- Award is normative, and seeks to reflect that C's interest (integrity of body for battery) has been interfered with

###Actions in Tort

- By living C
- By administrator of C's estate suing in C's name: s 1(2)Law Reform (MP) Act 1924
- By living dependants of deceased suiing their own names: Fatal Accidents Act 1976

###Damages for Personal Injury

- Pecuniary loss (financial): e.g. loss of earnings, cost of medical treatment
- Non-pecuniary loss (non-financial): e.g. pain, suffering

####Pecuniary Loss

- Pre-trial pecuniary loss is recoverable in full
- Lump sum payment for future pecuniary loss
	- The multiplier method: number of years of loss will continue for x annual loss (after deductions) C will suffer
	- Complicating factors
		- Vicissitudes of life: Take account for the fact that severe misfortune (natural disasters/medical illness) may terminate D's liability by applying statistics to discount the claim. 
			- Where "vicissitude" is certain, there will be no need for statistical accounting _Jobling v Associated Diaries_ 
				- C had been injured in accident at work, and the injury would be suffered for 10 years. D admits liability for injury. C had condition that would in any event have forced him to stop working before then. UKHL held that loss of income can only be until the date when C would have to stop working otherwise
		- Effects of time
			- Acceleration element: awarding lump sum may generate income and therefore cause overcompensation
			- Inflation: treatment may cost more with time. Present annual loss might be less than future annual loss in absolte terms.
			- UKHL in _Wells v Wells_ held that court should assume investment in index-linked government security. Assume low-risk responsible investment.
			- Assumed rate of return of 2.5% per s. 1 Damages Act 1996
		- Where C suffers a lost of life expectancy, damages and awards shall also include the income that C would have received less what he would have spent on himself _Pickett v British Rail Engineering_
			- "Wages in heaven"
- Periodical payments
	- Lump sum payments are generally not employed sensibly by C
	- Previously, periodical payments were only available where D consents to it. UKHL in  _Wells v Wells_ suggested that amendment in this area of law was desirable
	- This amendment was made under s. 2 Damages Act 1996
		- Only applies to personal injury damages. Excludes torts such as:
			- Property damage
			- PEL
			- Defamation
			- Nuisance
		- S. 2(1) Court may order periodic payments for future pecuniary loss
			- No need for parties' consent
			- Courts shall consider: scale of payment, preferences of parties
			- C's preference was not decisive _Morton v Portal_
		- S. 2(2) Court may order periodic payments for other damages with parties' consent
		- S. 3 Court can only award periodic payments if continuity of payments is reasonably secure
		- S. 4 gives court criteria to assess whether payment is reasonably secure
			- Payments by public bodies are to be regarded as reasonably secure
			- Payments by way of protected annuity are regarded as reasonably secure

###Non-Pecuniary Loss

- Pain and suffering - subjective assessment as to suffering
	- Coma/unconsciousness - low quantum
- Loss of amenity - objective assessment on the degree of deprivation, although with regards to C's lifestyle
	- Value of lossed liberties
- Quantum of payments are generally informed by:
	- Reports of quandum of damages awarded in recent law (Kemp & Kemp on Damages)
	- Judicial studies board guideline
	- In _Heil v Rankin_, EWCA was confronted of argument that awards no longer reflected the value people place on losses suffered. EWCA held that personal non-pecuniary losses will have a one-off increase in order to better reflect the public valuation of such injuries. EWCA said that it shouldn't be necessary to engage in this exercise again.
		- Northern Ireland personal injuries cases generally awarded higher damages
		- Law Commission conducted a survey

###Aggravated Damages

- Reflect injuries to C's feelings/dignity
	- Compensatory in nature _Rowlandson_
	- Law Commission (1997) suggested that aggravated damages should be purely compensatory (no more punitive element). Subsequently, the government said that there was sufficient clarity in the law on this point. 
- E.g. in defamation where D persists until date of trial in asserting truth of statement but rescinding that assertion at trial: C had to suffer hurt to feelings/dignity for an unnecessarily long time
- In _Rowlandson v Merseyside Police_
	- C complained to police about noisy neighbours. Constable attended to the scene and arrests C, charging her with obstruction of a police officer in the performance of his duty. This was all a fabrication and D was abusing his powers. C suffers psychiatric illness and sues for both battery and false imprisonment. Court awarded additional �6000 for injury to pride and dignity.
- In _Choudhary v Martins_, court confirms that aggravated damages are available for harassment. Court also said that it is better to award a larger quantum of damage rather than grant a separate award of aggravated damages.
- In _Kralj v McGrath_, court held that aggravated damages should not be awarded for negligience.

###Death and Damages

- Originally, in common law, the claim died with the person (either party)
- C's estate may claim limited damages under Law Reform (MP) Act 1934
	- S. 1 provides that all causes of action that exist on the date of C's death continue for the befit of his estate.
		- Does not apply to defamation
	- S. 1(2) provides that such claims will not include exemplary damages and expected future earnings
	- In _Hicks v South Yorkshire Police_, court held that only a small award could be made: no earnings were lost, pain and suffering were minimal
		- Case concerned the Hillsborough disaster, and deceased claimants' death had been quick and had come about after quickly becoming unconscious.
- Dependants may claim for loss of dependany under Fatal Accidents Act 1976
	- S. 1(3) gives a list of people who can claim - close family members
	- S. 3 allows award of damages that are proportioned to the injury resulting from the death
		- "reasonable expectation of pecuniary benefit, as of right or otherwise, from the continuance of life" _Franklin v South Eastern Railway
	- S. 4 provides that any benefits that may have accrued upon death must be disregarded
- Claim for bereavement is available under s. 1A Fatal Accidents Act 1976
	- Only available to very limited people
	- Fixed sum of �11800

##Non-Compensatory Damages

- Nominal damages
	- Where tort is actionable per se and no special loss has been suffered: see Varuhas argument
- Comtemptuous damages
	- Reduced damages where C's conduct in bringing the case merits only very low damages
	- In _Grobbelaar v News Group Newspapers Ltd_, C was a football player who accepted money from investigative journalist for conceding gials. UKHL accepted that it could not be proved that he conceded goals as a result of the bribe, but nevertheless reduced damages to �1 because C has already destroyed his reputation by taking the bribe. It would be an affront to justice to allow him to recover the full amount. 
- Exemplary damages
	- Punitive damages
	- _Rookes v Barnard_, UKHL expressed concern that civil law of tort should not have so heavy a punitive element. That is the role of the criminal law. However, it was too entrenched to abolish. UKHL restricted exemplary damages to cases:
		- Oppressive or high-handed conduct by agents of government e.g. police or armed forces
			- Does not apply to private individuals or corporations
			- What about agents of the government who do not bring to bear the force of the state? Civil servants? Construction of "government" has been of much litigation in EU law _Marshall (No. 2)_
		- D has calculated that he would profit from the tort even after paying compensatory damages. 
			- In _Cassell v Broome_, a publisher decided to persist in publishing defamatory allegations because they expect significant profits that would outweigh any compensation paid
			- In _Kuddus v CC Leicestershire_, Lord Nicholls and Lord Scott queried whether this was still necessary as restitutionary claims are more available nowadays. To answer this question, it is necessary to decide the role of exemplary damages, whether its aim is to deter unlawful conduct, or to punish it. If deterrance is the aim, the next question is whether disgorgement is a sufficient deterrence, and if its aim is to punish, is the criminal law a more suitable means?
		- Statute provide for exemplary damages
		- Important considerations per Lord Devlin
			- C is victim of D's punishable behaviour
			- Examplary damages are a weapon that can be used against liberty. Some awards granted by juries amount to greater punishment than would be likely incurred if conduct was criminal. Therefore they must be awarded with restraint.
			- The means of the parties, irrelevant in the assessment of compensation, are material in the assessment of exemplary damages.
	- UKHL in _Kuddus v Chief Constable of Leicestershire_ held that _Rookes v Barnard_ did not restrict exemplary damages to those torts that exemplary damages have been awarded for prior to 1963
- Evaluation
	- Ultimately, one needs to determine the purpose of imposing exemplary damages in civil law. 
		- Lord Reid in _Cassel v Broome_ argues that it is an anomaly in the law:
			- There is no definition of the offence specific enough to form the subject of criminal offences
			- There is no limit to the punishment except that it must not be unreasonable
			- The quantum may be awarded by a jury (influenced by emotion)
			- Even where D needs to be punished, why should C benefit from this punishment? Should that not be the role of aggravated damages?
		- Lord Scott in _Kuddus_ also criticises this element of the law:
			- Aggravated damages should be preferred.
			- Where D is not the wrongdoer, and the damages are met out of public funds, the deterrent effect is limited unless it is to be achieved by encouraging better institutional practices (weak argument)
		- In general:
			- Why should the civil law impose sanctions when the criminal law, the area of law responsible for punishing wrongdoing, does not?
			- Exemplary damages for vicarious liability might be questionable? _Rowlands v CC Merseyside_
			- Can one be subject to both civil and criminal sanctions for the same act? Compare with _Riggs v Palmer_
		- Reform (Law Commission) (not adopted)
			- Punitive damages only available where D deliberately and outrageously disregarded the claimant's rights
			- Punitive damages should be decided by the judge
			- Punitive damages should be a last resort
- Gain-based damages for nuisance? Suggested in _Coventry v Lawrence_ but no conclusive ruling
	- Disgorge all profits? Unjust enrichment?
	- Hypothetical licensing fee?
		- License fee by definition must be less than all profits
	- Which approach depends on the objective to be achieved
		- Discourage individuals from disregarding others' rights where it is profitable to do so
	- Should C be allowed to choose?

##Injunction

- Equitable remedy to compel conduct
	- Not availabe as of right
- Types of injunctions
	- Final/interim
	- Mandatory/prohibitory
- Principles
	- C must come to equity with clean hands 
	- Delay and acquiescence may preclude an injunction (doctrine of laches)
	- Equity will not act in vain
	- Public interest: see _Coventry v Lawrence_
- Special rule for defamation
	- C may not stop a defamatory publication with an injunction where there is any doubt over whether words are actual defamatory, or if D indicates they intend to rely on defence of truth _Bonnard v Perryman_
