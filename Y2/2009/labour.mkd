#Freedom of Movement of Labour

##General

-	Basic provisions in Art 45 TFEU
	-	Concerns interstate movement of worker for economic purposes
		-	Subsequent developement removed the link between interstate movement, and economic activity i.e. interstate movement of economically active EU citizen will be sufficient as in _Hendrix_. 
			- This is the interpretation put forward by Tryfonidou
			- ECJ is re-reading Art 45 in light of EU Citizenship
			- Preference for Art 45 is due to the stronger protections available
			- Possible injustice arising out of reverse discrimmination: Only those sufficiently affluent to move their place of residence may enjoy the protections of EU law
			- Is it more appropriate to disregard the link traditionally required between the objective of a provision and the situation at hand? See also EU Citizenship case law.
		-	In _Ritter-Coulais_, a frontier worker, who was employed in her state of origin but lived in another MS was held to be protected under Art 45. This is an example of "reverse frontier workers"
	-	Art 45 applies where work was done outside the Community if the legal relationship of employment was entered within the Community _Walrave and Koch v Assiociation Union Cycliste Internationale_
	-	Art 45 applies where employment was entered into and primarily performed in a non-member country to all aspects of the employment relationship which were governed by the law of the employing Member State _Boukhalfa v BRD_
		-	Note that employer in this case was the State, and not a private entity
	-	Art 45 rights are horizontally applicable _Angonese v Cass di Riparmio di Bolzano SpA_
	-	Art 45 can also be relied on by employer (_CPM Meeusen v Hoofddirectie van de Informatie Beheer Groep_) or relevant third party (_Clean Car Autoservice GMBH v Landeshauptmann von Wien_)
-	Art 46 TFEU provides for EP and Council to adopt secondary legislation to give effect to Art 45
	-	Many of the abovementioned legislation were consolidated by Directive 2004/38
- Art 18 TFEU is the anti-discrimminination provision
-	General purpose based approach similar to other aspects of EU law
-	Objectives of free movement law
	- Optimal allocation of resources
	- Foster closer union of European people
	- Increase integration
	- Improvement of living conditions _Levin_
	- Fundamental right under Art 45 EU Charter
- Data:
	- ~7.5% of UK population is immigrants
	- Immigrants contribute 12% more than they take out -Dustman and Frattini

##Scope of Protection (Definition of "worker")
	
-	Definition is a matter of EU and not national law _Hoekstra v Bestuur der Bedriffsvereniging voor Detailhandel en Ambachten_
-	General definition given in _Meeusen_
>	Any person who pursues activities which are effective and genuine, to the exclusion of activities on such a small scale as to be regarded as purely marginal and ancillary, must be regarded as a "worker".
	-	Relationship of subordination _Jany v Staatssecretaris van Justitie_
-	Minimum income and working time requirements
	-	Rules on free movement of persons are fundamental and must be broadly interpreted _Levin v Staatssecretaris van Justitie_
		-	Creation of single labour market for the benefit of Member States' economies
		-	Right for the worker to raise standard of living, even if minimum subsistence level of income is not reached
	-	Purpose or motive of the worker is immaterial _Levin_
	-	Open to "workers" to supplement their income from sources other than their employment _Levin_
	-	Claiming social assistance from public funds does not give rise to presumption against "worker" status _Kempf v Staatsecretaris van Justitie_
	-	Irregular income is no obstacle to attaining "worker" status _R v Ministry of Agriculture, Fisheries and Food, Ex p Agegate Ltd_
	-	Preparatory activities for employment (teaching) could still allow "worker" status for treaty purposes _Lawrie-Blum v Land Baden-Wuttemberg_
	>	That concept must be defined in accordance with objective criteria which distinguish the employment relationship by reference to the rights and duties of the persons concerned. The essential feature of an employment relationship, however, is that for a certain period of time a person __performs services__ for and __under the direction of another person__ in return for which he __receives renumeration__.
	-	Work that was "unpaid" in conventional terms does not mean that it was not effective economic activity. _Steymann v Staatsecretaris van Justitie_
		-	Services of (economic) value must be provided
	-	AG Slynn on social benefit shopping:
		-	Valid issue of potential social benefit shopping. However, consider:
		-	Increasing dependence on part-time work in times of general and personal economic difficulty
		-	Exclusion of part-time work would exclude women, elderly, disabled who might wish to only work part time, as well as those seeking full-time employment but were obliged to accept part-time work.
		-	States could regulate social benefits for part-time workers but could not exclude them from EU "worker" status.
-	Purpose of employment
	-	General rule that purpose for employment is irrelevant
	-	Rehabilitation and social reintegration exception
		-	Work cannot be regarded as effective and genuine economic activity if it were "merely a means of rehabilitation or reintegration" _Bettray v Staatssecretaris van Justitie_
			-	Activities for "employees" were chosen based on their capabilities in order to maintain, re-establish or develop their capacity for (normal) work.
			-	Activities were pursued in the framework of policy created solely for that purpose by public authorities
			-	In _Bettray_, appicant was unable for an indefinite period, on account of his drug addiction, to work under normal conditions.
		-	Low pay from public funds and low productivity of the worker do not prevent application of Art 45 _Bettray_
		-	National court must ascertain whether services performed are capable of being regarded as "forming part of the normal labour market." _Trojani_
	-	Education preparation exception
		-	Work undertaken purely in order to prepare for the course of study, rather than an occupation or employment, would not entitle one to the full benefits accorded to Art 45 "workers" _Brown v Secretary of State for Scotland_
			-	Employment was merely "ancillary" to his desired course of study
			-	Compare with _Lawrie Blum_
			-	See also _Bidar_
	-	"Objective" factors such as hours worked and remuneration should be given more weight over more subjective factors _Ninni-Oraschi v Bundesminister fur Wissenschaft, Verkehr and Kunst_
		-	Conduct before and after employment are not relevant in establishing "worker" status
		-	Relative length of stay and employment is not a relevant factor
		-	National court may consider abovementioned factors when deliberating national social assistance, but EU "worker" status is unaffected and hence the individual may enjoy the protection of EU law.
-	Job-seeker
	-	Right of individuals to "look for or pursue an occupation" _Royer_
	-	Individuals actively seeking work do not have full "worker" status, but are nonetheless covered by Art 45 _R v Immigration Appeal Tribunal, ex p Antonissen_
		-	Literal interpretation of Art 45 would undermine the free movement of workers
		-	Rights expressly enumerated in Art 45 are not exhaustive
			- Note the approach here compared with Art 36 TFEU. Expansive reading of rights, narrow reading of derogations.
	-	Member states retain the power to expel a job-seeker who does not have prospects of finding work after a reasonable period of time. _Antonissen_
		-	"Six months [...] does not appear in principle to be insufficient"
		-	If an individual can provide evidence that he is continuing to seek employment and that he has genuine chances of being engaged, he cannot be required to leave the territory of the host Member State.
	-	Social and tax advantages guaranteed to workers were not available to those moving in search of work _Lebon_
		-	However, under EU Citizenship, job-seekers would be entitled to equal access to social assistance for those seeking work _Collins_
		-	_Collins_ was confirmed in _Vatsouras_, holding that benefits for those seeking work fall outside "social assistance" for the purposes of Art 24(2) Dir 2004/38
	-	Job-seekers can only benefit from provisions concerning access to employment _Collins_
-	New member state exception
	-	Transitional scheme for new member states delaying the full implementation of their rights ended on 31 Dec 2013

##Protections

###Discrimmination

-	Direct effect: vertical and sometimes horizontal
	-	Labour market rules (even when not public body) _Bosman_
	-	Private employers cannot discrimminate _Angonese_
-	Direct discrimmination
	-	Direct discrimmination on grounds of nationality is rare and difficult to justify _Bosman (n 5)_
-	Indirect discrimmination
	-	Domestic laws operate a distinction between certain categories of workers, which is not nationality, but is "intrinsically liable" to affect migrant workers more than nationals. _O'Flynn v Adjudication Officer_
		-	No need to prove that it actually affected a higher proportion of foreign workers
		-	Language, residence, place-of-education requirements
		-	Double-burden qualification requirements
		-	Conditions more easily satisfied by nationals than non-nationals
	-	Conditions of linguistic knowledge are allowed if necessary and proportionate (such as for teachers) - Art 3 Reg 492/2011 _Groener v Minister for Education_
	-	Consider Art 7(2) Reg 492/2011: Workers should enjoy same social and tax advantages as national workers.
		-	Art 9(1) guarantees same housing benefits
		-	PM proposes residency requirements for tax, child, and social housing benefits
	- See _Kalliope Sch�ning-Kougebetopoulou (Greek doctor case wrongly interpreted in Kobler)_:
		- Worker could not factor in employment in Germany for purposes of calculating salary
			- "Manifestly work to the detriment of migrant workers"
		- Justification of rewarding employee loyalty was unjustified
	- In _Ritter-Coulais_, the ECJ reiterated that measures may not place Community nationals at a disadvantage when pursuing economic activities in another MS.
		- Case concerned negative income of homes outside Germany
		- Non-nationals are more likely to own a home outside Germany, therefore more likely to be affected
-	Access to employment market
	-	Excessive obstacles to freedom of movement will contravene Art 45
		-	"Directly affect [...] access to employment [...], capable of impeding freedom of movement for workers" _Bosman_
			- Preclude or deter a MS national from leaving his country of origin
			- Cannot apply FMG analysis: no "selling arrangements" exception
		-	No discrimmination is necessary, as long as there is an obstacle not sufficiently justified on public-interest grounds (Daniele, _Non-Discriminatory Restrictions to the Free Movement of Persons_)
	- In Terhoeve, ECJ ruled that MS may not require workers who have transferred residency between MS for the purposes of taking up employment to make higher than otherwise social security contributions.
	-	Where impact is too "uncertain and indirect", national policy does not contravene Art 45 _Volker Graf v Filzmoser Mashinenbau GmbH_
		- This case concerned compensation on termination of employment, which was not available where the worker voluntarily ended the employment to take up employment elsewhere.
		- Graf argued that this would discourage him from taking up employment elsewhere, but this was rejected by the ECJ.
		- ECJ reiterated _Bosman_ market access principle.
	- Ultimately, it should be noted that an "access to employment market" approach is intended to remedy the problem that Art 45 only prohibits discrimmination on grounds of nationality. When a national seeks to contest the validity of rules in his own MS, it is impossible to find that he has been discrimminated against on the basis of his nationality. In this case, perhaps the preferred approach is to consider whether one is discrimminated against on the basis that he has exercised his EU law rights. 

###Purely internal situations

-	Wholly internal situations, where all relevant elements are purely internal to a Member State, remain outside the ambit of treaty terms in principle _Saunders_
-	A Community national who has exercised her Community rights as a worker would fall under the treaty provisions even when he returns to his country of origin _Terhoeve (n 63)_
	- Perhaps what is required is not just an exercise of free movement rights, the case itself must concern the exercise of that right.
	- However, see EU citizenship cases

###Objective Justifications

-	Where there are material differences in the positions of residents and non-residents, indirect discrimmination may be justifiable
	-	Differences between the territories from which income is primarily derived may justify discrimminatory tax allowance policies, but are impermissible where the non-resident could not benefit from tax allowances in his Member State of residence either _Finanzamt Koln-Altstadt v Roland Schumacker_
	-	Purely sport related concerns may justify differences in the administration of professional sporting organisations and competitions _Lehtonen_
	-	It appears that objective justifications cannot merely be for economic or practical reasons that mainly benefit the MS 
		- Simplyfying and coordinating the levying of social security contributions and technical difficulties were rejected in _Terhoeve_
		- Reducing financial burden on national social security scheme is not a valid justification _Rockler_
		- Need for fiscal coherence was rejected in _Ritter-Coulais_

##Public-service exception

-	Exception provided for by Art 45(4)
>	The provisions of this Article shall not apply to employment in the public service.
-	"Public service" is defined by the court _Sotgiu_
	-	Member States cannot deem a post to be "in the public service" by the name or designation of the post, or the fact that it is regulated by public law.
	-	Public-service positions must involve "a specific bond of allegiance and mutuality of rights and duties between state and employee" and both of the following are required: _Commission v Belgium_
		-	Participation in the exercise of powers conferred by public law
		-	Duties designed to safeguard the general interests of the state
			- Both of the two conditions above are required
		-	Special relationship of allegiance and reciprocity of rights and duties which form the foundation of the bond of nationality
-	Exercise of public powers must be on a regular basis _Anker_
	-	Exception must apply to specific post, and not just general category of work
	-	Posts influence the conduct of private individuals by requiring their obediance or compelling them to comply - AG Mancini's opinion in _Commission v France_
	-	Functional rather than institutional approach
		- Irrelevant that job is in the "public sector"
		- Comparison to Art 51 TFEU rejected by ECJ
-	May justify discrimmination as to access, but not terms of employment within public service _Sotgiu_

##Entry and Expulsion

-	Refusal of entry or expulsion can only be justified on public policy, public security, and public health grounds _Art 27(2) Dir 2004/38_
	- Based on individual conduct and not previous criminal record
	- "Genuine, present and sufficiently serious threat to one of the fundamental interests of society"
	- Provisions for general prevention are not allowed _Calfa_
-	Threat to fundamental interest of society _Bouchereau_
-	There must be genuine and effective measures to combat the same domestic threat for it to be "sufficiently serious" _Adoui and Cornuaille_
	-	Principle of proportionality applies

__READ Case C-145/09 Tsakouridis__
