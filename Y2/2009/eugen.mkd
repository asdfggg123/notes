#EU General

- Four major categories of EU legislation:
	- Regulations
		- General application
		- Binding in its entirety and directly applicable
	- Directives
		- Applies only to member states to which it is addressed
		- Binding as to the result to be achieved, national authorities may determine form and methods
		- Not directly applicable
	- Decisions
		- Applies only to the party (nation/company) addressed
		- Binding in its entirety and directly applicable
	- Recommendations and Opinions
		- No binding force
- General principle of EU supremacy has not been set out explicitly in the treaty
	- Art I-6 Constitutional Treaty and Declaration 17 Treaty of Lisbon both state that EU law has primacy over national law
	- States have limited their sovereign rights in limited fields _Van Gend en Loos_
	- Member states have created a body of law which binds both their nationals and themselves. Law stemming from the treaty could not be overridden by domestic and legal provisions _Costa v ENEL_
	- Courts must set aside provisions of conflicting national law, whether prior or subsequent to the community rule _Simmenthal (No. 2)_
	- National court should suspend national legislation that __may__ be incompatible with EC law until a final determination on its compatibility has been made. _Factortame (No. 2)_

##Direct/Indirect Effect

###Treaty Articles

- Treaty articles have vertical direct effect _Van Gend en Loos_
	- Reasoning of the ECJ include:
		- Preamble and preliminary-ruling procedure suggest that citizens should have a role to play under the Treaties
		- Vision of the kind of legal community that the Treaties were designed to create
		- Strong enforcement was required to ensure member states complied
			- Since private individuals are the first to suffer loss in the breach of treaty articles, empowering them would enable better enforcement
		- 
- Initial conditions for direct effect were:
	- Provision must set out a negative obligation
	- Provision must be clear and unambiguous
		- Subsequently qualified to merely require it be sufficiently precise in _Becker v Finanzamt M�nster-Innenstadt_
		- Standard appears to be rather low, considering how the ECJ in _Dafrenne v Sabena No 2_ found "equal pay for equal work" to be a sufficiently precise principle for the court to uphold.
	- Provision must be unconditional, and its operation must not depend on further action or discretion by national or EU authorities _Molkerei-Zentrale Westfalen v Hauptzollamt Paderborn_
		- ECJ took a very loose approach in _Reyners v Belgium_ and allowed an article that seemed to envisage further implementing measures to have direct effect.
		- More stringent approach was taken in _Comitato di Coordinamento per la Difesa della Cava v Regione Lombardia_
			- Provision merely defined a framework and did not require the implementation of specific measures
- Treaty articles have horizontal direct effect _Dafrenne v Sabena_
- Key cases to remember are _Reyners v Belgium_ and _Dafrenne v Sabena_: court takes flexible approach depending on the issue at hand. Craig & de Burca consider the present situation as a Treaty Article will be accorded direct effect if it is intended to confer rights on individuals and it is sufficiently clear, precise and unconditional.

###Regulations

- Per Art 288 TFEU, a regulation "shall be binding in its entirety and directly applicable in all Member States"
	- Regulations immediately become part of the domestic law of Member States without needing transposition
- Regulations are directly effective both horizontally and vertically _Leonisio v Italian Ministry of Agriculture_ ("slaughtered cows case")
	- National implementing measures may not alter, obstruct or obscure the direct effect or nature of the regulation _Amsterdam Bulb_

###Decisions

- Decisions are binding and directly effective on the parties it is addressed to per Art 288 TFEU
- Both individuals and the Commission may invoke a decision against a party it is addressed to _Grad v Finanzamt Trautstein_

###Directives

- Direct effect of directives was first recognised by ECJ in _Van Duyn v Home Office_
	- Reasons in _Van Duyn_ were:
		- Directives are binding and will be better enforced if individuals can rely on them
		- Art 267 TFEU, which allows courts to refer questions on directives to the ECJ, implies that such acts can be invoked by individuals before national courts
	- Reason in _Ratti_:
		- Estoppel argument that Member States were precluded by their failure to implement a directive properly from refusing to recognise its binding effect where it was pleaded against them
- The general requirement for direct effect applies, except that discretion clearly cannot preclude the direct effect of directives.
	- It appears that what is necessary is for individuals to be given a sufficiently clear, precise and unconditional right under the directive. He may then invoke this right even when a MS exercises its discretion in implementing a directive per _Verbond van Nederlands Ondernemingen_
		- Affirmed in _Marks & Spencer_
- Additional requirement for directives to be directly effective is that their prescribed period for implementation had elapsed _Ratti_
	- During the period for transposition, member states must refrain from adopting measures "liable seriously to compromise the result prescribed" per _Inter-Environnement Wallonie ASBL_
	- It appears from _Mangold_ that national court must set aside provisions of national law which may conflict with Community law during this period as well.
- Directives are only vertically directly effective and may not be invoked between individuals _Marshall v Southampton and Sout-West AHA_
	- Article 288 specified that directives only bind those addressed	
		- Textual fidelity is surprising considering ECJ's loose approach to legislation in other cases.
			- It is also suggested that the language in Art 288 is only used to distinguish it from generally applicable directives and does not mean that individuals within the MS should not be bound 
	- Rule of law issue as directives were not required to be notified or published but this argument is obsolete after the Maastricht Treaty
	- ECJ argued that horizontal direct effect would erode the distinction between regulations and directives, since it would mean that directives can have legal impact even when they were not implemented. Criticisms of this argument are:
		- Directives still have vertical direct effect and therefore legal force without implementation
		- Distinctions still remain as to the presence of MS discretion as to form and methods of implementations, as well as the temporal difference (immediately effective vs transposition period)
	- Legal certainty issue:
		- Not clearly elaborated upon in _Marshall_
		- Not necessarily a major concern since direct effect requires sufficient certainty in the provisions
		- Indirect effect doctrine to sidestep _Marshall_ introduce greater uncertainty
	- ECJ in _Faccini Dori v Recreb_ directly confronted and denied horizontal direct effect
	- Criticised for creating unfairness between MS since private sector in infringing MS would be in an advantageous position.
	- Where benefits under a regulation are conditional on compliance with a directive, the directive may be horizontally applicable _Viamex_
- Broad conception of the state for the purposes of vertical direct effect per _Marshall_
	- In _Marshall_: Directive may be invoked against State, whether acting in capacity as employer or public authority
		- Unfairness between public and private sectors was dismissed as a product of the MS's own wrongdoing that could have been easily avoided.
	- Further guidance on what is an emanation of the state is given in _Foster v British Gas_
	> It follows from the foregoing that a body, whatever its legal form, which has been made responsible, pursuant to a measure adopted by the State, for providing a public service under the control of the State and has for that purpose special powers beyond those which result from the normal rules applicable in relations between individuals is included in any event among the bodies against which the provisions of a directive capable of having direct effect may be relied upon.
	- Noteably, in an Irish case of _Doughty v Rolls-Royce Plc_, it was held that mere service to the state was insufficient. The body must be providing a public service. In light of _Kampelmann v Landschaftsverband_, _Doughty v Rolls-Royce_ is likely wrongly decided since the ECJ held that the body needs to be either under the control of the state or have special powers beyond those of ordinary individuals, not both. _Rolls-Royce_ was fully owned by the State.
	- Appears from _Rieser v Asfinag_ that where a body has special powers to provide a public service, state control is not required.
	- Contrast this approach with the estoppel argument in _Marshall_, since commercial enterprises may be liable where there was some element of state participation or control even though they bear no responsibility for the state's failure to implement relevant directives.
- Vertical direct effect is applicable even where it may lead to adverse consequences for another individual _Delena Wells_ but it may not be invoked to create new obligations
- Incidental horizontal effect refers to when a directive is invoked in the course of litigation on other grounds so as to affect the outcome of the litigation.
	- Generally, the directive is invoked to disapply certain national legislation so that other pre-existing national law fills the gap (exclusionary effect). Parties will not be directly subjected to the obligations set out in the directive.
		- In _Unilever_, the ECJ specifically said that the unimplemented directive does not define the substantive scope of the legal rule on the basis of which the national court must decide the case before it.
	- _CIA Security International_ and _Unilever v Central Food_ were two cases where technical regulations were disapplied for being contrary to an unimplemented directive so as to subject one party to liability in tort and contract respectively.
	- In _Arcor v Germany_, the ECJ clearly considers that negative consequences suffered by third parties (including the removal of benefits) cannot be regarded as an obligation falling on a third party pursuant to the directives relied on. 
- The ECJ held that "general principles" of EU law could be horizontally directly applicable in _Mangold_ (affirmed in _K�c�kdeveci v Swedex GmbH & Co. KG_
	- Pre-existing principle of equal treatment was fleshed out by the relevant directive. Since the court was applying the principle and not the directive itself, it was not limited by the _Marshall_ judgement.
		- Principle may also be applied before the conclusion of the transposition period.
	- May be criticised on the grounds that it essentially goes against Art 288 TFEU by making the substance of the directive enforceable against individuals (instead of MS to whom it was addressed)
	- Question of legal certainty since it circumvents the transposition period and also it is unclear what are the relevant principles that could be uphold. There is also much incoherence in the ECJ's approach in _Marshall_ and _Mangold_, since the ECJ has not explained how the issues raised in _Marshall_ were resolved or inapplicable in _Mangold_. 
- Directives may also have indirect effect within MS due to requirements for harmonious interpretation that were first set out in _Von Colson_
	- Member states must interpret their national law in the light of the wording and purpose of any relevant directives _Von Colson_
	- Directives need not satisfy the specific justiciability criteria in _Van Gend en Loos_
	- This obligation only arises when the time limit for implementation expires _Adeneler_
		- This obligation continues even after a directive is transposed into national law _Marks & Spencer_
	- Obligation arises in both vertical (_Von Colson_) and horizontal (_Marleasing_) contexts
	- On the extent of the obligation on national courts, _Marleasing SA_ establishes that national legislation enacted both before and after the directive must be read in line with the directive as far as possible
		- Appears therefore that contra-legem interpretation is not necessary
			- ECJ is generally deferential to national courts on whether a harmonious interpretation is possible _Wagner-Miret_
			- It appears that contra-legem interpretation may be required as in _Dominguez_, where injuries sustained on the way to work could be included under a provision covering accidents at work. While not exactly contra-legem, it appears to set a very high requirement for flexible interpretation.
		- The interpretative obligation may arise even when domestic legislation has no specific connection to the directive
	- Obligation for harmonious interpretation is strong per _Pfeiffer_ and national courts must do whatever lies within its jurisdiction, having regard to the whole body of rules of naitonal law to ensure that a directive is effective
		- National courts are required, where possible, to decline its own jurisdiction conferred upon it by a term contravening a Directive: _Oceano Groupo_. However, national courts are not required to disapply national legislation that conflict with a Directive (what AG Saggio proposed)
	- Harmonious interpretation cannot result in the imposition or aggravation of criminal liability _Kolpinghuis Nigmegen_
	- Harmonious interpretation cannot lead to the imposition on an individual of an obligation laid down by a directive which has not been transposed _Arcaro_
		- Reaffirmed in _Kofoed_, therefore it is necessary to distinguish between an imposition of an obligation and some other legal disadvantage
		- AG Jacobs in _Centrosteel_ observed that a directive cannot, of itself, impose obgliations in the absence of proper implementation but national courts must nevertheless interpret national law, as far as possible, in light of the relevant directives and in so doing may impose or aggravate greater civil liabilities. 

##State Liability

###Introduction

- The ECJ in _Francovich_ established a cause of action for damages against a MS for failure to implement a directive. Conditions were:
	- Directive must confer rights for the benefit of individuals
	- Content of rights must be identifiable from directive
	- Causal link between the damages suffered and the breach
- _Factortame (No. 3)_ and _Brasserie du Precheur_ elaborated on the details of the action:
	- The conditions under which a MS would be liable depends on the nature of the breach but generally cannot differ from those governing the liability of the Community in similar situations
	- The cause of action requires:
		- Rule of law breached must be intended to confer rights on individuals
		- The breach must be sufficiently serious: the test is whether there has been a manifest and grave disregard of the limits of the discretion (see [56] in _Precheur_)
		> The factors which the competent court may take into consideration include the clarity and precision of the rule breached, the measure of discretion left by that rule to the national or Community authorities, whether the infringement and the damage caused was intentional or involuntary, whether any error of law was excusable or inexcusable, the fact that the position taken by a Community institution may have contributed towards the omission, and the adoption or retention of national measures or practices contrary to Community law
			- National rules on fault may be relevant but are not supplementary. They may not impose a higher standard of breach required for state liability. 
		- Direct causal link between breach and loss suffered (per national law principles)
	- General principles concerning the interaction between EU and national law apply
		- Equivalence
		- Effectiveness

###Sufficiently Serious Breach

- Where a state incorrectly implements a Directive, it opens itself up to the possibility of state liability, although not every incorrect implementation would attract per _ex parte British Telecoms_.
	- In this case, the Directive was unclear and capable of the bona fide interpretation given to it by the British authorities. The breach was therefore not sufficiently serious to attract compensatory liability.
- Concerning actions of administrative bodies, their mistakes are more likely to attract liability since they are generally accorded with less discretion than the legislature _ex parte Hedley Lomas_
	- Potentially one reason why liability was stricter on executive bodies is to ensure that national legislative bodies would not be easily found liable in their transpositions of EU law and thus discouraged from their transposition function
- _Dillenkofer_ establishes that the conditions were the same whether in _Francovich_ or subsequent cases concerning other issues. ECJ spefically states that non-implementation is clearly sufficiently serious to attract state liability for damages. 
- _Kobler_ establishes that judicial decisions (even of courts of final appeal) could also attract state liability for damages, but only "in exceptional situations where the court has manifestly infringed the applicable law".
	- _Kobler_ concerned a case where the final court failed to make a reference to the ECJ. Perhaps most other judicial errors would not be sufficient to attract liability.
	- ECJ also held that it was up to MS to decide which court should hear cases on state liability for judicial mistakes to address the issue that judicial hierarchy would be upset.
	- Importance of the effectiveness principle
	- Subsequently reinforced in _Traghetti del Mediterraneo_, where Italian legislation which sought to restrict state liability for the damage caused by a last instant court was condemned by the ECJ.
	- Possibly weakening of inter-judicial collaboration by challenging the jurisdiction of national supreme courts
- Breach of settled case law could attract liability _Fub v Stadt Halle_
	- Perhaps settled case law by definition restricts the relevant body's discretion and hence breach of it would constitute a sufficiently serious offence.

##Effectiveness and National Procedural Autonomy

###Actions for Enforcement of EU Law

- General principle requires that EU rights must be effectively protected per Art 47 EU Charter of Fundamental Rights
- In general, MS retain the right to determine the procedural conditions governing actions at law intended to ensure the protection of rights under EU law per _Rewe-Zentralfinanz v Landwirtschaftskammer_.
	- Such procedural conditions cannot be less favourable than those relating to similar actions of a domestic nature (equivalence)
	- Conditions and time-limits must not make it impossible in practice to for EU rights to be exercised (practical impossibility)
	- This principle also applies to state liability under _Francovich_ and _Brasserie du Pecheur_
- Practical impossibility condition was subsequently changed to virtually impossible or excessively difficult in _Peterbroeck_
	- In order to determine this point, courts must look at the role of the procedural provision within the national legal system and the specific circumstances of the case
	- Time limits on actions serve a useful purpose, and therefore reasonable time limits will be acceptable _Palmisani v INPS_
	- National courts may be required to consider EU law of their own motion where that is necessary to ensure the exercise of EU law rights is not excessively difficult _Oceano Groupo_ (Unfair Contract Terms Directive)
- Legal certainty may need to be compromised and national administrative bodies may need to reopen a decision that had become final following a national court ruling that misinterpreted EU law provisions without resort to preliminary references _Kempter_
	- While the ECJ in _Kapferer_ seemed to recognise the importance of res judicata in national judicial proceedings, this was nevertheless required to give way in _Lucchini_ and _Olimpiclub_.
- Access to judicial control is important for the protection of EU law rights and provisions of national law encouraging nationals to avoid recourse to legal remedies would deprive individuals of effective judicial protection _Commission v Greece_
- Whether national procedural rules sufficiently safeguard EU law rights and comply with the principle of equivalence is for national courts to decide.
	- Consider the procedure as a whole and its special features
	- Whether individual would incur additional costs or delay by comparison with another invoking national law
	- Burden on individual invoking national and EU law must be equivalent
- Parties could be expected to exercise due diligence to mitigate their losses, although such conduct must be reasonably expected. Specifically, national courts could not require parties to specifically contravene national law _Metallgesellschaft_

###Remedies for Breach of EU Law

- The general principle is that in the absence of EU rules to that effect, states are not required to provide remedies which would not be available under national law; _Rewe Handelgesellschaft Nord v Hauptzollamt Kiel (the "butter-buying cruises" case)_
	- Basis of this principle is that the Court cannot create Community law where none exists; this must be left to the Community's legislative organs - AG Warner in _Ferwerda_
	- This is a strong principle that was reaffirmed in _Unibet_, and the court emphasised that there is no requirement for MS to create new remedies even where a direct action is not available if the plaintiff's EU law rights may be protected indirectly. 
		- On the lack of a self-standing action to challenge the compatibility of national provisions with EU law, there were other domestic actions through which the EU law provision may be raised indirectly. 
- However, the ECJ has, on some occasions, insisted on the availability of specific remedies:
	- In _San Giorgio_, the ECJ held that the repayment of charges levied in breach of EU law must be available, this remedy flowing from the substantive provisions of the EU law in question
		- However even then, the courts emphasised the primary role of national legal systems in laying down the conditions to such a claim
	- In _Factortame (No. 1)_, national courts were required to grant an interim injunction against the Crown even though this remedy was unavailable in principle under national law.
		- It was up to national courts to specify the conditions under which interim relief should be granted, but a rule prohibiting such remedies is unacceptable
	- State liability under _Francovich_ is also clearly a novel remedy created under EU law, although this could also be argued to flow directly from the substantive provision of the EU Treaties.
- On the provision of remedies under national law:
	- Initial strict and interventionist approach:
		- In _Marshall (No. 2)_, national statutory limit on compensation prevented EU right to equal treatment from being effectively protected.
			- Substantive rule on limit to damages had to be disapplied
			- Jurisdictional rule on power to award interest also had to be disapplied
		- Similarly, in _Von Colson_, limiting compensation to nominal costs failed to provide effective protection for the same right. 
			- National remedies must be adequate and effective
		- In _Sagulo_, ECJ held that penalties for breach of administrative requirements governing EU residence permits by migrant workers must not be disproportionate to the offence and constitute an obstacle to the exercise of fundamental EU rights
			- Once again, the general principle of proportionality applies
			- Where EU law only provides for civil sanctions, member states may nevertheless impose criminal sanctions as long as they are effective, proportionate and dissuasive
	- Subsequent more deferential approach:
		- _Steenhorst-Neerings_ concerned limits on retrospective payment of disability benefit that she was denied due to sex discrimmination contrary to EU law, and the one year limit was held to have satisfied the conditions of equivalence and practical possibility, and served a legitimate purpose of "preserving financial balance".
			- Contrasts with an earlier harsh ruling in _Emmott_, although this difference could be explained by state wrongdoing in _Emmott_
			- Retreat from adequacy and effectiveness principlpe in _Marshall 2_
			- _Ex p Sutton_ suggests that the requirement for full or adequate compensation applies to compensation for loss or damage but not the receipt of social benefits
		- Distinction in _Sutton_ and _Marshall 2_ applied in _Metallgesellschaft & Hoechst_, where the ECJ held that the repayment of interest accruing from prematurely levied tax would have to be available in an action for restitution even though national courts argued that it might not be available by characterising the action as damages for breach.
			- This case was analogous to _Marshall 2_ and interest was an essential component of the claim.
	- It therefore appears that the key question is the nature of the rights invoked before national courts, and market-related rights are better protected. An alternative distinction proposed by Engstrom is that between remedial rights (damages, injunctions, compensation, access to court) and ancillary procedural rights (interest, fair time-limits, legal aid, evidentiary rules)
		- Possibly a distinction between the creation of new remedies in contrast to the removal of existing bars to remedies under national courts?

##Residual State Liability?

- Suggested in some cases like _Sutton_ that where national remedies are unavailable or unsatisfactory, there might nevertheless be a claim in state liability, although this is less preferred due to the onorous conditions of sufficiently severe breach and causation.
- ECJ in _Stockholm Lindopark_ rejected argument that availability of state liability is precluded by availability of action under direct effect. 
	- Obligation of nullifying the unlawful consequences of a breach could be satisfied by revoking an unlawfully granted planning permission, or granting compensation by those affected
	- National courts had the discretion on which remedy is more suitable
- ECJ in _Transportes Urbanos_ left open whether a requirement for exhaustion of domestic remedies before commencing an action for state liability is permitted. 
- Prechal argues that state liability is a residual remedy where domestic measures are insufficient and EU law may even require litigants to first rely on direct or indirect effect before seeking state compensation.
	- Dougan argues that ECJ's use of state liability as a "safety net" where domestic remedies were inadequate could contribute to their inadequacy.
- Syndal criticises the ad-hoc judicial lawmaking and argues for more considered political approach.
- Dougan argues that remedial harmonization is a task that the ECJ is unsuited for. ECJ has only been capable of negative harmonisation. 
- Harding argues that the lack of EU legislative activity on harmonising judicial remedies could reflect a clear political objection to such EU measures, or reflect the difficulty, complexity, and ambition of the task. Both possibilities are good reasons why the ECJ is inherently unsuited to this task.

#Academic Opinion

##Paul Craig on Direct Effect

- Textual argument in Marshall based on Art 288 TFEU is unconvincing. 
	- Within the context of Art 288, what is meant is that directives bind only those MS who are addressed in contrast with regulations which bind everyone. It does not answer the question of who, within that MS, is bound by directives.
- Legal certainty was a reason cited on multiple occasions, and it is not exactly clear what it refers to, but the various possibilities all do not pose problems.
	- If it is the idea that the provision must be sufficiently clear, precise and unconditional, this is the requirement for direct effect anyway so there is no reason why it should prevent horizontal effect.
	- If it means that the directive must be published, this is irrelevant following the treaty of Maastricht.
	- If it refers to the principle that parties should not be subject to criminal sanctions as a result of an unimplemented directive, this can be accomodated for by way of an exception (Kolpinghus)
	- If it means that parties will be in a situation where they are unsure whether to comply with pre-existing national law or an unimplemented directive:
		- Firstly, the doctrine of supremacy means that the directive should be followed. 
		- Secondly, the various measures adopted by the courts to give horizontal effect to directives significantly increase the uncertainty in this context
		- Furthermore, the horizontal direct effect of treaty articles and regulations make this argument rather weak.
		- Finally, in the sense of whether the legal framework is coherent and clear such that individuals may plan their lives with an understanding of the legal consequences of their actions, the present law is much more unsatisfactory than if horizontal direct effect of directives were recognised.
- Argument that granting horizontal direct effect to directive would erode the distinction between regulations and directives is unconvincing. 
	- The argument typically goes that it would allow directive to thereby have legal impact even though they had not been implemented in the MS, eroding the distinction.
	- However, insofar as they have force, both directive and regulations have legal effect vertically.
	- Directives continue to leave MS choice as to form and methods of implementation, and they only take effect after the period for transposition. 
- Broad conception of the state
	- This is problematic because if the textual argument is to be taken seriously, it should incline towards a narrow conception of the state whereby it only binds those government organs that have the requisite powers under national law to implement the directive. 
	- The estoppel argument in Ratti does not apply to any of the other state organs that have been found liable. 
	- Unusual conception of inverse vicarious liability. There is often no normative relationship between the public body held liable and the state that justifies finding the public body responsible for the failing of the state.
- Indirect effect through harmonious interpretation
	- Harmonious interpretation has generally been justified on the basis that it is an interpretative obligation and the immediate source of legal obligations is in national law rather than the directive.
	- This argument is formalistic in that the premise is that directives may not of itself bind individuals, but may do so through other legal mechanisms. (Somewhat meaningless?)
	- ECJ in _Arcaro_ reiterated that the interpretative obligation may not be such as to impose upon an individual an obligation laid down by a directive which has not been transposed. 
		- This statement, if taken seriously, would radically reduce the scope of MS courts' interpretative obligation.
		- AG Jacobs in _Centrosteel_ argued that Arcaro should be read in the context of the criminal proceedings and does not apply to purely civil proceedings. 
		- The fact that the judiciary in _Arcaro_ found it necessary to make this pronouncement is a testament to the uncertainty as to the extent of the interpretative obligation.
		- While the obligation takes effect through national law, it is EU law that determines the outcome of the case. Thus, there is rather little difference between harmonious interpretation and horizontal direct effect.
	- This interpretative obligation significantly undermines the strength of the legal certainty argument. 
		- Individuals face much greater difficulty and complexity when attempting to understand their legal position. 
			- Horizontal direct effect is rather straightforward since individuals may look directly to the directive and his obligation ends there. Furthermore, a directive will commonly have greater precision than a treaty article.
			- Incidental horizontal direct effect requires an understanding of the extent of conformity of national law with the directive. This is difficult because directive do not need to be sufficiently clear, precise and unconditional to have indirect effect. Furthermore, the ECJ in Pfeiffer and Dominguez has required the entire domestic legal system to be considered, which exacerbates the difficulty.
		- Indirect effect requires a further step of determining whether national law is capable of being read in conformity with the directive. To determine this with certainty is difficult even for an experienced jurist.
		- The cross-border element would mean that a private individual, in attempting to understand his legal position and obligations, would have to consider the entire legal system of each country he conducts business in that has not implemented the directive. 
		- Central facets of this doctrine remain uncertainty, as expemplified by the continuing debate and uncertainty as to the extent of the interpretive obligation in the non-criminal context. 
			- The distinction between the alteration or substitution effect of directives is irrelevant in practice insofar as they lead to the imposition of significant legal obligations on private parties.
			- The normative difference between alteration or substitution does not change the fact that EU law is determinative on the outcome of the case.
- Incidental effect
	- The burden on private firms as a result of incidental horizontal effect is in some respects greater than if directives had horizontal direct effect since the private individuals would suffer legal consequences that arise not out of its own wrongdoing but rather the member states' failings (See CIA Security)
		- Lack of legal self-determination
		- Where a directive contains a complex legal scheme, it is difficult for private individuals to appreciate their legal position because they may only rely upon national law insofar as it is not in conflict with community law.
		- If clear, precise and unconditional terms in a directive are incapable of horizontal effect, how is it justifiable for directives to give rise to second-order legal effects in the context of private litigation over issues completely outside their control and that may be difficult to understand.
	- The distinction between exclusionary and substitutionary legal effect raises significant uncertainties:
		- Determination of whether a case is one of exclusion or substitution can be problematic and depends on how the question is framed.
		- In both cases, the outcome is determined by the directive
		- It presumes that an exclusionary effect is less legally intrusive than a substitutionary effect. 
		- If the basis of the exclusionary rule is the primacy of community law, there is no reason why it should not also demand a substitutionary effect for directives.
- Direct effect through general principles
	- The reconcilation between the ECJ's approach in Mangold and Marshall is highly formalistic, especially considering that the obligations read into the principle were the provisions of the directive.
		- If the textual argument holds true, what basis can there be for giving effect to directive provisions between private individuals during the transitionary period?
	- There are significant legal certainty issues since the defendant was in fact complying with national law before the expiration of the implementation period, and prior to the case, there was nothing to suggest that directives can have any sort of legal effect during this period. 
		- The source and scope of the principle was unclear
		- The Mangold rule, while confirmed in Kucukdevici, has been significantly criticised by various AGs
- State liability
	- The private individual would have to pursue two actions, one against the defendant and another against the state. 
	- Even greater uncertainty if he were to pursue both actions concurrently, or alternatively, it would take unacceptably long for him to vindicate his rights.
- Conclusion
	- Original arguments are unconvincing.
		- Existence of complex legal exceptions indicate doubt as to its soundness, the policy that underlies the rule, and its normative weight, which manifests in judicial willingness to allow the rule to be circumvented in the various situations. 
		- Qualifications to the rule are not consistent with the policy objective said to underlie it, and place individuals in a worse position than if directives had horizontal direct effect.
		- No "integrity" in Dworkinian terms
	- Despite the lack of stare decisis in EU law, the weight of the case law would make it highly unlikely that there will be positive developments in this area. 
