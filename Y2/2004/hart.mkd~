#HLA Hart

##Hart's Criticisms of Law as Coercive Orders

###Introduction

- Hart chooses not to use "command" but rather "coercive order" since command suggests a stable heirarchical structure. The presence of this structure is too close to what a theory of law seeks to establish.
- Characteristics that need to be added to the gunman analogy for it to supply a basic conception of law:
	- When the order is said to be addressed to someone, it must mean that the order applies to him, rather than that it is directly communicated to him, for the law applies even when one is left to find out about its existence for himself.
	- There must be a general belief that the orders are continuous and persistent ("standing orders") and that breach would likely bring about sanctions
	- There must be a general habit of obedience (this is essentially vague and imprecise notion)
		- This is a key feature. Mere temporary ascendancy of one over another is not law. 
- General observations:
	- Concept of general orders backed by threats given by one generally obeyed closer resembles criminal rather than other law. 
	- A concept of law must account for internal supremacy and external independance. 

###Some Issues

####Content of laws

- While the coercive order theory appears to match the criminal law, it is difficult to see how they apply to power conferring laws.
	- Further consider laws that confer public powers such as the right to try
		- An ultra-vires judgement of a court is not said to be an offence, nor is it said to be a "nullity". It retains its legal effect, although it is liable to be set aside by a higher court. This may not properly be described as the court disobeying a rule of jurisdiction.
		- Subordinate legislation created in excess of powers conferred by primary legislation is said to be a nullity. 
	- Hart acknowledges that there are some resemblances between power conferring rules and rules imposing sanctions
		- Both kinds of rules constitute standards by which actions may be appraised
		- Power conferring rules ultimately relate back to rules imposing sanctions
- Hart proceeds to examine a few ways in which power conferring rules have been amalgamate power conferring rules into coercive orders
	- Nullity as a sanction
		- Nullity is the sanction threatened by the civil law, even though it may sometimes only amount to a slight inconvenience. 
		- O: In many cases, nullity may not be an "evil" to the person who has failed to satisfy the condition
			- A judge may have no material interest in the validity of his order
			- A party may sometimes desire the "nullity"
		- O: Sanctions are meant to deter behaviour, power conferring rules are meant to encourage others
			- The only way to fit this is through a highly artificial argument as if scoring rules in sports serve to eliminate all moves except a certain designated kind.
		- O: Power conferring rules may not be described without reference to the "nullity". It is part of the rule rather than a way of enforcing a rule. 
	- Power conferring rules as fragments of laws
		- Argument is that law is a conditional rule that stipulates the application of sanctions.
			- All legal rules may be restated as conditional norms
			- Power conferring rules stipulate one of the conditions of a "complete" law
			- Not necessary that a sanction is attached to every "breach" of a law; only that every genuine law directs the application of some sanction.
		- Theory may take two forms:
			- The moderate view: criminal law requires no recasting; power conferring rules must be recasted as fragments of orders backed by threats. 
			- The extreme view: even criminal law must be recasted. There is no law against murder, but rather a law that prescribes criminal sanctions for murder.
		- O: The theory attains uniformity at the cost of distorting the function of law as a way of social control.
			- Law operates by prescribing rules, and applying sanctions when rules are broken. This theory, by contrast, makes provisions for the breakdown of the system its primary purpose.
				- These provisions may be indispensable but they are ancillary
			- Criminal law serves to provide rules that guide the course of citizens' conduct while other power-conferring or obligation imposing kinds do not. The theory conceals this important distinction. 
			- Rules should be looked at from the perspective of those who exercise them rather than those who fail to meet the conditions. 
				- They serve to confer law-making powers within the individual's limited personal sphere. 

####Range of application

- Top-down model of law as coercive orders imply that rules are other-regarding
- However, there is nothing intrinsically other-regarding about legislation, even though certain legislation may be so enacted. 
	- Reconciling this with the top-down model requires a distinction between someone acting in his official or personal capacity
- However, self-binding nature of legislation can be explained without resort to that mechanism
	- Like promising, legislation merely uses specified procedures to alter everybody's legal position by drawing on existing rules
	- It is better said to be an exercise of legal powers rather than the giving of an order to everybody, including himself

####Mode of origin

- While not all custom is law, most legal systems have some elements of customary law. 
- Law as coercive orders may only accomodate such law through the mechanism of tacit approval.
- However, this mechamism faces two challenges:
	- Custom as law by tacid approval necessarily implies that they may not be law until they have been applied in court without objection from the sovereign
		- While this may be the arrangement in some legal systems, it is not intrinsic in the nature of law that customs may not be law until applied in adjudication
	- A lack of outright disapproval does not necessarily mean tacit approval. 
		- Absence of disapproval may be due to other factors in play
		- Absence of disapproval may also be due to a lack of knowledge. 

###A Fundamental Objection: The Sovereign

- Law as coercive orders by a sovereign habitually obeyed presumes a top-down structure of a system with law
- Two issues:
	- Can the habit of obedience explain the continuity of lawmaking authority and the persistence of law after the death of the sovereign?
	- Is it necessary for legal systems to have a supreme lawmaker who is above the law

####The Habit of Obedience and the Continuity of Law

- Adopting a course of conduct in line with a law does not necessarily imply obedience. 
	- One may act the same way whether a law exists or not, especially if the law prohibits some conduct the person would not think of doing anyway.
- General obedience does not necessarily mean habitual obedience.
	- Habitual suggests natural inclination, while law, even obeyed, may run against strong natural inclinations.
	- Habitual obedience is personal and convergent
		- Very primative, and lacks a normative element
- Austin's system lacks a normative element and therefore fails to explain his successor's personal right to make law. 
	- It firstly cannot explain a right to make law.
	- Habitual obedience to the previous sovereign does not even render it probably or found any assumption that his appointed successor will be habitually obeyed as well.
- Rules and habits
	- Habit is merely convergent conduct, and is a purely factual observation; deviation from them does not necessarily bring about criticism
	- Rules are normative and deviation is criticised
		- Deviation is a good reason for the criticism, and are generally regarded as legitimate both  by those who make them and those to whom they are made. 
		- Normativity requires a critical reflective attitude to certain patterns of behaviour as a common standard. 
- Rules ant the continuity of law
	- The presence of a normative framwork allows for the conception of a normative office whose officeholder has the "right" to make law. 

####The Persistence of Law

- If law was merely convergent personal conduct, it is difficult to explain how law made before the predecessor's death continues to be law in the successor's reign.
	- This may easily be done by substituting habits of personal obedience with an acceptance of a rule conferring validity on laws made by a class of persons. 
		- This rule may be said to be timeless in that it can look both forward and backwardsin time. 
	- Austin and Bentham have attempted to argue that they remain law under the authority of the present sovereign with his tacit approval
		- Problem with tacit approval above is even more obvious here, for unlike customary law, such legislation isi most definitely law even before a court applies it. 
		- There is no legal basis to distinguish between law that has not been invoked in court and those that have been and therefore can be said to be tacitly approved of by the present sovereign.

####Legal Limitations on Legislative Power

- In Austin's theory, the sovereign posseses legally unlimited legislative power by definition. 
	- There could only be legal limits on legislative power where one is under the orders of another whom he habitually obeys, in which case he would no longer be sovereign. 
	- While legislative power is legally unlimited, he may continue to be under practical limitations such as deference to popular opinion.
- However, in many modern legal systems, the legislator is not a legally unlimited sovereign but are instead constrained in their lawmaking by a written constitution.
	- Such restrictions imposes not legal duties but legal disabilities: they qualify legislative power
		- Such restrictions cannot be described as the presence or absence of the sovereign's obedience to other persons. 
	- In the absence of such restrictions, habitual obedience (such as due to diplomatic concerns) does not undermine the legislator's lawmaking power nor does it confer upon the external party a lawmaking power within the sovereign's jurisdiction
		- The validity of a law rests in conditions for its enactment rather than the personal identity of its maker
- It is necessary to differentiate between a legally unlimited sovereign and an internally supreme sovereign. 

####The Sovereign Behind the Legislature

- Since in many systems, the supreme national legislature is legally limited and and yet their enactments are law, we must be able to identify a legally unlimited sovereign behind the legislature if we are to insist that he is a necessary element of legal systems. 
- In most legal systems, the legislature, while limited by a constitution, are empowered to amend it and may therefore be said to be legally unlimited in a sense. 
	- However, even in those cases, there are usually elements that are outside the scope of such powers. 
- Austin himself did not identify the sovereign behind the legislature even in England. 
	- The Queen in Parliament is often cited as a paradigm of a "sovereign legislature"
	- However, Austin appears to have taken the view that the electors are the true "sovereign body" and their elected representatives are merely trustees for their power. 
		- In England, the only direct exercise of their share in the sovereignty consists in their election of the representatives, and absolute delegation to them of their sovereign power.
		- In the USA, the delegation is restricted, and the electorate may be said  to be an "extraordinary and ulterior legislature", and is the sovereign free from all legal limitations as required by the theory.
	- Austin's identification of the sovereign in the legislature makes the notions of habitual obedience highly artificial
		- The initial appearance of a clearly top-down structure has been transformed into a system whereby the bulk of the population is habitually obeying themselves.
			- If one seeks to assert that these individuals make law in a separate capacity, it presupposes the existence of a normative framework and rules that Austin's theory denies.
			- It is not possible to say that the rules merely describe the way in which the sovereign may legislate, for the rules constitute the sovereign. 
		- Even if it can be said that the sovereign lies within the electorate, it is difficult to claim that the ordinary legislature, in complying with the constitution, is "habitually obeying" the electorate's orders that are manifested in the constitution
			- Same arguments concerning tacit obedience apply
		- Furthermore, if the soveign is a hereditary monarch with limited legal supremacy, earlier arguments that the electorate is the "true" sovereign lose their validity.

##Hart's Theory of Law

###Primary and Secondary Rules

- Primary rules set out the phsyical manifestations of the obligations, while secondary rules provide for their creation and variation

####Obligation

- Being obliged as in Austin's account is the result of coercion, while having an obligation denotes some internal normative justification
	- It is on this basis that Austin emphasises the likelihood of suffering a sanction
- The state of affairs required for one to be obliged to do something is both insufficient and unnecessary for the existence of an obligation
- Deviation from a rule not only serves as grounds to predict the application of sanctions, but also justify those sanctions
	- The predictive account suggests that where sanctions are unlikely, obligations do not arise
	- While sanctions are usually imposed for deviation from rules, it is not necessary for the existence of said rules
- Obligations suggest a standard of conduct
	- Not every standard of conduct imposes obligations. Some, like etiquette, only suggest the "proper" conduct, but do not demand it.
	- Rules impose obligations when the general demand for conformity is insistent and the social pressure on those who deviate is great.
		- However, it does not mean that the individual must have feelings of compulsion or pressure.
	- Rules that impose obligations are of importance to the functioning of society
	- Conduct required by those rules may require the subordination of personal desire
- Two points of view:
	- The external pov belongs to those who does not accept them
		- External observer records the regularities of conduct and their consequences such that he arrives at a predictive theory of sanctions
		- In this sense, the observations do not amount to rules at all, but rather patterns.
			- E.g. Red light is a sign that people would likely stop rather than something that imposes an obligation to stop.
	- The internal pov belongs to those who accept and use them as guides to conduct

####The Rules in Law

- Primary rules that demand certain standards of conduct generally exist in any organised group of persons
	- While some may reject the rules but comply due to fear of the consequences, they may not form the majority
	- A system of purely primary rules have the flaws of uncertainty, rigidity, and inefficiency
		- Uncertainty as to the existence and content of rules
		- Rigidity in that the rule is by its very nature self-perpetuating
			- Rigidity also appears in the sense that there are no ways to adapt to ad-hoc situations that occur
		- Where disputes arise, the lack of rules governing such disputes (secondary rules) leads to the unnecessary expenditure of time and resources
- Secondary rules remedy the flaws of a system containing purely primary rules by conditioning the primary rules. They are rules concerning primary rules.
	- Rule of recognition: uncertainty is resolved by a rule of recognition that proclaims a determinitive source of primary rules.
		- These may be purely textual, but in such cases the emphasis is not on the content of the text but its authority
		- In more complex systems, the rule of recognition may be a heirarchical system of sources of rules
			- The subordination of one source to another is different from saying that it derives from another (a necessary implication of Austin's account)
	- Rules of change: Rules governing the way in which primary rules may be amended. 
		- Clearly highly related to the rule of recognition
		- This also includes rules conferring upon indviduals power to vary their initial positions under the primary rules (limited legislative powers)
	- Rules of adjudication: Rules conferring upon certain individuals the power to make authoritative determinations on whether a primary rule has been breached
		- Also defines the procedures
		- A system which includes a rule of adjudication necessarily includes some sort of rule of recognition.
		- Includes the rules conferring upon judges the exclusive power to order the application of penalties

###Forming a Legal System

####Rule of Recognition and Legal Validity

- The acceptance of a rule of recognition gives an authoritative criteria for identifying primary rules of obligation.
- The rule of recognition is generally not expressly formulated as a rule, but its existence is shown in the way in which other rules are identified.
- The use of unstate rules of recognition in identifying particular rules of the system is characteristic of the internal pov.
	- Those who use the rules demonstrate their acceptance of them in their language.
	- Internal vs external statement: "It is the law that..." vs "In England, what is recognised as law is..."
- The concept of legal validity stems from the rule of recognition, in that what passes all the tests of the rule of recognition is considered to be valid.
	- Note that the statement itself is an external manifestation of the internal pov. The focus is on the internal acceptance of validity, rather than the express proclaimation.
	- Efficacy vs validity:
		- Efficacy means the rule is obeyed more often than not.
		- There is no necessary connection between the efficacy and validity of a rule, unless efficacy is part of the rule of recognition.
		- Distinguish between efficacy of one rule vs efficacy of the whole system, whose absence renders further analysis pointless.
			- Therefore, making an internal statement of the validity of a particular rule of the system presupposes the general efficacy of the system. However, this does not mean that the system actually is so, but rather such an exercise would be pointless if it were not.
		- The idea that legal validity means its likely efficacy is a combination of both the internal statement of validity and the external statement of the system's general efficacy.
- The rule of recognition is the ultimate and supreme rule by which the validity of all other rules of the system is determined.
	- It is supreme in the sense that the determination of legal validity by this rule overrides that by all other rules.
		- This supremacy is not to be confused with the idea of unlimited sovereignty under Austin's account.
	- It is ultimate in the sense that all other rules, even subordinate rules of recognition may trace their validity back to the ultimate rule.
		- The idea of validity is irrelevant to this rule. 
			- While some have argued that the validity of this rule is assumed, the fact of the matter is that there is no question of validity. There is no test of validity for it, but only a test of its existence.
- In a mature legal system, a statement of the existence of a legal rule means that it satisfies the rule(s) of recognition.
	- However, the statement of the existence of the rule of recognition is a statement of fact.

####New Questions

- There are some rules (such as the rule that the Queen may not refuse Her consent to a bill duly passed by the Peers and the Commons) that are difficult to classify.
	- It is not a mere convention in the sense that it is law that may be applied, but at the same time it is higher than ordinary laws.
	- It is law in the sense that it is one of the defining features of the legal system, and it is fact in the sense that its existence depends on the fact of its application in practice. 
	- Hart considers such statements both fact and law at the same time. Issues of law do not fall under a dichotomy of law vs fact.
- The statement of existence of a legal system is highly unclear. It is shorthand for the existence of a number of social facts, and it is difficult to determine when a "pre-legal" society develops a system of law.
	- The Austinian formula only caters for the observable "end product" of a legal system, but sheds little light about the actual functioning of it without significant distortions.
	- Taking Hart's formulation at face value i.e. that where laws which are valid within the system and obeyed by the bulk of the population to be determinative of the existence of a legal system is also unsatisfactory.
		- It conceals the second important element that there must be a critical reflective attitude towards the rules, in that the rule of recognition is not just complied with, but also internally accepted as binding the community. (Internal acceptance of an outward extending rule)
	- Hart considers two elements to be essential:
		- The valid rules of behaviour must be generally obeyed by the private citizens.
		- The rules of recognition must be effectively accepted as common public standards of official behaviour by its officials.
		- Hart considers that while general acceptance may be applicable in a simple society, it is not so in a complicated system. 
			- Ordinary persons do not necessarily have a sufficiently fundamental understanding of constitutional matters.
			- Therein Hart establishes his distinction between normal persons and officials. 
				- Officials acknowledge and accept explicity constitutional rules
				- Ordinary citizens manifest their acceptance largely by acquiescence.
		- SELF: Is there an issue here by removing the requirement for internal acceptance of the citizens? Hart's account necessarily means that citizens are obliged to obey the law, but are only obligated when they accept it. 
- Where there is a breakdown in the legal system such that there is no general obediance of rules that are valid:
	- There is no conclusive and decisive way to determine when a legal system ceases to exist.
	- In the event of an intervening occupation by a foreign body, the question of what was or was not law during the occupation is a normative one that looks either to international law or perhaps subsequent valid declarations by the restored government. 
		- Such declarations are normatively determinative within the jurisdiction, even if the factual circumstances suggest otherwise.
		- Law is both normative and factual. The internal position that something is law is is not necessarily compromised by the external observation that it is not complied with as if it were law.
		- The legal validity of a statement within a system depends on its compliance with the relevant rule of recognition.

###Formalism and Rule Scepticism

- 
