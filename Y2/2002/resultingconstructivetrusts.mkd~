#Resulting Trusts

##Doctrine

###Classification of Resulting Trusts

- In _Re Vandervell's Trusts (No. 2)_, Megarry J suggested a distinction between automatic and presumed resulting trusts
	- Automatic resulting trusts refer to when a disposition fails to exhaust the trust fund and a resulting trust over the remainder "atomatically" arises in favour of the settlor (no relationship to parties' intention)
	- Presumed resulting trusts arise when there is a transfer of property and the law presumes that he intends for the recepient to hold property on trust for him (presumed transferor's intention)
-	In _Westdeutsche Landesbank Girozentrale v Islington London Borough Council_, Lord Browne-Wilkinson considered Megarry J's earlier formulation
	- To him, there is no difference between automatic and presumed resulting trusts, both are products of parties' common intention
		- Furthermore, a resulting trust is not imposed against the intentions of the trustee (as is a constructive trust) but gives effect to his presumed intention under his conception. 
			- However, resulting trusts frequently arise even when transferee clearly has an intention to retain beneficial interest in land. Presuming even when there is clearly a contrary intention?
			- Inconsistency as to when a resulting trust is imposed as compared to constructive trust when considering transferee's intention
	- Problems
		- Clearly unworkable because a resulting trust may arise even when testator manifests no intention to retain any beneficial interest in property _Vandervell v IRC_
		- Also fails to explain where the resulting trustee is a seven-year-old child as in _Re Vinogradoff_
- One (wrong) theory proposed is that there is a positive intention to retain an equitable interest. However, there is no equitable interest to retain where one has absolute ownership of land. The equitable interest only comes into being when a trust is created either explicitly or by operation of law; _Westdeutsche_.
- Compare with Chambers' theory of resulting trusts: resulting trusts arise where property is transferred without an intention to benefit the recipient
	- B's intention is irrelevant
	- A's positive intention to part with property is also irrelevant - Lord Millett in _Air Jamaica_\
	- Example is _Hodgson v Marks_, where donor did not intend to make a gift and a resulting trust was found in her favour

###Intention or Unjust Enrichment?

- Two major views, resulting trusts either respond to the donor's intention or the donee's unjust enrichment
- On intention:
	- Favoured by Lord Browne-Wilkinson in _Westdeutsche_
	- Cannot explain where donor intends to part with all beneficial interest in property, or when a resulting trust arises when an express trust fails, since express trust directly demonstrates his intention
- On unjust enrichment
	- Read the articlesssss
	- Chambers argues that all resulting trusts respond to unjust enrichment
		- It follows that all unjust enrichment cases should lead to resulting trusts in two ways:
			- Where a proprietary remedy results from an action for unjust enrichment, it should be a resulting trust (appears fine)
			- Where a personal claim is awarded, a proprietary remedy of resulting trust should have been applied instead (questionable)

##Failing Trusts

- Where property is transferred to trustees on an express trust that fails entirely or fails to exhaust the trust fund, a resulting trust on the remainder arises in favour of the testator. Two examples worth noting are _Vandervell v IRC_ and _Air Jamaica Ltd v Charlton_.
	- In _Vandervell_, X transfers shares to the RCS so that they can receive dividends, stipulating that a specified trust corporation has the rights to repurchase the shares on a later date. The trust company subsequently exercises this right and it was unclear for whose benefit it held the shares, since it could not hold it for itself. UKHL held that since there was no beneficiary, a resulting trust arises in favour of the original donor.
	- In _Air Jamaica_, Air Jamaica was privatised and its employee pensions fund was discontinued. The question was for whose benefit was the balance of $400m held. To be noted is the presence of a term in the original trust deed stating that moneys contributed by the Company under the terms of the settlement shall not be repayable to the company. UKPC held that the term did not prevent a resulting trust on the balance from arising in favour of the employees. A manifest intention to dispose of property does not interfere with the operation of resulting trusts
- Resulting trusts do not arise where it is clear that the beneficiary is to receive the property absolutely subject to certain stipulations.
	- Where a purpose was expressed but the court applies the rule of construction to deem it as only an expression of the settlor's motive _Re Osoba_
	- Where B takes property absolutely subject to a charge for some purpose _Re Foord_
	- Where B takes property absolutely with trusts engrafted or imposed on that interest that subsequently fail _Hancock v Watson_
	- Doctrine of acceleration applies: when the recipient of a life interest disclaims his beneficiary interest, the recipient of the remainder will have his interest accelerated so as to take effect immediately so long as no contrary intention is manifested in the trust document _Re Flower's ST_
	- Where property is transferred under a contract
	
##Apparent Gifts

###Purchase of Property

- Purchase money resulting trust does not arise where money is provided as a loan or where circumstances clearly show that the donor intends confer a benefit on the recepient of property. 
- Where X purchases property in Y's name, Y holds it on resulting trust for X _Vandervell v IRC_
- Where X purchases property in X+Y's names, X and Y together hold it on resulting trust for X _Rider v Rider_
- Where X and Y jointly purchase property on X's name, Y's name, or X+Y's names, the legal owner(s) hold it on resulting trust for the parties in proportion to their contributions towards the purchase price.
	- Only capital sums are considered. Income contributions are not. 
	- Existence of resulting trust and the apportionment of parties' shares is determined at the time of purchase _Huntingford v Hobbs_
		- Where a mortgage over property is granted and one party personally covenants to repay it, the principal of the mortgage is his contribution towards the purchase price. 
		- Where a mortgage over property is granted with both parties undertaking liability to repay, they are deemed to have contributed towards the principal of the mortgage in equal shares. 
	- A constructive trust analysis will be appled instead in the context of a family home per _Stack v Dowden_ where:
		- Property is purchased in joint names
		- Property is purchased for joint occupation
		- Property is purchased by a couple, married or unmarried
		- If there is a mortgage, both must be responsible for it
		- Does not apply where family members acquire property primarily for investment _Laskar v Laskar_
- Monetary contributions towards the puchase of property is reflected in the resulting trust. 

###Transfer of Property

- Where X gratuitously transfers personal property (not land) to Y without manifesting an intention to benefit Y, a resulting trust arises in favour of X. 
	- Does not apply where X conveys Y to land by virtue of s. 60(3) LPA 1925, confirmed in _Lohia v Lohia_
- Where X gratuitously transfers personal property (not land) to X+Y in joint names without a manifest intention to benefit Y, X+Y hold it on resulting trust for X
	- Same s. 60(3) LPA 1925 exception applies
	- Where X and Y open a joint account and X provides in his will that Y should receive whatever is left in the account, X having sole control of the account during his lifetime, it is treated as an immediate gift of a fluctuating asset _Young v Sealey_

###Presumptions

- General presumption against an intention to benefit recipient 
	- Presumptions only apply in the absence of evidence
- Presumption of advancement in certain specific situations
	- Effect of a presumption of advancement is to reverse the burden of proof
	- Husband to wife _Tinker v Tinker_
	- In loco parentis (only father-like relationship) _Lavelle v Lavellee_
	- Does not arise in cohabitation or from wife to husband/child
	- Gender bias probably contravenes Art. 5 Seventh Protocol to ECHR
		- Supposed to be removed under Equality Act 2010 but not yet in force
	- Presumption is nowadays weak between spouses (_Pettitt v Pettitt_), CASC considered that the presumption of advancement should no longer apply to transfers by parents to adult children
- Rebutting presumptions
	- Evidencial requirement varies depending on strength of presumption, which in turn depends on the facts and circumstances giving rise to it _Vajpeyi v Yusaf_
	- While the focus is on the donor's subjective intention at the time of transfer, all evidence both before and after the transfer is admissible, and courts will decide the relative weight to be placed on them _Lavelle v Lavelle_
	- On illegality, general principles are:
		- He who comes to equity must come with clean hands
		- Ex turpi causa non oritur actio (no legal action arises out of bad conduct)
		- In pari delicto potior est conditio defendetis (or possidentis) (when both parties are in the wrong the defendant or possessor of property is in the stronger position)
		- One may not adduce evidence of his illegality to establish his claim _Tinsley v Milligan_
			- Exception lies where he repents before it had been put into action ("wholly or partly carried into effect" per Millett LJ in _Tribe v Tribe_)
			- In the case of resulting trusts, the presumption of advancement would therefore affect whether the wrongdoer may claim his property because it changes the party on whom the burden of proof lies. 
- Is there actually a presumption?
	- Chambers: "Presumption" against intention to benefit recipient is not a true presumption. It is a statement reflecting the lack of evidence to establish an intention. 
		- Default position: no intention unless established. Not a true presumption.
		- True presumption in cases of presumption of advancement

#Constructive Trusts

##Secret Trusts

###Introduction

- S. 9 Wills Act 1837 gives that a valid will needs to be in signed writing and witnessed by two witnesses
- Secret trusts arise when it is not (completely) stated in the will
	- Due to a desire to restrict knowledge of terms of trust
	- By accident (circumstances just so happen to give rise to a secret trust)
- Two types of secret trusts
	1. Fully secret trusts
		- Absolute disposition in testamentary deed with no mention of secret trust
	1. Half-secret trusts
		- Disposition in testamentary deed specifies states the existence of but does not specify the terms of the secret trust
- Two questions:
	- Why do courts recognise secret trusts
	- What is the nature of the trust? Express vs constructive trust?
		- Both require intention
		- Express trust has specific formality requirements
		- Secret trusts require communication and acceptance, which is unnecessary for express trusts.
- Three general requirements given in _Kasperbauer v Griffith_
	- Intention
		- Intention to create a secret trust. B must be intended to be a trustee and not just hold it on a moral obligation.
	- Communication
		- Communication of testator's intention to secret trustee (beneficiary)
		- If intention was not communicated to secret trustee, he will not be bound
	- Acceptance
		- B must accept the terms of the trust

###Fully Secret Trusts

- Communication
	- Intention may be communicated before or after the execution of the will _Wallgrave v Tebbs_
	- Substance of the communication must be so as to mean that beneficiary is intended to hold property on secret trust for another _Walgrave v Tebbs_
	- Communication must contain the terms of the trust _Re Boyes_
		- Where testator does not communicate the terms of the trust, but only its existence, only a resulting trust arises
	- Property to be held on trust must be specified in the communication per _Re Colin Cooper_
	- Communication may be orally, writing (not compliant with formality requirements), or sealed instructions _Re Keen_
		- B must know that he is to be a trustee and have the means of discovering the terms of the trust
	- Only those to whom the intention was communicated are bound by it _Re Stead_
		- Where property is conveyed to multiple people as joint tenants, only the individual who had notice is bound
		�v- However, where intention for all joint tenants to hold property on trust is communicated to one of them, they are all bound
		- __READ CASEBOOK__
- Acceptance
	- Acceptances may be express or acquiescence _Moss v Cooper_
	- Argument that testator left his will unrevoked in reliance on B's acceptance/acquiescence in _Moss v Cooper_
		- Reliance argument also adopted in _Strickland v Aldridge_, where S died without making a will because he had an agreement with his next of kin for property to be held on secret trust
- Effects of compliance
	- Secret beneficiary have all the rights of a benefiiary under an express trust
	- In _Ottaway v Norman_, secret trustee agreed to bequeath property received from testamentary deed and was therefore compelled to perform his agreement
		- B is compelled to perform his promise
- Effects of non-compliance
	- If B knew of the existence of a trust but not its term, a resulting trust arises in favour of the testator's estate _Re Boyes_
	- If B did not know of testator's intention before his death, B takes it absolutely

###Half-Secret Trusts

- Communication
	- Communication must occur before or at the time testator's will is executed _Blackwell v Blackwell_, _Re Bateman's Will Trusts_
		- Cannot contradict the wording of the will
		- Court does not want to allow testator to reserve a power to make future unwitnessed dispositions. Do not want to carve out too big of an exception from the Wills Act. 
	- If the will permits communication to one trustee only, and if the communication is made before the execution of the will, all trustees are bound _Re Gardon_
	- Terms of the secret trust cannot conflict with the will
		- In _Re Keen_, will said that B will be notified during testator's life, but he only opened the envelope after testator died
		- Obvious exception that is the fully secret trust
- Effect of non-compliance
	- Resulting trust always arises _Re Keen_

###Death

- Where secret trustee dies before the testator
	- Fully secret trust will fail _Re Maddock_
		- Will must operate before a secret trust can take effect. Only secret trustee is bound. General principle that equity will not allow a trust to fail for want of a trustee does not apply to secret trusts
		- Obiter statement in _Blackwell v Blackwell_ to the opposite effect but this is likely incorrect
	- Half-secret trust will not fail _Sonley v Clock Makers' Company_
		- Since trust deed already establishes the existence of a secret trust, the court may intervene to substitute a trustee
- Where secret beneficiary dies before the testator
	- No case law on fully secret trusts, the trust should fail and property will be held on resulting trust
	- For half-secret trusts, it was held in _Re Gardner (No. 2)_ that property will go to secret beneficiary's estate, but this is generally considered to be a wrong decision.
		- Will should similarly fail for want of a beneficiary.
		- Reasoning in _Re Gardner_ suggests that the secret beneficiary obtains an interest as soon as the secret trust was "created", but this is clearly wrong since secret trusts are testamentary and not inter vivos trusts

###Doctrine

- __READ CRITCHLEY'S ARTICLE IN HANDOUT__
- Penner argues that secret trusts take effect completely outside the will and therefore the Wills Act should not apply
 	- Critchley argues that a secret trust, while it lies outside the will, remains a testamentary disposition and should therefore fall under the Wills Act
		- Main possible response is that secret trusts are inter vivos dispositions instead of testamentary dispositions
	- Express trust? 
- Fraud theory:
	- Secret trusts are testamentary dispositions that fall within the Wills Act. Principle that statute cannot be used as an instrument of fraud allows the Wills Act to be bypassed so that secret trustee cannot claim the property for himself.
		- Argument that Wills Act is completely bypassed
		- Argument that constructive trusts do not fall under the Wills Act (not contradictory to the Act)
	- Criticisms:
		- Fraud theory does not always work since secret trustee has perpetrated no wrongdoing. 
		- Fraud against whom? C has died and beneficiary is a mere volunteer.
		- Fraud theory fails in half-secret trusts since resulting trust is always possible

##Mutual Wills

###Introduction

- Two parties that both make testamentary dispositions of property to the survivor for life and remainder to agreed upon third party
	- A, B are testators, C is beneficiary
- Two parties that both make testamentary dispositions of property to the same person _Re Dale_
	- Not important

###Requirements

- Firm agreement
	- Presence of (near) identical wills of two parties is not sufficient. 
	- What is necessary is evidence of an intention that the agreement is to be irrevocable following the death of either party. _Re Goodchild_
	- Court will bear in mind that parties usually do not intend to give up their freedom to make/modify testamentary dispositions _Charles v Fraser_
- Subject matter
	- In _Re Hagger_, parties set out their interests (upon A's death, B is entitled to the income and C is entitled to the capital)
	- Generally, language used is unclear, such as "whatever is left goes to C"
		- Analysis is that constructive trust (_Re Walters_) immediately arises over all of B's property on A's death per _Birmingham v Renfrew_ 
			- Australian court holds that this is a floating trust that does not bind until certain events take place
			- Constructive trust crystalises upon B's death or when B improperly disposes of property during his lifetime
			- _Thomas and Agnes Carvel Foundation v Carvel_ confirms that the constructive trust arises on A's death
			- __READ DAVIS ARTICLE ON FLOATING RIGHTS IN HANDOUT__ (She attempts to analyse floating trusts in comparison to floating charges)
			- Penner argues that B has a life interest and C has a remainder interest. B also has a power of appointment of anyone for up to the entirety of the trust fund. 

##The Doctrine in _Rochefoucauld v Boustead_

###Introduction

- S. 53(1)(b) LPA 1925 gives that a declaration of trust relating to an interest in land must be evidenced by signed writing (mentioned earlier)
	- In the absence of signed writing, the trust is valid but unenforceable
- Question: Is it justified to recognise an informal transaction? Are we subverting Parliament's intention?
- In _Rochefoucauld v Bousted_
	- Facts:
		- Rochefoucauld owned estates over which she had granted a mortgage to a Dutch company.
		- Dutch company wanted to call in the mortgage, but she could not pay
		- Estate was to be sold at an auction
		- Rochefoucauld made an informal agreement with Boustead in that if Boustead succeeds at the auction, Rochefoucauld will not insist on her equity of redemption but Boustead will instead hold it on trust for Rochefoucauld subject to her repayment of Boustead's outlay
		- Boustead sells the estate in breach of trust, and has much of the money left over from this transaction
		- Rochefoucauld seeks to recover the proceeds of sale from Boustead
	- Issue: Is there a constructive trust over the property or will Boustead be allowed to retain the proceeds of sale.
	- Lindley LJ: The statute of frauds does not prevent proof of a fraud, and it is a fraud for a person to whom land is conveyed as a trustee and who knows it is so conveyed to deny the trust and claim the land himself.
		- Statute may not be used to perpetrate a fraud.
		- Fraud in question is the denial of trust so as to claim land beneficially
	- Question: What is the fraud? 

###Application of the Doctrine

- Where A transfers property to B to hold on trust for A
	- Elderly woman conveyed cottage to brother-in-law based on the informal understanding that he would allow her to live rent-free in the cottage for as long as she wanted _Bannister v Bannister_
	- Hodgson conveys house to Evans to hold on trust for her. Evans then sold the property to Marks, but Marks does not take property free of Hodgson's interest since she was in actual occupation. EWCA held that Hodgson had beneficial interest in land and therefore Marks held it on trust for her _Hodgson v Marks_
	- Duchess conveys land to Duke so that the Duke may raise money in his own name, on the understanding that it would be reconveyed to her after that. Duke died before reconveying said property. Despite informality of agreement, the Duchess was entitled to the land _Re Duke of Marlborough_
- Where A eventually gets more than what A had initially
	- Rochefoucauld eventually obtained a beneficial interest in the whole property instead of merely an equity of redemption _Rochefoucauld_
		- Same situation in _Lincoln v Wright_
- Where A declares himself as trustee without satisfying formality agreement
	- Doctrine in _Rochefoucauld v Boustead_ does not apply in this situation; _Smith v Matthews_, _Wratten v Hunter_ __AUSC__
	- There is no fraud when A denies the trust.
	- C is a volunteer and equity will not assist a volunteer.
	- Proprietary estoppel may still apply
- Where A conveys land to B but B does not agree to be a trustee, the doctrine does not apply. A resulting trust arises instead.
	- Where A conveys land to B without B's knowledge in order to avoid being the sheriff, A may reclaim his land; _Birch v Blagrave_. No intention to part with beneficial interest. 
	- In _Childers v Childers_, A transferred land to B so that B can qualify as a bailiff. B died soon after without knowing about the transfer of the land. EWCA held that B's estate held land on trust for A because A had no intention to give B a beneficial interest in land. 

###Issues

- Nature of trust
	- __READ CHAMBERS ARTICLE ON RESULTING TRUST__
		- Chambers argues that intention is one of the key differences between resulting, constructive and express trusts.
		- Express trusts respond to the settlor's unilateral, properly manifested interest
		- Resulting trusts respond to the settlor's negative intention
		- While many commentators consider that constructive trusts are unrelated to intention, he argues that it is wrong to think so. Those observations are made in related to B's current intention. The better approach is to consider parties' positive intention at the time the trust was created, as well as some element of reliance. 
	- Similar facts have been described as the three different types of trusts:
		- _Rochefoucauld v Boustead_ - Express trust
		- _Bannister v Bannister_ - Constructive trust
		- _Hodgson v Marks_ - Resulting trust
	- On resulting vs constructive trust:
		- Can be constructive trust since there was common intention and reliance. 
		- Can be resulting trust since A did not intend for B to obtain a beneficial interest in land.
		- Constructive trust analysis is preferred:
			- Fails to take into account B's role in the transaction. Resulting trust analysis only concerns A's unilateral intention.
			- Resulting trust does not allow for a third party to take any beneficial interest in the property
			- A eventually received a different interest in land to what B received from him initially in _Rochefoucauld v Boustead_ and _Lincoln v Wright_. This cannot be possible under resulting trust analysis.
		- Possibility that _Hodgson v Marks_ was held to be a resulting trust because this was the pleading of parties. No constructive trust was pleaded. 
	- On express vs constructive trust:
		- EWCA in _Rochefoucauld v Boustead_ held that what arose was an express trust for the purposes of limitation period. It was not to establish the nature of the trust that arose.
		- It was similarly not explaind in _Bannister v Bannister_ how the court arrived at the conclusion of a constructive trust.
		- Express trust only arises where A is able to and properly declares a trust. Formality requirements are important for an express trust analysis.
		 	- Disapplication thesis on s. 53(1)(b) LPA 1925
		- __READ YING's ARTICLE__
			- No actual settlor in _Rochefoucauld v Boustead_ 
			- A cannot declare a trust over a bigger interest than what he has
			- B cannot have declared a trust since he never had a full interest in the land over which to declare a trust
- On three party cases:
	- Can doctrine in _Rochefoucauld v Boustead_ apply to enforce a trust in favour of C?
	- Secret trust cases are a clear case in which C obtains a beneficial interest under three party cases.
		- This is because A is dead and cannot redo his testamentary disposition. Therefore in three party inter vivos cases, property should go back to the testator. 
	- Youdan says C should get property
		- Statements of doctrine say that we should enforce the promise
	- Feltham argues that A should get property
		- If C obtains beneficial interest in property, it would defeat the purpose of s. 53(1)(b)
	- Ying argues that these are clear cases of common intention constructive trust that falls outside the scope of s. 53(1)(b) and within s. 53(2).
	- Read _De Bruyne v De Bruyne_
		- A(s) were beneficiaries under an irrevocable discretionary trust of shares. All of them made an agreement with B (one of the beneficiaries) that if they convinced the trustee to appoint the shares to B alone, B would hold property on trust for certain specified individuals (C). EWCA held that B could not rescind his promise and there was a constructive trust in favour of C.

##The Doctrine in _Pallant v Morgan_ 

###Introduction

- Facts of _Pallant v Morgan_
	- A's agent and B's agent were at an auction for land. 
	- Before the auction commenced, A and B agreed that A would refrain from bidding and if B won the auction, B would sell part of the land to A.
	- B won and attempted to rescind his promise.
	- It was later discovered that B's agent was authorised to submit a higher bid than A's agent. Because A stayed out of the auction, B was able to pay a much lower sum. 
		- A suffered no detriment since he would not have won the auction anyway.
		- B enjoyed monetary savings as a result of his agreement with A.
	- Judge held that B held agreed on portion of land for A subject to A's payment of relevant expenses.
		- It does not matter that A has not suffered a detriment.
		- B has induced A not to bid and has obtained an advantage in relation to buying the land. 
- In _Banner Homes Group plc v Luff Developments Ltd_
	- Facts:
		- A and B wanted to enter a joint venture into buying and developing property through a JV company.
		- B subsequently changed his mind but did not notify A.
		- B then secretly bought land for himself and attempted to kick A out of the transaction.
			- B bought all the shares in the JV company and used JV company to buy the land. 
	- EWCA held that the doctrine in _Pallant v Morgan_ applies and B holds property on trust for A and B. 
		- A did not suffer any detriment since A would not have acquired the land by itself (in the absence of the joint venture)
		- B enjoyed an advantage because B had the security of knowing that he would be the only party attempting to buy the land.
	- LJ Chadwick set out five factors of the doctrine:
		- Parties' agreement must precede acquisition of land in question
		- That agreement does not need to be contractually enforceable
			- May not be sufficiently certain
			- S. 2(1) LP(MP)A formality requirements
		- B must promise to give A an interest in land
		- A must suffer a detriment or B must obtain an advantage in relation to the acquisition of land
		- It must be inequitable for B to keep the land
- What is the subject-matter of the trust?
	- Where there is an express agreement, courts will give effect to it.
		- In _Banner Homes_, B held shares in JV company on trust for A and B.
		- In _Time Products Ltd v Combined English Stores Group Ltd_, parties agreed that B would acquire land on equal shares for A and B, and this was the distribution upheld by the Court.
	- Where there is no agreement between parties
		- In _Holiday Inns Inc v Broadhead_, it was held that the default position is that property is held in equal shares since equality is equity.
		- Facts:
			- A and B negotiate to enter a joint venture.
			- Pursuant to the negotiations, B acquires an option to purchase over land. 
			- A commits time and effort to secure planning permission over said land.
			- B rescinds original agreement and then exercises his original option to purchase.
		- Observations made on _Holiday Inns Inc v Broadhead_:
			- Some judges think that this is not a constructive trust case but rather a constructive trust case. Others think the converse.
			- Look at five requirements in _Banner Homes_
				- There was an agreement prior to transaction
				- Agreement was not contractually enforceable
				- A was intended to obtain some interest in land
				- Did B receive an advantage in relation to the acquisition of interest in land?
					- Whether "acquisition" refers to when B obtains the option to purchase or exercises that option?
						- Ying suggests the former.
						- Option to purchase is an interest in land in itself
					- Since what A does has nothing to do with B's acquisition of the option to purchase, it should not be a _Pallet v Morgan_ case. 

