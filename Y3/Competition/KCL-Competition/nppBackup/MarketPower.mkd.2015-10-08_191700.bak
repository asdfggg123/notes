# Market Definition and Power

##Introduction

- Fundamental building blocks of competition policy
- Three concepts:
	- Market definition
	- Market power (share)
	- Entry
- Focus on firms' ability to raise price
	- Produce less than consumers want
		- Generally through mechanism of reducing output
	- Convert consumer surplus to producer surplus
	- Ability to do so is market power

##Analysis of Market Power

- Definition and market share thresholds are sometimes provided for by law, either statute or alternative jurisprudence.
- Market share as proxy for economic significance.
	- Presumptions as to market effects by reference to market share
- Object defence?????
	- Conclusive presumption based on assumption that the behaviour always produces undesirable market effects.
- Three ways to identify market power
	- Identify behaviour that so often is destructive to presume a dominant market share e.g. establishment of cartels
	- Introduce evidence of actual effects
		- Computing + econometric models + real time information (big data)
		- Price inelastic demand suggests strong market power
	- Draw inferences from market share
		- Presume market power where there is market share
		- Focus is on substantial and durable market share
- Start from demand side
	- Products that users regard as substitutes to satisfy a particular need
		- Watch past conduct; historical data not always reliable or useful
		- Examine internal market analysis documentation
- Question of Entry
	- See whether other producers enter the market quickly
- Relevant geographic market
	- Key issues are transport costs and import barriers
- General threshold for monopolisation in EU: 40%
- Substitutability does not always mean market power is limited