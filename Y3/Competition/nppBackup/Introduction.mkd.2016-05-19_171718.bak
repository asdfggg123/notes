#Competition law

##Course Structure

- Art 101 TFEU (Anti-competitive agreements) 
- Art 102 TFEU (Abuse of dominant position) 
- Merger control

##Introduction: Economic Efficiency

- Three types of efficiencies:
	- Allocative efficiency - efficient allocation of resources
		- Anti-competitive behaviour converts consumer surplus to producer surplus
		- Total surplus decreases due to dead-weight loss
	- Productive efficiency - effiencient employment of resources
		- Uncompetitive marketplace reduces the pressure on companies to be more productive
	- Dynamic efficiency - creation of new processes/products that improve efficiency
		- An uncompetitive marketplace reduces the pressure on companies to innovate
- EU emphasises consumer welfare; some other jurisdictions focus more on other aspects such as total welfare.

##EU Competition Law Goals

- Economic efficiency
	- Commission, Art 81(3) guidelines, para 13 v _Case C-501/06 P GlaxoSmithKline v Commission_
- Single market
	- _Consten and Grundiv v Commission_
	- Grundiv appointed Consten as their exclusive distributor in France (with other exclusive distributors in other states). ECJ held that the single market was an overriding concern and such conduct was unacceptable despite the potential for greater economic efficiency. 

##Market Definition

- Two main areas:
	- Relevant product market
		- All products and/or services which are regarded as interchangeable or substitutable (para 7)
	- Relevant geographic market
		- The area in which the undertakings concerned are involved in the supply and demand of products or services, in which the conditions of the competition are sufficiently homogeneous and which can be distinguished from neighbouring areas because the conditions of competition are appreciably different in those areas. (para 8)
- Consider both demand and supply sides
	- Basic principles (para 13):
		- Demand substitution
			- Range of products which are viewed as substitutes by consumer.  (para 15)
			- Test with SSNIP test (para 17)
		- Supply substitution
			- Taken into account where its effects are equivalent to those of demand substitution in terms of effectiveness and immediacy. (para 20)
			- Suppliers are able to switch production to the relevant products and market them in the short term without incurring significant additional costs or risks in response to small and permanent changes in relative prices. (para 20)
			- If substitutability would require significant adjustments in tangible/intangible assets, additional investments, strategic decisions or time delays, it would not be considered at this stage (para 23)
		- Potential competition
- Apply SSNIP test (small but significant non-transitory increase in price) to determine relevant market
	- Would a 5-10% increase in price (over a year) be profitable
	- Identifies the smallest bundle of goods that a hypothetical monopolist would find profitable to raise prices for.
		- Assumes that present price is competitive (cellophane fallacy)
- Determine market power using market share as a first indicator
	- How durable is the market share
	- Are competitor shares rising or falling
	- Barriers to entry/exit
	- Buyer power
	
##Article 101 TFEU

###Introduction

- Burden of proof lies on the party relying on Art 101(1), after which it shifts to the party relying on the Art 101(3) exceptions

###Undertakings

- Original definition in _Hofner and Elser v Macrotron [1991]_
> ... every entity engaged in an economic activity, regardless of [its] ... legal status ... and the way in which it is financed.
	- Case concerns a state run job matching service with a monopoly granted by statute.
	- Economic activity is defined as "buying and selling on the marketplace", "offering goods (and presumably services) on the market" _Commission v Italy_

####Exceptions

- Single economic entity
	- A corporate group (e.g. parent and subsidiary) is treated as a single undertaking
	- When firms belong to the same concern, have the status of parent and subsidiary, and the firms form one economic unit within which the subsidiary has no real freedom to act, they will be treated as a single economic entity.
		- Degree of control required is unclear
- Agents
	- Read the Commission guidelines
	- Agent is person vested with power to negotiate and/or conclude contracts on behalf of another person for the purchase/sale of goods and services
	- Issue is financial risk borne (not just transferred or assumed i.e. risks originally borne by the agent prior to entering the agency agreement likely still be caught) by the "agent", which must be negligable or insignificant (except for risks specific to the activity of providing agency services)
		- Contract-specific risks
		- Market-specific risks
		- Other risks
		- Exceptions:
			- Where the agency agreement facilitates collusion. All colluding parties will be liable for anticompetitive behaviour.
			- Exclusive supplier agreements where the agent has significant market power.
- Employees acting at the direction of the firm
- Not economic activity if 
	- State is acting in imperium (core state functions)
	- When state is not buying or selling on the marketplace _Commission v FENIN_
		- Spanish national health service was held not to be an economic entity because they are buying but not selling on the marketplapce since the service was provided for free.
		- Contrast with _Hofner_
		- Consider that foreigners would have to pay for the services so they are also sold on the market to a certain extent

####Normative concerns

- Goals of competition policy and how they influence the determination of what constitutes an undertaking
- See _Glaxo_ [2009]:
> Art 101 "aims to protect not only the interests of competitors or of consumers, but also the structure of the market and, in so doing, competition as such."
- Commission welfare?

###Restrictions of Competition

####Restriction by Object

- Some forms of collusion can be regarded as, by their very nature, injurious to normal competition _Groupement des Cartes Bancaires_ (2014)
	- Read category restrictively
	- Do you need to look at the legal and economic context? 
		- Cartes Bancaires says no but Consten and Grundiv says yes.
- Likely to cause harm to market competition? _T-Mobile_ (CHECK CASE)
- ECJ overturned general court in _Glaxo_ which held that it is necessary to conduct an abridged but real analysis of the effects.
- Judge Easterbrook (US) suggests efficient rules: rebuttable presumptions

####Restriction by Effect