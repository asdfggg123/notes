#Article 101

##General

- Language of the statutory provision: agreements, decisions, and concerted practices
- Agreement means a concurrence of wills between at least two parties, the form in which it is manifested being unimportant so long as it constitutes the faithful expression of parties' intention. _Bayer v Commission_, _Spanish Glaxo_
	- Not limited to legally enforceable contracts but also includes:
		- Gentlemen's agreements _Chemiefarma_
		- Mutual understandings, even where there are no enforcement mechanisms _PVC_ (Commission decision)
		- Oral agreements _Tepea_
		- Expired agreements with lingering effects _Hercules_
	- Agreement exists even if one or more undertaking was forced into it. _Musique diffusion francaise_
	- Non-implementation of agreement does not negate its existence _Industrial and medical gases_ (Commission decision)
	- Key issue is whether there was a genuine agreement between parties:
		- Unilateral conduct, which is not caught by Art 101, is often implemented through agreements. Look to the substance of the conduct rather than whether there was a legal agreement in place. 
			- The fact that all the circumstances taken together created the effect of an export ban does not necessarily mean that there was an agreement to that effect, especially where some of the involved undertakings have their conduct restrained by regulatory requirements.
		- Public distancing may be an indication that there was no concurrence of wills
		- BMW v Commission: letter was not signed but terms in the letter were complied with. All parties were in breach.
		- Bayer v Commission: Attempts to circumvent terms of the purported agreement may be evidence that there is no concurrence of wills even if parties are in continued business relations
	- Note the possibility of finding a single overall agreement:
		- Presence of a single overall agreement is an objective fact, not a discretionary decision of the Commission. 
		- Conditions per _Term Relocations_ are:
			- Overall plan pursuing a common objective
			- Intentional contribution of the undertaking to that plan
			- Awareness of the offending conduct of the other participants
		- Implications:
			- Each infringing undertaking is responsible for the overall cartel
			- Quantum of fines
				- Duration of infringement may be longer
				- Anti-competitive effects may be more severe
			- Commission may impose fines in respect of illegal practices that would otherwise be time-barred
			- It may be easier to satisfy the Art 101 requirements.
	- While agreements are conceptually distinct from concerted practices, the GC in _PVC_ held that it is possible to classify conduct as agreement and/or concerted practice. Not necessary to draw a distinct line between agreements and concerted practices. 
- Decisions by associations of undertakings may be another way in which coordination takes place.
	- Examples:
		- Trade associations
		- Professional associations _Wouters_
		- Agricultural Co-operative _Gottrup-Klim_
	- Association itself does not need to perform economic activity. In determining the quantum of fines, the Commission may take into account members' turnover, even if decisions by the undertaking are not binding upon them. 
- Concerted practices allow for conduct not attributable to an agreement or a decision to still be prohibited. This reflects the difficulty of establishing the presence of an agreement where there are only loose informal understandings, and the possibility that cartels may proactively destroy incrimminating evidence that could establish the presence of an agreement.
	- It is a form of coordination between undertakings which, without having reached the stage where an agreement properly so-called has been concluded, knowingly substitutes practical cooperation between them for the risks of competition _Dyestuffs_
		- This may include any direct or indirect contact between undertakings or operators, the object or effect of which is to influence the conduct of an actual or potential competitor or to disclose to such a competitor the course of conduct which they themselves have decided to adopt or contemplate adopting on the market. _Sugar Cartel_
	- There needs to be some element of reciprocity
	- Concerted practice is caught by Art 101 even in the absence of anti-competitive effects on the market _Huls_
		- Rebuttable presumption that there is a causal connection between parties' contact and their subsequent common market conduct _ANIC v Commission_
			- Presumption can be rebutted by adducing evidence of independant decision making. 
	- Normative evaluation
		- Concept may not be necessary as agreement already has a wide definition. However, the key difference is that concerted practices includes conduct with an element of mutuality but no actual agreement.
			- These are two forms of collusion with the same nature, distinguishable only by the intensity and the forms in which they manifest _ANIC_ and _T-mobile_
		- Emphasis on intependent decisionmaking by parties
		- Potential difficulties may arise as perfect competition and collusion are outwardly very similar _ICI v Commission_
	
##Horizontal Agreements

- Generally, look to see if the agreement achieves the following outcomes:
	- Facilitate anti-competitive arrangements
	- Increase transparancy of the market so as to allow easier market monitoring by undertakings
	- Facilitate punishment

###Cartels

- General consensus that cartels should be prohibited
	- Incentives for businesses and consumers are often in tension
	- General policy of placing consumer interests before companies' commercial interests
	- Cartels are likely to reduce overall welfare (imperfect competition leads to dead weight loss)
- Within a cartel:
	- Some firms would benefit more than others
	- Costs will be incurred in negotiations, market surveillance, and enforcement
		- These functions are especially difficult of goods are not homogenous or if market size is unstable
		- Costs increase with the number of firms within the cartel, and they tend to break down in the long term, although some cartels have proven to be very sustainable (_Soda Ash_).

####Horizontal Price Fixing

- For background, price fixing was historically thought to be beneficial, providing stability and facilitating rational decisionmaking. 
- Any form of price-fixing is caught, even if it is only temporary (decisions to discount or not to discount goods) 
	- Agreement may be caught even if it only facilitates or indirectly distorts price competition. _Glass Containers_
- In regulated markets, the Commission would be particularly careful in their examination to ensure that firms do nothing further to limit competition. _British Sugar_
- Generally difficult to justify horizontal price fixing under Art 101(3) outside a few notable exceptions such as _Uniform eurocheques_, _AuA/LH_ and some others

####Horizontal Market Sharing

- Market sharing may involve dividing the market geographically or by other categories such as classes of customers, etc. 
- Market sharing is easier to police than price fixing and leaves no room for competition (quality competition still possible for price fixing)
- Market sharing is viewed particularly seriously in the EU as they involve the artificial partitioning of the common market. 
- Market sharing in any form and context is caught by Art 101.
- Market sharing is unlikely to be justifiable under Art 101(3) due to the single market imperative. In exceptional circumstances, Art 101(3) may be apaplicable to market sharing that is indispensable for improvements in efficiency. 

####Quotas and Other Restrictions on Production

- Horizontal agreements to limit production need careful monitoring because over-production by members would cause prices to fall.
	- Quotas are often implemented in conjunction with price fixing agreements.
	- Quotas may be either absolute or proportional
	- Surveillance and enforcement systems are often necessary to prevent cheating.
- Irrelevant that quotas are not always meticulously observed _White Lead_

####Collusive Tendering

- Horizontal agreements to collaborate over responses to invitations to tender.
	- Such agreements have the effect of raising prices and lowering overhead costs for contractors

###Information Exchanges

- Note that information (and by extension information exchange) can be highly beneficial to the competitive process and therefore the context is very important. 
- Information exchange may be direct or indirect (through a trade association, third party, or even suppliers/customers)
	- The exchange of information may be the main economic function of an agreement in itself or part of a wider agreement. 
- Information exchange may generate efficiencies or restrict competition
	- Generate efficiencies:
		- Reduce information asymmetries e.g. Asnef-Equifax on the sharing of customer solvency information
		- Benchmarking may improve internal efficiency
		- Allow cost savings through reducing inventories or better predicting demand
		- Improve customer choice
	- Restrict competition
		- Sharing of strategic data diminishes competition _Bananas case_, _T-mobile_
			- Particularly relevant factor is whether the information shared would ordinarily be considered to be confidential _LIBOR_
		- Information exchange may be in support of a cartel, facilitating negotiations, surveillance, or enforcement operations.
		- Sharing of public data generally permitted (para 92)
			- Data is genuinely public if the costs of obtaining it are the same for all competitors and customers.
		- The sharing of data my one firm may be sufficient to establish a concerted practice, an undertaking seeking to avoid liability under Art 101 should distance itself publicly from the practice. 
- Assessment of the competition effects of information exchange is highly fact specific. 
	- Consider whether market conditions favour coordination. Relevant factors under the Horizontal Guidelines include:
		- Market transparency, concentration, complexity, stability, symmetry
		
##Vertical Agreements

- Remember that Art 101 does not apply to agreements within a single economic entity.
- Art 101 applies to all agreements capable of distorting competition, even those between firms operating at different levels of the economy and which are not competitors. _Consten v Gundig_

###Commercial Agents

- Where an agent acts on behalf of a principal, it is treated under EU competition law as forming the same economic entity as the principal.
- Commission Vertical Guidelines provides that the determining factor in assessing whether Art 101(1) applies is the financial or commercial risk borne by the agent in relation to the activities for which it has been appointed as an agent by the principal (_CEPSA_, para 13).
	- The agent may only bear insignificant risks of the following three types for the agency to fall outside Art 101:
		- Contract specific risks that are directly related to the contracts concluded
		- Risks related to market specific investments
		- Risks related to other activities that the principal requires the agent to perform on the same market. 
	- No limit on any risks borne by the agent that are related to the activity of prividing agency services in general. 
	- Only obligations imposed on the agent which delineate the scope of the agency relationship are inherent to the agency agreement and fall outside Art 101. 
- Art 101 may be infringed in the case of an agency agreement where:
	- There are exclusivity provisions that could lead to foreclosure of the market (para 19-20)
	- The agency agreement facilitates collusion
	
###Independant Distributors

####Policy Considerations

- Detriment to competition:
	- ECJ in _Allianz Hungaria_: Vertical agreements are often less damaging to competition than horizontal agreements. 
		- Aside from agreements that restrict competition by object, vertical agreements are likely to raise competition concerns only where there is a degree of market power at the level of the supplier or buyer or at both levels. 
		- Vertical agreements may harm competition by restricting inter-brand and intra-brand competition. 
			- There could be anti-competitive foreclosure of other suppliers or buyers by raising barriers to entry
			- Softening of competition between the supplier and its competitors and/or facilitation of collusion (inter-brand competition)
			- Softening of competition between the buyer and its competitors and/or facilitation of collusion (intra-brand competition)
	- As a general proposition, competition law is more concerned with restrictions of inter-brand competition than intra-brand competition: restrictions of intra-brand competition would only raise concerns where inter-brand competition is weak
	- Single market imperative: Commission is concerned with preventing firms from using vertical agreements to artificially segment the market. 
	- Softening of competition could harm consumers by raising wholesale prices, depriving consumers of choice, lowering quality, or reducing the level of competition. 
	- In the following circumstances, vertical restraits are more likely to have negative effects:
		- Exclusive arrangements are generally more anti-competitive than non-exclusive arrangements.
		- Combination of vertical restraints will usually increase their individual negative effects.
		- Negative effects arising from vertical restraints are reinforced when several suppliers and their respective buyers organise their trade in a similar way, leading to cumulative effects within the market.
- Benefits to competition:
	- Vertical restraits often have positive effects, in particular by promoting non-price competition and improved quality of service
	- Vertical restraints could resolve the free rider problem in respect of pre-sales services (only where the product is relatively new or complex and of reasonably high value).
	- Vertical restraints could facilitate the opening up and entering into new markets by protecting parties' "first time investments".
	- Vertical restraints could promote parties to make client-specific investments.
	- Where know-how is supplied by one firm to another, it may be necessary to implement a non-compete obligation on the recipient. 
	- Vertical restraints could be used to align the incentivies of parties (e.g. maximum resale price)
	- Economies of scale in distribution.
	- Where one contracting party is providing capital to the other party, they may wish to impose a non-compete obligation. 
	- Vertical restraints help promote the brand image of a product and increase its attractiveness to consumers by bringing about uniformity and quality standardisation. 
	
####Types of Vertical Restraints

- Exclusive distribution agreements (territorial or customer allocation) (para 135)
	- Reduce intrabrand competition (particularly of concern where inter-brand competition is low)
	- Could lead to partitioning of the common market
	- Could foreclose other distributors
	- Does not restrict competition by object _STM_
	- Consider market conditions to see if competition is restricted by effect
		- Presence of strong competitors
		- Risk of collusion
		- Maturity of market
- Selective distribution agreements are often deployed by producers of branded products. The producer establishes a system in which the products can be bought and resold only by authorised distributors and retailers to other members of the system or the final consumer.
	- Restric intra-brand competition
	- May foreclose access to the market
	- May soften competition and/or facilitate collusion.
	- Key factor determining whether Art 101 is applicable is whether the system is purely qualitative or quantitative
		- Per _Metro v Commission_, system is purely qualitative where:
			- The product is of a type that justifies the restriction
				- Technically complex goods that require specialist sales staff and/or after sales service
				- Goods are such that the restriction is necessary to protect the brand image e.g. luxury goods
			- The criteria set by the supplier is purely qualitative in nature, laid down uniformly for all potential retailers and applied in a non-discriminatory manner
				- This requirement raises difficulties when the criteria involves stocking obligations (minimum quantity, range of products)
			- Any restrictions that are imposed on authorised distributors go no further than what is objectively necessary to protect the quality of the product.
				- Where criteria is so extensive such that the supplier could control prices, it would likely go further than what is objectively necessary. 
		- ECJ in _Pierre Fadre_ held that selective distribution systems have the object of restricting copmetition in the absence of objective justification
			- Dublious judgment unsupported by precedent or economic analysis (Whish and Bailey)
	- Quantitive selective distribution systems may be justified under Art 101(3) where it improves distribution of goods _YSL_
- Single branding agreements require the buyer to concentrate its orders for a particular product on one supplier. Examples are exclusive purchase and non-compete obligations. (para 129-150)
	- May restrict inter-brand competition by foreclosing access to distributors (and hence the market) for other suppliers
	- May soften competition
	- May facilitate collusion
	- May limit in-store inter-brand competition
	- Consider economic context. Relevant factors include:
		- Whether supplier is an unavoidable trading partner (producer of a must-stock item)
		- Duration of the agreement
		- Market share of the distributor
		- Whether there are parallel networks that cumulatively foreclose the market. 
		- Actual/potential competition
		- Barriers to entry for the downstream market