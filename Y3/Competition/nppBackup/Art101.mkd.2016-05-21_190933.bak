#Article 101

##General

- Language of the statutory provision: agreements, decisions, and concerted practices
- Agreement means a concurrence of wills between at least two parties, the form in which it is manifested being unimportant so long as it constitutes the faithful expression of parties' intention. _Bayer v Commission_, _Spanish Glaxo_
	- Not limited to legally enforceable contracts but also includes:
		- Gentlemen's agreements _Chemiefarma_
		- Mutual understandings, even where there are no enforcement mechanisms _PVC_ (Commission decision)
		- Oral agreements _Tepea_
		- Expired agreements with lingering effects _Hercules_
	- Agreement exists even if one or more undertaking was forced into it. _Musique diffusion francaise_
	- Non-implementation of agreement does not negate its existence _Industrial and medical gases_ (Commission decision)
	- Key issue is whether there was a genuine agreement between parties:
		- Unilateral conduct, which is not caught by Art 101, is often implemented through agreements. Look to the substance of the conduct rather than whether there was a legal agreement in place. 
			- The fact that all the circumstances taken together created the effect of an export ban does not necessarily mean that there was an agreement to that effect, especially where some of the involved undertakings have their conduct restrained by regulatory requirements.
		- Public distancing may be an indication that there was no concurrence of wills
		- BMW v Commission: letter was not signed but terms in the letter were complied with. All parties were in breach.
		- Bayer v Commission: Attempts to circumvent terms of the purported agreement may be evidence that there is no concurrence of wills even if parties are in continued business relations
	- Note the possibility of finding a single overall agreement:
		- Presence of a single overall agreement is an objective fact, not a discretionary decision of the Commission. 
		- Conditions per _Term Relocations_ are:
			- Overall plan pursuing a common objective
			- Intentional contribution of the undertaking to that plan
			- Awareness of the offending conduct of the other participants
		- Implications:
			- Each infringing undertaking is responsible for the overall cartel
			- Quantum of fines
				- Duration of infringement may be longer
				- Anti-competitive effects may be more severe
			- Commission may impose fines in respect of illegal practices that would otherwise be time-barred
			- It may be easier to satisfy the Art 101 requirements.
	- While agreements are conceptually distinct from concerted practices, the GC in _PVC_ held that it is possible to classify conduct as agreement and/or concerted practice. Not necessary to draw a distinct line between agreements and concerted practices. 
- Decisions by associations of undertakings may be another way in which coordination takes place.
	- Examples:
		- Trade associations
		- Professional associations _Wouters_
		- Agricultural Co-operative _Gottrup-Klim_
	- Association itself does not need to perform economic activity. In determining the quantum of fines, the Commission may take into account members' turnover, even if decisions by the undertaking are not binding upon them. 
- Concerted practices allow for conduct not attributable to an agreement or a decision to still be prohibited. This reflects the difficulty of establishing the presence of an agreement where there are only loose informal understandings, and the possibility that cartels may proactively destroy incrimminating evidence that could establish the presence of an agreement.
	- It is a form of coordination between undertakings which, without having reached the stage where an agreement properly so-called has been concluded, knowingly substitutes practical cooperation between them for the risks of competition _Dyestuffs_
		- This may include any direct or indirect contact between undertakings or operators, the object or effect of which is to influence the conduct of an actual or potential competitor or to disclose to such a competitor the course of conduct which they themselves have decided to adopt or contemplate adopting on the market. _Sugar Cartel_
	- There needs to be some element of reciprocity
	- Concerted practice is caught by Art 101 even in the absence of anti-competitive effects on the market _Huls_
		- Rebuttable presumption that there is a causal connection between parties' contact and their subsequent common market conduct _ANIC v Commission_
			- Presumption can be rebutted by adducing evidence of independant decision making. 
	- Normative evaluation
		- Concept may not be necessary as agreement already has a wide definition. However, the key difference is that concerted practices includes conduct with an element of mutuality but no actual agreement.
			- These are two forms of collusion with the same nature, distinguishable only by the intensity and the forms in which they manifest _ANIC_ and _T-mobile_
		- Emphasis on intependent decisionmaking by parties
		- Potential difficulties may arise as perfect competition and collusion are outwardly very similar _ICI v Commission_
	
##Horizontal Agreements

- Generally, look to see if the agreement achieves the following outcomes:
	- Facilitate anti-competitive arrangements
	- Increase transparancy of the market so as to allow easier market monitoring by undertakings
	- Facilitate punishment

###Cartels

- General consensus that cartels should be prohibited
	- Incentives for businesses and consumers are often in tension
	- General policy of placing consumer interests before companies' commercial interests
	- Cartels are likely to reduce overall welfare (imperfect competition leads to dead weight loss)

####Horizontal Price Fixing

- 

##Vertical Agreements