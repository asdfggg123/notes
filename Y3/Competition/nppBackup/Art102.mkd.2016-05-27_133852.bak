#Art 102

##Introduction

###Goals

- Consumer welfare
	- Commission Article 101(3) Guidelines, para 13; DG COMP Discussion Paper, para 4 ��the objective of Article 82 is the protection of competition on the market as a means of enhancing consumer welfare��
- Protection of competition
	- Statements to this effect can be found in various judgments including Deutsche Telekom, TeliaSonera, Post Danmark.
		- Courts have stressed that Art 102 only protects as-efficient competitors
	- Comission Art 102 Enforcement Priorities: FOcus is on ensuring that markets function properly and that consumers benefit from the efficiency and productivity which result from effective competition. What really matters is protecting an effective competitive process (para 5-6)
	- Commission will normally only intervene where the conduct has been or is capable of hampering from as efficient competitors (para 23)
- Protection of competitors?
	- Does Art 102 subjectt dominant firms to a competitive handicap?
	- US Assistant Attorney General (Antitrust) suggested that the EU is primarily concerned with the protection of competitors
	- In _Intel_, the General Court rejected the as-efficient competitor test for exclusivity rebates. 
		- Whish & Bailey argue that the primary mischief is exclusivity and not the quantum of the rebate. This case is not inconsistent with the commission's focus on protection of competition. 
- Integration of national markets _Sot Lelos Kai_

###Non-Legislative Documents

- Commission Guidance on Art 102 Enforcement Priorities
	- Guidance is not a source of law or even guideline on the application of the law.
	- Possibility for legal uncertainty as the Courts may apply different standards
		- In _TeliaSonera_, the court's interpretation of margin squeeze is stricter than the Guidance.
		- In _Tomra_ and Intel_, the courts applied stricter standards to the application of Art 102 to exclusivity rebates than the Guidance suggests the commission would adopt. 
		- Akman argues that the publication of the Guidance leaves undertakings more confused about the law in this area than before
	- Whish & Bailey argue that the Guidance simply sets out how the Commission would apply its limited resources. Guidance is not a binding source of law, although it may have an influence on the future application of Art 102.
		- AG Mazak in _TeliaSonera_ said that the Guidance may provide a useful point of reference. 
- DG Comp Discussion Paper on exclusionary abuses
	- Not an official document of the commission
	- Not guidelines on the application of Art 102
	- Paper noted four points of consensus:
		- Art 102 should not be applied simply to protect competitors as such
		- Dominant firm that defeats its rivals as a result of its greater efficiency ought not to be condemned as acting abusively (also mentioned in _Post Danmark_
		- Economics of abuse are sufficiently complex that there should not be per-se rules
		- Exclusionary behaviour should only be condemned where it can be demonstrated that the behaviour has had, or is likely to have, a significant anti-competitive effect. 
		
###Elements of an Art 102 Infringement

####Undertakings

- Undertakings mean the same for both Art 101 and Art 102. 
- Statutory monopolies are not exempt from Art 102
- Per Art 106(2), MS cannot confer immunity on undertakings from Art 102 outside the narrow provisions in Art 106(2)

####Effect on Inter-State Trade

- Same definition as in Art 101
- Commission Guideline on effect on trade aspect also applies
	- Appreciability concept is implied (para 18)
	- No quantitative guideline on appreciability
- In _Commercial Solvents_, ECJ held t hat this requirement would be satisfied where the conduct brough about an alteration in the structure opf competition in the internal market. 

####Dominance

- Art 102 only applies where one or more undertakings are of a dominant position within the internal market or a substantial part of it
- Basic position given in _United Brands v Commission_: The dominant position relates to a position of economic strength enjoyed by an undertaking which enables it to prevent effective competition being maintained on the relevant market by affording it the power to behave to an appreciable extent independently of its competitors, customers, and ultimate of its consumers.
	- Economic concept of having substantial market power (capable of profitably increasing prices above the competitive level for a significant period of time) (para 10)
- Two stage test per _Continental Can_:
	- Define the relevant market
		- Be especially wary of the cellophane fallacy
	- Determine dominance by assessing competitive restraints (para 12)
		- Actual competitors
			- There may be a statutory monopoly
			- Where there is no actual monopoly, market shares provide a useful first indication of market structure (para 13)
				- Large market shares are in themselves, and save in exceptional circumstances, evidence of the existence of a dominant position _Hoffmann-La Roche_
				- ECJ in _AKZO_ held that a market share of 50% would lead to a presumption of dominance save in exceptional circumstances. 
				- ECJ in _United Brands_ held that the firm with a market share between 40-45% was dominant, although other factors were considered to be significant and the market share alone would not have been able to sustain a finding of dominance. 
			- Market conditions must be taken into account, including market dynamics, product differentiation, market share trends.
		- Potential competitors
		- Countervailing buyer power