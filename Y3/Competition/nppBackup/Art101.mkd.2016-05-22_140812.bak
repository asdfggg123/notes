#Article 101

##General

- Language of the statutory provision: agreements, decisions, and concerted practices
- Agreement means a concurrence of wills between at least two parties, the form in which it is manifested being unimportant so long as it constitutes the faithful expression of parties' intention. _Bayer v Commission_, _Spanish Glaxo_
	- Not limited to legally enforceable contracts but also includes:
		- Gentlemen's agreements _Chemiefarma_
		- Mutual understandings, even where there are no enforcement mechanisms _PVC_ (Commission decision)
		- Oral agreements _Tepea_
		- Expired agreements with lingering effects _Hercules_
	- Agreement exists even if one or more undertaking was forced into it. _Musique diffusion francaise_
	- Non-implementation of agreement does not negate its existence _Industrial and medical gases_ (Commission decision)
	- Key issue is whether there was a genuine agreement between parties:
		- Unilateral conduct, which is not caught by Art 101, is often implemented through agreements. Look to the substance of the conduct rather than whether there was a legal agreement in place. 
			- The fact that all the circumstances taken together created the effect of an export ban does not necessarily mean that there was an agreement to that effect, especially where some of the involved undertakings have their conduct restrained by regulatory requirements.
		- Public distancing may be an indication that there was no concurrence of wills
		- BMW v Commission: letter was not signed but terms in the letter were complied with. All parties were in breach.
		- Bayer v Commission: Attempts to circumvent terms of the purported agreement may be evidence that there is no concurrence of wills even if parties are in continued business relations
	- Note the possibility of finding a single overall agreement:
		- Presence of a single overall agreement is an objective fact, not a discretionary decision of the Commission. 
		- Conditions per _Term Relocations_ are:
			- Overall plan pursuing a common objective
			- Intentional contribution of the undertaking to that plan
			- Awareness of the offending conduct of the other participants
		- Implications:
			- Each infringing undertaking is responsible for the overall cartel
			- Quantum of fines
				- Duration of infringement may be longer
				- Anti-competitive effects may be more severe
			- Commission may impose fines in respect of illegal practices that would otherwise be time-barred
			- It may be easier to satisfy the Art 101 requirements.
	- While agreements are conceptually distinct from concerted practices, the GC in _PVC_ held that it is possible to classify conduct as agreement and/or concerted practice. Not necessary to draw a distinct line between agreements and concerted practices. 
- Decisions by associations of undertakings may be another way in which coordination takes place.
	- Examples:
		- Trade associations
		- Professional associations _Wouters_
		- Agricultural Co-operative _Gottrup-Klim_
	- Association itself does not need to perform economic activity. In determining the quantum of fines, the Commission may take into account members' turnover, even if decisions by the undertaking are not binding upon them. 
- Concerted practices allow for conduct not attributable to an agreement or a decision to still be prohibited. This reflects the difficulty of establishing the presence of an agreement where there are only loose informal understandings, and the possibility that cartels may proactively destroy incrimminating evidence that could establish the presence of an agreement.
	- It is a form of coordination between undertakings which, without having reached the stage where an agreement properly so-called has been concluded, knowingly substitutes practical cooperation between them for the risks of competition _Dyestuffs_
		- This may include any direct or indirect contact between undertakings or operators, the object or effect of which is to influence the conduct of an actual or potential competitor or to disclose to such a competitor the course of conduct which they themselves have decided to adopt or contemplate adopting on the market. _Sugar Cartel_
	- There needs to be some element of reciprocity
	- Concerted practice is caught by Art 101 even in the absence of anti-competitive effects on the market _Huls_
		- Rebuttable presumption that there is a causal connection between parties' contact and their subsequent common market conduct _ANIC v Commission_
			- Presumption can be rebutted by adducing evidence of independant decision making. 
	- Normative evaluation
		- Concept may not be necessary as agreement already has a wide definition. However, the key difference is that concerted practices includes conduct with an element of mutuality but no actual agreement.
			- These are two forms of collusion with the same nature, distinguishable only by the intensity and the forms in which they manifest _ANIC_ and _T-mobile_
		- Emphasis on intependent decisionmaking by parties
		- Potential difficulties may arise as perfect competition and collusion are outwardly very similar _ICI v Commission_
	
##Horizontal Agreements

- Generally, look to see if the agreement achieves the following outcomes:
	- Facilitate anti-competitive arrangements
	- Increase transparancy of the market so as to allow easier market monitoring by undertakings
	- Facilitate punishment

###Cartels

- General consensus that cartels should be prohibited
	- Incentives for businesses and consumers are often in tension
	- General policy of placing consumer interests before companies' commercial interests
	- Cartels are likely to reduce overall welfare (imperfect competition leads to dead weight loss)
- Within a cartel:
	- Some firms would benefit more than others
	- Costs will be incurred in negotiations, market surveillance, and enforcement
		- These functions are especially difficult of goods are not homogenous or if market size is unstable
		- Costs increase with the number of firms within the cartel, and they tend to break down in the long term, although some cartels have proven to be very sustainable (_Soda Ash_).

####Horizontal Price Fixing

- For background, price fixing was historically thought to be beneficial, providing stability and facilitating rational decisionmaking. 
- Any form of price-fixing is caught, even if it is only temporary (decisions to discount or not to discount goods) 
	- Agreement may be caught even if it only facilitates or indirectly distorts price competition. _Glass Containers_
- In regulated markets, the Commission would be particularly careful in their examination to ensure that firms do nothing further to limit competition. _British Sugar_
- Generally difficult to justify horizontal price fixing under Art 101(3) outside a few notable exceptions such as _Uniform eurocheques_, _AuA/LH_ and some others

####Horizontal Market Sharing

- Market sharing may involve dividing the market geographically or by other categories such as classes of customers, etc. 
- Market sharing is easier to police than price fixing and leaves no room for competition (quality competition still possible for price fixing)
- Market sharing is viewed particularly seriously in the EU as they involve the artificial partitioning of the common market. 
- Market sharing in any form and context is caught by Art 101.
- Market sharing is unlikely to be justifiable under Art 101(3) due to the single market imperative. In exceptional circumstances, Art 101(3) may be apaplicable to market sharing that is indispensable for improvements in efficiency. 

####Quotas and Other Restrictions on Production

- Horizontal agreements to limit production need careful monitoring because over-production by members would cause prices to fall.
	- Quotas are often implemented in conjunction with price fixing agreements.
	- Quotas may be either absolute or proportional
	- Surveillance and enforcement systems are often necessary to prevent cheating.
- Irrelevant that quotas are not always meticulously observed _White Lead_

####Collusive Tendering

- Horizontal agreements to collaborate over responses to invitations to tender.
	- Such agreements have the effect of raising prices and lowering overhead costs for contractors

###Information Exchanges

- Note that information (and by extension information exchange) can be highly beneficial to the competitive process and therefore the context is very important. 
- Information exchange may be direct or indirect (through a trade association, third party, or even suppliers/customers)
	- The exchange of information may be the main economic function of an agreement in itself or part of a wider agreement. 
- Information exchange may generate efficiencies or restrict competition
	- Generate efficiencies:
		- Reduce information asymmetries e.g. Asnef-Equifax on the sharing of customer solvency information
		- Benchmarking may improve internal efficiency
		- Allow cost savings through reducing inventories or better predicting demand
		- Improve customer choice
	- Restrict competition
		- Sharing of strategic data diminishes competition _Bananas case_, _T-mobile_
			- Particularly relevant factor is whether the information shared would ordinarily be considered to be confidential _LIBOR_
		- Information exchange may be in support of a cartel, facilitating negotiations, surveillance, or enforcement operations.
		- Sharing of public data generally permitted (para 92)
			- Data is genuinely public if the costs of obtaining it are the same for all competitors and customers.
		- The sharing of data my one firm may be sufficient to establish a concerted practice, an undertaking seeking to avoid liability under Art 101 should distance itself publicly from the practice. 
- Assessment of the competition effects of information exchange is highly fact specific. 
	- Consider whether market conditions favour coordination. Relevant factors under the Horizontal Guidelines include:
		- Market transparency, concentration, complexity, stability, symmetry
		
##Vertical Agreements

- Remember that Art 101 does not apply to agreements within a single economic entity.

###Commercial Agents

- Where an agent acts on behalf of a principal, it is treated under EU competition law as forming the same economic entity as the principal.
- Commission Vertical Guidelines provides that the determining factor in assessing whether Art 101(1) applies is the financial or commercial risk borne by the agent in relation to the activities for which it has been appointed as an agent by the principal (para 13).
	- The agent may only bear insignificant risks of the following three types for the agency to fall outside Art 101:
		- Contract specific risks that are directly related to the contracts concluded
		- Risks related to market specific investments
		- Risks related to other activities that the principal requires the agent to perform on the same market. 
	- No limit on any risks borne by the agent that are related to the activity of prividing agency services in general. 