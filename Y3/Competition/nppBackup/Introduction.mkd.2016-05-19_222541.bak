#Competition law

##Course Structure

- Art 101 TFEU (Anti-competitive agreements) 
- Art 102 TFEU (Abuse of dominant position) 
- Merger control

##Introduction: Economic Efficiency

- Three types of efficiencies:
	- Allocative efficiency - efficient allocation of resources
		- Anti-competitive behaviour converts consumer surplus to producer surplus
		- Total surplus decreases due to dead-weight loss
	- Productive efficiency - effiencient employment of resources
		- Uncompetitive marketplace reduces the pressure on companies to be more productive
	- Dynamic efficiency - creation of new processes/products that improve efficiency
		- An uncompetitive marketplace reduces the pressure on companies to innovate
- EU emphasises consumer welfare; some other jurisdictions focus more on other aspects such as total welfare.

##EU Competition Law Goals

- Economic efficiency
	- Commission, Art 81(3) guidelines, para 13 v _Case C-501/06 P GlaxoSmithKline v Commission_
- Single market
	- _Consten and Grundiv v Commission_
	- Grundiv appointed Consten as their exclusive distributor in France (with other exclusive distributors in other states). ECJ held that the single market was an overriding concern and such conduct was unacceptable despite the potential for greater economic efficiency. 

##Market Definition

- Two main areas:
	- Relevant product market
		- All products and/or services which are regarded as interchangeable or substitutable (para 7)
		- Question is whether the product is singled out by such special features distinguishing it from other products that it is only to a limited extent interchangeable with them and is only exposed to their competition in a way that is hardly perceptible _United Brands_
	- Relevant geographic market
		- The area in which the undertakings concerned are involved in the supply and demand of products or services, in which the conditions of the competition are sufficiently homogeneous and which can be distinguished from neighbouring areas because the conditions of competition are appreciably different in those areas. (para 8), _United Brands_
- Consider both demand and supply sides
	- Basic principles (para 13):
		- Demand substitution
			- Range of products which are viewed as substitutes by consumer.  (para 15)
			- Test with SSNIP test (para 17)
		- Supply substitution
			- Taken into account where its effects are equivalent to those of demand substitution in terms of effectiveness and immediacy. (para 20)
			- Suppliers are able to switch production to the relevant products and market them in the short term without incurring significant additional costs or risks in response to small and permanent changes in relative prices. (para 20)
			- If substitutability would require significant adjustments in tangible/intangible assets, additional investments, strategic decisions or time delays, it would not be considered at this stage (para 23)
		- Potential competition
			- Not considered at the market definition stage (para 24)
- Apply SSNIP test (small but significant non-transitory increase in price) to determine relevant market
	- Would a 5-10% increase in price (over a year) be profitable
	- Identifies the smallest bundle of goods that a hypothetical monopolist would find profitable to raise prices for.
		- Assumes that present price is competitive (cellophane fallacy)
		- Pricing data may not be available _British Interactive Broadcasting_
- Determine market power using market share as a first indicator
	- How durable is the market share
	- Are competitor shares rising or falling
	- Barriers to entry/exit
	- Buyer power
- Issues to be considered when assessing market power per Enforcement Priorities Guidance:
	- Actual competitors
		- Market shares provide a useful first indication of the market structure. 
			- Consider market conditions 
				- Market dynamics
				- Degree of product differentiation.
				- Development of market shares
			- ICN recommends that determination of market power should be based on a comprehensive analysis.
		- Herfindahl-Hirschman Index
			- Thresholds are: 1000, 1800
			- Sum of [number of undertakings with a certain market share] x [market share]^2
			- Limited use since it does not reflect market dynamics and innovation
	- Potential competitors
		- Barriers to entry
		- Network effects, economies of scale/scope
		- Cost/other impediments
		- Conduct of dominant undertakings
	- Countervailing buyer power
		- Bargaining power of customers
	
##Article 101 TFEU

###Introduction

- Burden of proof lies on the party relying on Art 101(1), after which it shifts to the party relying on the Art 101(3) exceptions

###Undertakings

- Original definition in _Hofner and Elser v Macrotron [1991]_
> ... every entity engaged in an economic activity, regardless of [its] ... legal status ... and the way in which it is financed.
	- Case concerns a state run job matching service with a monopoly granted by statute.
	- Economic activity is defined as "buying and selling on the marketplace", "offering goods (and presumably services) on the market" _Commission v Italy_

####Exceptions

- Single economic entity
	- A corporate group (e.g. parent and subsidiary) is treated as a single undertaking
	- When firms belong to the same concern, have the status of parent and subsidiary, and the firms form one economic unit within which the subsidiary has no real freedom to act, they will be treated as a single economic entity. _Centrafarm v Sterling Drug_
	- Employees are part of the same economic entity as their employer _Jean Claude Becu_
	- Implications of doctrine:
		- While an agreement between firms that form a single economic entity does not infringe Art 101(1), agreements between the subsidiary and external parties may fall within the scope of the Art. 
		- A parent can be liable for the conduct of its subsidiary.
		- Art 23(2) Reg 1/2003: Maximum permitted fine is assessed with regards to the entire undertaking
		- Jurisdiction issues.
		- Desirable to attribute responsibility for infringements to the highest possible entity within a corporate group.
		- A single economic entity is considered as one party for the purposes of block exemptions.
- Agents
	- Read the Commission guidelines
	- Agent is person vested with power to negotiate and/or conclude contracts on behalf of another person for the purchase/sale of goods and services
	- Issue is financial risk borne (not just transferred or assumed i.e. risks originally borne by the agent prior to entering the agency agreement likely still be caught) by the "agent", which must be negligable or insignificant (except for risks specific to the activity of providing agency services)
		- Contract-specific risks
		- Market-specific risks
		- Other risks
		- Exceptions:
			- Where the agency agreement facilitates collusion. All colluding parties will be liable for anticompetitive behaviour.
			- Exclusive supplier agreements where the agent has significant market power.
- Associations of undertakings:
	- Is the entity an institutionalised form of coordination between undertakings with a commonality of itnerests _Mastercard v Commission_
- Not economic activity if 
	- State is acting in imperium (core state functions) _SILEX Sistemi_
	- Entity is providing social services on the basis of solidarity _Poucet_
		- Benefits must not depend on contributions
	- When state is not buying or selling on the marketplace _Commission v FENIN_
		- Spanish national health service was held not to be an economic entity because they are buying but not selling on the marketplapce since the service was provided for free.
		- Contrast with _Hofner_
		- Consider that foreigners would have to pay for the services so they are also sold on the market to a certain extent


####Normative concerns

- Goals of competition policy and how they influence the determination of what constitutes an undertaking
- See _Glaxo_ [2009]:
> Art 101 "aims to protect not only the interests of competitors or of consumers, but also the structure of the market and, in so doing, competition as such."
- Commission welfare?

###Restrictions of Competition

- Evaluate by considering all factors rather than just formal terms _Visa Europe v Commission_
- Focus is on economic effects
- Relevant does not need to be defined to show that an agreement restricts competition.
- Art 101 protects not only the interests of competitors or of consumers, but also the structure of the market, and in so doing, "competition as such"

####Restriction by Object

- Some forms of collusion can be regarded as, by their very nature, injurious to normal competition _Groupement des Cartes Bancaires_ (2014)
	- No need to show subjective intention, although it may be a relevant factor. 
	- Read category restrictively
	- Unclear if this is simply a legal presumption (para 51)
	- Do you need to look at the legal and economic context? 
		- Cartes Bancaires says no but Consten and Grundiv says yes.
		- Unclear what is the necessary extent of analysis
- Categories of restrictions by object cannot be reduced to an exhaustive list _Competition Authority v Beef Industry Development Society_
- Wide test in _T-Mobile_:
	- Does the agreement have the potential to have a negative impact on competition
	- Effects are only relevant as to the extent of the penalty. 
	- Unclear if the T-Mobile test is now obsolete following _Cartes Bancaires_
- ECJ overturned general court in _GSK_ which held that it is necessary to conduct an abridged but real analysis of the effects.
- Restrictions by object include:
	- Price fixing and exchange of information in relation to future prices _T-Mobile_, 
	- Market sharing, collective exclusive dealing _Beef Industry Development Society_
	- Export bans (within the internal market) _GSK_
	- Minimum resale price _Pronuptia de Paris v Schillgalis_
- Judge Easterbrook (US) suggests efficient rules: rebuttable presumptions
- Justification given by AG Kokott in _T-Mobile_:
	- Conserve resources of competition authorities and the justice system
	- Legal certainty allows parties to adapt their conduct accordingly

####Restriction by Effect

- Extensive analysis of market effects is necessary
	- See _Delimitis v Henninger Brau_:
		- Define relevant product and geographic market
		- Determine whether access to market was impeded
		- If market access was impeded, ask whether the greements entered into contributed to the foreclosure effect
- Need to establish a theory of harm
- Demonstrate restrictive effect in comparison with what the position would have been in the absence of the agreement (counterfactual) _O2 v Commission_
- Can demonstrate harm to potential competition, but this may not be a mere hyphothesis unsupported by evidence or analysis from which it might be concluded that the harm is a real, concrete possibility _Visa Europe v Commission_
- Exceptions:
	- Exclusive licence granted to a distributor might not infringe Art 101(1) where it seemed to be necessary for the penetration of a new area by an undertaking _STM v Maschinenbau Ulm_
	- Certain restructions may be permitted if they are ancillary restraints directly related and necessary to the implementation of a main (permitted) operation _Metropole v Commission_
	- Key issue is objective necessity: mere difficulty in implementation or lower profits without the restrictions would not be sufficient _Mastercard v Commission_
	- _Wouters_ (ECJ)
		- Case concerned a rule adopted by the Dutch Bar Council that prohibited lawyers in the Netherlands from entering into a partnership with non-lawyers.
		- ECJ appears to suggest that non-competition objectives can be balanced against a restriction of competition
		- Case concerned an objectively necessary restriction ancillery not to a commercial outcome but a regulatory function. 