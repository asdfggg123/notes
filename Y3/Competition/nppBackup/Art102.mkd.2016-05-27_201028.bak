#Art 102

##Introduction

###Goals

- Consumer welfare
	- Commission Article 101(3) Guidelines, para 13; DG COMP Discussion Paper, para 4 ��the objective of Article 82 is the protection of competition on the market as a means of enhancing consumer welfare��
- Protection of competition
	- Statements to this effect can be found in various judgments including Deutsche Telekom, TeliaSonera, Post Danmark.
		- Courts have stressed that Art 102 only protects as-efficient competitors
	- Comission Art 102 Enforcement Priorities: FOcus is on ensuring that markets function properly and that consumers benefit from the efficiency and productivity which result from effective competition. What really matters is protecting an effective competitive process (para 5-6)
	- Commission will normally only intervene where the conduct has been or is capable of hampering from as efficient competitors (para 23)
- Protection of competitors?
	- Does Art 102 subjectt dominant firms to a competitive handicap?
	- US Assistant Attorney General (Antitrust) suggested that the EU is primarily concerned with the protection of competitors
	- In _Intel_, the General Court rejected the as-efficient competitor test for exclusivity rebates. 
		- Whish & Bailey argue that the primary mischief is exclusivity and not the quantum of the rebate. This case is not inconsistent with the commission's focus on protection of competition. 
- Integration of national markets _Sot Lelos Kai_

###Non-Legislative Documents

- Commission Guidance on Art 102 Enforcement Priorities
	- Guidance is not a source of law or even guideline on the application of the law.
	- Possibility for legal uncertainty as the Courts may apply different standards
		- In _TeliaSonera_, the court's interpretation of margin squeeze is stricter than the Guidance.
		- In _Tomra_ and Intel_, the courts applied stricter standards to the application of Art 102 to exclusivity rebates than the Guidance suggests the commission would adopt. 
		- Akman argues that the publication of the Guidance leaves undertakings more confused about the law in this area than before
	- Whish & Bailey argue that the Guidance simply sets out how the Commission would apply its limited resources. Guidance is not a binding source of law, although it may have an influence on the future application of Art 102.
		- AG Mazak in _TeliaSonera_ said that the Guidance may provide a useful point of reference. 
- DG Comp Discussion Paper on exclusionary abuses
	- Not an official document of the commission
	- Not guidelines on the application of Art 102
	- Paper noted four points of consensus:
		- Art 102 should not be applied simply to protect competitors as such
		- Dominant firm that defeats its rivals as a result of its greater efficiency ought not to be condemned as acting abusively (also mentioned in _Post Danmark_
		- Economics of abuse are sufficiently complex that there should not be per-se rules
		- Exclusionary behaviour should only be condemned where it can be demonstrated that the behaviour has had, or is likely to have, a significant anti-competitive effect. 
		
###Elements of an Art 102 Infringement

####Undertakings

- Undertakings mean the same for both Art 101 and Art 102. 
- Statutory monopolies are not exempt from Art 102
- Per Art 106(2), MS cannot confer immunity on undertakings from Art 102 outside the narrow provisions in Art 106(2)

####Effect on Inter-State Trade

- Same definition as in Art 101
- Commission Guideline on effect on trade aspect also applies
	- Appreciability concept is implied (para 18)
	- No quantitative guideline on appreciability
- In _Commercial Solvents_, ECJ held t hat this requirement would be satisfied where the conduct brough about an alteration in the structure opf competition in the internal market. 

####Dominance

- Art 102 only applies where one or more undertakings are of a dominant position within the internal market or a substantial part of it
- Basic position given in _United Brands v Commission_: The dominant position relates to a position of economic strength enjoyed by an undertaking which enables it to prevent effective competition being maintained on the relevant market by affording it the power to behave to an appreciable extent independently of its competitors, customers, and ultimate of its consumers.
	- Economic concept of having substantial market power (capable of profitably increasing prices above the competitive level for a significant period of time) (para 10)
- Two stage test per _Continental Can_:
	- Define the relevant market
		- Be especially wary of the cellophane fallacyo
		- Note that the relevant market may be very narrow (spare parts for certain goods) _Hugrin_
	- Determine dominance by assessing competitive restraints (para 12)
		- Actual competitors
			- There may be a statutory monopoly
			- Where there is no actual monopoly, market shares provide a useful first indication of market structure (para 13)
				- Large market shares are in themselves, and save in exceptional circumstances, evidence of the existence of a dominant position _Hoffmann-La Roche_
				- ECJ in _AKZO_ held that a market share of 50% would lead to a presumption of dominance save in exceptional circumstances. 
				- ECJ in _United Brands_ held that the firm with a market share between 40-45% was dominant, although other factors were considered to be significant and the market share alone would not have been able to sustain a finding of dominance. 
				- Firms with market share <40% are not likely to be dominant, although there may be exceptions depending on the structure of the market as in _Virgin/British Airways_
			- Market conditions must be taken into account, including market dynamics, product differentiation, market share trends.
		- Potential competitors
			- Consider whether expansion or entry by a competitor is likely, timely and sufficient. Factors that my influence this include barriers to entry, likely reaction of the dominant undertaking and other competitors, and the risks and costs of failure. 
			- Entry is timely if it is sufficiently swift to defeat or deter the exercise of substantial market power.
			- Barriers to entry include:
				- Economies of scale _United Brands_
				- Control of essential facility
				- An undertaking's superior technology may be an indicator of dominance _United Brands_, _Hoffmann-La Roche_, _Michelin_
				- Access to the international capital market _United Brands_
				- Vertical integration and well-established distribution that a competitor would not have access to _Hoffmann-La Roche_
				- Advertising campaign and brand recognition _United Brands_. High advertising expenditure could make entry difficult into the market for FMCG.
				- Network effects _Microsoft_
				- Dominant undertaking's conduct may itself constitute a barrier to entry _United Brands_, _Michelin_
					- While criticised, this approach is sensible as the undertaking's conduct likely has a very real impact on the market conditions. 
					- Since dominance refers to an undertaking's ability to act independently of its competitors, customers, and consumers, the such conduct by the undertaking should naturally be taken into account.
		- Countervailing buyer power
		- General Court in _Coca-Cola_ held that the Commission must make a fresh analysis of market dominance for each decision. A national court would also not be bound in a later case by a previous finding of dominance by the Commission in a different case. However, a finding of dominance may be a basis for subsequent actions for damages brought by parties in relation to the same facts.

####Substantial Part of the Market

- Separate assessment from the relevant geographic market
- Must consider the pattern and volume of the production and consumption of the said product as well as the habits and economic opportunities of vendors and purchasers _Suiker Unie_
	- Not just a question of relative physical size of geographic market to that of EU as a whole
- Each MS would likely be considered a subtantial part of the internal market, particularly where there is a statutory monopoly. 
	- Even parts of a MS can be a substantial part of the internal market as in _Suiker Unie_
- There is no set percenteage threshold.
	- In _BP_, a market constituting 4.6% of the entire EU market for petrol was considered substantial by AG Warner (not addressed by ECJ as case was quashed on other grounds)
- Dominance in relation to a single facility may be sufficient to satisfy the test of substantiality. _MCP di Genova_
	
####Abusive Conduct

- The determination of whether conduct is abusive is controvertial as it involves a competition authority or court deciding what is "normal", "fair", or "undistorted" competition. 
- A firm in a dominant position is under a special responsibility not to allow its conduct to impair undistorted competition on the internal market _Michelin_
- Art 102 does not contain an exhaustive list of abusive conduct.
- The broader the definition of abuse, the closer the law comes to prohibiting dominance
- Jurisprudence focuses principally on exclusionary conduct.

#####General Conception

- In _Hoffman-La Roche_, ECJ found that abuse is: an objective concept relating to the conduct of an undertaking in a dominant position which through recourse to methods different from those which condition normal competition has the effect of hindering the maintenance of the degree of competition still existing in the market or growth of that competition. 
	- Does not cover exploitative conduct
- In _Deutsche Telekom_, the ECJ said that a dominant firm must not strengthen its dominant position by using methods other than those which come with the scope of competition on the merits. 
	- ECJ in _Post Danmark_ recognised that competition on the merits may drive out less efficient competitors and thereby have an exclusionary effect that does not infringe competition law. 
- An abuse of dominance does not require there be an exercise of market power. _Continental Can_
	- The conduct of an undertaking may be regarded as abusive in the absence of any fault and irrespective of the intention of the dominant undertaking.

#####Exploitative Abuses

- A monopolist may be in a position to reduce output and increase price above the competitive level, thereby exploiting cunsumers
- A monopolist may be freed from the need to innovate and improve efficiency due to the lack of competition. Art 102(2)(b) may be construed to cover this situation. 
- Difficult to determine whether the price is excessive
	- Excessive price as defined in _United Brands v Commission_ as a price which has no reasonable relation to the economic value of the product supplied.
	- While a cost/price analysis may be one step of the evaluation process, merely showing that a lower price would still be profitable is insufficient _United Brands_
	- The fact that a price is excessive may be established by looking at the prices charged by the dominant undertaking for other goods/services _Deutsche Post_
	- Excessive pricing may be established by reference to comparable goods/services in other markets or independant international standards _S&P_
	- Excessive costs arising out of a monopoly's inefficiency do not justify a higher price _Ministere Public v Tournier_
- Having found that a price is excessive, it must then be considered whether that price is unfair in itself or when compared to competing products _United Brands_
- Arguments against direct price control:
	- If normal market forces operate, the presence of monopoly profits should attract competition. The monopoly is therefore self-defeating if there are minimal barriers to entry.
	- Artificially restricting prices could distort competition by reducing the economic incentive in achieving growth or entering a market.
	- Monopolist should be permitted to charge a monopoly so that it can earn sufficiently large profits to carry out expensive and risky R&D. 
	- Difficult to translate a policy against exploitative pricing into an administrable legal test.
	- Preventing future explotative pricing is difficult
	- High amount of market information required
- Where there are natural monopolies, ex ante price control by non-competition regulators or legislation may be desirable. 

#####Exclusionary Abuses

- _Continental Can_ confirmed that Art 102 applies to both exclusionary and exploitativie abuses.
- Art 102 applies to both vertical and horizontal foreclosure. 
- The dominant position, abuse, and effects may be in different markets.
	- In _Michelin_, all elements were in one market
	- In _Commercial Solvents_, the dominance and abuse were in the upstream market while the benefits (effects) were in the downstream market.
	- In _British Gypsum_, the abuse was committed in the non-dominated market in order to safeguard its position in the dominated market.
	- In _Tetra Pak_, the dominance was in one market while the abuse and effects were in a separate market that waas closely associated to the former.
- Since competitive and exclusionary behaviour often look similar, potential anti-competitive effects should be demonstrated before the conduct is condemned as unlawful _TeliaSonera_
	- Consider all relevant market and legal factors
	
####Defences/Objective Justification

- Conduct which is objectively justified and proportionate will not violate Art 102. 
- Where there is an objective justification, is the conduct abusive in the first place? 
- Guidance on EP suggest that a claim to objective necessity would have to be based on factors external to the dominant undertaking. 
- A dominant undertaking may justify it's conduct in the following means _Post Danmark_:
	- Objective necessity
		- Cutting off supplies to a bad debtor
		- Public interest justifications (public health/safety)
		- Avoid the risk of criminal charges.
	- Conduct creates efficiencies that outweigh the anti-competitive effects. Guidance sets out four cumulative conditions:
		- Efficiences would have to be realised or likely to be realised as a result of the conduct
		- The conduct must be indispensible to the realisation of those efficiencies
		- The efficiences would have to outweigh any negative effects on competition and consumer welfare in the affected markets
		- The conduct must not eliminate all effective competition. 
		- Note that this defence has yet to be successfully pleaded
- Competition concerns may outweigh a property owner's right to determine how his property is used. _Frankfurt Airport_, _Microsoft_