#Article 101

##General

- Language of the statutory provision: agreements, decisions, and concerted practices
- Agreement means a concurrence of wills between at least two parties, the form in which it is manifested being unimportant so long as it constitutes the faithful expression of parties' intention. _Bayer v Commission_, _Spanish Glaxo_
	- Not limited to legally enforceable contracts but also includes:
		- Gentlemen's agreements _Chemiefarma_
		- Mutual understandings, even where there are no enforcement mechanisms _PVC_ (Commission decision)
		- Oral agreements _Tepea_
		- Expired agreements with lingering effects _Hercules_
	- Agreement exists even if one or more undertaking was forced into it. _Musique diffusion francaise_
	- Non-implementation of agreement does not negate its existence _Industrial and medical gases_ (Commission decision)
	- Key issue is whether there was a genuine agreement between parties:
		- Unilateral conduct, which is not caught by Art 101, is often implemented through agreements. Look to the substance of the conduct rather than whether there was a legal agreement in place. 
			- The fact that all the circumstances taken together created the effect of an export ban does not necessarily mean that there was an agreement to that effect, especially where some of the involved undertakings have their conduct restrained by regulatory requirements.
		- Public distancing may be an indication that there was no concurrence of wills
		- BMW v Commission: letter was not signed but terms in the letter were complied with. All parties were in breach.
		- Bayer v Commission: Attempts to circumvent terms of the purported agreement may be evidence that there is no concurrence of wills even if parties are in continued business relations
	- Note the possibility of finding a single overall agreement:
		- Presence of a single overall agreement is an objective fact, not a discretionary decision of the Commission. 
		- Conditions per _Term Relocations_ are:
			- Overall plan pursuing a common objective
			- Intentional contribution of the undertaking to that plan
			- Awareness of the offending conduct of the other participants
		- Implications:
			- Each infringing undertaking is responsible for the overall cartel
			- Quantum of fines
				- Duration of infringement may be longer
				- Anti-competitive effects may be more severe
			- Commission may impose fines in respect of illegal practices that would otherwise be time-barred
			- It may be easier to satisfy the Art 101 requirements.
	- While agreements are conceptually distinct from concerted practices, the GC in _PVC_ held that it is possible to classify conduct as agreement and/or concerted practice. Not necessary to draw a distinct line between agreements and concerted practices. 
- Decisions by associations of undertakings 
	
	
##Horizontal Agreements

##Vertical Agreements