#Choice of Law

##Common law approach

- 3 stage test _Amin Rasheed Shipping Corp v Kuwait Insurance Co_:
	1. Express choice?
	2. Implied choice?
	3. What is the law with the closest and most real connection to the contract
- Similar factors considered in stages 2 and 3 but they are conceptually distinct elements _Hellenic Steel Co v Svolomar Shipping Co Ltd (The Komninos S)_

##EU law approach

###Introduction

- Rome Convention governs contracts entered into after 1 April 1991
	- Original objective were:
		- To harmonise the approaches taken by courts across the internal market (decisional harmony)
		- Legal certainty as to what law governs the contract
	- Originally, most jurisdictions already respected parties' express choice of law
	- Convention allowed parties to determine the law governining their contract without going through litigation
	- A new protocol was needed to give ECJ jurisdiction over Convention issues
		- This protocol did not come into force for most of the duration of the convention
		- National courts interpreted the Convention themselves
			- Art. 18: National courts should consider the jurisprudence of other signatory parties when interpreting the Convention
			- Art. 33: All language versions are equally authentic
		- Criticism that English courts were too heavily influenced by the common law approach
		- Protocol was finally signed by the last MS in 2004
		- Only 1 judgement from ECJ on the interpretation of the Rome Convention 
	- English aproach:
		- Contracts (Applicable Law) Act 1990 s.3(1), s.3(2) give that ECJ opinions are binding
		- Contracts (Applicable Law) Act 1990 s.3(3)(a) gives that English Courts should have regard to the Giuliano-Lagarde Report when interpreting the Convention
			- Presumption of continuity: Where the Convention is the same as the Rome I Regulation, the report may still be relied upon for guidance
	- Most litigation still concerns contracts governed by the Convention
- Rome I Regulation governs contracts entered into after 17 December 2009
	- Practical concerns (no need to for additional protocol)
	- Additional certainty
		- UK already has an opt out in this area of law.
			- UK chose to opt out of the regulation at the beginning of the negotiation process
			- UK participated in negotiations but would only opt in if they liked the outcome
	- Little case law available on the Regulation

###Substance of the instruments

####Scope

- Contractual obligations _Art 1(1) Convention; Art 1(1) Regulation_
	- EU definition of "contractual obligations": includes unilateral voluntary undertaking (contract without consideration)
- Convention/Regulation completely displaces national law where it is relevant. There is no residual role for national law in this space. _Art 2 Convention; Art 2 Regulation_
- Exclusions given in Art 1(2) Convention; Art 1(2) Regulation. 
	- Two noteworthy exclusions: 
		1. Art 1(2)(d): Arbitration agreements and agreements on the choice of court. Convention/Regulation is not applied when determining the relevant law governing these agreements i.e. English courts apply common law.
			- Two choices for arbitration agreements: law of the seat of arbitration; law of the contract.
			- Jurisdiction agreement in favour of the courts of MS: Brussels Recast Regulation