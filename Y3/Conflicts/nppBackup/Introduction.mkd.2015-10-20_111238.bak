#Conflict of Laws

##Introduction

- Approx 80% of cases involve one foreign party, 50% involve two foreign parties.
- Focus is on English courts
- Heavy EU Regulation
- Courts never apply foreign public law, but may apply foreign private law.

##Content

- EU Rules apply if claim is against EU domiciled defendant
	- Against EU parties
	- If subject matter has some special connection with EU law
- Five types of jurisdiction
	- Personal jurisdiction
		- Idea that jurisidiction can be based on the person of the defendant. The person can be sued in the courts of their domicile.
			- Not desirable for claimants due to cost and inconvenience.
			- Nationality
			- Presence
	- In Rem jurisdiction
		- Not covered heavily in this course.
		- Title to real or personal property might be grounds for jurisdiction.
			- Ships are a special case. A party can be sued in England if he owns a ship in England by seizing the ship. Claim does not have to be related to the ship.
	- Subject matter jurisdiction
		- Example: English courts might have jurisdiction in tort if the tort was perpetrated in England.
			- If tort is committed in one place but damage is suffered in another jurisdiction, usually both courts have jurisdiction.
	- Consensual jurisdiction
		- Submission by conduct (suit commenced, then jurisdiction is established where one party shows up in court)
		- Jurisdiction clauses in contracts.
	- Additional party jurisdiction
		- If claim is against more than one party and jurisdiction can be established over one party, the English court may claim jurisdiction over the other party.
		- Efficient
		- Issue of justice: related claims heard together to reduce the risk of conflicting judgements
- Court has discretion as to whether jurisdiction should be exercised
	- Case management efficiency perspective
		- Location of witnesses, languge of relevant documents, etc.
- Negative declarations
	- In litigation, either party can sue. A party may go to court to seek a declaration of non liability.
	- Important because either party can commence a claim and therefore affects the jurisdiction
	- Each party has a preferred court.
	- Potential for race to courtroom
- Anti-suit injunction
	- Court may order parties to refrain from or discontinue foreign proceedings
- Parallel proceedings
	- Concept of deference/restraint
	
##Brussels Regulation

- EU approach is not about finding the best court to hear the dispute but rather legal certainty and predictability
- EU pushing for unification of substantial private law in the Business context
	- MS backlash
	- Alternative is traditional conflicts of laws approach.
	- Even where cases are heard in more than one jurisdiction, the PIL principles should be the same across the jurisdictions such that the same national law is applied in both jurisdictions.
- Recast Regulation proposes that national rules of jurisdiction should be replaced with a harmonised set of EU rules; hostile national response - subsuquently abandoned.
	- Mutual trust and respect between MS judicatures. No MS court should rule on whether another court has decided appropriately.
- Necessary for EU definitions of the terms
	
###Art 1

- Civil and commercial matters
	- Claims in relation to the exercise of public powers are not "civil and commercial matters"
	_LTU v Eurocontrol_
		- Eurocontrol was a public authority bringing a claim for unpaid fees against LTU. It was a public authority acting in the course of its public powers and therefore not a civil/commercial matter.
		- Where public authority (or representative) is not exercising public powers, it may still fall under "civil and commercial matters" _Sonntag v Waidmann_
		- Look behind form of the action to the reality of the situation _QRS 1 ApS v Frandsen_
			- Danish tax authorities as the sole creditor of the company directed the company to make claim in restitution to recover back taxes. Ultimately, the issue was the recovery of taxes and therefore not a "civil and commercial matter"
		- Bringing a claim for conspiracy to defraud tax authority may be a "civil and commercial matter" since it is a claim that any private creditor can make anyway. _Her Majesty's Revenue & Customs v Sunico ApS_
			- Claim brought by UK tax authority arising out of VAT fraud. Claim was brought against third party in tort for conspiracy to defraut the IRS.
		- Claim may fall outside "civil and commercial matters" if defendant was exercising their public powers _Lechouritou v Germany_
			- Claim brought for damage caused by German forces during WW2
- Exclusions have generally been interpreted narrowly
	- Bankruptcy exception only includes cases related to actual declaration of bankruptcy _Ashurst v Pollard_
		- Trustee appointed by bankrupt party brought proceedings to compel him to liquidate certain property to repay creditors. This was held to be not covered by the bankruptcy exception.
	- Arbitration exception
		- If the parties have an arbitration agreement states that the seat is in London:
			- If one or more parties refuse to cooperate, proceedings should be brought in the seat of arbitration and fall outside the Regulation.
			- If one party begins proceedings in Italian courts instead and the Italian court rules that the arbitration agreement is invalid, the substantive claim will fall under the Regulation.
				- Q: Is the judgment of the Italian court binding on English courts? Yes: it is itself a decision under the Regulations and due to the principle of mutual trust, it binds English courts.
					- Recital 12 of the recast regulations appears to reverse the decision but it has yet to be tested in litigation.
					
###Domicile

- EU definition in Art 62, 62
	- UK definition not relevant
	- Art 62: No real definition, apply national test i.e. English court apply French law to determine whether he is domiciled in France
		- UK Law requires a person be a resident and have a substantial connection with the UK per Civil Jurisdiction and Judgments Order 2001
			- Residence creates a rebuttable presumption of substantial connection
		- Possible for someone to be domiciled in more than one MS. He may be sued in any place in which he is domiciled
	- UK Courts held that compulsory residence does not establish substantial connection. _Petrotrade Inc. v Smith_
		- Smith accused of criminal fraud, his bail condition prohibits him from leaving. After the criminal case, he was sued in civil courts because he was resident for the period of his case. 
- Internal management of companies
	- Parties cannot contract out of exclusive jurisdiction rules in Art 24 _Speed Investments Ltd v Formula One Holdings Ltd_
		- Litigation was over the rights of shareholders to appoint directors to the company. Disagreement over interpretation. Shareholder agreement had exclusive swiss jurisdiction clause.
		