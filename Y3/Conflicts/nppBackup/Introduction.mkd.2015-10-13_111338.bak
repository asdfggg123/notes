#Conflict of Laws

##Introduction

- Approx 80% of cases involve one foreign party, 50% involve two foreign parties.
- Focus is on English courts
- Heavy EU Regulation
- Courts never apply foreign public law, but may apply foreign private law.

##Content

- EU Rules apply if claim is against EU domiciled defendant
	- Against EU parties
	- If subject matter has some special connection with EU law
- Five types of jurisdiction
	- Personal jurisdiction
		- Idea that jurisidiction can be based on the person of the defendant. The person can be sued in the courts of their domicile.
			- Not desirable for claimants due to cost and inconvenience.
			- Nationality
			- Presence
	- In Rem jurisdiction
		- Not covered heavily in this course.
		- Title to real or personal property might be grounds for jurisdiction.
			- Ships are a special case. A party can be sued in England if he owns a ship in England by seizing the ship. Claim does not have to be related to the ship.
	- Subject matter jurisdiction
		- Example: English courts might have jurisdiction in tort if the tort was perpetrated in England.
			- If tort is committed in one place but damage is suffered in another jurisdiction, usually both courts have jurisdiction.
	- Consensual jurisdiction
		- Submission by conduct (suit commenced, then jurisdiction is established where one party shows up in court)
		- Jurisdiction clauses in contracts.
	- Additional party jurisdiction
		- If claim is against more than one party and jurisdiction can be established over one party, the English court may claim jurisdiction over the other party.
		- Efficient
		- Issue of justice: related claims heard together to reduce the risk of conflicting judgements
- Court has discretion as to whether jurisdiction should be exercised
	- Case management efficiency perspective
		- Location of witnesses, languge of relevant documents, etc.
- Negative declarations
	- In litigation, either party can sue. A party may go to court to seek a declaration of non liability.
	- Important because either party can commence a claim and therefore affects the jurisdiction
	- Each party has a preferred court.
	- Potential for race to courtroom
- Anti-suit injunction
	- Court may order parties to refrain from or discontinue foreign proceedings
- Parallel proceedings
	- Concept of deference/restraint
	
##Brussels Regulation

- EU approach is not about finding the best court to hear the dispute but rather legal certainty and predictability
