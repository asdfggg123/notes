#Breach of Confidence

##Origins and History

- Breach of confidence gives rise to its own cause of action _Prince Albert v Strange_
- Breach of confidence is based in equity (conscience) _Seager v Copydex (No 1)_
- Reasonable expectation of privacy? _Campbell v MGN_ (Tort case covered last year)
- ECHR has developed this area of the law
	- No need for a relationship between parties for there to be an obligation.
	- Art 8, 10 action

##Traditional Action

- No distinction bewteen different types of information
- Confidence and novelty (patents)
	- Longer protection as long as it is a secret
- Confidence and copyrights
	- Protection of some ideas by placing person under obligation of confidence
- Elements under _Coco v Clark_
	1. Information must be of a confidential nature
		- Must identify information that is to be confidential (considering that very often it is not written down)
		- Excluded:
			- Trivial information
				- Equity ought not to be invoked to protect trivial tittle tattle _Coco v Clark_
				- Government information is probably never excluded _AG v Guardian (No. 2)_
				- Narrow conception of trivial information
			- Immoral information
			- Vague information
				- _De Maudsley v Palumbo_ tells us that the idea has to be sufficiently developed and have a degree of particularity
					- De Maudsley talked about his ideas on the opening of a nightclub that were quite generic. Action failed.
			- Public domain information
				- Disclosure to small number of people would not overcome the confidential nature of it _Albert v Strange_
					- Compare with stricter rule for patents
				- If information has already been widely disclosed, there is no need to refrain further publication _AG v Guardian Newspapers_
				- Compare with Springboard doctrine, where it is allowed to "reverse engineer" confidential information but not directly employ confidential information. _Seager v Copydex_
	1. Circumstances must be such that an obligation of confidence exists
		- Whether the reasonable person would have realised that the information was given in confidence _Coco v Clark_
			- Objective test in light of subjective knowledge
		- Employees:
			- Obligation of confidence generally exists for current employees (term in employment contracts)
			- For ex-employees, it is necessary to distinguish between three types of information per _Faccenda Chicken v Fowler_:
				1. Trade secrets: always protected
					- Nature of employment (degree of access to secret information)
					- Nature of the information
					- Did the employer specifically emphasise the confidentiality of the information
					- Can the trade secret be easily isolated from other disclosable information
				1. Commercially sensitive information: rely on contractual provisions
				1. General skill and knowledge: never protected
				- Ex-employees remembered customer information and used it to set up competing business.
			- Third Party Recipients
				- Appears to apply general principle of whether a reasonable recipient would think it to be confidential _AG v Guardian Newspapers_
				- Level of knowledge required? Include "constructive knowledge"? _Vestergard v Bestnet_
	1. There must be an unauthorised use of information
		- Usually a question of fact with an obvious answer
		- Does it require detriment? _AG v Guardian Newspapers_
- Public interest defence?
	- Clarify misconception that has been created about himself _Woodward v Hutchins_
		- Question of balance
	- Disclosure must be in the public interest, not just of public interest _Douglas v Hello!_
		- Not just idle gossip or curiousity
	- Classic example: _Lion Laboratories v Evans_
		- Case concerned a breathalyzer machine: it was in the public interest for the press to expose problematic calibration.
		- Margin of discretion; public disclosure more likely to induce public body response
- Privacy and breach of confidence
	- Difficult prior to HRA
		- 