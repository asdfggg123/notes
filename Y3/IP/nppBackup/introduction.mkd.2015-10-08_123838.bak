#Introduction

##What is IP

- See Phillips v Mulcaire 2015
- Bundle of exclusive rights?
	- Right to a monopoly
		- Typical issues with having monopolies
- Definitely actual property (statutory definition)
- Intangible rights
- Need to determine the scope of property rights since it is intangible. This is defined by reference to legal concepts.

###Difference between normal and intellectual property

- Non-excludable
- Non-rivalrous

###Why does IP matter

- Practical significance
- "Information age" and "knowledge economy"
- See Hargreaves Report (2011): UK firms spend more money on intangible property than tangible property

##Copyright

- Arises automatically on the creation of a work
- Categories of works
	- Authorial works
	- Dramatic works
	- Musical works
	- Artistic works
- Entrepreneurial Works
	- Films
	- Broadcasts
	- Sound recordings
	- Typographical arrangements of published editions
- Authorial works have certain moral rights
	- Right to claim that one was the author
	- Right against derogatory remarks? (Check)
- Partial harmonisation
	- Closed group of works? More in depth later
- Concept of originality
	- There must be an exercise of "skill, labour and/or judgment" (old) or the author's "own intellectual creation" (new)
		- Possibly a change in standard
		- Low standard
- Fixation
	- Sufficient permanence
- No need for formalities
- Author is generally the first owner
	- Presumption that the employer owns works produced by emplyee
- Author has certain exclusive rights e.g. copy. issue copies, rent or lend, perform, communicate to public, adapt
- Copyright protection lasts a long time (Authorial works: life + 70 yrs)

###Infringement

- Infringement occurs where a party perpetrates an act exclusively reserved to the copyright owner.
- Causal connection to the copyright work
	- Independent creation is a defence
- In relation to a "substantial part" or "part" of the work
	- This might include not literal copying
	- It is no infringement to copy the ideas only -> idea/expression dichotomy
- Defences/exceptions
	- Criticism
	- Review
	- Reporting
	- Research
	- Parody

##Patents

- Registration (prosecution) is necessary
	- UKIPO
	- EPC
	- WIPO
	- Unitary Patent??? Insufficient states ratifying the treaty
- Date of application is the "priority date"
- Subject matter:
	- Inventions (certain statutory exclusions)
	- New + inventive + industrial application
		- Cannot be obvious to a person skilled in the art
	- Products or processes
- Sufficiency of disclosure
	- Quid pro quo of patent protection
- Ownership
	- Presumption of employer ownership
- Duration: Max 20 years
- Patent creates property in invention
- Exclusive rights of a broad scope:
	- Products: dispose of, offer to dispose, keep, make, use, import
	- Processes: use, dispose of, keep
	- Independent creation is not a defence to infringement
- Exclusions are limited:
	- Private/non-commercial/experimental acts
	
###Design Patents

- Registered Designs and Unregistered Designs (UK & EU)
	- Registered: more patent-like
	- Unregistered: more copyright-like
		- Compare with copyright (difference in length of protection, avoid double-protection)

##Trade Marks

- Creation and Protection of a Brand
- Protection under the Common Law
	- Tort of passing off
	- Requires sufficient goodwill
- Protection under Registered Trade Marks
	- Subject matter
		- Signs: names, shapes, smells/colours/sounds/gestures?
		- Signs must distinguish between goods or services on the basis of their commercial origin
	- Absolute grounds of objection (Inward looking, intherent objections to protection)
		- Is it too generic and devoid of distinctive character?
		- Insufficient demarcation of origin
		- Acquire distinctiveness over time
	- Relative grounds of objection
		- Double identity, confusion, dilution, tarnishment, unfair advantage
- Exclusive rights are relative. Prevention of harmful or otherwise wrongful use.
- Exceptions: descriptive use, comparative advertising
- Duration: 10 years, indefinitely renewable as long as the mark is continuously in use.
- Other IP rights?
	- Breach of confidence
	- Geographic indications e.g. Champagne, Stilton cheese
	- Sui Generis Rights e.g. database rights
	- Plant breeder's rights
	- Traditional knowledge
	- Semiconductor topography rights

##Justifications

- Deontological Theories
	- Labour based (Locke)
	- Personality based (Hegel/Kant)
- Consequentialist Theories
	- Incentive
		- IP provides an incentive to create by preventing free-riding
		- Necessary due to public nature of the property (non-rivalrous, non-excludable)
		- Necessary to create a legal monopoly to avoid market failure due to free-rider problem
		- Issues:
			- Difficult to the the balance right; social cost might be too high
			- Is this the best incentive? See negative spaces e.g. magic/culinary/comedy ideas
	- Social goals