#Copyright

##International BG

- Berne Convention
	- 167 signatory states
	- Principle of equal treatment of domestic and foreign authors
	- No formalities
	- OT: USA requires copyright registration for damages
	- Minimum protection for copy rights
	- Moral rights protection
	- Can't implement exception unless:
		- Limited to special cases
		- Does not conflict with normal exploitation of the work
		- Must not prejudice the legitimate interest of the author
	- At least life + 50 yr
- Rome Convention
- Agreement on Trade-Related Aspects of IP Rights (TRIPS)
	- WTO members must implement substantive copyright provisions of Berne Convention (except for moral rights)
	- WTO provides dispute resolution process
	- Computer programs protected as literary works
	- Similar requirements for exception
- EU Harmonisation
	- Upwards harmonisation
	- Only one horizontal directive: Information Society Directive
	- CJEU case law harmonisation

##Statutory Starting Point

- S1(1) CDPA 1988: Copyright is a property right subsisting in accordance with the following descriptions (exclusive list)
- S2: Exclusive rights of copyright owner

##Nature of the right

- Requirement of originality (lower than novelty for patents)
- No formality
- Subconscious/indirect infringement
- Authorising infringement
- Infringement
	- Element of copying
	- Causal connection
	- Not infringing to use
- Copyright as a bundle of rights
- Orphan works problem where it is difficult to find the author of the copyright
	- Particular problem for digital works
- 2014 Licensing scheme?
- Copyright vs Moral Rights?
- Justifications for copyright	
	- Free-rider problem
	- Economic incentive

##Idea/Expression Dichotomy

- In Norowzian v Arks
	- Similar video technique to recreate effect
	- It was held to be an idea; no copying of the expression
- Baigent & Leigh v Random House
	- Holy blood, holy grail vs Da Vinci Code
	- Alleged that Dan Brown copyright the central theme
	- Level of abstraction is too high to protect via copyright
	- Copyright does not give the right to monopolise historical facts or knowledge
- Temple Island Collections v New English Teas
	- Similar pictures: red bus on b/w background
	- Court held was that there was infringement
	- Interesting case cos the alleged copy was photoshoped with the intention of imitating the original
	
##Copyright subsistence requires

- A Work
	- That is original
	- Entrepreneurial works must be "not copied"
	- LDM works must be "recorded in writing or otherwise"
- Qualified right
- In _Infopaq_ (EU Case),
	- Any subject matter that is an "intellectual creation" would be protected
	- Read the case
	- For the moment, not much change
	
## Category of works

- Defined in the act
- Literary Works
	- In _Nova Productions v Mazooma Games Ltd_
		- D made other games similarly simulating a game of pool.
		- No direct copying so no problem
	- What is the minimum threshold?
		- _Exxon v Exxon Insurance_ concerned the word "Exxon"
	- In _UOL Press v University Tutorial Press_
		- Court held that it just refers to written printed matter rather than works of literature
	- In _Meltwater v NLA_
		- Newspaper headlines are capable of being literary works
- Prefence for database rather than table/compilation
	- Database has to be the author's own intellectual creation
- Musical Works
	- _Sawkins v Hyperion_
		- Not mere noise
		- Intended to produce an effect on the listener's intellect

## Fixation

- Literary, dramatic or musical work needs to be recorded (s. 3(2)) whether it is recorded with or without the permission of the author (s. 3(3))
- No need for fixation for artistic works 
	- Requirement for permanence? Doubtful: Ice scupture was held to be a sculpture in _Metix v Maughan (Plastics)_

## Artistic Works

- _British Northrop v Texteam Blackburn_ concerned drawings of spare parts. Court held that none of the drawings were too simple to count as a drawing
- See s. 51


##Infringement

###Primary Infringement

- Exclusively reserved rights
- Causal connection between works
- Substantial part of the work
	- Question of quality 
		- Parameters of the work
		- Distinguishing protected bits
		- Assessing substance
	- Infopaq:
		- Previous case law focused on importance of features to the claimant's work.
			- Depends on the type/nature of work (NLA v M&S)
		- Parts enjoy same protection as the whole of the work. The parts must contain elements which are the expression of the intellectual creation of the author of the work _SAS v WPL_
		- Difference between UK & EU approach: UK ask if it is a material part of the whole, EU just prrotects intellectual creations
	
- Start with features that cause claimant to allege infringement: are they sufficiently close/numerous/extensive
