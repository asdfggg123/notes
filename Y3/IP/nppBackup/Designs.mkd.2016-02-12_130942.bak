#Designs

##Introduction

- What is a design
	- Shape of a functional article
	- Aesthetics of a product or its ornamentation
	- Mix of functional and aesthetic aspects in a product
- Design law is concerned with appearance and not function
- Design protection regimes
	- EU registered design
	- EU unregistered design (short term eu-wide protection for designs that do not require registration)
	- UK registered design (more in common with patents)
	- UK unregistered design (more in common with copyright)
	- Artistic copyright (residual protection
	- Trade mark law
	- Passing off

##Copyright/Design Frontier

- Protection of a design by copytright
	- Direct route as a 3D artistic work (see copyright)
	- Indirect route through design documents
- Potential difficulties as seen in _British Leyland v Armstrong_
	- Case concerned replacement exhaust parts
	- Original replacement is expensive
	- Armstrong created replacement by copying the actual exhaust part
	- BL argued that Armstrong indirectly infringed the copyright in the drawings
	- UKHL held that this was infringement but created a defence of derogation from grant in the context of repair of complex products, and was limited to parts that require infrequent repair
- Statutory limit on copyright given in s 51 CDPA 
	- Not an infringement of any copyright in a design document or model recording embodying a design for anything other than an artistic work or typeface to make an article to the design or to copy an article made to the design. 
	- Note that copyright itself is not excluded.
- Scope of s 51
	- _Lucasfilm v Ainsworth_
		- "The genesis of [s51] lay in the desire to allow generic industrial spare parts to be made by third party suppliers without their being accused of infringement of copyright in underlying drawings which usually preceded the manufacturer's "original" spare part, though its text is not confined to that situation"
		- Not just limited to spare parts
		- Two questions:
			- Was there a design document i.e. something intended to be followed by the creation of an article
			- Was the design for an artistic work
	- _Flashing Badge Co v Groves_
		- Case concerned copying flashing badges. Both parties agreed that the drawings on the badges themselves were artistic works
		- D argued it's not enough that the design itself is an artistic work. What matters is what it's for (the badge)
		- C argued that the documents were designs for an artistic work (the images). The parameters of the design were limited by the artistic works. Images were only surface docoration
		- Court held that the drawings were design documents incorporating two types of designs (artistic works and badge outline)
		