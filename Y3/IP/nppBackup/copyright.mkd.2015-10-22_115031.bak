#Copyright

##International BG

- Berne Convention
	- 167 signatory states
	- Principle of equal treatment of domestic and foreign authors
	- No formalities
	- OT: USA requires copyright registration for damages
	- Minimum protection for copy rights
	- Moral rights protection
	- Can't implement exception unless:
		- Limited to special cases
		- Does not conflict with normal exploitation of the work
		- Must not prejudice the legitimate interest of the author
	- At least life + 50 yr
- Rome Convention
- Agreement on Trade-Related Aspects of IP Rights (TRIPS)
	- WTO members must implement substantive copyright provisions of Berne Convention (except for moral rights)
	- WTO provides dispute resolution process
	- Computer programs protected as literary works
	- Similar requirements for exception
- EU Harmonisation
	- Upwards harmonisation
	- Only one horizontal directive: Information Society Directive
	- CJEU case law harmonisation

##Statutory Starting Point

- S1(1) CDPA 1988: Copyright is a property right subsisting in accordance with the following descriptions (exclusive list)
- S2: Exclusive rights of copyright owner

##Nature of the right

- Requirement of originality (lower than novelty for patents)
- No formality
- Subconscious/indirect infringement
- Authorising infringement
- Infringement
	- Element of copying
	- Causal connection
	- Not infringing to use
- Copyright as a bundle of rights
- Orphan works problem where it is difficult to find the author of the copyright
	- Particular problem for digital works
- 2014 Licensing scheme?
- Copyright vs Moral Rights?
- Justifications for copyright	
	- Free-rider problem
	- Economic incentive

##Idea/Expression Dichotomy

- In Norowzian v Arks
	- Similar video technique to recreate effect
	- It was held to be an idea; no copying of the expression
- Baigent & Leigh v Random House
	- Holy blood, holy grail vs Da Vinci Code
	- Alleged that Dan Brown copyright the central theme
	- Level of abstraction is too high to protect via copyright
	- Copyright does not give the right to monopolise historical facts or knowledge
- Temple Island Collections v New English Teas
	- Similar pictures: red bus on b/w background
	- Court held was that there was infringement
	- Interesting case cos the alleged copy was photoshoped with the intention of imitating the original
	
##Copyright subsistence requires

- A Work
	- That is original
	- Entrepreneurial works must be "not copied"
	- LDM works must be "recorded in writing or otherwise"
- Qualified right
- In _Infopaq_,
	- Any subject matter that is an "intellectual creation" would be protected
	- Read the case
	- For the moment, not much change
	
## Category of works

- Defined in the act
- Literary Works
	- In _Nova Productions v Mazooma Games Ltd_
	- D made other games similarly simulating a game of pool.
		- No direct copying so no problm