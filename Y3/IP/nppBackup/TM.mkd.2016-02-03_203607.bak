#Trade Marks

##Introduction

###General

- Sources of law
	- The Trade Marks Directive
	- The Trade Marks Act 1994
		- While this is an implementing legislation, the wording used differs from the directive. 
- Things that can be trade marks
	- Traditional marks: words, numbers, letters, pictures, combinations thereof
	- Non-traditional marks: shapes/sounds/smells/gestures are more contentious
		- Issues: How to enter onto the register; feature vs indication of origin
- Boundary of conduct that is permissible is quite vague
- If mark is not used for ~5 yrs, it will be taken off the register
- Can renew every 10 years indefinitely
- Presumption that a registered trade mark is valid
	- Increased certainty compared to passing off

###Functions of a Mark

- Indication of origin (source rather than location)
- Product differentiation
- Guarantee the product of the product (consistency)
- Producer can use mark to communicate a particular quality to the consumer
- Protection of brand investment (free-rider problem)

###Process of Registration

- Filing of the application (goods and services classification)
- Examination by the Registry
- Publictaion and notification
- Opposition
- Registration

###The Average Consumer

- The average consumer of the goods in question
- Reasonably well informed, observant, and circumspect
- Level of attention to detail depends on the type of goods

##The Meaning of a Trade Mark

###S. 1 TMA Requirements

- A sign
	- In principle, any message that is capable of perception by the senses can constitute a message to the consumer. They are not limited to what can be perceived by the eyes. _Sieckmann v Deutsches Patent- und Markenamt_
	- Clear collection bin on a vacuum cleaner does not constitute a sign because it does not convey specific information _Dyson_
- Capable of graphic representation
	- The mark must unambiguously represent the origin of the product.
	- ECJ held that this requirement is to defining the precise subject matter of protection, making it easier to examine by authorities and making it easier for other traders to know how to avoid infringement. _Sieckmann_
		- Graphic representation by means of lines, images or characters. Clear and precise. Self contained. Easily accessible. Intelligible. Durable. Unequivocal or objective. 
		- Chemical formula given didn't represent the specific odour and was not sufficiently clear or accessible
		- Description was not sufficiently clear, precise, and objective.
		- Physical sample was not a graphic representation and was not sufficiently durable
	- Sounds:
		- Musical notation, practical instructions, and verbal description provided for registration in _Shield Mark BV v Joost Kist H.O.D.N. Memex_
			- Muscial stave could constitute a faithful representation of the mark.
			- Verbal description was not sufficient (commented that it would rarely be sufficient)
			- Onomatopoeic representation wasn't sufficiently objective or unequivocal.
		- Sonogram not sufficiently intelligible _Edgar Rice Burroughs_
	- Colours:
		- _Libertel v Benelux Merkenbureau_ concerned the use of the colour orange for telecommunication services (books). In order to represent their mark they included the word orange and a sample of the colour. Question was whether the representation was sufficiently objective and durable. 
			- Sample was not durable enough
			- Verbal description was not clear or precise enough
			- Court held that in the context of colours, issues are very fact specific
			- Colour can be registered by referring to an international standard of colours. (pantone)
	- EU Commission recently published a reform package that no longer requires a graphic representation.
- Capable of distinguishing
	- This is a preliminary requirement and is aimed at excluding the registration of things that are generally incapable of acting as trade marks. If mark is in fact capable of being distinctive, it will also pass this hurdle. Low minimum threshold. This should be read with the absolute grounds. _Koninklijke Philips v Remington_
	
##Absolute Grounds for Refusal to Register a Trade Mark

###Introduction

- Generally concerned with the internal characteristics of the trade mark
	- Looking into the innate quality of the sign in relation to the specific goods or services
- Consider the average consumer
- Three grounds
	- Devoid of distinctive character
		- Ability of mark to act as trade mark
	- Descriptive mark
		- Keeping things free for other traders to use
	- Signs customary in the current language
- Three absolute grounds are overlapping but independent. Each ground must be interpreted with the specific policy reasons behind them in mind. Each has its own underlyinig policy rationale. _SAT.1 SatellitenFernsehen GmbH v OHIM (SAT.2)_
	- Some debate about which policy consideration justifies which ground.

###Marks devoid of any distinctive character

- 