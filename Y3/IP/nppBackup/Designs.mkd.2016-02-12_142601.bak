#Designs

##Introduction

- What is a design
	- Shape of a functional article
	- Aesthetics of a product or its ornamentation
	- Mix of functional and aesthetic aspects in a product
- Design law is concerned with appearance and not function
- Design protection regimes
	- EU registered design
	- EU unregistered design (short term eu-wide protection for designs that do not require registration)
	- UK registered design (more in common with patents)
	- UK unregistered design (more in common with copyright)
	- Artistic copyright (residual protection
	- Trade mark law
	- Passing off

##Copyright/Design Frontier

- Protection of a design by copytright
	- Direct route as a 3D artistic work (see copyright)
	- Indirect route through design documents
- Potential difficulties as seen in _British Leyland v Armstrong_
	- Case concerned replacement exhaust parts
	- Original replacement is expensive
	- Armstrong created replacement by copying the actual exhaust part
	- BL argued that Armstrong indirectly infringed the copyright in the drawings
	- UKHL held that this was infringement but created a defence of derogation from grant in the context of repair of complex products, and was limited to parts that require infrequent repair
- Statutory limit on copyright given in s 51 CDPA 
	- Not an infringement of any copyright in a design document or model recording embodying a design for anything other than an artistic work or typeface to make an article to the design or to copy an article made to the design. 
	- Note that copyright itself is not excluded.
- Scope of s 51
	- _Lucasfilm v Ainsworth_
		- "The genesis of [s51] lay in the desire to allow generic industrial spare parts to be made by third party suppliers without their being accused of infringement of copyright in underlying drawings which usually preceded the manufacturer's "original" spare part, though its text is not confined to that situation"
		- Not just limited to spare parts
		- Two questions:
			- Was there a design document i.e. something intended to be followed by the creation of an article
			- Was the design for an artistic work
	- _Flashing Badge Co v Groves_
		- Case concerned copying flashing badges. Both parties agreed that the drawings on the badges themselves were artistic works
		- D argued it's not enough that the design itself is an artistic work. What matters is what it's for (the badge)
		- C argued that the documents were designs for an artistic work (the images). The parameters of the design were limited by the artistic works. Images were only surface docoration
		- Court held that the drawings were design documents incorporating two types of designs (artistic works and badge outline)
			- No infringement if just copy badge outline
			- Distinguish artistic vs design element of drawing/document

##Unregistered Design Right

###Definition

- S 213(2) CDOA 1988: In this Part �design� means the design of the shape or configuration (whether internal or external) of the whole or part of an article.
	- In _Ocular Sciences v Aspect Vision_, court held that dimension differences between parts of contact lens that could only be detected by specialised equipment were aspects of shapes that could be protected by design rights. 
		- Very broad definition
		- Court warned about the broad scope of design right. Potential for abuse if proprietor chooses to assert the design in either the whole or specific part of a product.
	- No need for artistic merit
- Shape or configuration...
	- Language used to be "any aspect of the shape or configuration"
	- Shape: outward appearance
	- Configuration: relativie arrangement of parts _Mackie Designs v Behringer_
		- Circuit diagram is a design; circuit may be different in practice even when made to the same circuit diagram
		- Selection of items in a collection might not be enough to constitute a configuration, but the layering of those items may suffice _Clinisupplies v Park_
		- Configuration has to be a relative arrangement of separate parts, and not just different superficial colours _Lambretta v Teddy Smith_
			- Demonstrates the hole left by s 51 that is not completely covered by s 213
- ... of the whole or part of an article
	- Article is assumed to mean something 3D
	- Only design protected, not the article itself
	- _Electronic Techniques v Critchley_ concerned circuit diagrams for a transformer. Court confirmed that design rights are not concerned with particular articles but instead protecting the designs. Protection afforded is not particular to that article. 
	
###Exclusions under s 213(3) CDPA 1988

- S 213(3)(b)(i): Features which Must Fit
	- Design right does not subsist in features of shape or configuration of an article which enable the article to be connected to or placed in, around or against, another article so that either article may perform its function
	- Prevent the protection of interfaces between spare parts.
	- _Dyson v Qualtex_ concerned spare part for Dyson vacuum cleaner. Joints attaching parts to the vacuum were excluded from design protection. 
		- Design will be excluded under this exception even where it has another purpose or function
	- The "other article" can be part of the human body _Ocular Sciences v Aspect Vision_
		- Doesn't mean that exclusion must apply to every design which in some way fits the human body or part of it
		- _UWUG Ltd v Ball_ concerned whether straps on a sex frame were excluded from design protection. The key issue is a fit that enables a connection. 
			- Choice of design was not limited by the need to fit a particular user
- S 213(3)(b)(ii): Features which Must Match
	- Design right does not subsist in features of shape or configuration of an article which are dependent upon the appearance of another article of which the article is intended by the designer to form an integral part
	- Concerned solely with the appearance of articles (dependency on the appearance of another article) e.g. car door
	- So that manufacturers cannot hold consumers ransom by maintaining a monopoly over a product on the basis of aesthetic conformity _Dyson v Qualtex_
- S 213(3)(a): Methods or Principles of Construction
	- Mere fact that there was some functionality doesn't mean it's excluded _Landor & Hawa v Azure Designs_
		- "The real meaning is this: that no design shall be construed so widely as to give its proprietor a monopoly in a method or principle of construction ... any conception which is so general as to allow several different specific appearances as being made within it, it is too broad and will be invalid"