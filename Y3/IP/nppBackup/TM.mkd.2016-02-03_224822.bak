#Trade Marks

##Introduction

###General

- Sources of law
	- The Trade Marks Directive
	- The Trade Marks Act 1994
		- While this is an implementing legislation, the wording used differs from the directive. 
- Things that can be trade marks
	- Traditional marks: words, numbers, letters, pictures, combinations thereof
	- Non-traditional marks: shapes/sounds/smells/gestures are more contentious
		- Issues: How to enter onto the register; feature vs indication of origin
- Boundary of conduct that is permissible is quite vague
- If mark is not used for ~5 yrs, it will be taken off the register
- Can renew every 10 years indefinitely
- Presumption that a registered trade mark is valid
	- Increased certainty compared to passing off

###Functions of a Mark

- Indication of origin (source rather than location)
- Product differentiation
- Guarantee the product of the product (consistency)
- Producer can use mark to communicate a particular quality to the consumer
- Protection of brand investment (free-rider problem)

###Process of Registration

- Filing of the application (goods and services classification)
- Examination by the Registry
- Publictaion and notification
- Opposition
- Registration

###The Average Consumer

- The average consumer of the goods in question
- Reasonably well informed, observant, and circumspect
- Level of attention to detail depends on the type of goods

##The Meaning of a Trade Mark

###S. 1 TMA Requirements

- A sign
	- In principle, any message that is capable of perception by the senses can constitute a message to the consumer. They are not limited to what can be perceived by the eyes. _Sieckmann v Deutsches Patent- und Markenamt_
	- Clear collection bin on a vacuum cleaner does not constitute a sign because it does not convey specific information _Dyson_
- Capable of graphic representation
	- The mark must unambiguously represent the origin of the product.
	- ECJ held that this requirement is to defining the precise subject matter of protection, making it easier to examine by authorities and making it easier for other traders to know how to avoid infringement. _Sieckmann_
		- Graphic representation by means of lines, images or characters. 
		- Mark must be: ckearr; objective; precise; self-contained; durable; intelligible; easily accessible
		- Chemical formula given didn't represent the specific odour and was not sufficiently clear or accessible
		- Description was not sufficiently clear, precise, and objective.
		- Physical sample was not a graphic representation and was not sufficiently durable
	- Sounds:
		- Musical notation, practical instructions, and verbal description provided for registration in _Shield Mark BV v Joost Kist H.O.D.N. Memex_
			- Muscial stave could constitute a faithful representation of the mark.
			- Verbal description was not sufficient (commented that it would rarely be sufficient)
			- Onomatopoeic representation wasn't sufficiently objective or unequivocal.
		- Sonogram not sufficiently intelligible _Edgar Rice Burroughs_
	- Colours:
		- _Libertel v Benelux Merkenbureau_ concerned the use of the colour orange for telecommunication services (books). In order to represent their mark they included the word orange and a sample of the colour. Question was whether the representation was sufficiently objective and durable. 
			- Sample was not durable enough
			- Verbal description was not clear or precise enough
			- Court held that in the context of colours, issues are very fact specific
			- Colour can be registered by referring to an international standard of colours. (pantone)
	- EU Commission recently published a reform package that no longer requires a graphic representation.
- Capable of distinguishing
	- This is a preliminary requirement and is aimed at excluding the registration of things that are generally incapable of acting as trade marks. If mark is in fact capable of being distinctive, it will also pass this hurdle. Low minimum threshold. This should be read with the absolute grounds. _Koninklijke Philips v Remington_
	
##Absolute Grounds for Refusal to Register a Trade Mark

###Introduction

- Generally concerned with the internal characteristics of the trade mark
	- Looking into the innate quality of the sign in relation to the specific goods or services
- Consider the average consumer
- Three grounds
	- Devoid of distinctive character
		- Ability of mark to act as trade mark
	- Descriptive mark
		- Keeping things free for other traders to use
	- Signs customary in the current language
- Three absolute grounds are overlapping but independent. Each ground must be interpreted with the specific policy reasons behind them in mind. Each has its own underlyinig policy rationale. _SAT.1 SatellitenFernsehen GmbH v OHIM (SAT.2)_
	- Some debate about which policy consideration justifies which ground.

###Marks devoid of any distinctive character

- Policy reason is to exclude marks that do not perform the function of identifying a trade source. _SAT. 2_
	- Initially in _Libertel Groep BV v Benelux-Merkenbureau_, the court was concerned with keeping certaian marks (colours) available for other traders.
- The test
	- Would the average consumer of the specific goods or services would see the mark as indicative of commercial origin.
	- Overall assessment of the mark, including considering the level of attention the consumer would pay.
		- Cannot divide the mark into its elements. Consider whether the whole mark is sufficiently distinctive. _SAT. 2_
	- Non-standard marks?
		- Same test, but harder to satisfy
		- Shapes, smell, and colour would only rarely be distinctive. Consumers would typically depend on the additional text on it. 
		- In _Libertel_, the court said that if the colour is considered to be part of the good, it would not be considered a trade mark. Simple colours are likely to be treated as part of the good and devoid of any distinctive character. 
		- In _Linde_, the court held that it is generally harder to establish that the shape of a product is distinctive, although they might acquire distinctiveness through advertising.
		- In _Procter & Gamble v OHIM_ held that the shape of laundry tablets was devoid of distinctive character. Consumers do not make assumptions of trade origin through the shape of a laundry tablet. Only shapes that significantly depart from the norms and customs of the trade are likely to not be devoid of any distinctive character. 

###Descriptive marks

- Policy reason is to leave certain signs free for other traders to use. Ensure that trade marks do not inhibit competition. Requirement of availability. _OHIM v Wm Wrigley Junior Co (DOUBLEMINT)_
	- Court said that the term does not need to be currently used to describe the goods. It just must be a kind of descriptive term that other traders might want to use. Sufficient if just one possible meaning can be considered to be descriptive. 
	- Care must be exercised when considering whether a geographical name can be registered as a trade mark. _Windsurfing Chiemsee Produktions und Vertriebs GmbH v Boots und Segelzubehor Walter Huber_
		- No free-standing public interest exception for geographical names
- The test
	- In _Windsurfing Chiemsee Produktions und Vertriebs GmbH v Boots und Segelzubehor Walter Huber_, the court said that the question is whether the sign designates a place which is currently associated in the mind of the relevant class of persons with the category of goods concerned, or whether it is reasonable to assume that such an association may be established in the future. 
		- Consider how consumers would interpret the word/sign
			- Degree of consumer familiarity with the name
			- Characteristics of the place designated by the name
			- The category of goods concerned
	- In _Procter & Gamble Company v OHIM (BABY DRY)_, court held alluding to some quality of the product does not make it descriptive per se. In order for a mark to be barred on this ground, it must be wholly descriptive. "Baby Dry" was a syntactically unusual construction (lexical invention) and therefore not descriptive. 
		- Criticised by AG in _Doublemint_ as excessively academic.
		- This case is essentially confined to its facts.
	- In _Doublemint_, CJEU held that an application will be refused under Art 3(1)(c) if at least one of the possible meanings of the mark designates a characteristics of the goods or services concerned.
		- Enough if it could potentially be used descriptively. 
		
###Signs customary in the current language or bona fide and established practices of the trade

- Incapable of being distinguishing goods' commercial origin.
- Probably also protective function.
- No forward looking aspect (compare with descriptive marks)
- In _Merz & Krell_, German registry refused to register the mark "BRAVO" for writing implements. Court held that this covers signs that designate the characteristics of the goods/services and is not confined to terms that describe them. 

###Acquiried distinctiveness

- If consumers now in fact do see these marks as indications of commercial origin despite their lack of intrinsic distinctiveness, they are now registrable. 
	- "No longer purely descriptive"
	- Sign must have taken on a secondary meaning and must be distinctive in fact _Windsurfing Chiemsee Produktions und Vertriebs GmbH v Boots und Segelzubehor Walter Huber_
- Courts will consider _Windsurfing Chiemsee Produktions und Vertriebs GmbH v Boots und Segelzubehor Walter Huber_:
	- the nature of the name in question (the better known the geographical name, the more long-standing and intensive must be the use) 
	- the market share held by the mark
	- how intensive, widespread and long-standing the use has been
	- the amount invested in promoting the mark
	- the proportion of the relevant class of persons who identify the goods as originating from a particular undertaking
	- statements from chambers of commerce & other trade & professional associations
	
###The shapes provision

- S. 3(2)/Art 3(1)(e): Signs which consist exclusively of shapes which result from the nature of the goods or are necessary to obtain a technical result or give substantial value to the goods may not be registered.
	- Allow competition
	- Interpreted widely
	- Shape has to be completely excluded under one category
	- Cannot be overcome by acquired distinctiveness
- The technical result exception ensures that trademark law is not used to sidestep patent law _Dyson_
	- Prevent a single trader from getting a monopoly in a technical solution
- The test:
	- _Phillips v Remington_: Where the essential functional characteristics of the shape of a product are attributable solely to the technical result, can’t register that shape, even if that technical result can be achieved by other shapes.
	- _Lego Juris v OHIM_:
		- "Necessary" doesn't mean the only way
		- Consider the essential characteristics of the shape on a case by case basis. 
		- Consider whether the essential characteristics of the shape perform a technical function. 