#Trade Marks

##Introduction

###General

- Sources of law
	- The Trade Marks Directive
	- The Trade Marks Act 1994
		- While this is an implementing legislation, the wording used differs from the directive. 
- Things that can be trade marks
	- Traditional marks: words, numbers, letters, pictures, combinations thereof
	- Non-traditional marks: shapes/sounds/smells/gestures are more contentious
		- Issues: How to enter onto the register; feature vs indication of origin
- Boundary of conduct that is permissible is quite vague
- If mark is not used for ~5 yrs, it will be taken off the register
- Can renew every 10 years indefinitely
- Presumption that a registered trade mark is valid
	- Increased certainty compared to passing off

###Functions of a Mark

- Indication of origin (source rather than location)
- Product differentiation
- Guarantee the product of the product (consistency)
- Producer can use mark to communicate a particular quality to the consumer
- Protection of brand investment (free-rider problem)

###Process of Registration

- Filing of the application (goods and services classification)
- Examination by the Registry
- Publictaion and notification
- Opposition
- Registration

###The Average Consumer

- The average consumer of the goods in question
- Reasonably well informed, observant, and circumspect
- Level of attention to detail depends on the type of goods

##The Meaning of a Trade Mark

###S. 1 TMA Requirements

- A sign
	- In principle, any message that is capable of perception by the senses can constitute a message to the consumer. They are not limited to what can be perceived by the eyes. _Sieckmann v Deutsches Patent- und Markenamt_
	- Clear collection bin on a vacuum cleaner does not constitute a sign because it does not convey specific information _Dyson_
- Capable of graphic representation
	- The mark must unambiguously represent the origin of the product.
	- ECJ held that this requirement is to defining the precise subject matter of protection, making it easier to examine by authorities and making it easier for other traders to know how to avoid infringement. _Sieckmann_
		- Graphic representation by means of lines, images or characters. 
		- Mark must be: ckearr; objective; precise; self-contained; durable; intelligible; easily accessible
		- Chemical formula given didn't represent the specific odour and was not sufficiently clear or accessible
		- Description was not sufficiently clear, precise, and objective.
		- Physical sample was not a graphic representation and was not sufficiently durable
	- Sounds:
		- Musical notation, practical instructions, and verbal description provided for registration in _Shield Mark BV v Joost Kist H.O.D.N. Memex_
			- Muscial stave could constitute a faithful representation of the mark.
			- Verbal description was not sufficient (commented that it would rarely be sufficient)
			- Onomatopoeic representation wasn't sufficiently objective or unequivocal.
		- Sonogram not sufficiently intelligible _Edgar Rice Burroughs_
	- Colours:
		- _Libertel v Benelux Merkenbureau_ concerned the use of the colour orange for telecommunication services (books). In order to represent their mark they included the word orange and a sample of the colour. Question was whether the representation was sufficiently objective and durable. 
			- Sample was not durable enough
			- Verbal description was not clear or precise enough
			- Court held that in the context of colours, issues are very fact specific
			- Colour can be registered by referring to an international standard of colours. (pantone)
	- EU Commission recently published a reform package that no longer requires a graphic representation.
- Capable of distinguishing
	- This is a preliminary requirement and is aimed at excluding the registration of things that are generally incapable of acting as trade marks. If mark is in fact capable of being distinctive, it will also pass this hurdle. Low minimum threshold. This should be read with the absolute grounds. _Koninklijke Philips v Remington_
	
##Absolute Grounds for Refusal to Register a Trade Mark

###Introduction

- Generally concerned with the internal characteristics of the trade mark
	- Looking into the innate quality of the sign in relation to the specific goods or services
- Consider the average consumer
- Three grounds
	- Devoid of distinctive character
		- Ability of mark to act as trade mark
	- Descriptive mark
		- Keeping things free for other traders to use
	- Signs customary in the current language
- Three absolute grounds are overlapping but independent. Each ground must be interpreted with the specific policy reasons behind them in mind. Each has its own underlyinig policy rationale. _SAT.1 SatellitenFernsehen GmbH v OHIM (SAT.2)_
	- Some debate about which policy consideration justifies which ground.

###Marks devoid of any distinctive character

- Policy reason is to exclude marks that do not perform the function of identifying a trade source. _SAT. 2_
	- Initially in _Libertel Groep BV v Benelux-Merkenbureau_, the court was concerned with keeping certaian marks (colours) available for other traders.
- The test
	- Would the average consumer of the specific goods or services would see the mark as indicative of commercial origin.
	- Overall assessment of the mark, including considering the level of attention the consumer would pay.
		- Cannot divide the mark into its elements. Consider whether the whole mark is sufficiently distinctive. _SAT. 2_
	- Non-standard marks?
		- Same test, but harder to satisfy
		- Shapes, smell, and colour would only rarely be distinctive. Consumers would typically depend on the additional text on it. 
		- In _Libertel_, the court said that if the colour is considered to be part of the good, it would not be considered a trade mark. Simple colours are likely to be treated as part of the good and devoid of any distinctive character. 
		- In _Linde_, the court held that it is generally harder to establish that the shape of a product is distinctive, although they might acquire distinctiveness through advertising.
		- In _Procter & Gamble v OHIM_ held that the shape of laundry tablets was devoid of distinctive character. Consumers do not make assumptions of trade origin through the shape of a laundry tablet. Only shapes that significantly depart from the norms and customs of the trade are likely to not be devoid of any distinctive character. 

###Descriptive marks

- Policy reason is to leave certain signs free for other traders to use. Ensure that trade marks do not inhibit competition. Requirement of availability. _OHIM v Wm Wrigley Junior Co (DOUBLEMINT)_
	- Court said that the term does not need to be currently used to describe the goods. It just must be a kind of descriptive term that other traders might want to use. Sufficient if just one possible meaning can be considered to be descriptive. 
	- Care must be exercised when considering whether a geographical name can be registered as a trade mark. _Windsurfing Chiemsee Produktions und Vertriebs GmbH v Boots und Segelzubehor Walter Huber_
		- No free-standing public interest exception for geographical names
- The test
	- In _Windsurfing Chiemsee Produktions und Vertriebs GmbH v Boots und Segelzubehor Walter Huber_, the court said that the question is whether the sign designates a place which is currently associated in the mind of the relevant class of persons with the category of goods concerned, or whether it is reasonable to assume that such an association may be established in the future. 
		- Consider how consumers would interpret the word/sign
			- Degree of consumer familiarity with the name
			- Characteristics of the place designated by the name
			- The category of goods concerned
	- In _Procter & Gamble Company v OHIM (BABY DRY)_, court held alluding to some quality of the product does not make it descriptive per se. In order for a mark to be barred on this ground, it must be wholly descriptive. "Baby Dry" was a syntactically unusual construction (lexical invention) and therefore not descriptive. 
		- Criticised by AG in _Doublemint_ as excessively academic.
		- This case is essentially confined to its facts.
	- In _Doublemint_, CJEU held that an application will be refused under Art 3(1)(c) if at least one of the possible meanings of the mark designates a characteristics of the goods or services concerned.
		- Enough if it could potentially be used descriptively. 
		
###Signs customary in the current language or bona fide and established practices of the trade

- Incapable of being distinguishing goods' commercial origin.
- Probably also protective function.
- No forward looking aspect (compare with descriptive marks)
- In _Merz & Krell_, German registry refused to register the mark "BRAVO" for writing implements. Court held that this covers signs that designate the characteristics of the goods/services and is not confined to terms that describe them. 

###Acquiried distinctiveness

- If consumers now in fact do see these marks as indications of commercial origin despite their lack of intrinsic distinctiveness, they are now registrable. 
	- "No longer purely descriptive"
	- Sign must have taken on a secondary meaning and must be distinctive in fact _Windsurfing Chiemsee Produktions und Vertriebs GmbH v Boots und Segelzubehor Walter Huber_
- Courts will consider _Windsurfing Chiemsee Produktions und Vertriebs GmbH v Boots und Segelzubehor Walter Huber_:
	- the nature of the name in question (the better known the geographical name, the more long-standing and intensive must be the use) 
	- the market share held by the mark
	- how intensive, widespread and long-standing the use has been
	- the amount invested in promoting the mark
	- the proportion of the relevant class of persons who identify the goods as originating from a particular undertaking
	- statements from chambers of commerce & other trade & professional associations
	
###The shapes provision

- S. 3(2)/Art 3(1)(e): Signs which consist exclusively of shapes which result from the nature of the goods or are necessary to obtain a technical result or give substantial value to the goods may not be registered.
	- Allow competition
	- Interpreted widely
	- Shape has to be completely excluded under one category
	- Cannot be overcome by acquired distinctiveness
- The technical result exception ensures that trademark law is not used to sidestep patent law _Dyson_
	- Prevent a single trader from getting a monopoly in a technical solution
- The test:
	- _Phillips v Remington_: Where the essential functional characteristics of the shape of a product are attributable solely to the technical result, can’t register that shape, even if that technical result can be achieved by other shapes.
	- _Lego Juris v OHIM_:
		- "Necessary" doesn't mean the only way
		- Consider the essential characteristics of the shape on a case by case basis. 
		- Consider whether the essential characteristics of the shape perform a technical function. 
		
##Infringement

###Introduction

- Three kinds:
	- Art 5(1)(a) TMD/s10(1) TM Act: Double identity
		- Use of mark
		- In the course of trade
		- In relation to goods or services
	- Art5(1)(b), s10(1) & (2): Confusingly similar use
		- Similarity or identity between mark and goods/services
		- Likelihood of confusion
	- Art 5(2)/s10(3): Marks with a reputation
		- Use of similar or identical mark
		- Goods or services not similar to those for which the mark was registered
		- Proprietor of mark has a reputation
		- Use of that sign without due cause takes unfair advantage of or is detrimental to the distinctive character or the repute of the mark

###Trade mark use

- Non-exhaustive list given in Art 5(3) / s10(4)
- Griffiths: types of uses
	1. origin-guaranteeing use: use by D to indicate origin of its goods/services (direct conflict with D's rights)
	1. origin-describing use: use to identify or refer to goods/services of the designated kind that do have the origin that C's TM signifies e.g. _BMW v Deenik_
	1. purely descriptive use: use to indiicate that certain goods/services of the designated kind have characteristics  whicih products with origin that C's TM signifies are known to possess
	1. descriptive use: use to convey info about the characteristics of goods/services of the designated kind because the sign has an intrinsic or underlying meaning and consumers (or others) are likely to perceive it in this way rather than as signifying that the goods or services have a specific origin.
	1. use of C's mark as a feature or attribute of D's goods e.g. "contains LYCRA"
	1. use of C's mark as a decorative of aesthetic feature e.g. logos on a t-shirt
	- Griffiths think that 2-6 could potentially constitute confusingly similar use but should not automatically constitute infringement of TM rights
- Historically, 1, 2 of Griiffiths categories constitute infringement under the old Act. 
	- In _Mothercare v Penguin Books_, Dillion LJ held that even where descriptive words have been legitimately registered as a mark, their use in the descriptive sense still does not constitute infringement
- Post harmonisation
	- Art. 5(1) suggests "all course of trade", but Recital 11 emphasises that marks are used as an indication of origin
	- AG Jacobs suggests that use falling within Art 5(1)(a) are presumed to be confusion
		- Similar to Art 16 TRIPS
		- This interpretation means that Art 5(1)(a) removes the evidential burden placed on the TM owner under Art 5(1)(b) to have to prove confusion
	- Initially, ECJ adopted the traditional english approch and interpreted "use" as use to indicate origin and defences dealt with over-inclusiveness _BMW v Deenik_
	- In _Hölterhoff v Ulrich Freiesleben_, ECJ held that the use of a TM for purely descriptive uses where there is no potential confusion as to origin. 
		- Odd because there is a defence that covers such uses
		- Use in question cannot be prohibited because it did not infringe any of the interests Art 5(1) is intended to protect
	- In _Arsenal v Reed_, Reed sold unauthorised Arsenal-branded merchandise with identical signs explicitly saying they're not official goods (no confusion as to origin).
		- AG's opinion:
			 - To state that a registered proprietor may prevent a third party from using “the trade mark as a trade mark” is as good as saying nothing at all
			 - TM has lots of functions and origin-identifying is only one staging post on the road to the final objective: to ensure a system of genuine competition in the internal market 
			- Limiting TM's function to origin alone is "simplistic reductionism"
		- CJEU:
			- Reed's use is "in the course of trade" since it takes place in the context of commercial activity with a view to economic advantage
			- Art 5(1) doesn't give absolute protection. Instead provides protection of specific interests of proprietors
			- Essential function is to guarantee the identity of origin of the marked goods or services to the consumer or end user by enabling him, without any possibility of confusion, to distinguish the goods or services from others which have another origin.
			- Material connection to the proprietor of the TM. Post-sale confusion.
		- EWHC: ECJ exceeded its powers by deciding on facts rather than giving general guidance
		- EWCA:
			- ECJ was not concerned with whether use complained about is TM use. Consideration is whether 3P's use affects or is likely to affect the functions of TM. An instance of where that will occur is given, namely where a competitor wishes to take unfair advantage of the reputation of the TM by selling products illegally bearing the mark. That would happen whether or not 3rd P’s use was TM use or whether there was confusion
				- Me: Question comparing with the marks with a reputation offence?
	- In _R v Johnstone_, case concerned the sale of bootleg Bon Jovi CDs. For there to be criminal TM infringement, there had to be TM infringement. Johnstone argued that the TM use was purely descriptive. UKHL accepted the argument. UKHL appears to imply that you can't harm the essential use of a TM if you're not using it as a TM (to indicate origin)
		- Appears to be using traditional common law approach
		- Norman argues that this may be justified on the necessity to give magistrates a simple rule to apply.
		- Lords Nicholls and Walker appear to assume that descriptive use is the converse of TM use.
		- Treat this case as confined to s. 92 (authority?)
			- Subsequent ECJ approach consistently departs from this approach
	- In _Adam Opel v Autech_, case concerned infringement under Art 5(1)(a).
		- Court held that Art 5 is to protect the functions of proprietor's TM. Infringement only if D's use affects or is liable to affect C's use of the TM in its essential function of guaranteeing origin of goods.
		- If public don't see OPel logos on toys as an indication that cars come from Adam Opel or an undertaking economically linked to it, the essential function of the mark is not affected
			- Fundamental issue of confusion?
	- In _L'Oreal v Bellure_, case concerned smell-alike perfumes perfume comparison lists. Court acknowledged that there was no impact on the TM's origin function. Court held that there was potential damage to the reputation of the mark. Infringement of Art 5(1)(a) if use would adversely affect the other functions of marks such as "communication, investment or advertising". (non-exhaustive list)
		- How does this protect marks if they have not acquired a particular reputation
	- In _Google France_, court reiterated that Art 5(1)(a) is to protect the functions of TM. Infringement when it would adversely these functions (not only origin designation but also other functions). 
		- Court held that protection under Art 5(1)(a) is more extensive than Art 5(1)(b)
			















