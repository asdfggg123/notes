#Sale of Goods

##Passage of Property

###Introduction

- Types of property
	- Real property (land)
	- Chattels real (leasehold interest in land)
	- Personal chattels
		- Things in possession
		- Intangible property
			- Documentary intangibles e.g. cheque, bearer shares
			- Pure intangibles e.g. debt, share, patent
		- Money
- Significance of property rights
	- Right in rem vs right in personem
	- Affects remedies available to parties
	- Distribution of risk between parties
	- Ability to give a third party good title
	- Claims in insolvency
	- Fluctuation in property value
	- Frustration if goods perish (contract only frustrated if goods perish when they are seller's property)
	- Property gives rise to causes of action in tort 
		- Conversion
			- Interference with another's right to possession
			- Remedy per ss 1, 3 Torts (Interference with Goods) Act 1977
				- Damages
				- Order for delivery
			- As stated in _OBG v Allen_ __UKHL__
			> Anyone who converts a chattel, that is to say, __does an act inconsistent with the rights of the owner, however innocent he may be, is liable for the loss caused__ which, if the chattel has not been recovered by the owner, will usually be the value of the goods. Fowler v Hollins was a claim for conversion of bales of cotton bought in good faith through a broker in Liverpool. The purchasers were nevertheless held strictly liable. Cleasy B said robustly that: �the liability under it is founded upon what has been regarded as a salutary rule for the protection of property, namely, that __persons deal with the property in chattels or exercise acts of ownership over them at their peril__
		- Negligence
			- Where one party contractually assumes risk for goods but does not take the title to them, they have no cause of action against third parties who negligently damage the goods _The Aliakmon_ __UKHL__
		- Imposes a duty of non-interference on the rest of the world
			- No defence that third party was unaware of owner's right _Fowler v Hollins_
			- Note that third party is not liable for a mere interference with use _Spartan Steel v Martin_
			- Interference with the thing is sufficient to constitute a tort; no interference with use is necessary _The Mediana_
- Equitable rights
	- Where a party is the absolute owner of some property, he does not simultaneously hold an equitable interest in that property. The legal title carries with it all rights. _Westdeutsche Landesbank Girozentrale v Islington LBC_ 
	- Binds third parties in some situations, subject to defences and priority rules
- Default rule is that unless otherwise agreed, risk (of innocent damage or loss of goods) remains with the seller until property passes to the buyer _Healy v Howlett_
	- This principle is now codified by s. 20 SGA
- Until property in goods passes to the buyer, the seller may not sue for the price of the goods unless there is a contractually stipulated date of payment irrespective of delivery per s 49 SGA.

###Sale of Goods Act 1979

####Introduction

- S. 2(1): Contract of sale of goods is a contract by which the seller transfers or agrees to transfer the property in goods to the buyer for a money consideration.
	- s. 2(4): Where the property in goods is transferred, it is called a sale.
	- s. 2(5): Where the transfer of property takes place at a future time or subject to some condition, it is an agreement to sell.
	- s. 2(6): An agreement to sell becomes a sale when the time elapses or the conditions are fulfilled. 
	- Note: Equity does not operate on a contract of sale of goods to see as done what ought to be done (i.e. it does not create an equitable interest)
		- This was established in _In re Wait_ and _The Aliakmon_
- S. 61(1): Goods defined as all personal chattels other than things in action and money.
- S. 5(1): Act applies to both existing and future goods (goods to be manufactured or acquired by the seller after making the contract).
	- S. 5(3): A contract that purports to effect a present sale of future goods operates as an agreement to sell the goods.
- Passing of property requires both identification and intention.
- Parties must agree to the specific goods to which the contract attaches. Mere description is not sufficient. The very goods to be sold must be ascertained. - Lord Blackburn, The Effect of the Contract of Sale (1845)
	- Under a contract for sale of chattels not specific, the property does not pass to the purchaser unless there is afterwards an appropriation of the specific chattels to pass under the contract. _Mirabita v Imperial Ottoman Bank_ 
	
####Identification

- S. 16: Where there is a contract for the sale of unascertained goods, no property in the goods is transferred until the goods are ascertained.
- S. 17(1): Where there is a contract for the sale of specific or ascertained goods, the property in them is transferred to the buyer at such time as the parties to the contract intend it to be transferred.
	- (2): Regard shall be had to the terms of the contract, the conduct of the parties, and the circumstances of the case when ascertaining their intention.
- Per s. 61(1): 
	- Specific goods are goods identified and agreed on at the time a contract of sale is made and includes and undivided share, specified as a fraction or percentage, of goods identified and agreed on as aforesaid.
- "Ascertained" means identified in accordance with the agreement after the time a contract of sale is made _Re Wait_
	- Where goods form part of a bulk, but have not been earmarked, identified or appropriated under the contract, the goods have not been ascertained. 
	- Moving bottles into a separate storage unit suffices for ascertainment _Re Stapylton Fletcher Ltd_
		- It is the segregation of stock from the company's trading assets, whether done physically or by giving instructions to a bonded warehouse keeper, which causes the goods to be ascertained for the purpose of section 16. 
- S. 20A: In a contract for the sale of a specified quantity of unascertained goods, where goods form part of a bulk identified in the contract or by subsequent agreement and the buyer has paid the price for some or all of the goods:
	- (2): Property in an undivided share in the bulk is transferred to the buyer and the buyer becomes an owner in common of the bulk. 
	- (3): The undivided share of a buyer in a bulk is (quantity of goods paid for)/(quantity of goods in the bulk) at that time.
	- (4): Where the aggregate of the undivided shares exceed the whole of the bulk at that time, the undivided share of each buyer shall be reduced proportionately so that the aggregate of the undivided shares is equal to the whole bulk.
	- (5): Where the buyer has only paid for some of the goods due to him, delivery to the buyer out of the bulk shall be ascribed first to the goods in respect of which payment has been made. 
- S. 20B: Deemed consent of any owner in common of goods in a bulk to delivery of goods to any other owner in common or any dealing with goods in the bulk by any other person who is an owner in common. 

####Intention

- Rules for ascertaining parties' intention given in s. 18 SGA
- Rule 1:
	- Where there is an unconditional contract for the sale of specific goods in a deliverable state, property passes to the buyer when the contract is made and it is immaterial whether the time of payment or the time of delivery are postponed.
		- Deliverable state is defined in s. 61(5) to mean a state of the goods that the buyer would under the contract be bound to take delivery of them.
			- Does not depend on seller having good title _Kulkarni v Motor Credit_
	- The exclusion of this rule must take place at the time when the contract is concluded; attempting to retain property in goods following the conclusion of an unconditional contract for sale would be ineffective as property has already passed _Dennant v Skinner and Collom_
		- Even after property passes, the seller might still have a right to possession until he parts with possession of the goods.
	- _Clough Mill v Martin_ provides an example of an effective retention of title clause.
		- Note that any retention of title over items produced from goods sold would constitute a charge which would need to be registered in the Companies House if granted by a company.
- Rule 2:
	- Where there is a contract for the sale of specific goods and the seller is bound to do something to do the goods for the purpose of putting them in a deliverable state, property does not pass until the thing is done and the buyer has notice that it has been done
	- The test is whether anything remained to be done to the thing by the sellers to put it in a deliverable state (a state in which the thing will be the article contracted for by the buyer) _Underwood Ltd v Burgh Castle 
		- This does not refer to packing or anything of that kind, but rather situations such as when assembly or disassembly was necessary to produce the specific thing contracted for.
- Rule 3:
	- Where there is a contract for the sale of specific goods in a deliverable state but the __seller__ is bound to weigh, measure, test, or do some other act or thing with reference to the goods for the purpose of ascertaining the price, the property does not pass until the act or thing is done and the buyer has notice that it has been done.
	- Note that this rule does not apply when someone other than the seller is to weigh, measure, test or do some other act. If parties seek to make passage of property conditional on a third party doing such acts, it must be a clear contractual provision. _Nanka-Bruce v Commonwealth Trust_ __UKPC__
- Rule 4:
	- When goods are delivered to the buyer on approval or on sale or return or other similar terms, property in the goods passes to the buyer when he signifies his approval or acceptance to the seller or does any other act adopting the transaction; if he does not signify his approval or acceptance to the seller but retains the goods without giving notice of rejection, then property passes on the expiration of a stipulated time period, or in its absence, the expiration of a reasonable time. 
	- Any act by the buyer which would be inconsistent with his being other than the buyer would cause property to pass. _Kirkham v Attenborough_
- Rule 5:
	- (1): Where there is a contract for the sale of unascertained or future goods by description, and goods of that description in a deliverable state are unconditionally appropriated to the contract, either by the seller with the assent of the buyer or by the buyer with the assent of the seller, property passes to the buyer; the assent may be express or implied, and may be given either before or after the appropriation is made. 
		- Packing and marking with shipping marks were not be regarded as intented appropriation but rather preparation for shipment (contract stipulated that property passes upon shipment) in _Carlos Federspiel v Charles Twigg_.
			- Mere setting apart or selection of the goods by the seller is insufficient. To constitute an appropriation of the goods to the contract, the parties must have had, or be reasonably supposed to have had, an intention to __attach the contract irrevocably to the goods__, so that those goods and no others are the subject of the sale and become the property of the buyer.
			- It is by agreement of the parties that the appropriation is made, although in some cases the buyer's assent to an appropriation by the seller is conferred in advance.
	- (2): Where, in pursuance of the contract, the seller delivers the goods to the buyer, or to a carrier or other bailee or custodier for the purpose of transmission to the buyer, and does not reserve the right of disposal, he is taken to have unconditionally appropriated the goods to the contract. 
		- Example case is _Wardar's (Import & Export) Co v W Norwood & Sons_
	- (3): Where there is a contract for the sale of a specified quantity of unascertained goods in a deliverable state forming part of an identified bulk and the bulk is reduced to (or to less than) that quantity, then, if the buyer under the contract is the only buyer to whom goods are then due out of the bulk, the remaining goods are taken as appropriated to that contract and property in those goods passes to that buyer. 
	- (4): The rule in (3) also applies where the bulk is reduced to (or less than) the aggregate of the quantities due to a single buyer under separate contracts. 

##Transfer of Title

###Introduction

- SS 16-20: Transfer of seller's property to the buyer
- SS 21-26: Exceptional circumstances where even though the seller does not have property in the goods or has defective property, he can nevertheless transfer the property, or as the case may be, transfer the property free from the defect, to a person who buys in good faith.
- Basic rule is _nemo dat quod non habat_ e.g. _Cundy v Lindsay_
	- Now codified in s. 21 SGA
	- Example case is in _Farquharson Bros v King_ __UKHL__:
		- FB appointed Capon as agent with some authority to sell timber on behalf of FB. C fraudulently assumed the name of Brown and purported to sell timber outside authority to K.
		- UKHL held that there was no estoppel because K were not misled in any way by FB's conduct; FB was not precluded from denying Capon's authority to sell.
- Battersby & Preston: defences to the nemo dat principle do not necessarily grant indefeasible title. The notion here is similarly relative, and the majority of the exceptions operate not by conferring a perfect title on the ultimate purchaser, but only by overriding a prior transaction or interest. 
- Note that for this section, the parties are: 
	- A: The party with the superior property in the goods (might be the owner)
	- S: The seller of the goods
	- B: The purchaser of the goods

###Defence 1: A's Consent

- If S sells goods with A's consent or authority, A cannot assert his right against B.
- This applies even if A's does not have absolute property in the goods. 
- A's consent only gives B a defence against A's pre-existing property right, but if A is not the true owner, A's consent is not a defence against the owner's property right.
- Contrast with situation where S holds goods on trust for A and transfers goods without the authority of A, whereby whereby priority rules apply.

###Defence 2: Estoppel

- Where A creates an appearance that S has the requisite authority (that he in fact lacks) or that S is an owner, A is precluded from asserting A's right against B.
- Note that B acquires real title (A's title) _Eastern Distributors v Goldring_

###Defence 3: S. 2 Factors Act 1889

- If S, a factor (a mercantile agent), with A's consent, has physical control of A's goods and makes a disposition of the goods in the ordinary course of S's business and B acquires his right in good faith without notice that S exceeded  his authority, then the disposition is valid as if S were expressly authorised by A to make that disposition.
- Example is Folkes v King, where A gives S authority to sell his car for �575 or more, and S sells to B for �340, and B is unaware of the limit on S's authority, B acquires good title.

###Defence 4: S. 24 SGA, S. 8 FA

- If S, despite having sold goods to A, is or continues to be in possession of A's goods/documents of title to A's goods, and S makes a disposition of the goods to B or agrees to do so, and S delivers the goods/documents of title to B, and B receives the goods/documents of title in good faith without notice of the previous sale to A, then the disposition to B has the same effect as if S were expressly authorised by A.
- Note that the capacity in which S retains possession is not crucial _Pacific Motor Auctions Property Ltd v Motor Credits (Hire Finance) Ltd_

###Defence 5: S. 25(1) SGA, S. 9 FA

- If A sells or agrees to sell goods to S and allows S to take physical control of those goods/document of title before property passes to S, and S makes a disposition of the goods to B or agrees to do so and S delivers the goods/documents of title to B and B receives the goods/documents of title in good faith without notice of A's right, the disposition to B has the same effect as if S were a mercantile agent in possession of the goods/documents of title with A's consent. 
- Note: B's right to the property is not perfected by this defence. It is subject to the same defects of A's title. _National Employers Mutual General Iinsurance Association v Jones_

###Defence 6: S. 27 Hire Purchase Act 1964

- If A and S concluded a hire-purchase agreemeent or a conditional sale of a motor vehicle and allows S to take possession before property vests in him, and S subsequently disposes of the vehicle to B who is a private purchaser in good faith without actual notice or a trade or fiance purchaser who then sells to a private purchaser in good faith without notice, that disposition shall take effect as if A's title to the vehicle has been vested in the debtor immediately before that disposition. 
- Illustrated in _Kulkarni v Manor Credit Ltd_:

###Defence 7: S. 23 SGA

- If A's pre-existing right is a power to rescind a transfer to S and thus regain title, and S sells to B before A has exercised that power to rescind, and B buys in good faith without notice of the facts giving A a power to rescind (constructive notice), then B acquires good title to the goods. 
- Normally, the exercise of the power to rescind must be communicated to S, but where this is not possible, it might be sufficient for A to take all reasonable steps to communicate his exercise of the power _Car & Universal Finance Ltd v Caldwell_
- Note: Original contract between S and A must be voidable (not void) for this defence to apply. 
	- Mistake/misrepresentation distinction. 
	- See cases of _Cundy v Lindsay_, _Lewis v Averey_, _Shogun Finance v Hudson_

##Contractual Duties of Buyer and Seller

###Overview

- S. 27 SGA: Duty of seller to deliver the goods and duty of buyer to accept and pay for them in accordance with the terms of the contract.
- Express and implied terms:
	- Where contract is silent, sellers duties are found in:
		- Ss 12-15: Ownership and quality
		- S 28: Duty to hold himself ready and willing to deliver
		- S 27: Duty to deliver
		- Ss 29-32: Manner of delivery
	- Buyer's duties:
		- S 27: duty to pay and accept
		- S 28: Duty to hold himself ready and willing to pay the price
		- S 37: Duty to take delivery
- Types of contractual terms
	- Warranty: A minor term of contract "collateral to the main object of it" (_Chanter v Hopkins_) breach of which gives the innocent party standard contractual remedies and he remains bound to perform under the contract.
	- Condition: A term that gives to the root of the contract, breach of which gives the innocent party a power to terminate the contract.
- In the event of a repudiatory breach of contract, the innocent party may elect to:
	- Terminate the contract, which irrevocably absolves both parties from future performance of their obligations under the contract; or
	- Keep the contract alive, which would give him a right to claim/receive performance as well as a claim for damages for loss resulting from the breach.
- Note: termination and recission are conceptually different (_Johnson v Agnew_, _Heyman v Darwins_):
	- Recission (recission ab initio) treats the contract in law as never having been entered into.
	- Termination (acceptance of repudiatory breach) has the effect of ending or disicharging a contract that has come into existence. 
		- Courts sometimes use the word recission when referring to acceptance of repudiatory breach.
- Whether a term is a condition or a warranty depends on the construction of the contract; a stipulation may be a condition even though it is called a warranty in the contract. _s 11(3) SGA_
- Sellers LJ in _Hong Kong Fir Shipping Co Ltd v Kawasaki Kisen Kaisha Ltd_:
> The formula for deciding whether a stipulation is a condition or a warranty is well recognised; the difficulty is in its application. 
	
	- Where an event occurs the occurence of which neither the parties nor Parliament have expressly stated will discharge one of the parties from further performance of his undertakings, it is for the court to determine whether the event has this effect or not. - Diplock LJ
- Concept of intermediate terms applies to sale of goods contractas _The Hansa Nord_ __EWCA__
- In the event of a breach of intermediate terms, the question is whether the breach would deprive the party who has further undertakings still to perform of substantially the whole benefit which was intended that he should obtain from the contract as consideration for performing those undertakings. _Hong Kong Fir Shipping_

###Terms Implied in SGA in Favour of the Buyer

- S 12(1): __C__ (s 12(5A)): S has the right to sell the goods, or will do at the time when the property is to pass
- S 12(2): __W__ (s 12(5A)): Goods are free from a charge or encumbrance not known to B and B will enjoy quiet possession of the goods
- S 13: __C__ (s 13(1A)): In a contract for the sale of goods by description, the goods will correspond with the description.
- S 14(2): __C__ (s 14(6)): Where S sells goods in the course of a business, goods supplied under contract are of satisfactory quality.
- S 14(3): __C__ (s 14(6)): Where S sells in the course of a business, and B makes known to S a particular purpose for which the goods are bought, goods are reasonably fit for that purpose.
- S 15(2) __C__ (s 15(3)): In a sale by sample, the bulk will correspond with the sample in quality and the goods will be free from any defect, making their quality unsatisfactory, not be apparent on reasonable examination of sample. 
- Abovementioned terms do not apply to contracts to which Ch 2 of Part 1 of the Consumer Rights Act 2015 applies. 
- In the absence of reasons to conclude otherwise, where S delivers to B a quantity of goods less than he contracted to sell, B may reject them, but if B accepts the goods he must pay for them. _s 30(1) SGA_
	- Delivery of wrong quantity not a condition unless time is of the essence
	- S 30(2A): B does not have a right to reject where excess or shortfall in delivery is so small that rejection would be unreasonable.
- S 15A: Where S breaches a term implied by ss 13, 14, or 15 but the breach is so slight that it would be unreasonable for him to reject the goods, the breach is not to be treated as a breach of condition but may be treated as a breach of warranty unless a contrary intention appears in or is to be implied from the contract.
	- (3): Burden of proof falls on the Seller.
- Stipulations as to time:
	- S 10 (1): Stipulations as to time of payment are not of the essence the the absence of a contrary intention.
	- S 10(2): Whether any other stipulations as to time is of the essence of the contract depends on the terms of the contract. 
		