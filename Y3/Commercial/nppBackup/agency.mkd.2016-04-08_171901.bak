#Agency

##Introduction

- Three parties are involved in an agency situation
- A true agent puts his principal into a contractual relationship with a third party - Sir Donaldson MR _Potter v Customs & Excise Commissioners_
- Hohfeld and Dowrick: The essential characteristic of an agent is that he is consensually invested with a legal power to alter his principal�s legal relations with third persons: the principal is under a correlative liability to have his legal relations with third parties altered. This power-liability relation, is the essence of the relationship of principal and agent.
- The concept of a power
	- A power is a type of legal interest
	- A power holder is one who, through their own violation, is able to effect a change in a given legal relation
	- A power has a corresponding liability
	- Those subject to a liability may have their own legal relations changed when a power is exercised

##The External Aspect of an Agency

- It is necessary to distinguish between the situations where an agent has actual authority or apparent authority. - Diplock LJ in _Freeman & Lockyer v Buckhurst Park Properties_ 
	- Actual and apparent authority are independent of one another. Although they generally co-exist, it is possible for either to exist independantly of the other. 
- Krebs argues that on his contract law conception of agency relationship, there is no real distinction between actual and apparent authority. 

###Actual Authority in a Disclosed Situation

- An agent's actual authority is the legal power conferred upon him by his principal to alter his principal's legal relations with third parties.
	- Actual authority is created consensually and its scope is determined by applying principles of construction to the agency agreement. If the agent enters into a contract pursuant to his actual authoritty, his actions create contractual rights and liabilities between the principal and the third party. - Diplock LJ in _Freeman & Lockyer_
- Doctrinal theories as to how the agent creates direct contractual relations between his principal and the third party:
	- Magic (as referred to by Krebs)
		- When A properly exercises his power, he changes P's legal position in relation to the third party. 
	- General contractual principles (advocated by Krebs)
		- The agent with actual authority might be thought of a messenger, communicating on behalf of the principal but taking no part in substantive events himself.
		- Krebs considers the role of the agent analogous to that of the ticket machine in _Thornton v Shoe Lane Parking_. P makes the offer through A, and X's acceptence does not need to be communicated to P to complete the contract. Krebs therefore argues that the X's contractual liability to the principal can be explained on straightforward contractual grounds.
			- However, it is quite difficult to set out the scope of P's offer under Krebs' account, especially where A and X enter into extended negotiations as is common in commercial practice. Even in the simplistic example of where P appoints A to sell his car for no less than �300, P is not contractually bound to sell his car to anyone who offers A �300. A is entitled to negotiate on P's behalf to secure a better price (without P's intervention), even though P's offer under Krebs' account would merely be to sell his car for no less than �300. This could perhaps be explained on the basis that P offers to sell his car on the conditions that the price is above �300 and A agrees to it. However, this then fails to explain the basis on which A's fiduciary duty to P arises, if A merely holds a power under P's offer. A fiduciary duty is significantly more onerous than a mere obligation to act in good faith (if there is one under _BT v Telefonica principles_). Furthermore, if A is merely a messenger, there is little justification for English law to impose fiducary duties on such agents. 
- The scope of A's actual authority
	- The scope may be expressly set out in the agency agreement (or mutual consent) _Ramsey v Love_
	- The scope may also be set out impliedly through parties conduct _Hely-Hutchinson v Brayhead_
		- Facts: A, chairman of P (a company), entered into a contract with X. A's official position did not confer upon him the authority to enter into such contracts, but because P acquiesced to A acting as the de facto managing director of the company (for a significant period of time), P's conduct had the effect of impliedly conferring upon A the authority of a managing director to enter the company into such contracts. 
	- The general rule is that the scope of an agent's authority necessarily includes powers to perform intermediate acts which are not expressed but which are necessary in order to attain the accomplishment of the object of the principal power. - Eyre CJ in _Howard v Baillie_
	
###Apparent Authority in a Disclosed Situation

- Diplock LJ in _Freeman & Lockyer_: A has apparent authority where:
	- P makes a (clear and unambiguous) representation to X that A has the authority to enter into a certain kind of contract.
	- P's representation was intended to be and in fact acted upon by X.
		- P does not need to be aware that he is making a representation.
		- The representation may be by words or conduct.
			- For example, where P permits A to act in some way in the conduct of P's business, he represents to anyone who becomes aware of the circumstances that A has the actual authority that a person acting in his capacity would usually have. 
	- X, (reasonably) relying on P's representation, purported to enter into a contract with P via A.
		- This is a causal requirement; X merely has to show that P's representation was a significant factor in his decision to enter into the contract with A. _Steria v Hutchinson_ __EWCA__
		- If X is aware that A does not actually have the authority they appear to have, but continues to rely on the a contrary representation by P, P is not liable under the contract. _The Ocean Frost_ 
			- Contrast the case of _First Energy (UK) v Hungarian International Bank_
				- Facts: P was a bank. A was head of its Manchester office as, as such, a �senior manager�. X approached A�s office seeking a loan. X knew that A had no authority to give them a loan, and that it would have to be centrally approved by P. A wrote to X offering the loan as if it had been approved, even though it had not, and X accepted. Question is whether bank are bound by X�s offer.
				- EWCA accepted X's argument that in appointing A as "senior" manager for an extensive region, even though X knew A had no actual authority to make the offer, P represented to X that A had the authority to communicate offers of loans from P. 
				- Reynolds expressed concern that this could compromise the principle in _The Ocean Frost_ since any party who knows that A had no actual authority may nevertheless argue that A had the apparent authority to communicate any authorised offers from P. 
			- Note: S 40 Companies Act gives that a company would be liable under a contract entered into on its behalf by a director with a third party acting in good faith even if the third party knew that the director had no actual authority to do so. 
	- P would then be liable to perform any obligations imposed upon him by such contract.
		- P does not have any rights under the contract against X unless he ratifies it. 
		- In the context of the sale of goods by A, the effect of the law of apparent authority is to cause the transfer of real title. - Devlin J in _Eastern Distributors v Goldring_
	- Scope of A's actual authority is irrelevant.
- Ratification
	- Ratification has retrospective effect. _Bolton Partners v Lambert_
	> The rule as to ratification by a principal of acts done by an [unauthorised] agent is that the ratification is thrown back to the date of the act done, and [so] that the agent is put in the same position as if he had had authority to do the act at the time the act was done by him.
	
	> I think the proper view is that the acceptance by�[A]�did constitute a contract, subject to its being shewn that�[A]�had authority to bind [P]. If that were not shewn there would be no contract on the part of [P], but when and as soon as authority was given to�[A]�to bind [P] the authority was thrown back to the time when the act was done by�[him], and prevented the [T.P.] withdrawing his offer, because it was then no longer an offer, but a binding contract.
	
	- The retrospective effect of ratification is highly favourable to P:
		- As in _Bolton Partners v Lambert_, if an unauthorised A enters into a contract with X (no contract yet), X subsequently rescinds his offer, after which P ratifies A's initial act, X would be bound by the contract notwithstanding his purported rescission of his offer prior to P's ratification. 
	- In _Davison v Vickery Motors Ltd_ (An Australian case concerning similar facts), Isaacs J criticised _Bolton Partners_:
		- Under _Bolton Partners_, the suggestion is that A may bind P to a bilateral contract with X without P's authority or even contrary to P's express direction.
			- There is no instant binding effect on P and yet X is as good as instantly bound.
			- A contract is necessarily bilateral. There cannot be a contract which at the same time is not a contract.
		- At the moment when A accepted X's offer, P was a stranger to the transaction, having received no offer and given no reply. A also did not have P's authority to enter him into the contract. There are no bilateral relations between X and P.
		- Therefore, the reasoning in _Bolton Partners_ cannot be supported. 
	- Krebs, taking the offer and acceptance model of agency, criticises _Bolton Partners_:
		- It is difficult to square ratification under _Bolton Partners_ with the offer and acceptance model. At no time are the parties ad idem: initially, the principal has not expressed his intention to be bound; subsequently, the third party has expressly disavowed any such intention on his part. 
	- Ratification is not possible where an undisclosed agent is acting outside the scope of his authority. 
	- Side note: ratification is also relevant in the context of tort law. 
- Doctrinal theories as to how apparent authority functions to impose liabilities on P to X:
	- Magic?
		- Doctrine similar to estoppel by representation applies. However, here, the doctrine does not just preclude one party from denying the truth of his representation, but functions to create liabilities on P. It functions as a sword and not a shield. 
			- Note: X's act of entering into the contract is analogous to the detriment element in estoppel. 
		- Apparent authority shares a substantially similar public policy foundation with estoppel by representation i.e. it would be unconscionable for P to deny that he was bound by a transaction which had been entered into apparently on his behalf by someone whom he had permitted to represent to third parties that he had the principal's authority to bind the principal to transactions of the kind in question. - Colman J in _Homburg Houtimport v Agrosin ("The Starsin")_
			- To permit the principal to rely on lack of actual authority in such circumstances to the prejudice of X would be to permit a similarly prejudicial inconsistency of conduct to that against which the law of estoppel is directed. 
	- Standard contract law principles (advocated by Krebs)
		- P's representation is a prospective offer that contains an authorisation of any subsequent offers made by A regardless of A's actual authority. 
			- P is essentially making an offer to X to contract with A on terms to be agreed between X and A, and this offer is accepted by X doing so.
		- Krebs' view fails to account for P's lack of rights against X without ratifying the contract. 
- Apparent authority is a very important concept in commercial practice. _Freeman & Lockyer_
	- Parties usually enter into contracts on the basis of A's apparent authority. 
	- X rarely is able to rely on A's actual authority, for only P and A know the actual scope of A's authority. X must rely on representations from either P or A. 

###The Undisclosed Principal

- This refers to situation where A does not disclose that he is acting for P and instead purports to be acting for himself. X believes he is contracting with A alone. 

####Where the Agent has Actual Authority

- If X is willing to treat as a party to the contract anyone on whose behalf the agent may have been authorised to contract: A and X will share a contract; P and X will share a contract. - Diplock LJ in _Teheran-Europe v S T Belton (Tractors)_
	- In the case of an ordinary commercial contract, such willingness on X's part may be assumed unless X manifests his unwillingness or there are other circumstances which should lead A to realise that X was not so willing.
- Diplock LJ in _Freeman & Lockyer_ suggests:
> It may be that this rule � which is peculiar to English law, can be rationalized as avoiding circuity of action, for the principal could in equity compel the agent to lend his name in an action to enforce the contract against the contractor, and would at common law be liable to indemnify the agent in respect of the performance of the obligations assumed by the agent under the contract

	- This situation is also beneficial to X since he may sue either A or P in the event of a default. _Armstrong v Stokes_
- Krebs' contract model of agencies falls apart in the context of an authorised but undisclosed agent because X is unaware of P's existence.

####Where the Agent is Unauthorised

- Where an unauthorised an undisclosed A enters into a contract with X, there are no direct contractual relations between X and P. - Lord Halsbury in _Keighly, Maxsted & Co v Durant_
	- A made a contract on his own behalf and without the authority of anybody else. The disclosure of P's existence does not alter or affect the contract already made. 
- Even under the _Teheran-Europe_ rule, since A has no authority, even if X can impliedly consent to contract with any P that A may have, the consent has no impact as A has no authority to exercise. 
- Ratification is also not possible in these cases - Lord Halsbury in _Keighley, Maxsted & Co v Durant_
	- No principled basis for ratification in such situations.
		- Retrospective effect of ratification is irrelevant since X and A have entered into a binding contract.
		- Ratification here would allow P to unilaterally deprive A of the benefit of a contract entered into on his own basis. Furthermore, there are alternative doctrines available to P to take the benefit of the contract when that is proper. 
			- If A acted in breach of his fiduciary duty to P in entering into the contract, A would hold the benefit of the contract on trust for P under the law of fiduciary duty.
			- Alternatively, if A and P are so agreeable, A may confer the whole benefit of the contract on P via an assignment, declaration of trust, or sub-contract. 
			
####Watteau v Fenwick

- Facts of the case: X sues P for the price of goods due on a sales contract between X and A. X alleges that A was P's agent. A ran P's hotel for them. A's name was above the door and P had no presence there at all. Even though the purchase of cigars would not be unusual by such an establishment's manager, A had been expressly forbidden from buying cigars. At the time of contracting, X was unaware of P's existence and thought that A owned the hotel.
- Divisional court held that P was liable to pay under the contract. 
	- It cannot be that P can only be liable where he represented to X that A had actual authority. Otherwise, in every case of undisclosed principal, the secret limitation of authority would prevail and defeat the action of X dealing with the agent who subsequently discovers the presence of P.
- Tettenborn suggested that there is a second form of estoppel in _Watteau v Fenwick_.
	- By putting A in charge of their business in such a way that he seemed to all the world that he and he alone was its proprietor, P gave third parties the impression that the person they were doing business with was not a distinct legal entity from them. P represented to X that A was the owner of the hotel, and should not be allowed to resile from that representation and assert his separate identity. 
	- Problem with this suggestion is that X was not relying on A being the owner of the hotel. X gave no thought to this issue. 
- Brown suggested that _Watteau v Fenwick_ is a product of the specific time period surrounding the case and is no longer relevant. 
- Bingham J in _The Rhodian River_ suggested a potential principle in _Watteau v Fenwick_ but expressed his hesitancy at applying it:
	> [It] perhaps reflects an undeveloped doctrine that an undisclosed principal should be vicariously liable on contracts made by an agent where they are contracts which a person would ordinarily make in the position which the principal has allowed the agent to assume.
- This case is possibly of little modern relevance considering the extent of development of company law. The case is also explicitly contrary to _Keane, Maxsted & Co_

##Agency and Other Legal Phenomena

###Agency of Necessity

- The law recognises that in an emergency, where it is impossible for A to communicate with B, A may have powers to act for the benefit of B and a right to indemnity from B for costs thus reasonably incurred. 
- This is restricted to very limited cases and could possibly be more properly considered to be a part of the law of unjust enrichment.

###Vicarious Liability

- Precise relationship between agency and vicarious liability is unclear
- If vicarious liability can arise as a result of acts of A carried out in the course of his employment even if unauthorised, can it similarly be said that via apparent authority, P can be made liable on a contract that P did not authorise A to enter?
	- Lord Wilberforce in _Branwhite v Worcester Works Finance_ suggested that there may be some wider conception of vicarious responsibility.

##The Internal Aspect of an Agency

- Mutual consent gives rise to an agent's power and his principal's corresponding liability. - Lord Pearson in _Garnac Grain v HMF Faure and Fairclough_ 
	- Only P's consent can give rise to his liability and A's power.
	- A's consent also gives P a different power and A is under the corresponding liability.
		- P has the power to revoke A's power under the agency. 
	- This agreement need not be contractual (no need for consideration).

###The Legal and Equitable Regulation of the Mutual Power/Liability Relationship

####The Law of Contract

- An agen