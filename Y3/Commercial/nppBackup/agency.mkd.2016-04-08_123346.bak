#Agency

##Introduction

- Three parties are involved in an agency situation
- A true agent puts his principal into a contractual relationship with a third party - Sir Donaldson MR _Potter v Customs & Excise Commissioners_
- Hohfeld and Dowrick: The essential characteristic of an agent is that he is consensually invested with a legal power to alter his principalís legal relations with third persons: the principal is under a correlative liability to have his legal relations with third parties altered. This power-liability relation, is the essence of the relationship of principal and agent.
- The concept of a power
	- A power is a type of legal interest
	- A power holder is one who, through their own violation, is able to effect a change in a given legal relation
	- A power has a corresponding liability
	- Those subject to a liability may have their own legal relations changed when a power is exercised

##The Internal Aspect of an Agency

- Mutual consent gives rise to an agent's power and his principal's corresponding liability. - Lord Pearson in _Garnac Grain v HMF Faure and Fairclough_ 
	- This agreement need not be contractual
	
##The External Aspect of an Agency

- It is necessary to distinguish between the situations where an agent has actual authority or apparent authority. - Diplock LJ in _Freeman & Lockyer v Buckhurst Park Properties_ 
	- Actual and apparent authority are independent of one another. Although they generally co-exist, it is possible for either to exist independantly of the other. 

###Actual Authority in a Disclosed Situation

- An agent's actual authority is the legal power conferred upon him by his principal to alter his principal's legal relations with third parties.
	- Actual authority is created consensually and its scope is determined by applying principles of construction to the agency agreement. If the agent enters into a contract pursuant to his actual authoritty, his actions create contractual rights and liabilities between the principal and the third party. - Diplock LJ in _Freeman & Lockyer_
- Doctrinal theories as to how the agent creates direct contractual relations between his principal and the third party:
	- Magic (as referred to by Krebs)
		- When A properly exercises his power, he changes P's legal position in relation to the third party. 
	- General contractual principles (advocated by Krebs)
		- The agent with actual authority might be thought of a messenger, communicating on behalf of the principal but taking no part in substantive events himself.
		- Krebs considers the role of the agent analogous to that of the ticket machine in _Thornton v Shoe Lane Parking_. P makes the offer through A, and TP's acceptence does not need to be communicated to P to complete the contract. Krebs therefore argues that the TP's contractual liability to the principal can be explained on straightforward contractual grounds.
			- However, it is quite difficult to set out the scope of P's offer under Krebs' account, especially where A and TP enter into extended negotiations as is common in commercial practice. 