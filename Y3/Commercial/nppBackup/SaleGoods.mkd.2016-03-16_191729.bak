#Sale of Goods

##Passage of Property

###Introduction

- Types of property
	- Real property (land)
	- Chattels real (leasehold interest in land)
	- Personal chattels
		- Things in possession
		- Intangible property
			- Documentary intangibles e.g. cheque, bearer shares
			- Pure intangibles e.g. debt, share, patent
		- Money
- Significance of property rights
	- Right in rem vs right in personem
	- Affects remedies available to parties
	- Distribution of risk between parties
	- Ability to give a third party good title
	- Claims in insolvency
	- Fluctuation in property value
	- Frustration if goods perish (contract only frustrated if goods perish when they are seller's property)
	- Property gives rise to causes of action in tort 
		- Conversion
			- Interference with another's right to possession
			- Remedy per ss 1, 3 Torts (Interference with Goods) Act 1977
				- Damages
				- Order for delivery
			- As stated in _OBG v Allen_ __UKHL__
			> Anyone who converts a chattel, that is to say, __does an act inconsistent with the rights of the owner, however innocent he may be, is liable for the loss caused__ which, if the chattel has not been recovered by the owner, will usually be the value of the goods. Fowler v Hollins was a claim for conversion of bales of cotton bought in good faith through a broker in Liverpool. The purchasers were nevertheless held strictly liable. Cleasy B said robustly that: �the liability under it is founded upon what has been regarded as a salutary rule for the protection of property, namely, that __persons deal with the property in chattels or exercise acts of ownership over them at their peril__
		- Negligence
			- Where one party contractually assumes risk for goods but does not take the title to them, they have no cause of action against third parties who negligently damage the goods _The Aliakmon_ __UKHL__
		- Imposes a duty of non-interference on the rest of the world
			- No defence that third party was unaware of owner's right _Fowler v Hollins_
			- Note that third party is not liable for a mere interference with use _Spartan Steel v Martin_
			- Interference with the thing is sufficient to constitute a tort; no interference with use is necessary _The Mediana_
- Equitable rights
	- Where a party is the absolute owner of some property, he does not simultaneously hold an equitable interest in that property. The legal title carries with it all rights. _Westdeutsche Landesbank Girozentrale v Islington LBC_ 
	- Binds third parties in some situations, subject to defences and priority rules
- Default rule is that unless otherwise agreed, risk (of innocent damage or loss of goods) remains with the seller until property passes to the buyer _Healy v Howlett_
	- This principle is now codified by s. 20 SGA
- Until property in goods passes to the buyer, the seller may not sue for the price of the goods unless there is a contractually stipulated date of payment irrespective of delivery per s 49 SGA.

###Sale of Goods Act 1979

####Introduction

- S. 2(1): Contract of sale of goods is a contract by which the seller transfers or agrees to transfer the property in goods to the buyer for a money consideration.
	- s. 2(4): Where the property in goods is transferred, it is called a sale.
	- s. 2(5): Where the transfer of property takes place at a future time or subject to some condition, it is an agreement to sell.
	- s. 2(6): An agreement to sell becomes a sale when the time elapses or the conditions are fulfilled. 
	- Note: Equity does not operate on a contract of sale of goods to see as done what ought to be done (i.e. it does not create an equitable interest)
		- This was established in _In re Wait_ and _The Aliakmon_
- S. 61(1): Goods defined as all personal chattels other than things in action and money.
- S. 5(1): Act applies to both existing and future goods (goods to be manufactured or acquired by the seller after making the contract).
	- S. 5(3): A contract that purports to effect a present sale of future goods operates as an agreement to sell the goods.
- Passing of property requires both identification and intention.
- Parties must agree to the specific goods to which the contract attaches. Mere description is not sufficient. The very goods to be sold must be ascertained. - Lord Blackburn, The Effect of the Contract of Sale (1845)
	- Under a contract for sale of chattels not specific, the property does not pass to the purchaser unless there is afterwards an appropriation of the specific chattels to pass under the contract. _Mirabita v Imperial Ottoman Bank_ 
	
####Identification

- S. 16: Where there is a contract for the sale of unascertained goods, no property in the goods is transferred until the goods are ascertained.
- S. 17(1): Where there is a contract for the sale of specific or ascertained goods, the property in them is transferred to the buyer at such time as the parties to the contract intend it to be transferred.
	- (2): Regard shall be had to the terms of the contract, the conduct of the parties, and the circumstances of the case when ascertaining their intention.
- Per s. 61(1): 
	- Specific goods are goods identified and agreed on at the time a contract of sale is made and includes and undivided share, specified as a fraction or percentage, of goods identified and agreed on as aforesaid.
- "Ascertained" means identified in accordance with the agreement after the time a contract of sale is made _Re Wait_
	- Where goods form part of a bulk, but have not been earmarked, identified or appropriated under the contract, the goods have not been ascertained. 
	- Moving bottles into a separate storage unit suffices for ascertainment _Re Stapylton Fletcher Ltd_
		- It is the segregation of stock from the company's trading assets, whether done physically or by giving instructions to a bonded warehouse keeper, which causes the goods to be ascertained for the purpose of section 16. 
- S. 20A: In a contract for the sale of a specified quantity of unascertained goods, where goods form part of a bulk identified in the contract or by subsequent agreement and the buyer has paid the price for some or all of the goods:
	- (2): Property in an undivided share in the bulk is transferred to the buyer and the buyer becomes an owner in common of the bulk. 
	- (3): The undivided share of a buyer in a bulk is (quantity of goods paid for)/(quantity of goods in the bulk) at that time.
	- (4): Where the aggregate of the undivided shares exceed the whole of the bulk at that time, the undivided share of each buyer shall be reduced proportionately so that the aggregate of the undivided shares is equal to the whole bulk.
	- (5): Where the buyer has only paid for some of the goods due to him, delivery to the buyer out of the bulk shall be ascribed first to the goods in respect of which payment has been made. 
- S. 20B: Deemed consent of any owner in common of goods in a bulk to delivery of goods to any other owner in common or any dealing with goods in the bulk by any other person who is an owner in common. 

####Intention

- 