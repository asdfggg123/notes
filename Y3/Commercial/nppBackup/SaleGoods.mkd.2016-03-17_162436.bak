#Sale of Goods

##Passage of Property

###Introduction

- Types of property
	- Real property (land)
	- Chattels real (leasehold interest in land)
	- Personal chattels
		- Things in possession
		- Intangible property
			- Documentary intangibles e.g. cheque, bearer shares
			- Pure intangibles e.g. debt, share, patent
		- Money
- Significance of property rights
	- Right in rem vs right in personem
	- Affects remedies available to parties
	- Distribution of risk between parties
	- Ability to give a third party good title
	- Claims in insolvency
	- Fluctuation in property value
	- Frustration if goods perish (contract only frustrated if goods perish when they are seller's property)
	- Property gives rise to causes of action in tort 
		- Conversion
			- Interference with another's right to possession
			- Remedy per ss 1, 3 Torts (Interference with Goods) Act 1977
				- Damages
				- Order for delivery
			- As stated in _OBG v Allen_ __UKHL__
			> Anyone who converts a chattel, that is to say, __does an act inconsistent with the rights of the owner, however innocent he may be, is liable for the loss caused__ which, if the chattel has not been recovered by the owner, will usually be the value of the goods. Fowler v Hollins was a claim for conversion of bales of cotton bought in good faith through a broker in Liverpool. The purchasers were nevertheless held strictly liable. Cleasy B said robustly that: �the liability under it is founded upon what has been regarded as a salutary rule for the protection of property, namely, that __persons deal with the property in chattels or exercise acts of ownership over them at their peril__
		- Negligence
			- Where one party contractually assumes risk for goods but does not take the title to them, they have no cause of action against third parties who negligently damage the goods _The Aliakmon_ __UKHL__
		- Imposes a duty of non-interference on the rest of the world
			- No defence that third party was unaware of owner's right _Fowler v Hollins_
			- Note that third party is not liable for a mere interference with use _Spartan Steel v Martin_
			- Interference with the thing is sufficient to constitute a tort; no interference with use is necessary _The Mediana_
- Equitable rights
	- Where a party is the absolute owner of some property, he does not simultaneously hold an equitable interest in that property. The legal title carries with it all rights. _Westdeutsche Landesbank Girozentrale v Islington LBC_ 
	- Binds third parties in some situations, subject to defences and priority rules
- Default rule is that unless otherwise agreed, risk (of innocent damage or loss of goods) remains with the seller until property passes to the buyer _Healy v Howlett_
	- This principle is now codified by s. 20 SGA
- Until property in goods passes to the buyer, the seller may not sue for the price of the goods unless there is a contractually stipulated date of payment irrespective of delivery per s 49 SGA.

###Sale of Goods Act 1979

####Introduction

- S. 2(1): Contract of sale of goods is a contract by which the seller transfers or agrees to transfer the property in goods to the buyer for a money consideration.
	- s. 2(4): Where the property in goods is transferred, it is called a sale.
	- s. 2(5): Where the transfer of property takes place at a future time or subject to some condition, it is an agreement to sell.
	- s. 2(6): An agreement to sell becomes a sale when the time elapses or the conditions are fulfilled. 
	- Note: Equity does not operate on a contract of sale of goods to see as done what ought to be done (i.e. it does not create an equitable interest)
		- This was established in _In re Wait_ and _The Aliakmon_
- S. 61(1): Goods defined as all personal chattels other than things in action and money.
- S. 5(1): Act applies to both existing and future goods (goods to be manufactured or acquired by the seller after making the contract).
	- S. 5(3): A contract that purports to effect a present sale of future goods operates as an agreement to sell the goods.
- Passing of property requires both identification and intention.
- Parties must agree to the specific goods to which the contract attaches. Mere description is not sufficient. The very goods to be sold must be ascertained. - Lord Blackburn, The Effect of the Contract of Sale (1845)
	- Under a contract for sale of chattels not specific, the property does not pass to the purchaser unless there is afterwards an appropriation of the specific chattels to pass under the contract. _Mirabita v Imperial Ottoman Bank_ 
	
####Identification

- S. 16: Where there is a contract for the sale of unascertained goods, no property in the goods is transferred until the goods are ascertained.
- S. 17(1): Where there is a contract for the sale of specific or ascertained goods, the property in them is transferred to the buyer at such time as the parties to the contract intend it to be transferred.
	- (2): Regard shall be had to the terms of the contract, the conduct of the parties, and the circumstances of the case when ascertaining their intention.
- Per s. 61(1): 
	- Specific goods are goods identified and agreed on at the time a contract of sale is made and includes and undivided share, specified as a fraction or percentage, of goods identified and agreed on as aforesaid.
- "Ascertained" means identified in accordance with the agreement after the time a contract of sale is made _Re Wait_
	- Where goods form part of a bulk, but have not been earmarked, identified or appropriated under the contract, the goods have not been ascertained. 
	- Moving bottles into a separate storage unit suffices for ascertainment _Re Stapylton Fletcher Ltd_
		- It is the segregation of stock from the company's trading assets, whether done physically or by giving instructions to a bonded warehouse keeper, which causes the goods to be ascertained for the purpose of section 16. 
- S. 20A: In a contract for the sale of a specified quantity of unascertained goods, where goods form part of a bulk identified in the contract or by subsequent agreement and the buyer has paid the price for some or all of the goods:
	- (2): Property in an undivided share in the bulk is transferred to the buyer and the buyer becomes an owner in common of the bulk. 
	- (3): The undivided share of a buyer in a bulk is (quantity of goods paid for)/(quantity of goods in the bulk) at that time.
	- (4): Where the aggregate of the undivided shares exceed the whole of the bulk at that time, the undivided share of each buyer shall be reduced proportionately so that the aggregate of the undivided shares is equal to the whole bulk.
	- (5): Where the buyer has only paid for some of the goods due to him, delivery to the buyer out of the bulk shall be ascribed first to the goods in respect of which payment has been made. 
- S. 20B: Deemed consent of any owner in common of goods in a bulk to delivery of goods to any other owner in common or any dealing with goods in the bulk by any other person who is an owner in common. 

####Intention

- Rules for ascertaining parties' intention given in s. 18 SGA
- Rule 1:
	- Where there is an unconditional contract for the sale of specific goods in a deliverable state, property passes to the buyer when the contract is made and it is immaterial whether the time of payment or the time of delivery are postponed.
		- Deliverable state is defined in s. 61(5) to mean a state of the goods that the buyer would under the contract be bound to take delivery of them.
			- Does not depend on seller having good title _Kulkarni v Motor Credit_
	- The exclusion of this rule must take place at the time when the contract is concluded; attempting to retain property in goods following the conclusion of an unconditional contract for sale would be ineffective as property has already passed _Dennant v Skinner and Collom_
		- Even after property passes, the seller might still have a right to possession until he parts with possession of the goods.
	- _Clough Mill v Martin_ provides an example of an effective retention of title clause.
		- Note that any retention of title over items produced from goods sold would constitute a charge which would need to be registered in the Companies House if granted by a company.
- Rule 2:
	- Where there is a contract for the sale of specific goods and the seller is bound to do something to do the goods for the purpose of putting them in a deliverable state, property does not pass until the thing is done and the buyer has notice that it has been done
	- The test is whether anything remained to be done to the thing by the sellers to put it in a deliverable state (a state in which the thing will be the article contracted for by the buyer) _Underwood Ltd v Burgh Castle 
		- This does not refer to packing or anything of that kind, but rather situations such as when assembly or disassembly was necessary to produce the specific thing contracted for.
- Rule 3:
	- Where there is a contract for the sale of specific goods in a deliverable state but the __seller__ is bound to weigh, measure, test, or do some other act or thing with reference to the goods for the purpose of ascertaining the price, the property does not pass until the act or thing is done and the buyer has notice that it has been done.
	- Note that this rule does not apply when someone other than the seller is to weigh, measure, test or do some other act. If parties seek to make passage of property conditional on a third party doing such acts, it must be a clear contractual provision. _Nanka-Bruce v Commonwealth Trust_ __UKPC__
- Rule 4:
	- When goods are delivered to the buyer on approval or on sale or return or other similar terms, property in the goods passes to the buyer when he signifies his approval or acceptance to the seller or does any other act adopting the transaction; if he does not signify his approval or acceptance to the seller but retains the goods without giving notice of rejection, then property passes on the expiration of a stipulated time period, or in its absence, the expiration of a reasonable time. 
	- Any act by the buyer which would be inconsistent with his being other than the buyer would cause property to pass. _Kirkham v Attenborough_
- Rule 5:
	- (1): Where there is a contract for the sale of unascertained or future goods by description, and goods of that description in a deliverable state are unconditionally appropriated to the contract, either by the seller with the assent of the buyer or by the buyer with the assent of the seller, property passes to the buyer; the assent may be express or implied, and may be given either before or after the appropriation is made. 
		- Packing and marking with shipping marks were not be regarded as intented appropriation but rather preparation for shipment (contract stipulated that property passes upon shipment) in _Carlos Federspiel v Charles Twigg_.
			- Mere setting apart or selection of the goods by the seller is insufficient. To constitute an appropriation of the goods to the contract, the parties must have had, or be reasonably supposed to have had, an intention to __attach the contract irrevocably to the goods__, so that those goods and no others are the subject of the sale and become the property of the buyer.
			- It is by agreement of the parties that the appropriation is made, although in some cases the buyer's assent to an appropriation by the seller is conferred in advance.
	- (2): Where, in pursuance of the contract, the seller delivers the goods to the buyer, or to a carrier or other bailee or custodier for the purpose of transmission to the buyer, and does not reserve the right of disposal, he is taken to have unconditionally appropriated the goods to the contract. 
		- Example case is _Wardar's (Import & Export) Co v W Norwood & Sons_
	- (3): Where there is a contract for the sale of a specified quantity of unascertained goods in a deliverable state forming part of an identified bulk and the bulk is reduced to (or to less than) that quantity, then, if the buyer under the contract is the only buyer to whom goods are then due out of the bulk, the remaining goods are taken as appropriated to that contract and property in those goods passes to that buyer. 
	- (4): The rule in (3) also applies where the bulk is reduced to (or less than) the aggregate of the quantities due to a single buyer under separate contracts. 

##Transfer of Title

###Introduction

- SS 16-20: Transfer of seller's property to the buyer
- SS 21-26: Exceptional circumstances where even though the seller does not have property in the goods or has defective property, he can nevertheless transfer the property, or as the case may be, transfer the property free from the defect, to a person who buys in good faith.
- Basic rule is _nemo dat quod non habat_ e.g. _Cundy v Lindsay_
	- Now codified in s. 21 SGA
	- Example case is in _Farquharson Bros v King_ __UKHL__:
		- FB appointed Capon as agent with some authority to sell timber on behalf of FB. C fraudulently assumed the name of Brown and purported to sell timber outside authority to K.
		- UKHL held that there was no estoppel because K were not misled in any way by FB's conduct; FB was not precluded from denying Capon's authority to sell.
- Battersby & Preston: defences to the nemo dat principle do not necessarily grant indefeasible title. The notion here is similarly relative, and the majority of the exceptions operate not by conferring a perfect title on the ultimate purchaser, but only by overriding a prior transaction or interest. 
- Note that for this section, the parties are: 
	- A: The party with the superior property in the goods (might be the owner)
	- S: The seller of the goods
	- B: The purchaser of the goods

###Defence 1: A's Consent

- If S sells goods with A's consent or authority, A cannot assert his right against B.
- This applies even if A's does not have absolute property in the goods. 
- A's consent only gives B a defence against A's pre-existing property right, but if A is not the true owner, A's consent is not a defence against the owner's property right.
- Contrast with situation where S holds goods on trust for A and transfers goods without the authority of A, whereby whereby priority rules apply.

###Defence 2: Estoppel

- Where A creates an appearance that S has the requisite authority (that he in fact lacks) or that S is an owner, A is precluded from asserting A's right against B.
- Note that B acquires real title (A's title) _Eastern Distributors v Goldring_

###Defence 3: S. 2 Factors Act 1889

- If S, a factor (a mercantile agent), with A's consent, has physical control of A's goods and makes a disposition of the goods in the ordinary course of S's business and B acquires his right in good faith without notice that S exceeded  his authority, then the disposition is valid as if S were expressly authorised by A to make that disposition.
- Example is Folkes v King, where A gives S authority to sell his car for �575 or more, and S sells to B for �340, and B is unaware of the limit on S's authority, B acquires good title.

###Defence 4: S. 24 SGA, S. 8 FA

- If S, despite having sold goods to A, is or continues to be in possession of A's goods/documents of title to A's goods, and S makes a disposition of the goods to B or agrees to do so, and S delivers the goods/documents of title to B, and B receives the goods/documents of title in good faith without notice of the previous sale to A, then the disposition to B has the same effect as if S were expressly authorised by A.
- Note that the capacity in which S retains possession is not crucial _Pacific Motor Auctions Property Ltd v Motor Credits (Hire Finance) Ltd_

###Defence 5: S. 25(1) SGA, S. 9 FA

- If A sells or agrees to sell goods to S and allows S to take physical control of those goods/document of title before property passes to S, and S makes a disposition of the goods to B or agrees to do so and S delivers the goods/documents of title to B and B receives the goods/documents of title in good faith without notice of A's right, the disposition to B has the same effect as if S were a mercantile agent in possession of the goods/documents of title with A's consent. 
- Note: B's right to the property is not perfected by this defence. It is subject to the same defects of A's title. _National Employers Mutual General Iinsurance Association v Jones_

###Defence 6: S. 27 Hire Purchase Act 1964

- If A and S concluded a hire-purchase agreemeent or a conditional sale of a motor vehicle and allows S to take possession before property vests in him, and S subsequently disposes of the vehicle to B who is a private purchaser in good faith without actual notice or a trade or fiance purchaser who then sells to a private purchaser in good faith without notice, that disposition shall take effect as if A's title to the vehicle has been vested in the debtor immediately before that disposition. 
- Illustrated in _Kulkarni v Manor Credit Ltd_:

###Defence 7: S. 23 SGA

- If A's pre-existing right is a power to rescind a transfer to S and thus regain title, and S sells to B before A has exercised that power to rescind, and B buys in good faith without notice of the facts giving A a power to rescind (constructive notice), then B acquires good title to the goods. 
- Normally, the exercise of the power to rescind must be communicated to S, but where this is not possible, it might be sufficient for A to take all reasonable steps to communicate his exercise of the power _Car & Universal Finance Ltd v Caldwell_
- Note: Original contract between S and A must be voidable (not void) for this defence to apply. 
	- Mistake/misrepresentation distinction. 
	- See cases of _Cundy v Lindsay_, _Lewis v Averey_, _Shogun Finance v Hudson_

##Contractual Duties of Buyer and Seller

###Overview

- S. 27 SGA: Duty of seller to deliver the goods and duty of buyer to accept and pay for them in accordance with the terms of the contract.
- Express and implied terms:
	- Where contract is silent, sellers duties are found in:
		- Ss 12-15: Ownership and quality
		- S 28: Duty to hold himself ready and willing to deliver
		- S 27: Duty to deliver
		- Ss 29-32: Manner of delivery
	- Buyer's duties:
		- S 27: duty to pay and accept
		- S 28: Duty to hold himself ready and willing to pay the price
		- S 37: Duty to take delivery
- Types of contractual terms
	- Warranty: A minor term of contract "collateral to the main object of it" (_Chanter v Hopkins_) breach of which gives the innocent party standard contractual remedies and he remains bound to perform under the contract.
	- Condition: A term that gives to the root of the contract, breach of which gives the innocent party a power to terminate the contract.
- In the event of a repudiatory breach of contract, the innocent party may elect to:
	- Terminate the contract, which irrevocably absolves both parties from future performance of their obligations under the contract; or
	- Keep the contract alive, which would give him a right to claim/receive performance as well as a claim for damages for loss resulting from the breach.
- Note: termination and recission are conceptually different (_Johnson v Agnew_, _Heyman v Darwins_):
	- Recission (recission ab initio) treats the contract in law as never having been entered into.
	- Termination (acceptance of repudiatory breach) has the effect of ending or disicharging a contract that has come into existence. 
		- Courts sometimes use the word recission when referring to acceptance of repudiatory breach.
- Whether a term is a condition or a warranty depends on the construction of the contract; a stipulation may be a condition even though it is called a warranty in the contract. _s 11(3) SGA_
- Sellers LJ in _Hong Kong Fir Shipping Co Ltd v Kawasaki Kisen Kaisha Ltd_:
> The formula for deciding whether a stipulation is a condition or a warranty is well recognised; the difficulty is in its application. 
	
	- Where an event occurs the occurence of which neither the parties nor Parliament have expressly stated will discharge one of the parties from further performance of his undertakings, it is for the court to determine whether the event has this effect or not. - Diplock LJ
- Concept of intermediate terms applies to sale of goods contractas _The Hansa Nord_ __EWCA__
- In the event of a breach of intermediate terms, the question is whether the breach would deprive the party who has further undertakings still to perform of substantially the whole benefit which was intended that he should obtain from the contract as consideration for performing those undertakings. _Hong Kong Fir Shipping_

###Terms Implied in SGA in Favour of the Buyer

- S 12(1): __C__ (s 12(5A)): S has the right to sell the goods, or will do at the time when the property is to pass
- S 12(2): __W__ (s 12(5A)): Goods are free from a charge or encumbrance not known to B and B will enjoy quiet possession of the goods
- S 13: __C__ (s 13(1A)): In a contract for the sale of goods by description, the goods will correspond with the description.
- S 14(2): __C__ (s 14(6)): Where S sells goods in the course of a business, goods supplied under contract are of satisfactory quality.
- S 14(3): __C__ (s 14(6)): Where S sells in the course of a business, and B makes known to S a particular purpose for which the goods are bought, goods are reasonably fit for that purpose.
- S 15(2) __C__ (s 15(3)): In a sale by sample, the bulk will correspond with the sample in quality and the goods will be free from any defect, making their quality unsatisfactory, not be apparent on reasonable examination of sample. 
- Abovementioned terms do not apply to contracts to which Ch 2 of Part 1 of the Consumer Rights Act 2015 applies. 
- In the absence of reasons to conclude otherwise, where S delivers to B a quantity of goods less than he contracted to sell, B may reject them, but if B accepts the goods he must pay for them. _s 30(1) SGA_
	- Delivery of wrong quantity not a condition unless time is of the essence
	- S 30(2A): B does not have a right to reject where excess or shortfall in delivery is so small that rejection would be unreasonable.
- S 15A: Where S breaches a term implied by ss 13, 14, or 15 but the breach is so slight that it would be unreasonable for him to reject the goods, the breach is not to be treated as a breach of condition but may be treated as a breach of warranty unless a contrary intention appears in or is to be implied from the contract.
	- (3): Burden of proof falls on the Seller.
- Stipulations as to time:
	- S 10 (1): Stipulations as to time of payment are not of the essence the the absence of a contrary intention.
		- Not difficult to displace the presumption, especially where payment and acceptance are concurrent
	- S 10(2): Whether any other stipulations as to time is of the essence of the contract depends on the terms of the contract. 
		- Timely delivery and acceptance generally of essence in commercial cases _Westbrook Resources v Globe Metallurgical Inc_
		- EWCA in _Peregrine Systems v Steria_ rejected the argument that timely delivery and acceptance are prima facie of the essence. 
			- READ CASE TO CONFIRM

####S 12(1)

- In a contract of sale, other than one to which subsection (3) applies, there is an implied term on S that S has the right to sell the goods, or will do at the time when the property is to pass.
- Where S breaches this term, B is entitled to recover the full price he paid even if he had used the goods prior to discovering the breach and terminating the contract. _Rowland v Divall_
	- Consideration that the buyer had contracted for was the property in and lawful possession of the goods.
- Note that this is not limited to defective title, but also other situations in which S lacks the right to sell the goods e.g. TM infringement _Niblett Ltd v Confectioners' Materials Co_

####S 12(2)

- In a contract of sale, other than one to which subsection (3) applies, there is an implied warranty that the goods are free and will remain free until the time when the property is to pass from any charge or encumbrance not disclosed or known to the buyer before the contract is made and the buyer will enjoy quiet possession of the goods.
- This subsection is usually of little practical relevance. 

####S 12(3)-(5)

- (3): There is an express or implied intention that the seller should only transfer such title that he or a third person may have.
- (4): In a contract where (3) applies, there is an implied warranty that all charges or encumbrances known to the seller and not known to the buyer have been disclosed to the buyer before the contract is made. 
- (5): In a contract where (3) applies, there is an implied warranty that none of the following will disturb the buyer's quiet possession of the goods:
	- (a): the seller
	- (b): in a case where the parties to the contract intend that the seller should transfer only such title as a third person may have, that person
	- (c): anyone claiming through or under the seller or that third person otherwise than under a charge or encumbrance disclosed or know to the buyer before the contract is made

####S 13 Correspondence with Description

- S 13(1): Where there is a contract for the sale of goods by description, there is an implied term that the goods will correspond with the description.
	- Applies also to specific goods.
	- Description must form part of the contract. 
- This section is applied strictly. Buyer is not bound to accept goods that do not correspond with the description merely because they were merchantable under that description. _Arcos v EA Ronaasen_
- Note the difficulty that may arise in determining the substance of the implied warranty, as in _Reardon Smith Line v Hansen-Tangen (The Diana Prosperity)_ __UKHL__.
	- Case concerned a vessel to be chartered on completion; ship to be built at Osaka with the hull number 354, but eventually built at Oshima 004; all physical attributes of the vessel corresponded to those required; charterers sought to reject the vessel on the basis that it did not correspond with the contractual description in that it was Oshima 004 and not Osaka 354.
	- Lord Wilberforce: Distinguish between quality of the goods and indication (identification) of the goods. Consider whether a particular item in a description constitutes a substantial ingredient of the identity of the thing sold. 
	- May likely now rely on s 15A
- Courts have typically applied this section especially strictly. 

####S 14(2) Satisfactory Quality

- S 14(2): Where the seller sells goods in the course of a business, there is an implied term that the goods supplied under the contract are of satisfactory quality.
	- "Goods supplied" includes containers and other articles mixed with contract goods. _Geddling v Marsh_
		- Mineral water sold, bottle not included in sale (remained property of the seller) but because of a defect in the bottle, it exploded. Buyer was entitled to recover for breach of condition.
	- "Sells in the course of a business": Defined in s 61(1) to include a profession and the activities of any government department � or local or public authority
		- Wide meaning
			- No need for regular sale
			- Financial reward is not necessary _Rolls v Miller_
			- No need for good to be something generally sold in the course of S's business _Stevenson v Rogers_
		- S 14(5): Where an agent sells in the ordinary course of business for a purely private principal, it will fall under S 14(2) except when the buyer knows that fact or reasonable stapes are taken to bring it to the notice of the buyer before the contract is made.
- S 14(2A): Goods are of satisfactory quality if they meet the standard that a reasonable person would regard as satisfactory, taking account of all relevant circumstances.
- S 14(2B): Factors to be considered in assessing the quality of goods.
- In _Business Application Specialists Ltd v Nationwide Credit Corpn Ltd_, a case concerning the sale of a second-hand car that subsequently developed a fault, even though the fault was unusual, it was not a breach of condition because some wear and tear is expected of a second-hand car and the cost of repair was not particularly great in relation to the price of the car.
	- Balance all factors
- In _Jewson v Boyhan_: Case concerned the sale of 12 electric boilers for flats. Standard Assessment Procedure ratings of flats were so low (note that there is no minimum rating that must be satisfied) that there was substantial risk flats would not be sold. B sued J for breach of implied terms (not of satisfactory quality - fit for purpose).
	- Trial judge held that it was not of satisfactory quality. 
	- EWCA held that the key issue was what circumstances can be properly regarded as relevant to the question of satisfactory quality. This is to be determined by the reasonble person test. In this context, the question is whether the goods are intrinsically satisfactory and fit for all purposes for which goods of the kind in question are supplied. The fact that the boilers were not sufficient for the particular purpose of the developer (subsequent sales) is irrelevant. 
		- Predictable use of the goods is an important factor. 
- S 14(2C): The term implied in S 14(2) does not extend to any matter making the quality unsatisfactory 
	- (a): which is specifically drawn to the buyer's attention before the contract is made. 
		- S must inform B of the existence of the defect and must not mislead him about its nature
	- (b): where the buyer examines the goods before the contract is made, which that examination ought to reveal.
		- Actual examination, not just opportunity to examine _Thornett & Fehr v Beers & Son_
	- (c): in the case of a contract for sale by sample, which would have been apparent on a reasonable examination of the sample.

####S 14(3) Fitness for purpose

- Where the seller sells goods in the course of a business and the buyer, expressly or by implication, makes known to the seller any particular purpose for wh ich the goods are being bought, there is an implied term that the goods supplied under the contract are reasonabyl fit for that purpose, whether or not that is a purpose for which such goods are commonly supplied, except where the circumstances show that the buyer does not rely, or that it is unreasonable for him to rely on the skill or judgment of the seller.
- Restated in _Jewson v Boyhan_ into a number of questions to be asked by the court
	- Whether the buyer, expressly or by implication, made known to the seller the purpose for which the goods were being bought;
		- "Expressly or by implication makes known": interpreted liberally by the courts.
			- Buyer's purpose must be stated with sufficient particularity to enable the seller to exercisie his skill and judgment in making or selecting appropriate goods _Henry Kendall v William Lillico_
			- It is sufficient for the seller to be aware of the buyer's purpose even if not expressly told as long as a reasonable person in the seller's position would have understood or been reasonably expected to infer that purpose _Slater & Others v Finning Ltd_ 
			- Example is _BSS Group v Makers (UK)_
		- "To the seller": includes a person with actual or apparent authority to receive communication about the purpose on behalf of the seller. 
	- If so, whether they were reasonably fit for that purpose;
	- If they were not reasonably fir for that purpose, whether the seller has shown
		- that the buyer did not rely upon his skill or judgement,
		- or if it did, that it was unreasonable for him to do so.
- Further clarification given in _Christopher Hill v Ashington_
	- The key to s 14(1), (2) SGA 1983 is reliance: the reasonable reliance of the buyer upon the seller�s ability to make or select goods which are reasonably fit for the buyer�s purpose coupled with the seller�s acceptance of responsibility to do so. The seller has a choice whether to accept that responsibility. To enable him to exercise that choice, he must be supplied by the buyer with sufficient information to acquaint him with what he is being relied upon to do and to enable him to appreciate what exercise of skill or judgment is called for in order to make or select goods which will be fit for the purpose for which the buyer requires them. - Lord Diplock
	- Partial reliance on the seller's skill or judgement may nevertheless bring the case within this section. The question is whether the defect in the goods lay within the sphere of the expertise of the buyer to detect and avoid, in which case the seller would not be contractually responsible for it. 

####Restriction or Exclusion of Liability. 

- S 55(1): Where a right, duty or liability would arise under a contract for sale of goods by implication of law, it may be negatived or varied by express agreement, or by the course of dealing between the parties, or by such usage as binds both parties to the contract.
- Restrictions or Exclusions of liability are limited by the Unfair Contract Terms Act
	- S 6 UCTA: Liability for breach of the obligations arising form s 12 SGA cannot be excluded or restricted by contract; some other liabilities (ss 13, 14, 15 SGA) cannot be excluded by contract unless the contract term is reasonable.
	- S 3 UCTA: Where one party deals on another's written standard terms of business, the other cannot by reference to any contract term when himself in breach of contract, exclude or restrict liability unless the contract term is reasonable.
		- Dealing on one party's standard terms of business is irrespective of any negotiations that may have preceded it, as long as the contract with the written standard terms of business is entered into _St Albans City and DC v International Computers Ltd_
	- As of 1 Octboer 2015, neither apply to consumer contracts per Consumer Rights Act 2015.
	- S 11(1) UCTA: Whether a term is fair and reasonable is decided with regard to the circumstances which were, or ought reasonably to have been, known to or in the contemplation of the parties when the contract was made.
		- (2): Schedule 2 givies some factors to be considiered
		- (4): Where restriction of liability is to a specified sum of money, resonableness shall be assessed with regard to in particular resources the party in breach could expect to have to meet the liability and how far it was open to him to cover himself by insurance.
		- (5): Burden of proof of reasonableness falls on the defendant.
		
####Seller's Duties as to Delivery

- S 27: Seller has a duty to deliver the goods in accordance with the terms of the contract of sale
- S 28: Seller must be ready and willing to give possession to B in exchange for the price
	- S 61(1): Delivery means voluntary transfer of possession from one person to another
	- Delivery can be:
		- Actual delivery
		- Constructive delivery: transfer of a document of title or delivery to a carrier (s 32)
		- Note that "delivery" has different meanings depending on the context in which it is used.
- Requirements on manner of delivery (when and how) where contract is silent are given in ss 29-32
- S 29(1): Manner of delivery is a question depending on the contract.
	- (2): Where unstated in the contract, place of delivery are given in this subsection.
	- (3): Where time of delivery is not stipulated in the contract, S must send goods within reasonable time.
		- (3A): (3) does not apply to consumer contracts
	- (4): Where godos are held by a third person, there is no delivery unless and until the third p erson acknowledges to B that he hodls the goods on his behalf.
- S 30(1)-(6): Delivery of a quantity less or larger than S contracted to sell - B may reject unless the shortfall or excess is so slight that it would be unreasonable to do so (s 30(2A))
- S 31(1): Unless otherwise agreed, B is not bound to accept delivery of goods by instalments.
	- (2): Whether defective performance by either party in a contract for sale of goods delivered by instalments constitutes a repudiatory breach depends on the facts of the case.
	- (3): This section does not apply to consumer contracts.
	- Where no stalments are agreed, acceptance by B of some quantity of goods does not bind him to accept the rest _Behrend & Co v Produce Brokers & Co_
	- Where delivery is agreed to be in instalments, and
		- obligations in relation to each instalment are severable: a breach of an obligation does not repudiate the whole contract
		- even if obligations are not severable; if B accepts a quantity, B is not bound to accept the rest (s 11(4)) except where the accepted instalment is part of the same commercial unit as later instalments (s 35A)
- S 32(1): Where, in pursuance of a contract of sale, S is authorised or required to send the goods to B, delivery of the godos to a carrier for the purpose of transmission to B is prima facie deemed to be a delivery of the goods to B.
	- (2): Unless otherwise authorised by B, S must make such contract with the carrier on behalf of B as may be reasonable having regard to all circumstances of the case; if S omits to do so, and the goods are lost or damaged in the course of transit, B may decline to treat the delivery to the carrier as delivery to himself or may hold S responsible in damages. 
	- (3): Unless otherwise agreed, where goods are sent by sea, S must give such notice to B as may enable him to ensure him to insure them during their sea transit; failing which the goods are at S's risk during such sea transit.
	- (4): This section does not apply to consumer contracts
	
####Buyer's Duties and Possibility of Rejection

- S 27: Buyer has a duty to pay and accept goods in accordance with the terms of the contract of sale
	- Duty to accept is used in the sense of duty to not reject the goods. 
- Effect of buyer's termination
	- If the contractual duty to pay has not yet arisen, it can no longer be imposed on the buyer e.g. _Arcos v EA Ronaasen and Son_
		- See also s 15A
	- If the price was due and has been paid, this does not mean that it has been unconditionally earned by the seller. If the goods can be rejected, B can reclaim the price. e.g. _Rowland v Divall_
		- Where a sum has been unconditionally acquired or earned (as may be stipulated in the contract), the buyer would not be able to reclaim that sum _Bank of Boston Connecticut v European Grain and Shipping Ltd ("The Dominique")_
		- In a sale of goods contract, it may be that the contract was in actual fact for the design, construction, and transfer of the good, in which case instalments stipulated to be for already performed duties may be unconditionally earned and therefore not recoverable by the buyer.
		- Note that stipulations of when payment is due does not necessarily mean that the payment is unconditionally earned at that time. 
- Two questions in termination
	- Does B have a power to terminate
	- Has S's right to price accrued
		- Pre-paid sums are not necessarily earned by the seller _Dies v British and International Mining_
 