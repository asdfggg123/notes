#Security Rights

##Introduction

- Purpose of credit
	- Allow parties to obtain liquidity in order to pursue business interests or make significant purchases.
- Types of credit
	- Loan credit (typical loan agreement)
	- Sale credit (goods sold on credit allowing repayment over time)
- Debtor-creditor relation
	- Creditor:
		- Duty to advance loan (pursuant to agreement)
		- Right to repayment of loan with interest
	- Debtor:
		- Right to obtain loan (pursuant to agreement)
		- Duty to pay the capital sum with interest
- Risks faced by creditors
	- Risk of non-payment
	- Risk of asset withdrawal
	- Risk of multiple debts with other creditors (full recovery might then be impossible in the event of insolvency)
- Creditor protection against credit risk
	- Contractual protections might be in place
		- Loan agreement (with debtor) includes clauses in favour of the creditor:
			- Restrict the debtor's ability to borrow from other lenders
			- Restrtct the debtor's ability to create security over its assets to another lender
			- Provide the creditor with other forms of control over the debtor's operations
		- Creditor enters into separate contract with third parties (possibly as a condition for the loan)
			- Guarantees
			- Insurance
		- Shortcomings of contractual protections
			- These are only enforceable against the contractual party
			- Creditor ranks pari passu on debtor's insolvency
	- Proprietary protection against credit risk
		- Security interests
			- Benefits of security interests to debtor:
				- Access to credit where D lacks creditworthiness or credit is for the purpose of embarking on a high risk project
			- Benefits of security interests to creditor:
				- Reduce credit risk:
					- Effective in debtor's insolvency
					- Priority in insolvency
					- Enforceable security where necessary (no need to wait for insolvency to resort to debtor's assets
				- Gives creditor some influence over the debtor
				- Reduction of cost of monitoring debtor (since creditor is assured that he can resort to the property over which security is granted.)
				- Deter unsecured creditors from participating in enforcement. 
		- Retention of title in conditional sale as well as hire-purchase agreements and finance leases of goods.
		- Trusts (Quistclose trusts) _Twinsectra v Yardley_
- Lord Browne-Wilkinson on what is security in _Bristol Airport v Powdrill_
	- Security is created where the creditor, in addition to the personal promise of the debtor to discharge obligation, obtains rights exercisable against some property in which the debtor has an interest in order to enforce the discharge of the debtor's obligation to the creditor. The statutor right confers on the airport (as the creditor) the right to detain and, with the leave of the court, sell the aircraft for the purpose of discharging debt incurred to that airport by the airport and is therefore a security.
	- Key elements:
		- Debt between creditor and debtor
		- Right granted over some asset
		- Creditor has the right to resort to the asset in priority to others to discharge the debt. 
		- Some security interests are capable of binding buyers of assets (security)

###Types of Security under English Law

- Four types of consensual security interests given by Millet LJ in _Re Cosslett_ __EWCA__
	1. Pledge (legal)
		- Possessory security
		- Debtor gives possession over property as security for loan; creditor may take ownership of property in the event of default.
	1. Contractual lien (legal)
		- Possessory security
		- Creditor may retain possession of property 
	1. Mortgage (legal or equitable)
		- Non-possessory security
		- Legal mortgage: debtor transfers title in property to creditor as security. 
	1. Charge (equitable)
		- Non-possessory security
		- May be fixed or floating
- Security interests can also arise by operation of law (non-conseual):
	- Non-consensual common law lien
	- Non-consensual equitable "charge" (also called lien)
		- E.g. as a result of tracing as in _Foskett v McKeown_
- Legal security has priority over subsequent interests
- Equitable security is subject to the equitable rules of priority. They arise in a number of situations:
	- Security relates to future property
	- There is no transfer or agreement for transfer of property at all
	- There is no present transfer of property, but merely an agreement to transfer or a declaration of trust by the debtor
	- The transfer of property is not made in accordance with the formal requirements for the transfer of legal title
	- Property is transferred to a third party as trustee for the creditor
	- Transferor's title to the asset is equitable

###Legal Regimes Governing Security in English law

- Charges and mortgages granted by a company must be registered at the Companies House per s 859A of the Companies Act 2006 in order to bind third parties; consequences of lack of registration under s 859H (need to know)
- Security created in writing in goods by an individual must compy with formalities and be registered at the High Court: Bills of Sale Acts 1878 - 1882
- Registration of security in relation to particular assets e.g. land, some IP rights
- Special rules present for financial collateral 

##Retention of Title

- Retention of title is possible under s 19(1) Sale of Goods Act 1979
	- Seller may reserve the conditional right of disposal of the goods under contract. 
- See _Clough Mill v Martin_:
	- Contract contained a clause where the seller purports to retain title in both unused material and goods produced from the material subject to payment of the purchase price of the material.
	- Goff LJ held that it was impossible to believe that parties intended the seller to gain the windfall of the full value of the new product and the clause must be held as creating either a trust or a charge.
		- Impossible as the product contains the contribution of other parties, it was not previously owned by the seller, and seller had no duty to account to buyer for the value in the new goods in excess of the contract price. 
	- Sir Donaldson MR decided on a different basis:
		- Where the material is incorporated in a way that leaves it in a separate and identifiable state, seller may retain property in it.
		- Where the material is incorporated in a way that leaves it unidentifiable i.e. a new product is produced, it would be necessary to deetermine who owned that product. To the extent that the buyer owns the new product, the seller may only acquire a proprietary interest over it by way of charge. 
	-Buckley LJ in _Borden v Scottish Timber Products_ __EWCA__: it is impossible for seller to reserve any property in the manufactured product, because they never had any property in it; the property in the product originates in the buyer when it is manufactured. 
- It may be possible to create an obligation on the buyer to account for sale proceeds in derivative products to the seller _Aluminium Industrie Vaassen v Romalpa Aluminium_ __EWCA__
	- Note: Buyer conceded that they were bailess of the goods for seller.
	- This is a fiduciary relationship which will generally not arise in bailment/retention of title situations _Re Andrabell_
- The _Clough Mill v Martin_ argument applies similary to retention of title in sale proceeds clauses _Pfeiffer Weinkellerei-Weinkauf v Arbuthnot Factors_
	- Claim in relation to proceeds of same of goods received by buyer after sub-sales.
	- Claim was only to the sums necessary to satisfy B's duty to pay the price
	- It was an attempt to give seller a security right and registration was required. 
	
###Security Interests (SI) and Agreements to Sell with Retention of Title (RoT)

- Differences:
	- Security is granted while title is retained. _Clough Mill v Martin_
	- RoT conceptually is not a security right(Read Welsh Development Agency v Export Finance Co [1992] BCLC 148 (CA) (Exfinco case))
		- Even though they may have similar economic effects, they are conceptually different. 
- Similarities
	- Seller has a property right in an asset in debtor's hands
	- Seller's right secures payment of money
	- If money is not paid, creditor can resort to the asset.
	- Same economic function is performed.
	
		
##Pledge

- Pledge 