#Sale of Goods

##Passage of Property

###Introduction

- Types of property
	- Real property (land)
	- Chattels real (leasehold interest in land)
	- Personal chattels
		- Things in possession
		- Intangible property
			- Documentary intangibles e.g. cheque, bearer shares
			- Pure intangibles e.g. debt, share, patent
		- Money
- Significance of property rights
	- Right in rem vs right in personem
	- Affects remedies available to parties
	- Distribution of risk between parties
	- Ability to give a third party good title
	- Claims in insolvency
	- Fluctuation in property value
	- Frustration if goods perish (contract only frustrated if goods perish when they are seller's property)
	- Property gives rise to causes of action in tort 
		- Conversion
			- Interference with another's right to possession
			- Remedy per ss 1, 3 Torts (Interference with Goods) Act 1977
				- Damages
				- Order for delivery
			- As stated in _OBG v Allen_ __UKHL__
			> Anyone who converts a chattel, that is to say, __does an act inconsistent with the rights of the owner, however innocent he may be, is liable for the loss caused__ which, if the chattel has not been recovered by the owner, will usually be the value of the goods. Fowler v Hollins was a claim for conversion of bales of cotton bought in good faith through a broker in Liverpool. The purchasers were nevertheless held strictly liable. Cleasy B said robustly that: �the liability under it is founded upon what has been regarded as a salutary rule for the protection of property, namely, that __persons deal with the property in chattels or exercise acts of ownership over them at their peril__
		- Negligence
			- Where one party contractually assumes risk for goods but does not take the title to them, they have no cause of action against third parties who negligently damage the goods _The Aliakmon_ __UKHL__
		- Imposes a duty of non-interference on the rest of the world
			- No defence that third party was unaware of owner's right _Fowler v Hollins_
			- Note that third party is not liable for a mere interference with use _Spartan Steel v Martin_
			- Interference with the thing is sufficient to constitute a tort; no interference with use is necessary _The Mediana_
- Equitable rights
	- Where a party is the absolute owner of some property, he does not simultaneously hold an equitable interest in that property. The legal title carries with it all rights. _Westdeutsche Landesbank Girozentrale v Islington LBC_ 
	- Binds third parties in some situations, subject to defences and priority rules
- Default rule is that unless otherwise agreed, risk (of innocent damage or loss of goods) remains with the seller until property passes to the buyer _Healy v Howlett_
	- This principle is now codified by s. 20 SGA
- Until property in goods passes to the buyer, the seller may not sue for the price of the goods unless there is a contractually stipulated date of payment irrespective of delivery per s 49 SGA.

###Sale of Goods Act 1979

####Introduction

- S. 2(1): Contract of sale of goods is a contract by which the seller transfers or agrees to transfer the property in goods to the buyer for a money consideration.
	- s. 2(4): Where the property in goods is transferred, it is called a sale.
	- s. 2(5): Where the transfer of property takes place at a future time or subject to some condition, it is an agreement to sell.
	- s. 2(6): An agreement to sell becomes a sale when the time elapses or the conditions are fulfilled. 
	- Note: Equity does not operate on a contract of sale of goods to see as done what ought to be done (i.e. it does not create an equitable interest)
		- This was established in _In re Wait_ and _The Aliakmon_
- S. 61(1): Goods defined as all personal chattels other than things in action and money.
- S. 5(1): Act applies to both existing and future goods (goods to be manufactured or acquired by the seller after making the contract).
	- S. 5(3): A contract that purports to effect a present sale of future goods operates as an agreement to sell the goods.
- Parties must agree to the specific goods to which the contract attaches. Mere description is not sufficient. The very goods to be sold must be ascertained.
	
####Substantial law