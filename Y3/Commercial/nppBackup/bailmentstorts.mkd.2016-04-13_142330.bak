# Bailments and Torts

##Protection of Rights in Tangibles

- In relation to goods, even in commercial contexts, bringing a tort claim is one of the principal ways of protecting one's property rights.
- In general, an equitable property right by itself is insufficient to support a tortious interference claim.
	- _MCC Proceeds v Shearson Lehmann_ establishes this in relation to conversion. The beneficiary under a trust may not bring a conversion claim unless his interest is supported by actual possession (which would then form the basis of the claim).
- By way of contrast, _Shell UK Ltd v Total UK Ltd_ __EWCA__ appears to suggest that an equitable interest in property is sufficient to form the basis of an action for unintentional interference with goods. This case is likely wrongly decided, but the parties settled (no appeal) and there is no higher authority on this point. 
	- Facts: C had an equitable interest under a trust in pipelines that were damaged by D. C wanted to claim for his economic loss as a result of the damage. The court held that C's loss is pure economic loss as C did not have a legal interest in the property. C therefore needed to prove that there was sufficient proximity between C and D (such as a voluntary assumption of responsbility by D) to succeed in his claim. 
	- Waller LJ held that:
		- The rule against recovery for pure economic loss is (partly) to limit D's liability to complete strangers. In this case, by virtue of C's beneficial interest, he is not a complete stranger. 
			- It is "legalistic" to deny C a right to recovery to his loss suffered.
		- C's beneficial ownership of the property is a special relationship sufficient to establish the relevant proximity. Therefore, C should be able to succeed in his claim.
			- It appears that he was considering the relationship between C and the damaged property rather than between C and D. This is further supported by the quote "[beneficial ownership of the damaged property] is, in fact, a closer relationship in many ways than that of a bare trustee having no more than the legal title." 
			- The rule cited by the court requires a special relationship between C and D. This judgment is therefore of questionable precendial value.
			- Note: Parties were considering appeal before settling and the claimants, who were successful in EWCA, were not intending to defend the EWCA's judgment. 

###Intentional Interference

- By way of context, there was an attempted reform of the law of tortious interference with goods in 1971 by replacing the 3 torts (conversion, detinue, trespass) with a single tort of intentional interference with goods. This was unsuccessful. In the 1977 Torts (Interference with Goods) Act, parliament instead set out some common rules that deal with "wrongful interference with goods".
- There is strict liability for the intentional interference torts.
	- "Intentional" here means D intended the act that constituted the interference (rather than D intended the interference). D may be liable even where he is an innocent party. 
		- Diplock LJ in _Marfani & Co v Midland Bank Ltd_:
		> [T]he tort at common law is one of strict liability in which the moral concept of fault in the sense of either knowledge by the doer of an act that is likely to cause injury, loss or damage to another, or lack of reasonable care to avoid causing injury, loss or damage to another, plays no part.
	- Where D acted negligently, he would not be liable under the intentional torts. 
		- Lloyd LJ in _BMW Financial Services v Bhagwanni_:
		> Ignorance of the rights of the true owner is not a defence [in conversion] but there must be a deliberate act which in fact is inconsistent with the rights of the owner.
		
		- However, consider that the unintentional tort (negligence) is also a strict liability tort to some extent. Where A negligently damages some property that he honestly believes to be his but in actual fact belongs to B, A may still be liable in negligence despite that belief. 
- Previously, there was an emphasis on the directness of D's act in the context of trespass and conversion as opposed to negligence. However, the law has since shifted its focus away from that requirement to the question of whether the act was intentional or unintentional. - Lord Denning in _Letang v Cooper_
		
####Conversion

- Basic elements as set out in _Kuwait Airways Corpn v Iraqi Airways Co_ __UKHL__:
	- D's conduct was inconsistent with the rights of the owner (or other person entitled to possession)
	- D's conduct was deliberate, not accidental
	- D's conduct was so extensive an encroachment on the rights of the owner as to exclude him from use and possession of the goods. 
		- Where D's conduct was not that extensive an encroachment, he may be liable in tresspass or negligence instead. 
- This tort vindicates the owner's property right in the goods. D's mere act of excluding C from the use and possession of the goods is sufficient whether or not D caused the state of affairs.
	- In _Kuwait Airways Corpn v Iraqi Airways Co_, the property (aeroplanes) in question were seized by the Iraqi government and given to Iraqi Airways Co. Iraqi Airways Co were found to be liable in conversion. 
- Merely excluding C from the use (without excluding possession) of the thing is insufficient. _Club Cruise Entertainment v Dept for Transport_
	- Physical interference is crucial. 
- Example cases:
	- _Marcq v Christie Manson _ __EWCA__: no liability in conversion if an auctioneer attempts to sell stolen goods on behalf of X and, having failed to do so, returns them to X, not realising the goods are stolen
	- _Fouldes v Willoughby_: no liability in conversion for taking claimant’s horses off a ferry and on shore in order to persuade the owner of the horses to disembark

####Trespass

- D is liable in trespass where he performs an act akin to but not sufficiently severe to amount to conversion. Atkin LJ in _Sanderson v Marsden and Jones_:
	> an act of conversion differs from a mere trespass in as much as the former must amount to a deprivation of possession to such an extent as to be inconsistent with the right of an owner and evidence an intention to deprive him of that right, whereas the latter includes every direct forcible injury or act disturbing the position of the owner, however slight the act may be.

- Even where C is obliged to disclose information to D (such as in family law contexts), where D takes the documents containing such information without C's consent, D will be liable in trespass. _White v Withers LLP_ __EWCA__

####Detinue

- Detinue is abolished per s 2(1) Torts (Interference with Goods) Act 1977.
- Previously, one element of detinue that was absent for conversion is that D must refuse to return the goods to C. This provided an advantage for C by providing him with a limitation period that commenced upon D's refusal rather than when he took the goods initially. 
- Conversion now includes the communication element previously part of an action for detinue. _Schwarzschild v Harrods Ltd_ __EWHC__
	- Where D has possession of C's goods, D's mere intention to keep the goods irrespective of C's wishes does not give rise to liability; he must communicate that intention to C. 
	
###Unintentional Interference

- In general, D is only liable in negligence for his unintentional physical interference with D's goods and not for his interference with D's use of the goods. _Spartan Steel & Alloys v Martin & Co_
	- A voluntary assumption of responsibility by D for C's use of his goods must be present for the latter kind of interference to be actionable. 

##Nature and Operation of Bailments

- D's duties to C depends on the particular type of bailment _Coggs v Bernard_
	- No need for C to have provided consideration for D to be under a duty of care
- The fact that there is no contract between paties does not mean that a bailment claim is tortious. The EWCA in _Yearworth v North Bristol NHS Trust_ was prepared to accept that a bailment is a separate type of claim. 
	- Possible distinction that may be drawn against tort claims is that D assumes responsibility by virtue of his acceptance of possession of the goods knowing that they do not belong to himself (_The Pioneer Container_). D's assumption of responsibility would be towards the owner of the goods, regardless of who he is or whether there are direct dealings between C and D. 
- A bailee's duty of care exists by virtue of his voluntary assumption of responsibiliy. Therefore, the scope of his duty may be defined by the terms of his assumption of responsibility. A sub-bailee may be able to rely upon the terms of his agreement with the bailee against the owner of the goods. _The Pioneer Container_ __UKPC__
- A bailee has an action against a third party who interferes with the bailed goods. If the claim is successful, the bailee is accountable to the bailor for the award (which is the equivalent of the bailed goods) and the third party is no longer liable to the bailor for his acts of interference. _The Winkfield_ __EWCA__

##Protection of Intangible Rights

- The crucial context in this context is whether intangible rights should be protected as strongly as rights in tangible goods.
	- Lady Hale (minority) in _OBG v Allan_ thought that conversion should be extended to protect intangible rights. 
- The extent of protection accorded by the common law on tangible goods is such as to impose a strict duty on everybody other than the proprietors not to interfere with the goods.
	- To impose similar duties with regards to intangible rights would be an interference on the public's liberty as people are generally unaware of and unable to find out about the intangible rights of others. It would therefore not be reasonable to hold them strictly liable when they interfere with another's intangible rights. By contrast, the physical limits of property serve as an effective sign indicating the scope of one's duty (to not interfere with goods not owned by them). 
	- Futhermore, the desirability of competition from a policy perspective is a good reason for the law to facilitate competitive conduct. Imposing a strict liability on commercial parties not to interfere with others' contractual right is undesirable as it would limit their ability and willingness to compete (chilling effect). 
- From a doctrinal perspective, parties to a contract acquire contractual rights by virtue of mutual agreement and provision of consideration. These rights correspond to the counterparty's obligations and the scope of these obligations is the terms of the counterparty's contractual promise. It would be absurd if the mutual agreement of two parties is capable of creating obligations on the part of a third party who is a complete stranger to the contract and has not consented to come under those obligations. Similar difficulties do not arise in the context of rights in tangible goods. A person who does not have a right to possess an object is under an obligation not to interfere with it regardless of the identity of the owner. The conduct required of him cannot be unilaterally altered by the conduct of the owner.

###Background

- In general, D is permitted to act however maliciously so long as his conduct is lawful. _Bradford v Pickles_
	- Crucial question is whether D infringed any right of C correlating to a duty on D. _Allen v Flood_
		- There is no general right belonging to everyone to their economic well being. 
	- See Hohfeld's conception of claim right - duty vs liberty.
	
###Procuring a Breach of Contract

- Elements: 
	- Breach of contract
		- The contractual party must have actually breached the contract _Lumley v Gye_
			- This tort is one of accessory liability _OBG v Allan_
			- Previous cases holding otherwise were overruled in _OBG v Allan_
	- Mental element: knowledge
		- To be liable for procuring a breach of contract, one must have actual knowledge that he is inducing a breach of contract. The act must be intentional. _OBG v Allan_
			- Constructive knowledge will not suffice.
				- D's honest (but mistaken and unreasonable) belief that he was not procuring a breach of contract will be sufficient for him to avoid liability.
			- Where a wrongdoer knowingly causes a breach of contract, it does not matter that the breach is the means by which the he intends to achieve some further end.
				- No need that D intended to cause loss to C.
			- If the breach of contract is neither an end in itself nor a means to an end, but merely a foreseeable consequence, it cannot be s aid to be indended. 
	- Procuring or facilitating
		- Persuasde _Lewis v Yeeles_ __EWCA__
		- Assisting or facilitating _OBG v Allan_
			- E.g. where D agrees to buy goods that he knows C is contractually bound not to sell _BMTA v Salvadori_
	- Defence: Justification
		- D would not be liable where he is exercising his pre-existing right. _Edwin Hill v First National Finance Corp_ __EWCA__
			- D exercised his security right to compel a company to dismiss C in breach of the company's contract with C.
			- This case may perhaps also be authority for the proposition that D would not be liable where he has a pre-existing right that allows him to justifiably cause the breach even if he was not exercising that right. 
	
###Causing Loss by Unlawful Means

- Per Lord Hoffman in _OBG v Allan_, the elements of this tort are:
	- 
