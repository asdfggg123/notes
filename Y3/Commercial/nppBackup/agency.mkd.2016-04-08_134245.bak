#Agency

##Introduction

- Three parties are involved in an agency situation
- A true agent puts his principal into a contractual relationship with a third party - Sir Donaldson MR _Potter v Customs & Excise Commissioners_
- Hohfeld and Dowrick: The essential characteristic of an agent is that he is consensually invested with a legal power to alter his principal�s legal relations with third persons: the principal is under a correlative liability to have his legal relations with third parties altered. This power-liability relation, is the essence of the relationship of principal and agent.
- The concept of a power
	- A power is a type of legal interest
	- A power holder is one who, through their own violation, is able to effect a change in a given legal relation
	- A power has a corresponding liability
	- Those subject to a liability may have their own legal relations changed when a power is exercised

##The Internal Aspect of an Agency

- Mutual consent gives rise to an agent's power and his principal's corresponding liability. - Lord Pearson in _Garnac Grain v HMF Faure and Fairclough_ 
	- This agreement need not be contractual
	
##The External Aspect of an Agency

- It is necessary to distinguish between the situations where an agent has actual authority or apparent authority. - Diplock LJ in _Freeman & Lockyer v Buckhurst Park Properties_ 
	- Actual and apparent authority are independent of one another. Although they generally co-exist, it is possible for either to exist independantly of the other. 

###Actual Authority in a Disclosed Situation

- An agent's actual authority is the legal power conferred upon him by his principal to alter his principal's legal relations with third parties.
	- Actual authority is created consensually and its scope is determined by applying principles of construction to the agency agreement. If the agent enters into a contract pursuant to his actual authoritty, his actions create contractual rights and liabilities between the principal and the third party. - Diplock LJ in _Freeman & Lockyer_
- Doctrinal theories as to how the agent creates direct contractual relations between his principal and the third party:
	- Magic (as referred to by Krebs)
		- When A properly exercises his power, he changes P's legal position in relation to the third party. 
	- General contractual principles (advocated by Krebs)
		- The agent with actual authority might be thought of a messenger, communicating on behalf of the principal but taking no part in substantive events himself.
		- Krebs considers the role of the agent analogous to that of the ticket machine in _Thornton v Shoe Lane Parking_. P makes the offer through A, and X's acceptence does not need to be communicated to P to complete the contract. Krebs therefore argues that the X's contractual liability to the principal can be explained on straightforward contractual grounds.
			- However, it is quite difficult to set out the scope of P's offer under Krebs' account, especially where A and X enter into extended negotiations as is common in commercial practice. Even in the simplistic example of where P appoints A to sell his car for no less than �300, P is not contractually bound to sell his car to anyone who offers A �300. A is entitled to negotiate on P's behalf to secure a better price (without P's intervention), even though P's offer under Krebs' account would merely be to sell his car for no less than �300. This could perhaps be explained on the basis that P offers to sell his car on the conditions that the price is above �300 and A agrees to it. However, this then fails to explain the basis on which A's fiduciary duty to P arises, if A merely holds a power under P's offer. A fiduciary duty is significantly more onerous than a mere obligation to act in good faith (if there is one under _BT v Telefonica principles_). Furthermore, if A is merely a messenger, there is little justification for English law to impose fiducary duties on such agents. 
- The scope of A's actual authority
	- The scope may be expressly set out in the agency agreement (or mutual consent) _Ramsey v Love_
	- The scope may also be set out impliedly through parties conduct _Hely-Hutchinson v Brayhead_
		- Facts: A, chairman of P (a company), entered into a contract with X. A's official position did not confer upon him the authority to enter into such contracts, but because P acquiesced to A acting as the de facto managing director of the company (for a significant period of time), P's conduct had the effect of impliedly conferring upon A the authority of a managing director to enter the company into such contracts. 
	- The general rule is that the scope of an agent's authority necessarily includes powers to perform intermediate acts which are not expressed but which are necessary in order to attain the accomplishment of the object of the principal power. - Eyre CJ in _Howard v Baillie_
	
###Apparent Authority in a Disclosed Situation

- Diplock LJ in _Freeman & Lockyer_: A has apparent authority where:
	- P makes a (clear and unambiguous) representation to X that A has the authority to enter into a certain kind of contract.
	- P's representation was intended to be and in fact acted upon by X.
		- P does not need to be aware that he is making a representation.
		- The representation may be by words or conduct.
			- For example, where P permits A to act in some way in the conduct of P's business, he represents to anyone who becomes aware of the circumstances that A has the actual authority that a person acting in his capacity would usually have. 
	- X, (reasonably) relying on P's representation, purported to enter into a contract with P via A.
		- This is a causal requirement; X merely has to show that P's representation was a significant factor in his decision to enter into the contract with A. _Steria v Hutchinson_ __EWCA__
		- If X is aware that A does not actually have the authority they appear to have, but continues to rely on the a contrary representation by P, P is not liable under the contract. _The Ocean Frost_ 
			- Contrast the case of _First Energy (UK) v Hungarian International Bank_
				- Facts: P was a bank. A was head of its Manchester office as, as such, a �senior manager�. X approached A�s office seeking a loan. X knew that A had no authority to give them a loan, and that it would have to be centrally approved by P. A wrote to X offering the loan as if it had been approved, even though it had not, and X accepted. Question is whether bank are bound by X�s offer.
				- EWCA accepted X's argument that in appointing A as "senior" manager for an extensive region, even though X knew A had no actual authority to make the offer, P represented to X that A had the authority to communicate offers of loans from P. 
				- 
	- P would then be liable to perform any obligations imposed upon him by such contract.
		- P does not have any rights under the contract unless he ratifies it. 
		- In the context of the sale of goods by A, the effect of the law of apparent authority is to cause the transfer of real title. - Devlin J in _Eastern Distributors v Goldring_
	- Scope of A's actual authority is irrelevant.
- Doctrinal theories as to how apparent authority functions to impose liabilities on P to X:
	- Magic?
		- Doctrine similar to estoppel by representation applies. However, here, the doctrine does not just preclude one party from denying the truth of his representation, but functions to create liabilities on P. It functions as a sword and not a shield. 
			- Note: X's act of entering into the contract is analogous to the detriment element in estoppel. 
		- Apparent authority shares a substantially similar public policy foundation with estoppel by representation i.e. it would be unconscionable for P to deny that he was bound by a transaction which had been entered into apparently on his behalf by someone whom he had permitted to represent to third parties that he had the principal's authority to bind the principal to transactions of the kind in question. - Colman J in _Homburg Houtimport v Agrosin ("The Starsin")_
			- To permit the principal to rely on lack of actual authority in such circumstances to the prejudice of X would be to permit a similarly prejudicial inconsistency of conduct to that against which the law of estoppel is directed. 
	- Standard contract law principles (advocated by Krebs)
		- P's representation is a prospective offer that contains an authorisation of any subsequent offers made by A regardless of A's actual authority. 
			- P is essentially making an offer to X to contract with A on terms to be agreed between X and A, and this offer is accepted by X doing so.