#Agency

##Introduction

- Three parties are involved in an agency situation
- A true agent puts his principal into a contractual relationship with a third party - Sir Donaldson MR _Potter v Customs & Excise Commissioners_
- Hohfeld and Dowrick: The essential characteristic of an agent is that he is consensually invested with a legal power to alter his principalís legal relations with third persons: the principal is under a correlative liability to have his legal relations with third parties altered. This power-liability relation, is the essence of the relationship of principal and agent.
- The concept of a power
	- A power is a type of legal interest
	- A power holder is one who, through their own violation, is able to effect a change in a given legal relation
	- A power has a corresponding liability
	- Those subject to a liability may have their own legal relations changed when a power is exercised

##The Internal Aspect of an Agency

- Mutual consent gives rise to an agent's power and his principal's corresponding liability. - Lord Pearson in _Garnac Grain v HMF Faure and Fairclough_ 
	- This agreement need not be contractual
	
##The External Aspect of an Agency

- It is necessary to distinguish between the situations where an agent has actual authority or apparent authority. - Diplock LJ in _Freeman & Lockyer v Buckhurst Park Properties_ 
	- Actual and apparent authority are independent of one another. Although they generally co-exist, it is possible for either to exist independantly of the other. 

###Actual Authority (in a Disclosed Situation)

- An agent's actual authority is the legal power conferred upon him by his principal to alter his principal's legal relations with third parties.
	- Actual authority is created consensually 