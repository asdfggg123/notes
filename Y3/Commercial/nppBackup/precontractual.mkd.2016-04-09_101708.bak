#Pre-Contractual Liability

##Introduction

###Background

- The starting point is that parties incur costs prior to the conclusion of a contract (e.g. during negotiations/tender) at their own risk. 
	- Parties to pre-contractual negotiations may break them off without liability at any time for any reason (or for no reason at all).
- Farnsworth gives one reason for the law's treatment of pre-contractual costs:
	- "Aleatory (dependant on luck, chance, or an an uncertain outcome) view" of negotiations: a party that enters negotiations in the hope of the gain that will result from ultimate agreement bears the risk of whatever loss results if the other party breaks off the negotiations
	- This view rests upon a concern that limiting the freedom of negotiation might discourage parties from entering negotiations. 