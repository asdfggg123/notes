#Pre-Contractual Liability

##Introduction

###Background

- The starting point is that parties incur costs prior to the conclusion of a contract (e.g. during negotiations/tender) at their own risk. 
	- Parties to pre-contractual negotiations may break them off without liability at any time for any reason (or for no reason at all).
- Farnsworth gives one reason for the law's treatment of pre-contractual costs:
	- "Aleatory (dependant on luck, chance, or an an uncertain outcome) view" of negotiations: a party that enters negotiations in the hope of the gain that will result from ultimate agreement bears the risk of whatever loss results if the other party breaks off the negotiations
	- This view rests upon a concern that limiting the freedom of negotiation might discourage parties from entering negotiations. 
- Allowing claims in respect of unsuccessful negotiations is likely to inhibit the efficient pursuit of commercial negotiations. - Arden LJ in _Crossco No 4 Unlimited v Jolan_
- Part of the reason of English law's treatment of pre-contractual liability is that the common law imposes no duty on parties to negotiate in good faith. 
	- The converse applies in civillian systems.
	- Even where parties expressly agree to negotiate in good faith, it is unenforceable because it lacks the necessary certainty for a court to enforce. _Walford v Miles_
		- Difficult for the court to determine with certainty whether there was a proper reason for the termination of negotiations. 
	- The concept of a duty to negotiate in good faith is inherently repugnant to the adversarial position of negotiating parties. Each party is entitled pursue his own interest, so long as he avoids making misrepresentations. To advance that interest he must be entitled to threaten to withdraw from further negotiations or to withdraw in fact in the hope that the opposite party may seek to reopen negotiations by offering him improved terms. 

###Efficiency

- In some situations, it might be in fact efficient to have rules that allow for pre-contractual libaility. 
- Schwartz and Scott: In commercial situations, the complexity of the environment may present good reasons why parties are unable to complete a contract at the beginning. However, investment during this interim work may accelerate the later realisation of returns. 
	- Preliminary agreements are commonly exploratory; their performance allows parties to pursue an efficient project later. It is efficient for contract law to protect priliminary agreements.
- 