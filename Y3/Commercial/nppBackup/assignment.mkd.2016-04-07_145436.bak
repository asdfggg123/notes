#Assignment

##Introduction

###Choses in Action

- Chose in action is a legal expression used to describe personal rights of property which can be claimed or enforced by action, and not by taking physical possession _Torkington v Magee_
- Is a chose in action properly considered to be property?
	- Baroness Hale in _OBG v Allan_: The essential feature of property is that it has an existence independent of a particular person: it can be bought and sold, given and received, bequeathed and inherited, pledged or seized to secure debts...
	- Choses in action often called property in commercial practice, but it is in fact only a personal right that can be exercised against one or more third parties. 

###Assignment Distinguished

- Where A has a contractual right to payment against X; A wishes to arrange for B to get the benefit of that right.
- Novation: Contract between A and X is replaced with a new contract between B and X, so that B has a direct contractual claim against X. 
	- Consent of all parties is required
	- B must provide consideration under the new contract
	- B gains a new right directly against X
		- Not a transfer of A's right
	- New contract may also impose duties on B to X
- Contract (Rights of Third Parties) Act: If the contract between A and X confers a benefit on B, B willbe able to bring a direct statutory claim against X
	- Consent of A and X is required
	- B need not provide consideration
	- B gains a new right directly against X
	- No duties can be imposed on B
- Acknowledgement: If X holds a specific fund from which X's debt to A must be satisfied, and A instructs X to pay (part of) the debt due from that fund to B, and X agrees, then B can bring a direct claim against X.
	- Consent of A and X is required
	- B need not provide consideration
	- B gains a new right directly against X
	- No duties can be imposed on B
- Declaration of trust: A declares that he holds the contractual right to payment against X on trust for B.
	- Consent of X is not required
	- B need not provide consideration
	- B does not gain a new right directly against X, although B can 
force A to sue X by joining A as a defendant (the Vanderpitte procedure _Vandepitte v Preferred Accident Insurance Corp of New York_ __UKPC__)
- Assignment: A's right against X is transfered to B. This transfer may be equitable or legal (statutory). 
	- A loses his right against X
	- B acquires a right against X
	- Consent of X is not required

##Three Forms of Assignment

###Common Law Assignment

- Basic rule is that choses in action cannot be assigned in common law, with the exception of debts owed by and due to the Crown, debts physically embodied by a negotiable instrument (e.g. cheques, bills of exchange, bearer bonds, etc), and registered company shares and debentures. 
	- Basic rule is that rights that cannot be asserted by taking possession of a physical thing cannot be transferred.
- Possible reasons behind the basic common law rule:
	- Treitel: Early lawyers found it hard to think of a transfer of an intangible right. Later the rule was based on the fear that assignments of choses in action might leat to maintenance (meddling in litigation in which a party has no concern).
	- _Lampet's Case_: Worry that there would be a multiplying of contentions and suits. 

###Equitable Assignment

- Main question: Does equitable assignment really involve the transfer of a right from A to B? It is unlikely that equity simply ignored the common law restrictions on the assignment of choses in action (and their justifications). (Burrows)
	- Tham: Fundamental issue that X only agreed to enter into contractual relations with A. His rights and duties against and to A exist by reason of his consent, which is absent between X and B. 
- Equitable assignment of a chose in action is not really a transfer of A's right but instead is more like a declaration of trust. B is able to exercise A's right against X, but does not have a direct claim against X. 
	- B only has a right against A. 

####Type 1 Equitable Assignment

- A expresses his intention to make an immediate transfer of the benefit of his right against X to B. This constitutes an equitable assignment of A's right. 
	- X's consent is not required
	- B need not provide consideration
	- No notice need be given to X
		- Doctrinally, no notice is required since B is merely exercising A's right, and X's contractual duties contine to be towards A. 
		- X can discharge his duties by performing them in favour of A (if he has no notice).
	- Writing is not required
		- A need not use the language of "assignment" as long as the meaning is plain that X should be given to understand that the debt has been transferred by A to B. _Brandt's Sons v Dunlop Rubber_ __UKHL__ (Lord Macnaughten)
	- An equitable assignment by A prior to A's insolvency would mean that A's right against X is not available to his unsecured creditors. _Gorringe v Irwell India Rubber and Gutta Percha Works_ __EWCA__, _Holt v Heatherfield Trust_
- Effect of an equitable assignment
	- B does not acquire a direct claim against X
		- In _Brandt's Sons v Dunlop Rubber_, B was allowed to bring a direct claim against X despite A's absence in the proceedings. This case is restricted to its facts.
		- In _Roberts v Gill_ __UKHL__, Lord Collins held that other than in the "most exceptional circumstances" such as in _Brandt's Sons v Dunlop Rubber_, in the case of an equitable assignment, B cannot proceed to judgment without joining A. 
			- If an equitable assignee sues a third party, the assignor must be joined as a defendant.
			- _Brandt's Sons_ was exceptional because the absence of A was overlooked by both parties until they reached the House of Lords. 
			- Lord Collins noted that in modern times, courts have been more relaxed about the requirement to join A. The action is validly constituted without joining A, although A must be joined before a final judgment can be obtained by B. 
		- In _MH Smith (Plant Hire) v DL Mainwaring_ __EWCA__: Case concerned B who had a right of subrogation to A's claim against X, where A is a company that has ceased to exist. Court held that a subrogation is like an equitable assignment. When B seeks to bring an action against X, he is in fact not invoking his own right against X (which he does not have) but is instead exercising A's right against X. The necessity for A's presence is a substantive requirement, not just a procedural one. 
			- Since A no longer existed, B was unable to exercise A's right against X. 
	- Quite a strong resemblance to a declaration of trust

###Type 2 Equitable Assignment

- Where A expects to acquire contractual rights against X, and A promises B that A will transfer those rights to B, if B provides consideration for A's promise, this constitutes an equitable assignment. 
	- B acquires an equitable interest as soon as A acquires his rights against X.
	- Consideration is necessary since the agreement places A under a future duty to do something. 