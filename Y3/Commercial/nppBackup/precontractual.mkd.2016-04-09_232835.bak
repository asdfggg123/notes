#Pre-Contractual Liability

##Introduction

###Background

- The starting point is that parties incur costs prior to the conclusion of a contract (e.g. during negotiations/tender) at their own risk. 
	- Parties to pre-contractual negotiations may break them off without liability at any time for any reason (or for no reason at all).
- Farnsworth gives one reason for the law's treatment of pre-contractual costs:
	- "Aleatory (dependant on luck, chance, or an an uncertain outcome) view" of negotiations: a party that enters negotiations in the hope of the gain that will result from ultimate agreement bears the risk of whatever loss results if the other party breaks off the negotiations
	- This view rests upon a concern that limiting the freedom of negotiation might discourage parties from entering negotiations. 
- Allowing claims in respect of unsuccessful negotiations is likely to inhibit the efficient pursuit of commercial negotiations. - Arden LJ in _Crossco No 4 Unlimited v Jolan_
- Part of the reason of English law's treatment of pre-contractual liability is that the common law imposes no duty on parties to negotiate in good faith. 
	- The converse applies in civillian systems.
	- Even where parties expressly agree to negotiate in good faith, it is unenforceable because it lacks the necessary certainty for a court to enforce. _Walford v Miles_
		- Difficult for the court to determine with certainty whether there was a proper reason for the termination of negotiations. 
	- The concept of a duty to negotiate in good faith is inherently repugnant to the adversarial position of negotiating parties. Each party is entitled pursue his own interest, so long as he avoids making misrepresentations. To advance that interest he must be entitled to threaten to withdraw from further negotiations or to withdraw in fact in the hope that the opposite party may seek to reopen negotiations by offering him improved terms. 

###Efficiency

- In some situations, it might be in fact efficient to have rules that allow for pre-contractual libaility. 
- Schwartz and Scott: In commercial situations, the complexity of the environment may present good reasons why parties are unable to complete a contract at the beginning. However, investment during this interim work may accelerate the later realisation of returns. 
	- Preliminary agreements are commonly exploratory; their performance allows parties to pursue an efficient project later. It is efficient for contract law to protect the promisee�s reliance interest if his promisor deviated from an agreed investment sequence. A reliance recovery will encourage parties to make preliminary agreements and will deter some strategic behaviour.
	- Question: If it is in both parties' interests to perform pre-contractual work, why should they not enter into a preliminary contract for such work?
- Ben-Shahar and Pottow: Where there are default rules, i.e. rules that apply in the absence of an agreement, it can be difficult in practice for parties to contract around them because their presence creates an expectation as to what is the right thing to do and if a party wants to depart from there and adopt unfamiliar terms, it may raise suspicions and scare away potential counterparties. 
	- When a party expresses a desire to depart from default rules, it may be taken as a signal of distrust or a sign that the party is unduely legalistic (that may be prejudicial to subsequent negotiations).
- Scott: Commercial parties may deliberately opt for incomplete agreements as they prefer to trust that notions of reciprocal fairness will provide a sufficient incentive for performance.

###The Current Position

- Although there are cases where the courts have recognised pre-contractual liability, it is difficult to formulate a clear general principle that governs all the different factual situations. _Countrywide Communications v ICI Pathway_
	- While a broad principle enabling recovery of pre-contractual expenditure may be desirable, no such principle is clearly established in English law.
	- The court may impose an obligation to pay for benefits resulting from services performed in the course of a contract which is expected to, but does not, come into existence, but only if justice requires it or it would be unconscionable for the plaintiff not to be recompensed. 
	
##Contractual Claims

- Limits to contractual claims
	- A bare agreement to negotiate (in good faith or not) has no legal content. There can be no obligation to negotiate until there is a "proper reason" to withdraw. _Walford v Miles_ __UKHL__
	- An agreement between two parties to enter into an agreement in which some critical part if the matter is left undetermined is no contract at all. _May & Butcher v R_ __UKHL__
- Finding a principal contract
	- This refers to where the court finds the existence of a contract (because all key terms have been determined) even though some of the terms have not been agreed upon. The court may imply those terms into the contract. 
	- E.g. Under s 8 Sale of Goods Act 1979 (codifying what the common law aleady did), where the price in a contract of sale is not determined by the contract or the course of dealing between the parties, the buyer must pay a reasonable price. 
	- In _Way v Latilla_, a shared understanding that C�s services were to be paid for was given effect to by means of an implied contractual term for reasonable remuneration:
		- There was a contract of employment which clearly indicated that the work was not to be gratuitous. 
	- In _RTS Flexible Systems v Molkerei Alois Muller_ __UKSC__, an agreed stipulation that the parties� agreement �shall not become effective until each party has executed a counterpart and exchanged it with the other� did not prevent a contract arising without such execution and exchange.
		- The circumstances pointed to the fact that there was a binding agreement. The price had been agreed and a significant amount of work had been carried out. A subsequent agreement to vary the contract was reached without any mention that the variation was subject to contract (implicitly accepting that the previous contract was validly concluded). Parties have in effect waived the stipulation that required the exchange of documents. 
		- A reasonable, honest businessman would have concluded that the parties intended that the work should be carried out for the agreed price on the agreed terms without the necessity for a formal written contract. 
- Finding a collateral contract
	- _Brewer Street Investments v Barclays Wollen Co_ __EWCA__ concerned "subject to contract" negotiations for the grant of a lease. The claimant, owners of the premises, agreed to make alterations requested by the defendant, the prospective tenant, and D promised to pay for alterations. Negotiations fail and the alterations confer no benefit on C. 
		- Somervell LJ found that D was liable in contract due the the existence of a collateral contract to pay for the alterations (even though the principal contract was not concluded).
		- Denning LJ thought that the promise was conditional on the lease being granted and therefore no contractual liability could be imposed. He allowed the claim on restitution grounds.
			- Problem with unjust enrichment claim in this case is that D was not enriched. 
		- Romer LJ agrees with both judgments. 
		
##Unjust Enrichment Claims

- Elements for an unjust enrichment claim
	- D is enriched
		- Determination of D's enrichment is difficult because courts are often attempting to categorise as an unjust enrichment what is really just a loss unfairly sustained by the claimant. _Countrywide Communications v ICL Pathway_
			- Where D requests for C to render a service, C's performance of that service may be treated as an enrichment on D's part even if the service was of no value to D. _CPS v Eastenders Group_ 
		- D receives a service from C that D did not pay for _Cobbe v Yeoman's Row Management.
			- Cobbe obtained planning permission for Yeoman's Row's property at his own expense on the basis that there would be a contract for the sale of the land. 
	- D's enrichment must be at the expense of C
	- Unjust factor
		- Mistake (about an existing matter of fact or law)
		- Representation
		- Failure of consideration
			- Where C does something for D on the shared basis that C would receive remuneration and the basis then fails, it may be unjust for D to retain the benefits of that work without paying for it. 
		- Duress
	- Defences
	- Quantum of award
		- Quantum of enrichment
			- It is open to D to argue that due to D's particular circumstances, D would not have paid market value for the services. That would reduce his liability to C. _Benedetti v Sawiris_
				- However, if D would have been willing to pay more than the objective value of the services, C may not claim a sum above the objective value of those services. 
				- Where there is no wider market for the services, and C and D agreed a price for those services, that price may determine the extent of D's enrichment as it provides the only evidence of the objective value of the services. 
		- Quantum meruit
			- The market price of the goods/services provided by C
- Where parties have not reached an agreement but one of them has rendered services (or delivered goods) to the other, a non-contractual restitutionary obligation may arise. No clear principle has been identified to govern the different factual situations which have arisen as to whether a restitutionary claim can successfully be made for work done in anticipation of a contract which does not materialise. _Benourad v Compass Group_
	- Relevant factors in favour of recovery:
		- Whether D requested C to provided services or accepted them knowing that C did not indend to give them for free (e.g. _British Steel Corp v Cleveland Bridge and Enginnering Co Ltd_)
		- Whether D behaved unconscionably in declining to pay for the benefit received
	- Relevant factors against recovery:
		- Did C take the risk that he or she would only be reimbursed for the expenditure if there was a concluded contract such that the risk, in all the circumstances, should fall on C
			- Usually difficult to determine on the facts.
		- Were C's costs incurred for the purpose of putting C in a position to obtain and then perform the contract (e.g. expenses incurred in the course of a tender process are not recoverable _MSM Consulting Ltd v United Republic of Tanzania_)
		
##Proprietary Estoppel

###Contractual Claims vs Proprietary Estoppel

- Elements of a contract
	- Intention to make an immediately binding agreement
	- Certainty of terms
		- Where finding a collateral contract, it is not necessary to have certainty of all the terms in the contract. 
	- Consideration
	- Formality
- In cases such as _Brewer Street Investments v Barclays Woollen Co_, it should be noted that because the promise was by the prospective tenant (that he would take the tenancy), the landlord had no way to hold him to the promise. 
	- If it had been the other way round, and the prospective tenant, relying on the landlord's promise that he would be granted the lease, carried out alteration works to the property, he may be able to hold the landlord to the promise through a proprietary estoppel claim. (e.g. _Lloyd v Dugdale_)
- Elements of proprietary estoppel per _Thorner v Major_:
	- Representation made by D to C
	- C's reasonable reliance on D's representation
	- Detriment to C in consequence of his reliance
- Comparison given in _Walton v Walton_:
	- In the case of promises made in a family or social context, the law is reluctant to assume that there was an intention to create legal relations. One reason for this is that such promises are often subject to unspoken and ill-defined qualifications. Uncertainty as to the future further complicates matters. However, a contract, subject to the narrow doctrine of frustration, must be performed come what may. 
	- The abovementioned reasoning does not apply to equitable estoppel, because it does not look forward to the future and guess what might happen. It looks backwards when the promise falls due to be performed and asks whether, in the circumstances which have actually happened, it would be unconscionable for the promise not to be kept.
		- All that is required is that the initial assurance must be clear enough, but what amounts to sufficient clarity depends on context. It must be unambiguous and must appear to have been intended to be taken seriously. It must have been a promise which one might reasonably expect to be relied upon by the person to whom it was made. 

###Proprietary Estoppel Claims and their Limits

- The nature of proprietary estoppel
	- Lord Scott in _Cobbe v Yeoman's Row Management_:
	> An �estoppel� bars the object of it from asserting some fact or facts, or, sometimes, something that is a mixture of fact and law, that stands in the way of some right claimed by the person entitled to the benefit of the estoppel. The estoppel becomes a �proprietary� estoppel � a sub-species of �promissory� estoppel � if the right claimed is a proprietary right, usually a right to or over land, but, in principle, equally available in relation to chattels or choses in action.
	- This statement is not entirely accurate. Successful proprietary estoppel claims not just prevent D from denying a state of affairs, but instead hold D to a promise such that C acquires a future right. It not just prevents D from denying the promise but also gives effect to the promise (which would ordinarily not be actionable without a contract). _Thorner v Major_
- Is there a domestic/commercial distinction?
	- Lord Neuberger in _Thorner_ suggests that in cases such as _Cobbe_, where the relationship between parties is arm's length and commercial, the party raising the estoppel was a highly experienced businessman, and the circumstances were such that the parties could well have been expected to enter into a contract but consciously chosen not to do so, the courts of equity would be less likely to allow a proprietary estoppel claim. 
		- By contrast, where the relationship between parties was familial and personal, and neither party had much commercial experience such that they would not contemplate entering into a formal contract, it is easier for the courts to find that a promise was made that was intended to be relied upon and C reasonably relied upon it. 
	- Arden LJ in _Crossco v Unltd v Jolan Ltd_: For the law in general to provide scope for claims in respect of unsuccessful negotiations that do not result in legally enforceable contracts would, in my judgement, be likely to inhibit the efficient pursuit of commercial negotiations, which is a necessary part of proper entrepreneurial activity.
	- However, in commercial contexts (such as in _Sutcliffe v Lloyd_), there is undoubtedly still room for proprietary estoppel to take effect. 
		- What matters is not whether C believes D to have made a legally binding promise. 
		- Equity intervenes at a significantly later stage (than when the promise was made) to make the promise unable to be revoked (and able to be enforced).
- Need for a promise capable of being reasonably understood by C as intended by D to be capable of being relied on by C
	- Where there is significant uncertainty about non-trivial elements of the promise, it is less likely to be capable of reasonable reliance. _Cobbe v Yeoman's Row_
	- _Kinane v Mackie-Conteh_ is an example of a commercial case where C reasonably relied upon D's promise.
		- Facts: D�s company urgently needs money to set up a letter of credit allowing it to buy rice from an overseas supplier � C is willing to lend (at 100% interest) but requires security � D sends a signed letter to C agreeing to give C a charge over D�s home � C provides the loan money to D.
		- No contract as the letter was not signed by both C and D � but C acted in reliance on D�s promise and would suffer a detriment if D were wholly free to renege on the promise � EWCA confirms that D is under a duty to grant C the promised charge.
- Need for the promise to relate to identified property owned, or about to be owned by D.
	- Need for promise to relate to identified property confirmed in _Thorner v Major_
	- Note the origin of the doctrine of proprietary estoppel in _Crabb v Arun DC_, which is in turn based on _Moorgate Mercantile Co Ltd v Twitchings_, a case decided on questionable doctrinal grounds.
		- Denning LJ