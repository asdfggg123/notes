# Bailments and Torts

##Protection of Rights in Tangibles

- In relation to goods, even in commercial contexts, bringing a tort claim is one of the principal ways of protecting one's property rights. 

###Intentional Interference

- By way of context, there was an attempted reform of the law of tortious interference with goods in 1971 by replacing the 3 torts (conversion, detinue, trespass) with a single tort of intentional interference with goods. This was unsuccessful. In the 1977 Torts (Interference with Goods) Act, parliament instead set out some common rules that deal with "wrongful interference with goods".
- There is strict liability for the intentional interference torts.
	- "Intentional" here means D intended the act that constituted the interference (rather than D intended the interference). D may be liable even where he is an innocent party. 
		- Diplock LJ in _Marfani & Co v Midland Bank Ltd_:
		> [T]he tort at common law is one of strict liability in which the moral concept of fault in the sense of either knowledge by the doer of an act that is likely to cause injury, loss or damage to another, or lack of reasonable care to avoid causing injury, loss or damage to another, plays no part.
	- Where D acted negligently, he would not be liable under the intentional torts. 
		- Lloyd LJ in _BMW Financial Services v Bhagwanni_:
		> Ignorance of the rights of the true owner is not a defence [in conversion] but there must be a deliberate act which in fact is inconsistent with the rights of the owner.
		
		- However, consider that the unintentional tort (negligence) is also a strict liability tort to some extent. Where A negligently damages some property that he honestly believes to be his but in actual fact belongs to B, A may still be liable in negligence despite that belief. 
- Previously, there was an emphasis on the directness of D's act in the context of trespass and conversion as opposed to negligence. However, the law has since shifted its focus away from that requirement to the question of whether the act was intentional or unintentional. - Lord Denning in _Letang v Cooper_
		
####Conversion

- Basic elements as set out in _Kuwait Airways Corpn v Iraqi Airways Co_ __UKHL__:
	- D's conduct was inconsistent with the rights of the owner (or other person entitled to possession)
	- D's conduct was deliberate, not accidental
	- D's conduct was so extensive an encroachment on the rights of the owner as to exclude him from use and possession of the goods. 
		- Where D's conduct was not that extensive an encroachment, he may be liable in tresspass or negligence instead. 
- This tort vindicates the owner's property right in the goods. D's mere act of excluding C from the use and possession of the goods is sufficient whether or not D caused the state of affairs.
	- In _Kuwait Airways Corpn v Iraqi Airways Co_, the property (aeroplanes) in question were seized by the Iraqi government and given to Iraqi Airways Co. Iraqi Airways Co were found to be liable in conversion. 
- Merely excluding C from the use (without excluding possession) of the thing is insufficient. _Club Cruise Entertainment v Dept for Transport_
	- Physical interference is crucial. 
- Example cases:
	- _Marcq v Christie Manson _ __EWCA__: no liability in conversion if an auctioneer attempts to sell stolen goods on behalf of X and, having failed to do so, returns them to X, not realising the goods are stolen
	- _Fouldes v Willoughby_: no liability in conversion for taking claimant’s horses off a ferry and on shore in order to persuade the owner of the horses to disembark

####Trespass

- D is liable in trespass where he performs an act akin to but not sufficiently severe to amount to conversion. Atkin LJ in _Sanderson v Marsden and Jones_:
	> an act of conversion differs from a mere trespass in as much as the former must amount to a deprivation of possession to such an extent as to be inconsistent with the right of an owner and evidence an intention to deprive him of that right, whereas the latter includes every direct forcible injury or act disturbing the position of the owner, however slight the act may be.

- Even where C is obliged to disclose information to D (such as in family law contexts), where D takes the documents containing such information without C's consent, D will be liable in trespass. _White v Withers LLP_ __EWCA__

####Detinue

- Detinue is abolished per s 2(1) Torts (Interference with Goods) Act 1977.
- Previously, one element of detinue that was absent for conversion is that D must refuse to return the goods to C. This provided an advantage for C by providing him with a limitation period that commenced upon D's refusal rather than when he took the goods initially. 