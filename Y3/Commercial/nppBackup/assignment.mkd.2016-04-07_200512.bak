#Assignment

##Introduction

###Choses in Action

- Chose in action is a legal expression used to describe personal rights of property which can be claimed or enforced by action, and not by taking physical possession _Torkington v Magee_
- Is a chose in action properly considered to be property?
	- Baroness Hale in _OBG v Allan_: The essential feature of property is that it has an existence independent of a particular person: it can be bought and sold, given and received, bequeathed and inherited, pledged or seized to secure debts...
	- Choses in action often called property in commercial practice, but it is in fact only a personal right that can be exercised against one or more third parties. 

###Assignment Distinguished

- Where A has a contractual right to payment against X; A wishes to arrange for B to get the benefit of that right.
- Novation: Contract between A and X is replaced with a new contract between B and X, so that B has a direct contractual claim against X. 
	- Consent of all parties is required
	- B must provide consideration under the new contract
	- B gains a new right directly against X
		- Not a transfer of A's right
	- New contract may also impose duties on B to X
- Contract (Rights of Third Parties) Act: If the contract between A and X confers a benefit on B, B willbe able to bring a direct statutory claim against X
	- Consent of A and X is required
	- B need not provide consideration
	- B gains a new right directly against X
	- No duties can be imposed on B
- Acknowledgement: If X holds a specific fund from which X's debt to A must be satisfied, and A instructs X to pay (part of) the debt due from that fund to B, and X agrees, then B can bring a direct claim against X.
	- Consent of A and X is required
	- B need not provide consideration
	- B gains a new right directly against X
	- No duties can be imposed on B
- Declaration of trust: A declares that he holds the contractual right to payment against X on trust for B.
	- Consent of X is not required
	- B need not provide consideration
	- B does not gain a new right directly against X, although B can 
force A to sue X by joining A as a defendant (the Vanderpitte procedure _Vandepitte v Preferred Accident Insurance Corp of New York_ __UKPC__)
- Assignment: A's right against X is transfered to B. This transfer may be equitable or legal (statutory). 
	- A loses his right against X
	- B acquires a right against X
	- Consent of X is not required

##Three Forms of Assignment

###Common Law Assignment

- Basic rule is that choses in action cannot be assigned in common law, with the exception of debts owed by and due to the Crown, debts physically embodied by a negotiable instrument (e.g. cheques, bills of exchange, bearer bonds, etc), and registered company shares and debentures. 
	- Basic rule is that rights that cannot be asserted by taking possession of a physical thing cannot be transferred.
- Possible reasons behind the basic common law rule:
	- Treitel: Early lawyers found it hard to think of a transfer of an intangible right. Later the rule was based on the fear that assignments of choses in action might leat to maintenance (meddling in litigation in which a party has no concern).
	- _Lampet's Case_: Worry that there would be a multiplying of contentions and suits. 

###Equitable Assignment

- Main question: Does equitable assignment really involve the transfer of a right from A to B? It is unlikely that equity simply ignored the common law restrictions on the assignment of choses in action (and their justifications). (Burrows)
	- Tham: Fundamental issue that X only agreed to enter into contractual relations with A. His rights and duties against and to A exist by reason of his consent, which is absent between X and B. 
- Equitable assignment of a chose in action is not really a transfer of A's right but instead is more like a declaration of trust. B is able to exercise A's right against X, but does not have a direct claim against X. 
	- B only has a right against A. 

####Type 1 Equitable Assignment

- A expresses his intention to make an immediate transfer of the benefit of his right against X to B. This constitutes an equitable assignment of A's right. 
	- X's consent is not required
	- B need not provide consideration - _re McArdle_ __EWCA__
	- No notice need be given to X
		- Doctrinally, no notice is required since B is merely exercising A's right, and X's contractual duties contine to be towards A. 
		- X can discharge his duties by performing them in favour of A (if he has no notice).
	- Writing is not required
		- A need not use the language of "assignment" as long as the meaning is plain that X should be given to understand that the debt has been transferred by A to B. _Brandt's Sons v Dunlop Rubber_ __UKHL__ (Lord Macnaughten)
	- An equitable assignment by A prior to A's insolvency would mean that A's right against X is not available to his unsecured creditors. _Gorringe v Irwell India Rubber and Gutta Percha Works_ __EWCA__, _Holt v Heatherfield Trust_
- Effect of an equitable assignment
	- B does not acquire a direct claim against X
		- In _Brandt's Sons v Dunlop Rubber_, B was allowed to bring a direct claim against X despite A's absence in the proceedings. This case is restricted to its facts.
		- In _Roberts v Gill_ __UKHL__, Lord Collins held that other than in the "most exceptional circumstances" such as in _Brandt's Sons v Dunlop Rubber_, in the case of an equitable assignment, B cannot proceed to judgment without joining A. 
			- If an equitable assignee sues a third party, the assignor must be joined as a defendant.
			- _Brandt's Sons_ was exceptional because the absence of A was overlooked by both parties until they reached the House of Lords. 
			- Lord Collins noted that in modern times, courts have been more relaxed about the requirement to join A. The action is validly constituted without joining A, although A must be joined before a final judgment can be obtained by B. 
		- In _MH Smith (Plant Hire) v DL Mainwaring_ __EWCA__: Case concerned B who had a right of subrogation to A's claim against X, where A is a company that has ceased to exist. Court held that a subrogation is like an equitable assignment. When B seeks to bring an action against X, he is in fact not invoking his own right against X (which he does not have) but is instead exercising A's right against X. The necessity for A's presence is a substantive requirement, not just a procedural one. 
			- Since A no longer existed, B was unable to exercise A's right against X. 
	- Quite a strong resemblance to a declaration of trust

###Type 2 Equitable Assignment

- Where A expects to acquire contractual rights against X, and A promises B that A will transfer those rights to B, if B provides consideration for A's promise, this constitutes an equitable assignment. 
	- B acquires an equitable interest as soon as A acquires his rights against X. _Tailby v Official Receiver_
	- Consideration is necessary since the agreement places A under a future duty to do something (a contract is created) -  Lord Eveshed in _re McArdle_ 

###Statutory Assignment

- Statutory assignment possible under s 136(1) LPA 1925
	- If there is an assignment 
	- That is absolute and in signed writing by A
		- Per Chitty LJ in _Durham Bros v Robertson_:
			- X must be under no uncertainty as to the person in whom the contractual right is vested. 
			- An assignment by way of mortgage can fall within s 136 as it purports to pass the entire interest of A: when A redeems, the transfer from B to A would again require notice to X. X would know with certainty in whom the legal right to sue him was vested. 
			- An assignment by way of charge is not with s 136, as B's right terminates when A's secured duty is performed, and X may be unaware of this event.
			- Assignment of part of X's debt cannot be within s 136 (e.g. per Lord Browne-Wilkinson in _Deposit Protection Board v Barclays Bank_) as X would be exposed to multiple actions, with a risk of conflicting decisions. 
	- Then if express notice in writing is given to X
		- Same document can serve both as written notice to X and the signed writing by A required for a statutory assignment _Curran v Newpark Cinemas_ __EWCA__
	- From the date of the notice:
		- B has a direct claim against X
		- A loses his right against X
	- B's claim is subject to any prior equities affecting the assigned right.
- Channell J in _Torkington v Magee_: This provision is merely a statutory tool ("merely machinery"); where B had some right that would  previously (prior to the act) recognised by the courts of Equity, this provision allows an action to be brought by B in his own name where previously he would have sued in A's name. 
- Effect of a statutory assignment:
	- Tham argues that unless the debt is whereby X is obliged to pay X or alternatively other parties such as A's assignees or nominees, all B is entitled to do is to insist that X makes payment of the sum due and owing to A. The statute only addresses the question to whom X is liable, but not what X is liable to do. 
	- Priorities: If a statutory assignment is made in favour of B2, who acts in good faith and gives value, and has no actual or constructive notice of an earlier equitable assignment in favour of B1, priority as between B1 and B2 depends on whether B1 or B2 first gave notice to X. 
		- The exact same rules apply as when the assignment in favour of B2 is only an equitable assignment. B2 cannot resort to the "bona fide purchaser for value without notice" defence _Compaq Computer v Abercorn Group_
			- This has been criticised by Oditah.
			- Also, this rule raises practical problems, since in commercial practice A often assigns a bundle of rights, and it is difficult for B2 to check with all previous Xs to see if an earlier assignee has given notice to them. 
		- Authority for this rule of priority is _Dearle v Hall_ 

###Limits on the Right of the Assignee

- No additional liability on X
	- _Dawson v GN & City Railway_ __EWCA__: Assignment to B of statutory claim against railway company for interference with A's land. B cannot recover for additional loss suffered as a result of trade carried on by B but not by A. 
	- Staughton LJ in _Linden Gardens_ __EWCA__: B can recover no more than A could have recovered if there had been no assignment, and if the building had not been transferred to B. (see subsequent section on prohibition of assignment) 
	- Rix LJ in _Offer-Hoar v Larkstore Ltd_: This princile is to ensure that X is not prejudiced by the assignment. It is not intended to enable X to rely on the fact of the assignment to escape all legal liability for breach of contract. 
	- Essentially, B can rely on the statutory assignment to recover B's losses (subject to remoteness/causation/mitigation) to the extent that X would have been liable to A for A's losses had A retained the benefit of the contract. 
- Set off
	- Where X owes a debt to A, and A also owes a debt to X, X would be able to set-off that liability of A, were A to sue X.
	- If X's claim v A arises out of the same contract as the assigned debt, X can rely on that claim against B. _Young v Kitchin_
	- If X's claim v A arises out of different circumstances, X can rely on that claim against B only if the claim arose before notice of the assignment was given to X. Templeman J in _Business Computers v Anglo-African Leasing Ltd_:
	> A debt which is neither accrued not connected may not be set off even though it arises from a contract made before the assignment.
	
	- Even though it appears unfair for A's assignment to affect X's ability to resort to set-off, this is necessary to protect the value of the right that B acquired as a result of the assignment. This rule balances the interests of X and B. Note also that unless the contract provided for it, X does not actually have a right to set-off per se. Set-off is an equitable doctrine based on considerations of fairness. 
	
##Prohibitions on Assignment

###Inherent Limits on Assignment

- Nature of the right between A and X may be inherently non-assignable
- Employment contracts: Benefits under employment contracts are non-assignable. Lord Atkin in _Nokes v Doncaster Amalgamated Collieries_ __UKHL__
	- Right of each person to choose who he wishes to serve. (Assignment does not require X's consent)
- Contracts involving personal confidence: Where contracts are based on the personal mutual confidence between the parties (e.g. boxer and promoter/manager) _Don King Productions v Warren_
	- In practice, these concerns would not be relevant for most contracts.
- Other personal contracts: The contract may impose a personal obligation on a party which is material to the contract, such that any other assignee would not be bound by that obligation unless there was something amounting to a novation. _Kemp v Baerselman_
	- Lightman J in _Don King v Warren_: Unles the contract expresly or impliedly otherwise provides, the benefit of an obligation is unassignable if the identity of the obligee is material to the obligor, for such an assignment can alter the substance of the obligation. 
	- Some contractual obligations are so personal that even where there is no assignment of the obligation in law, there are still good reasons to prevent the equitable assignment from taking place. This reflects the fundamental concern for the rights of X: the assignment cannot substantially alter his position. 
- Mere rights of action: Bare rights to litigate are unassignable as a matter of law unless the assignee can show that he has a genuine commercial interest in the enforcement of the claim and to that extent takes an assignment of that claim to himself (unless it falls foul of the law of champerty). _Trendtex Trading v Credit Suisse_
	- No similar issue arises where the assignment of the proceeds of an action, since the assignee would not gain the right to intervene in the action.
	
###Contractually Agreed Limits on Assignment

- Two ways to treat contractual limitations on assignment as a matter of policy:
	- Restrictions on transfers of property are undesirable and prohibited. Such clauses would therefore not have the effect of preventing the assignment but rather gives X a right to damages for breach of contract against A. 
		- This policy can be seen in some aspects of English law e.g. in relation to land.
		- The UKHL in _Linden Gardens Trust v Lenesta Sludge Disposals_ rejected this argument. Even though choses in action may be called "property", it does not necessarily follow that they are therefore inalienable. The UKHL decided similarly in _OBG v Allan_, holding that even though contractual rights are considered property, it does not mean that the doctrine of strict liability in conversion for interference with property applies to an interference with contractual rights. 
	- There are legitimate commercial reasons why parties may wish to insert such limitations into their contract to ensure that the original parties to the contract are not brought into direct contractual relations with third parties. - Lord Browne-Wilkinson in _Linden Gardens_
		- Note that this raises the question as to whether an equitable assignment has the effect of bringing an original party to the contract into direct contractual relations with the assignee. 
		- Even if the transfer view of assignments of contractual rights is not taken, and parties do not enter into direct contractual relations as a result of the assignment, there are still good reasons party may wish to avoid the possible effects of an assignment. 