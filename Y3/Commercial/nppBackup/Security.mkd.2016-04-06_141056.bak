#Security Rights

##Introduction

- Purpose of credit
	- Allow parties to obtain liquidity in order to pursue business interests or make significant purchases.
- Types of credit
	- Loan credit (typical loan agreement)
	- Sale credit (goods sold on credit allowing repayment over time)
- Debtor-creditor relation
	- Creditor:
		- Duty to advance loan (pursuant to agreement)
		- Right to repayment of loan with interest
	- Debtor:
		- Right to obtain loan (pursuant to agreement)
		- Duty to pay the capital sum with interest
- Risks faced by creditors
	- Risk of non-payment
	- Risk of asset withdrawal
	- Risk of multiple debts with other creditors (full recovery might then be impossible in the event of insolvency)
- Creditor protection against credit risk
	- Contractual protections might be in place
		- Loan agreement (with debtor) includes clauses in favour of the creditor:
			- Restrict the debtor's ability to borrow from other lenders
			- Restrtct the debtor's ability to create security over its assets to another lender
			- Provide the creditor with other forms of control over the debtor's operations
		- Creditor enters into separate contract with third parties (possibly as a condition for the loan)
			- Guarantees
			- Insurance
		- Shortcomings of contractual protections
			- These are only enforceable against the contractual party
			- Creditor ranks pari passu on debtor's insolvency