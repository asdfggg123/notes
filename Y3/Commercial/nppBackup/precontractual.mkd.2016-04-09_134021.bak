#Pre-Contractual Liability

##Introduction

###Background

- The starting point is that parties incur costs prior to the conclusion of a contract (e.g. during negotiations/tender) at their own risk. 
	- Parties to pre-contractual negotiations may break them off without liability at any time for any reason (or for no reason at all).
- Farnsworth gives one reason for the law's treatment of pre-contractual costs:
	- "Aleatory (dependant on luck, chance, or an an uncertain outcome) view" of negotiations: a party that enters negotiations in the hope of the gain that will result from ultimate agreement bears the risk of whatever loss results if the other party breaks off the negotiations
	- This view rests upon a concern that limiting the freedom of negotiation might discourage parties from entering negotiations. 
- Allowing claims in respect of unsuccessful negotiations is likely to inhibit the efficient pursuit of commercial negotiations. - Arden LJ in _Crossco No 4 Unlimited v Jolan_
- Part of the reason of English law's treatment of pre-contractual liability is that the common law imposes no duty on parties to negotiate in good faith. 
	- The converse applies in civillian systems.
	- Even where parties expressly agree to negotiate in good faith, it is unenforceable because it lacks the necessary certainty for a court to enforce. _Walford v Miles_
		- Difficult for the court to determine with certainty whether there was a proper reason for the termination of negotiations. 
	- The concept of a duty to negotiate in good faith is inherently repugnant to the adversarial position of negotiating parties. Each party is entitled pursue his own interest, so long as he avoids making misrepresentations. To advance that interest he must be entitled to threaten to withdraw from further negotiations or to withdraw in fact in the hope that the opposite party may seek to reopen negotiations by offering him improved terms. 

###Efficiency

- In some situations, it might be in fact efficient to have rules that allow for pre-contractual libaility. 
- Schwartz and Scott: In commercial situations, the complexity of the environment may present good reasons why parties are unable to complete a contract at the beginning. However, investment during this interim work may accelerate the later realisation of returns. 
	- Preliminary agreements are commonly exploratory; their performance allows parties to pursue an efficient project later. It is efficient for contract law to protect the promisee�s reliance interest if his promisor deviated from an agreed investment sequence. A reliance recovery will encourage parties to make preliminary agreements and will deter some strategic behaviour.
	- Question: If it is in both parties' interests to perform pre-contractual work, why should they not enter into a preliminary contract for such work?
- Ben-Shahar and Pottow: Where there are default rules, i.e. rules that apply in the absence of an agreement, it can be difficult in practice for parties to contract around them because their presence creates an expectation as to what is the right thing to do and if a party wants to depart from there and adopt unfamiliar terms, it may raise suspicions and scare away potential counterparties. 
	- When a party expresses a desire to depart from default rules, it may be taken as a signal of distrust or a sign that the party is unduely legalistic (that may be prejudicial to subsequent negotiations).
- Scott: Commercial parties may deliberately opt for incomplete agreements as they prefer to trust that notions of reciprocal fairness will provide a sufficient incentive for performance.

###The Current Position

- Although there are cases where the courts have recognised pre-contractual liability, it is difficult to formulate a clear general principle that governs all the different factual situations. _Countrywide Communications v ICI Pathway_
	- While a broad principle enabling recovery of pre-contractual expenditure may be desirable, no such principle is clearly established in English law.
	- The court may impose an obligation to pay for benefits resulting from services performed in the course of a contract which is expected to, but does not, come into existence, but only if justice requires it or it would be unconscionable for the plaintiff not to be recompensed. 
	
##Contractual Claims

- Limits to contractual claims
	- A bare agreement to negotiate (in good faith or not) has no legal content. There can be no obligation to negotiate until there is a "proper reason" to withdraw. _Walford v Miles_ __UKHL__
	- An agreement between two parties to enter into an agreement in which some critical part if the matter is left undetermined is no contract at all. _May & Butcher v R_ __UKHL__
- Finding a principal contract
	- This refers to where the court finds the existence of a contract (because all key terms have been determined) even though some of the terms have not been agreed upon. The court may imply those terms into the contract. 
	- E.g. Under s 8 Sale of Goods Act 1979 (codifying what the common law aleady did), where the price in a contract of sale is not determined by the contract or the course of dealing between the parties, the buyer must pay a reasonable price. 
	- In _Way v Latilla_, a shared understanding that C�s services were to be paid for was given effect to by means of an implied contractual term for reasonable remuneration:
		- There was a contract of employment which clearly indicated that the work was not to be gratuitous. 
	- In _RTS Flexible Systems v Molkerei Alois Muller_ __UKSC__, an agreed stipulation that the parties� agreement �shall not become effective until each party has executed a counterpart and exchanged it with the other� did not prevent a contract arising without such execution and exchange.
		- The circumstances pointed to the fact that there was a binding agreement. The price had been agreed and a significant amount of work had been carried out. A subsequent agreement to vary the contract was reached without any mention that the variation was subject to contract (implicitly accepting that the previous contract was validly concluded). Parties have in effect waived the stipulation that required the exchange of documents. 
		- A reasonable, honest businessman would have concluded that the parties intended that the work should be carried out for the agreed price on the agreed terms without the necessity for a formal written contract. 
- Finding a collateral contract
	- _Brewer Street Investments v Barclays Wollen Co_ __EWCA__ concerned "subject to contract" negotiations for the grant of a lease. The claimant, owners of the premises, agreed to make alterations requested by the defendant, the prospective tenant, and D promised to pay for alterations. Negotiations fail and the alterations confer no benefit on C. 
		- Somervell LJ found that D was liable in contract due the the existence of a collateral contract to pay for the alterations (even though the principal contract was not concluded).
		- Denning LJ thought that the promise was conditional on the lease being granted and therefore no contractual liability could be imposed. He allowed the claim on restitution grounds.
			- Problem with unjust enrichment claim in this case is that D was not enriched. 
		- Romer LJ agrees with both judgments. 
		
##Unjust Enrichment Claims

- Elements for an unjust enrichment claim
	- D is enriched
		- D receives a service from C that D did not pay for _Cobbe v Yeoman's Row Management.
			- Cobbe obtained planning permission for Yeoman's Row's property at his own expense on the basis that there would be a contract for the sale of the land. 
	- D's enrichment must be at the expense of C
	- Unjust factor
		- Mistake (about an existing matter of fact or law)
		- Representation
		- Failure of consideration
			- Where C does something for D on the shared basis that C would receive remuneration and the basis then fails, it may be unjust for D to retain the benefits of that work without paying for it. 
		- Duress
	- Defences
	- Quantum of award
		- Quantum of enrichment
		- Quantum meruit
			- The market price of the goods/services provided by C
- Where parties have not reached an agreement but one of them has rendered services (or delivered goods) to the other, a non-contractual restitutionary obligation may arise. No clear principle has been identified to govern the different factual situations which have arisen as to whether a restitutionary claim can successfully be made for work done in anticipation of a contract which does not materialise. 
	- Relevant factors in favour of recovery:
		- Whether D requested C to provided services or accepted them knowing that C did not indend to give them for free (e.g. _British Steel Corp v Cleveland Bridge and Enginnering Co Ltd_)
		- Whether D behaved unconscionably in declining to pay for the benefit received
	- Relevant factors against recovery:
		- 