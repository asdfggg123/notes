#Security Rights

##Introduction

- Purpose of credit
	- Allow parties to obtain liquidity in order to pursue business interests or make significant purchases.
- Types of credit
	- Loan credit (typical loan agreement)
	- Sale credit (goods sold on credit allowing repayment over time)
- Debtor-creditor relation
	- Creditor:
		- Duty to advance loan (pursuant to agreement)
		- Right to repayment of loan with interest
	- Debtor:
		- Right to obtain loan (pursuant to agreement)
		- Duty to pay the capital sum with interest
- Risks faced by creditors
	- Risk of non-payment
	- Risk of asset withdrawal
	- Risk of multiple debts with other creditors (full recovery might then be impossible in the event of insolvency)
- Creditor protection against credit risk
	- Contractual protections might be in place
		- Loan agreement (with debtor) includes clauses in favour of the creditor:
			- Restrict the debtor's ability to borrow from other lenders
			- Restrtct the debtor's ability to create security over its assets to another lender
			- Provide the creditor with other forms of control over the debtor's operations
		- Creditor enters into separate contract with third parties (possibly as a condition for the loan)
			- Guarantees
			- Insurance
		- Shortcomings of contractual protections
			- These are only enforceable against the contractual party
			- Creditor ranks pari passu on debtor's insolvency
	- Proprietary protection against credit risk
		- Security interests
			- Benefits of security interests to debtor:
				- Access to credit where D lacks creditworthiness or credit is for the purpose of embarking on a high risk project
			- Benefits of security interests to creditor:
				- Reduce credit risk:
					- Effective in debtor's insolvency
					- Priority in insolvency
					- Enforceable security where necessary (no need to wait for insolvency to resort to debtor's assets
				- Gives creditor some influence over the debtor
				- Reduction of cost of monitoring debtor (since creditor is assured that he can resort to the property over which security is granted.)
				- Deter unsecured creditors from participating in enforcement. 
		- Retention of title in conditional sale as well as hire-purchase agreements and finance leases of goods.
		- Trusts (Quistclose trusts) _Twinsectra v Yardley_
- Lord Browne-Wilkinson on what is security in _Bristol Airport v Powdrill_
	- Security is created where the creditor, in addition to the personal promise of the debtor to discharge obligation, obtains rights exercisable against some property in which the debtor has an interest in order to enforce the discharge of the debtor's obligation to the creditor. The statutor right confers on the airport (as the creditor) the right to detain and, with the leave of the court, sell the aircraft for the purpose of discharging debt incurred to that airport by the airport and is therefore a security.
	- Key elements:
		- Debt between creditor and debtor
		- Right granted over some asset
		- Creditor has the right to resort to the asset in priority to others to discharge the debt. 
		- Some security interests are capable of binding buyers of assets (security)

###Types of Security under English Law

- Four types of consensual security interests given by Millet LJ in _Re Cosslett_ __EWCA__
	1. Pledge (legal)
		- Possessory security
		- Debtor gives possession over property as security for loan; creditor may take ownership of property in the event of default.
	1. Contractual lien (legal)
		- Possessory security
		- Creditor may retain possession of property 
	1. Mortgage (legal or equitable)
		- Non-possessory security
		- Legal mortgage: debtor transfers title in property to creditor as security. 
	1. Charge (equitable)
		- Non-possessory security
		- May be fixed or floating
- Security interests can also arise by operation of law (non-conseual):
	- Non-consensual common law lien
	- Non-consensual equitable "charge" (also called lien)
		- E.g. as a result of tracing as in _Foskett v McKeown_
- Legal security has priority over subsequent interests
- Equitable security is subject to the equitable rules of priority. They arise in a number of situations:
	- Security relates to future property
	- There is no transfer or agreement for transfer of property at all
	- There is no present transfer of property, but merely an agreement to transfer or a declaration of trust by the debtor
	- The transfer of property is not made in accordance with the formal requirements for the transfer of legal title
	- Property is transferred to a third party as trustee for the creditor
	- Transferor's title to the asset is equitable

###Legal Regimes Governing Security in English law

- Charges and mortgages granted by a company must be registered at the Companies House per s 859A of the Companies Act 2006 in order to bind third parties
- Security created in writing in goods by an individual must compy with formalities and be registered at the High Court: Bills of Sale Acts 1878- 1882
- Registration of security in relation to particular assets e.g. land, some IP rights