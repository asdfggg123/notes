#Security Rights

##Introduction

- Purpose of credit
	- Allow parties to obtain liquidity in order to pursue business interests or make significant purchases.
- Types of credit
	- Loan credit (typical loan agreement)
	- Sale credit (goods sold on credit allowing repayment over time)
- Debtor-creditor relation
	- Creditor:
		- Duty to advance loan (pursuant to agreement)
		- Right to repayment of loan with interest
	- Debtor:
		- Right to obtain loan (pursuant to agreement)
		- Duty to pay the capital sum with interest
- Risks faced by creditors
	- Risk of non-payment
	- Risk of asset withdrawal
	- Risk of multiple debts with other creditors (full recovery might then be impossible in the event of insolvency)
- Creditor protection against credit risk
	- Contractual protections might be in place
		- Loan agreement (with debtor) includes clauses in favour of the creditor:
			- Restrict the debtor's ability to borrow from other lenders
			- Restrtct the debtor's ability to create security over its assets to another lender
			- Provide the creditor with other forms of control over the debtor's operations
		- Creditor enters into separate contract with third parties (possibly as a condition for the loan)
			- Guarantees
			- Insurance
		- Shortcomings of contractual protections
			- These are only enforceable against the contractual party
			- Creditor ranks pari passu on debtor's insolvency
	- Proprietary protection against credit risk
		- Security interests
			- Benefits of security interests to debtor:
				- Access to credit where D lacks creditworthiness or credit is for the purpose of embarking on a high risk project
			- Benefits of security interests to creditor:
				- Reduce credit risk:
					- Effective in debtor's insolvency
					- Priority in insolvency
					- Enforceable security where necessary (no need to wait for insolvency to resort to debtor's assets
				- Gives creditor some influence over the debtor
				- Reduction of cost of monitoring debtor (since creditor is assured that he can resort to the property over which security is granted.)
				- Deter unsecured creditors from participating in enforcement. 
		- Retention of title in conditional sale as well as hire-purchase agreements and finance leases of goods.
		- Trusts (Quistclose trusts) _Twinsectra v Yardley_
- Lord Browne-Wilkinson on what is security in _Bristol Airport v Powdrill_
	- Security is created where the creditor, in addition to the personal promise of the debtor to discharge obligation, obtains rights exercisable against some property in which the debtor has an interest in order to enforce the discharge of the debtor's obligation to the creditor. The statutor right confers on the airport (as the creditor) the right to detain and, with the leave of the court, sell the aircraft for the purpose of discharging debt incurred to that airport by the airport and is therefore a security.
	- Key elements:
		- Debt between creditor and debtor
		- Right granted over some asset
		- Creditor has the right to resort to the asset in priority to others to discharge the debt. 
		- Some security interests are capable of binding buyers of assets (security)

###Types of Security under English Law

- Four types of consensual security interests given by Millet LJ in _Re Cosslett_ __EWCA__
	1. Pledge (legal)
		- Possessory security
		- Debtor gives possession over property as security for loan; creditor may take ownership of property in the event of default.
	1. Contractual lien (legal)
		- Possessory security
		- Creditor may retain possession of property 
	1. Mortgage (legal or equitable)
		- Non-possessory security
		- Legal mortgage: debtor transfers title in property to creditor as security. 
	1. Charge (equitable)
		- Non-possessory security
		- May be fixed or floating
- Security interests can also arise by operation of law (non-conseual):
	- Non-consensual common law lien
	- Non-consensual equitable "charge" (also called lien)
		- E.g. as a result of tracing as in _Foskett v McKeown_
- Legal security has priority over subsequent interests
- Equitable security is subject to the equitable rules of priority. They arise in a number of situations:
	- Security relates to future property
	- There is no transfer or agreement for transfer of property at all
	- There is no present transfer of property, but merely an agreement to transfer or a declaration of trust by the debtor
	- The transfer of property is not made in accordance with the formal requirements for the transfer of legal title
	- Property is transferred to a third party as trustee for the creditor
	- Transferor's title to the asset is equitable

###Legal Regimes Governing Security in English law

- Charges and mortgages granted by a company must be registered at the Companies House per s 859A of the Companies Act 2006 in order to bind third parties; consequences of lack of registration under s 859H (need to know)
- Security created in writing in goods by an individual must compy with formalities and be registered at the High Court: Bills of Sale Acts 1878 - 1882
- Registration of security in relation to particular assets e.g. land, some IP rights
- Special rules present for financial collateral 

##Retention of Title

- Retention of title is possible under s 19(1) Sale of Goods Act 1979
	- Seller may reserve the conditional right of disposal of the goods under contract. 
- See _Clough Mill v Martin_:
	- Contract contained a clause where the seller purports to retain title in both unused material and goods produced from the material subject to payment of the purchase price of the material.
	- Goff LJ held that it was impossible to believe that parties intended the seller to gain the windfall of the full value of the new product and the clause must be held as creating either a trust or a charge.
		- Impossible as the product contains the contribution of other parties, it was not previously owned by the seller, and seller had no duty to account to buyer for the value in the new goods in excess of the contract price. 
	- Sir Donaldson MR decided on a different basis:
		- Where the material is incorporated in a way that leaves it in a separate and identifiable state, seller may retain property in it.
		- Where the material is incorporated in a way that leaves it unidentifiable i.e. a new product is produced, it would be necessary to deetermine who owned that product. To the extent that the buyer owns the new product, the seller may only acquire a proprietary interest over it by way of charge. 
	-Buckley LJ in _Borden v Scottish Timber Products_ __EWCA__: it is impossible for seller to reserve any property in the manufactured product, because they never had any property in it; the property in the product originates in the buyer when it is manufactured. 
- It may be possible to create an obligation on the buyer to account for sale proceeds in derivative products to the seller _Aluminium Industrie Vaassen v Romalpa Aluminium_ __EWCA__
	- Note: Buyer conceded that they were bailess of the goods for seller.
	- This is a fiduciary relationship which will generally not arise in bailment/retention of title situations _Re Andrabell_
- The _Clough Mill v Martin_ argument applies similary to retention of title in sale proceeds clauses _Pfeiffer Weinkellerei-Weinkauf v Arbuthnot Factors_
	- Claim in relation to proceeds of same of goods received by buyer after sub-sales.
	- Claim was only to the sums necessary to satisfy B's duty to pay the price
	- It was an attempt to give seller a security right and registration was required. 
	
###Security Interests (SI) and Agreements to Sell with Retention of Title (RoT)

- Differences:
	- Security is granted while title is retained. _Clough Mill v Martin_
	- RoT conceptually is not a security right(Read Welsh Development Agency v Export Finance Co [1992] BCLC 148 (CA) (Exfinco case))
		- Even though they may have similar economic effects, they are conceptually different. 
- Similarities
	- Seller has a property right in an asset in debtor's hands
	- Seller's right secures payment of money
	- If money is not paid, creditor can resort to the asset.
	- Same economic function is performed.
	
		
##Pledge

- Pledge is a type of bailment (legal interest) whereby the debtor remains the owner and the creditor acquires "special property" in the asset _Coggs v Bernard_
- Creditor gains a legal property right effect against debtor's insolvency officer
- Debtor has a right to redeem property upon payment. 
- Creditor must take actual possession of the goods or documents of title to the goods _Dublin City Distillery v Doherty_ __UKHL__
	- A mere agreement to transfer possession is insufficient
	- Physical delivery of the thing or a symbol (key to storage facilities even if they are on debtor's premises) _Wrightson v McArthur and Hutchisons
	- Constructive delivery (attornment) e.g. one party attorns to the other party that they hold the pledge property on behalf of the pledgee.
	- Delivery of a document of title
- General rule is that pledge ends if pledgee loses possession except in special circumstances (e.g. where debtor regains possession of goods, holding them on trust for creditor with an obligation to account to creditor for their value)
- Pledgee has a right to sell
- Surplus over the debt realised on sale is accountable to the pledgor (pledgee is accountable as a fiduciary) _Mathew v TM Sutton_
- Any deficit not covered by the sale of pledged goods remain due to the creditor (personal action against debtor)

##Lien

- May arise consensually but is usually non-consensual
- Statutory lien: e.g. unpaid seller's lien per ss 41-43 SGA 1979
- Common law liens
	- Based on possession and making improvement
		- e.g. in _Tappenden v Artus_, repairer of a car is entitled to retain possession of said car pending payment for his services. 
			- Diplock LJ: A lien is a self-help remedy. It is a remedy in rem exercisable upon goods, requiring no intervention by the courts. It is exercisable only by the repairer who has actual possession of the goods subject to the lien. It is enforceable even against a bailor's property right because the giving of actual possession of the goods by the bailee to the claimant was an act reasonably incidental to the bailee's reasonable use of the goods. 

##Mortgage

- Transfer of title from debtor to creditor: A mortgage is a conveyance of land or an assignment of chattels as a security for the payment of a debt or the discharg of some other obligation. The security is redeemable on the payment or discharge of such debt or obligation. 
	- Transfer of legal or equitable title
	- Transfer is as security for another obligation
	- Transfer is subject to obligation to retransfer upon payment. 
	- Equitable mortgages arise in the following situations:
		- Mortgage is granted over an equitable right
		- There is an agreement to transfer property by way of mortgage (that is yet to be executed or satisfy formality requirements). Buckley LJ in _Swiss Bank v Lloyd's Bank_: An agreement to mortgage property will normally be specifically enforceable to give rise to an equitable charge by way of mortgage. 
			- In order to create an equitable mortgage, there must be an agreement demonstrating a binding intention on the part of the mortgagor to create a security in favour of the mortgagee.
		- Mortgage is granted over future property
- Mortgagor has the equity of redemption - the right to redeem property upon payment.
- Equity of redemption cannot be restricted
	- Equity of redemption cannot be conditional on the mortgagor granting collateral advantages in favour of the mortgagee _Noakes v Rice_
	- EWCA in _Jones v Morgan_ said that they could see no reasons why restrictions on the equity of redemption are void. 
- If granted by companies, it must be registered under Companies Act 2006 s 859A to be valid against insolvency officers of the debtor and other creditors (s859H)
- Rights of the mortgagee on enforcement
	- Foreclosure i.e. mortgagee acquires full title to property free of mortgagor's rights.
	- A right to enter into possession
		- Mortgagee in possession has a duty to preserve the security
		- Mortgagee may not sell to themselves
	- Power of sale: can be statutory (where mortgage by deed: Law of Property Act 1925, ss101,103, 104) or consensual
	- Mortgagee is under a duty to account for surplus over debt owed
		- Requires a court order

##Charge

- Charge is hard to define per Lord Hoffman in _Re BCCI (No 8)_ _UKHL__
	- Creditor obtains a proprietary interest in the security asset but do not obtain legal or equitable title, nor are they required to take possession of the asset.
	- Creditor acquires the proprietary interest by way of security. _Re Cosslett (Contractors) Ltd_
- Charge involve the creation and grant of a new interest in property. 
- Charges and mortgages
	- Differences between charges and mortgages
		- Despite the distinction, equity judges appear to use the terms with no such precise distinction per Scrutton LJ in _London County & Westminster Bank v Tompkins_
		- Chargee cannot take possession by mere virtue of the presence of the charge
			- Charge agreement may confer extensive powers, including possessory rights.
			- Small practical difference
		- Equity of redemption in the mortgage enables the mortgagor to require the mortgagee to re-transfer the asset upon payment of the secured debt, whereas a charge "is at once extinguished by payment of the debt" _Kennard v Futvoye_
			- Small practical difference
	- Similarities between charges and mortgages
		- Both are effective in the insolvency of the chargor (bind the insolvency officer) (a charge or mortgage granted by a company must be registered to bind per s 859H Companies Act. ("charge" includes a mortgage per s 859A(7))
	- Both bind subsequent encumbrances/buyers (subject to legal or equitable rules _Re BCCI SA_)
	- The better distinction to draw is between charge/equitable mortgage and legal mortgage:	
		- Buckley LJ in _Swiss Bank Corp v Lloyds Bank_ noted obiter that a charge may take the form of an equitable mortgage or an equitable charge not by way of mortgage
- Advantages of creating a charge
	- Cheap and easy to create per Lord Hoffmann in _Re BCCI SA_
		- No need for creditor to take possession or for debtor to transfer title to creditor
		- No need for any formal act to create it
			- Equitable charge can be created by an informal transaction for value
			- However, disposition of a subsisting equitable interest is subject to formality requirements under s 53(1)(c) LPA 1925 (granting a charge over the interest appears to fall under this provision per _Kinane v Mackie-Conteh_)
	- Flexible: can be created over any kind of property
		- Charge/equitable mortgage may be created over future assets _Holroyd v Marshall_
			- A legal mortgage can only be taken over security owned by the mortgagor at the time of creation. _Robinson v Macdonnell_
			- Creation of a legal mortgage over a future asset requires the mortgagee to seize the asset when it comes into being _Congreve v Evetts_